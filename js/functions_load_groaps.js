$(document).ready(function(){
////////crete-groap
/*     $('div#step1').on('change', 'select[name="city"]', function() {
        if ($(this).val() && !$(this).prop('disabled'))
            $('a#step1_b').css('display', 'inline-block');
        else $('a#step1_b').hide();
    });

    $(document).on('click','div#step2 li.keyword-item',function(){
        $('a#step2_b').css('display', 'inline-block');
    });

    $(document).on('keyup','div#step1 input[name="city_new"]',function(){
        if ($(this).val() != '') {
            $('a#step1_b').css('display', 'inline-block');
        } else {
            $('a#step1_b').hide();
        }
    });

    $(document).on('keyup','div#step3 input[name=name]',function(){
        if ($('div#step3 input[name=name]').val() != '') {
            $('a#step3_b').css('display', 'inline-block');
        } else {
            $('a#step3_b').hide();
        }
    });

    $(document).on('keyup','div#step4 textarea[name=description]',function(){
        if ($('div#step4 textarea[name=description]').val() != '') {
            $('a#step4_b').css('display', 'inline-block');
        } else {
            $('a#step4_b').hide();
        }
    }); */
	
	/////////////YMap  index create-group 
	var userCountryIsKnown = false;

   //Эти переменные определенны перед регистрацией скрипта в site/itdex и create-groap
   //Эсли в настройках указан город и страны то эти данные будут браться оттуда
   /* var map = new YMaps.Map();
    var userCountry = '';
    var userCity = ''; 
    if (YMaps.location) {
        userCountry = YMaps.location.country;
        userCity = YMaps.location.city;
	}*/
	

        $('#maps').html('Ваш город: '+userCity);

        $('.complaint').click(function(){
            $('input[name=mid]').val($(this).attr('mid'));
        });

        $('#cities').ready(function(){
            $("select#countries option").each(function(index,countryOption){
                if ( $(countryOption).val() == userCountry ) {
                    $(countryOption).prop('selected', true);
                    userCountryIsKnown = true;
                }
            });
        });
        $('select#country ').ready(function(){
            $('select#country option').each(function(index,countryOption){
                if ( $(countryOption).text() == userCountry ) {
                    $(countryOption).prop('selected', true);
                    $('#country').change();
                    userCountryIsKnown = true;
                }
            });
        });
		
		
		
		/*.on('click', function(){
            if( $(this).val() == chooseCityMessage ) {
                $(this).val('');
            }
        });*/
        $.ajax({
            type: 'GET',
            url: '/city/getCityList',
            data: {country_id: userCountry},
            success: function(data){
                var cities = data.split(',');
                $( "#cities" ).autocomplete({
                    source: cities
                });
                if (data.indexOf(userCity) !== -1) {
                    $('input#cities').val(userCity);
                }
            }
        });
    
/////////////end YMap  index create-group   

/////////////YMap  index
    $(function() {
        var firstKeywordsRun = true;
        $("#categories") .combobox().change(function(){
                if ( $(this).val() == 'all' ) {
                    $('.keywords-container').slideUp();
                } else {
                    $.ajax({
                        type: 'GET',
                        url: '/ajax/getCategoryKeywords',
                        data: {
                            catId: $(this).val()
                        },
                        success: function(data){
                            if (data) {
                                var keywords = JSON.parse(data);
                                $('.keywords-box .keywords').fadeOut().remove();

                                $(keywords).each(function(index,keyword){
                                    if ($(keyword.name).html()) {
                                        keyword.name = $(keyword.name).html();
                                    }
                                    var keywordDivHtml = '<div class="keywords f-left" id="keywords-block-' + parseInt(keyword.id) + '">'
                                        + keyword.name + '</div>';
                                    var keywordInputHtml = '<input class="keywords-input" name="" ' + ' value="' + keyword.name
                                        + '" type="hidden" id="keywords-input-' + parseInt(keyword.id) + '" />';
                                    //onfocus="this.blur()" readonly="readonly"
                                    $('.keywords-box').append(keywordDivHtml + keywordInputHtml);
                                });

                                if (defaultGroupKey !== undefined && defaultGroupKey && firstKeywordsRun) {
                                    var defaultKeywordId = $('input.keywords-input[value="' + defaultGroupKey + '"]')
                                        .attr('id').split('-')[2];
                                    $('#keywords-block-' + parseInt(defaultKeywordId)).click();
                                    firstKeywordsRun = false;
                                }
                            }
                        }
                    });
                    $('.keywords-container').slideDown();
                }
        }).change();

        $("#countries").combobox().change(function(){
            $("#cities").val(chooseCityMessage);
        });

        var countryNoticeHtml = '<div class="keywords-notice">'
            + 'Воспользуйтесь поиском выше для отображения групп в вашем регионе. '
            + '<br />Ниже отображены популярные группы:</div>';
        if (!userCountryIsKnown) {
            countryNoticeHtml = '<div class="keywords-notice">Нам не удалось определить Ваше текущее месторасположение. '
                + 'Воспользуйтесь поиском выше для отображения групп в вашем регионе. '
                + '<br />Ниже отображены популярные группы на сайте:</div>';
        }
		var search = decodeURI(document.location.search);
		if(search.indexOf('country=') == -1) {
			$.ajax({
				type: 'POST',
				url: '/ajax/getPopularGroups',
				data: {
					country: userCountry,
					city: userCity,
					view: 'tab',
					YII_CSRF_TOKEN: yii_csrf_token
				},
				success: function(data) {
					$('.items')
						.hide()
						.html(data)
						.prepend(countryNoticeHtml)
						.slideDown();
				}
			});
		}
        var selCountries = $(".sel-countries");
        if (!userCountryIsKnown) {
            selCountries.val('');
        }
        selCountries.on('focusout',function(){
            $.ajax({
                type: 'GET',
                url: '/city/getCityList',
                data: {country_id: $( this ).val()},
                success: function(data){
                    var cities = data.split(',');
                    $( "#cities" ).autocomplete({
                        source: cities
                    });
                }
            });
            $( "#cities" ).autocomplete({
                source: countries
            });
        }).on('change', function(){
            $( "#cities" ).val(chooseCityMessage);
        });

    });

});
