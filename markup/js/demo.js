function renderHiddenCalender(){
    if($('#calendar-widget-hidden')[0]) {
        (function(){
            $('#calendar-widget-hidden').fullCalendar({
                contentHeight: 'auto',
                theme: true,
                header: {
                    right: '',
                    center: 'prev, title, next',
                    left: ''
                },
                monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                monthNamesShort: ['Янв.','Фев.','Март','Апр.','Май','Июнь','Июль','Авг.','Сент.','Окт.','Ноя.','Дек.'],
                dayNames: ["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],
                dayNamesShort: ["ВС","ПН","ВТ","СР","ЧТ","ПТ","СБ"],                
                defaultDate: '2016-01-28',
                editable: true,
                events: [
                    {
                        title: 'День рождения',
                        start: '2016-01-29',
                        className: 'bgm-amber'
                    }
                ]
            });
        })();
    }
}
$(document).ready(function(){
    $('.selectpicker').selectpicker();
    
    $("#view-switcher").change(function(){
        $(".list-view > div").toggleClass("line");

    });
    //Welcome Message (not for login page)
    function notify(message, type){
        $.growl({
            message: message
        },{
            type: type,
            allow_dismiss: false,
            label: 'Cancel',
            className: 'btn-xs btn-inverse',
            placement: {
                from: 'top',
                align: 'right'
            },
            delay: 2500,
            animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
            },
            offset: {
                x: 20,
                y: 85
            }
        });
    };
    
    $("#add-city").click(function(event){
        event.preventDefault();
        
        swal({   
            title: "Вы уверены, что хотите добавить город "+$("#new-city").val()+"?",   
            text: "Возможно вы имели ввиду Санкт-Петербург?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Да, я имел ввиду Санкт-Петербург!",
            cancelButtonText: "Нет, я имел ввиду "+$("#new-city").val()+"",  
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
          if (isConfirm) {
            swal({
                title: "Город выбран!", 
                text: "Вы выбрали город Санкт-Петербург", 
                type: "success"
            }, function(){
                $("#subcat-city option:selected").prop('selected',false);
                $("#subcat-city option[value='Санкт-Петербург']").prop('selected',true);
                $('.selectpicker').selectpicker('refresh');
                $("#modalDefault .close-dialog").click();
            });
          } else {
            swal({
                title: "Город добавлен!", 
                text: "Город станет доступным после проверки модератором.", 
                type: "info"
            }, function(){
                $("#subcat-city").append("<option value='"+$("#new-city").val()+"'>"+$("#new-city").val()+"</option>");
                $("#subsubcat-citycat option:selected").prop('selected',false);
                $("#subcat-city option[value='"+$("#new-city").val()+"']").prop('selected',true);
                $('.selectpicker').selectpicker('refresh');

                $("#modalDefault .close-dialog").click();
            });
          }
        });

    
    });
    $("#add-interest").click(function(event){
        event.preventDefault();

    });
    
    $(".btn-next-step").click(function(){
        //$(this).closest(".card").next().addClass('animated fadeInUp').removeClass('hidden'); из-за анимации прячется блок с тегами почему-то(
        $(this).closest(".card").next().removeClass('hidden');
    })
    
    $(".btn-tag").click(function(){
        $(this).remove();    
    });

    $(".toggle-pmb-block").click(function(e){
        e.preventDefault();
        $(this).closest('.pmb-block').find('.pmbb-body, .timeline ').slideToggle(500,function(){$('#calendar-widget').fullCalendar('render');});    
    });

    
    /*if (!$('.login-content')[0]) {
        notify('Welcome back Mallinda Hollaway', 'inverse');
    }      */
});

