<?php
class GroupExtender extends CComponent {


    public function init(){
    }
    public function extendGroup($id,$user_id = null){
        $modelIsset = PaidGroup::model()->findByAttributes(array('group_id'=>$id));
        if($modelIsset){
            if(strtotime($modelIsset->date_from)>= time()){
                $modelIsset->date_from = date('Y-m-d',time());
            }
            $date_to = $modelIsset->date_to;
            $modelIsset->date_to = date('Y.m.d',strtotime($date_to)+2678400); //31 day
            $modelIsset->save(false);
        } else {
            $modelPaid = new PaidGroup();
            $modelPaid->group_id = $id;
            if($user_id){
                $modelPaid->user_id = $user_id;
            } else {
                $modelPaid->user_id = Yii::app()->user->id;
            }
            $modelPaid->date_from = date('Y-m-d',time());
            $modelPaid->date_to = date('Y-m-d',time()+2678400);
            $modelPaid->save(false);
        }
    }
}