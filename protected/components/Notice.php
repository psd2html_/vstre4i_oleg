<?php

class Notice
{
    public static function getNotices($userId)
    {
        $db = Yii::app()->db;

        $command = $db->createCommand()
            ->select('stack.*, types.type')
            ->from('user_notice_stack AS stack')
            ->join('user_notice_types types', 'stack.type_id=types.id')
            ->where('stack.recipient_id = ' . (int)$userId);
        return $command->queryAll();
    }

    public static function deleteNotices($userId)
    {
        $db = Yii::app()->db;

        $db->createCommand()->delete('user_notice_stack', 'recipient_id=:id', array(':id'=>$userId));
    }

    public static function popNotices($userId)
    {
        $notices = self::getNotices($userId);
        if ( ! empty($notices) ) {
            self::deleteNotices($userId);
        }
        return $notices;
    }

    public static function addNotice($userId, $type, $message, $link='')
    {
        $db = Yii::app()->db;

        $typeId = $db->createCommand()
            ->select('id')
            ->from('user_notice_types')
            ->where('type=:type', array(':type'=>$type))
            ->queryScalar();

        return $db->createCommand()
            ->insert('user_notice_stack', array(
                'type_id'      => $typeId,
                'recipient_id' => $userId,
                'message'      => $message,
                'link'      => $link,
            ));
    }

    public static function addNoticeCheckSimilar($userId, $type, $message, $noticeMessageGroup, $link)
    {
        $db = Yii::app()->db;
        $typeId = $db->createCommand()
            ->select('id')
            ->from('user_notice_types')
            ->where('type=:type', array(':type'=>$type))
            ->queryScalar();

        $similarNotices = $db->createCommand()
            ->select('id')
            ->from('user_notice_stack')
            ->where('recipient_id=:recipient_id AND link=:link', array(':recipient_id'=>$userId, ':link'=>$link))
            ->queryScalar();
		
		if($similarNotices){
			$n = $db->createCommand()->update('user_notice_stack', 
			array(
				'count'=>new CDbExpression('count + 1'),
				'message'=>$noticeMessageGroup
			),'id=:id',array(':id'=>$similarNotices));	
			
		}else{
			$n = $db->createCommand()->insert('user_notice_stack',
				array(
					'type_id'      => $typeId,
					'recipient_id' => $userId,
					'message'      => $message,
					'link'         => $link,
				));	
		}
		return $n;

    }
}
