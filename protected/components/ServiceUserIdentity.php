<?php
class ServiceUserIdentity extends UserIdentity {
    const ERROR_NOT_AUTHENTICATED = 3;

    /**
     * @var EAuthServiceBase the authorization service instance.
     */
    protected $service;

    /**
     * Constructor.
     * @param EAuthServiceBase $service the authorization service instance.
     */
    public function __construct($service) {
        $this->service = $service;
    }

    /**
     * Authenticates a user based on {@link username}.
     * This method is required by {@link IUserIdentity}.
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {


        $pic = '';
		$gender = 0;
		$city = '';
		$city_id = 0;
		$region_id = 0;
		$country_id = 0;
		$birthday = NULL;
        if($this->service->hasAttribute('picture')) {
            $pic = $this->service->getAttribute('picture');
        }
        if($this->service->hasAttribute('gender')) {
            $gender = $this->service->getAttribute('gender');
        }
        if($this->service->hasAttribute('city')) {
            $city = $this->service->getAttribute('city');
        }
        if($this->service->hasAttribute('country')) {
            $country = $this->service->getAttribute('country');
			$country_id = Country::model()->getCountryId($country);
			if($country_id && $city){
				$cityInfo = City::model()->getCityInfo($city, $country_id);
				$city_id = (int)$cityInfo['id'];
				$region_id = (int)$cityInfo['region_id'];
			}
			
        }
        if($this->service->hasAttribute('birthday')) {
            $birthday = $this->service->getAttribute('birthday');
			$breakdate = explode('.', $birthday);
			$filled_int = sprintf("%02d", $breakdate[1]);
			$birthday = $breakdate[2].'-'.$filled_int.'-'.$breakdate[0];
        }
		//echo '$birthday: '.$birthday.'<br>';exit;
		//echo '$city: '.$city.'<br>';
		//echo '$city_id: '.$city_id.'<br>';
		//echo '$country_id: '.$country_id.'<br>';
		//echo '$region_id: '.$region_id.'<br>';
		//print_r($this->service->getAttributes());exit;
        //print_r($this->service);
        //die;
		//echo  $pic.'||';
        $users = User::model()->findByAttributes(array('serv_id' => $this->service->id,'service' => $this->service->serviceName));
        if($users===null) {
            if($this->service->hasAttribute('email')){
                $users = User::model()->findByAttributes(array('email' => $this->service->getAttribute('email')));
			}
            if($users===null){
                User::model()->serviceRegistrationUser($this->service->getAttribute('id'),$this->service->serviceName,$this->service->getAttribute('name'),$this->service->getAttribute('email'),$pic,$gender,$city,$city_id,$region_id,$country_id,$birthday);
            }else{
                User::model()->updateByPk($users->id, array(
					'serv_id' => $this->service->id, 
					'service' => $this->service->serviceName
				));
            }
        }else{
			$attr = array();
			if(!$users->name)
				$attr['name'] = $this->service->getAttribute('name');
			if(!$users->sex)
				$attr['sex'] = $gender;
			if(!$users->city)
				$attr['city'] = $city;
			if(!$users->city_id)
				$attr['city_id'] = $city_id;
			if(!$users->region_id)
				$attr['region_id'] = $region_id;
			if(!$users->country_id)
				$attr['country_id'] = $country_id;
			if((int)$users->birthday < 1)
				$attr['birthday'] = $birthday;
			if(!empty($attr))
				User::model()->updateByPk($users->id, $attr);
		}
        if ($this->service->isAuthenticated) {
            $this->username = $this->service->getAttribute('name');
            $this->setState('id', $this->service->id);
            $this->setState('name', $this->username);
            $this->setState('service', $this->service->serviceName);
            $this->errorCode = self::ERROR_NONE;
        }
        else {
            $this->errorCode = self::ERROR_NOT_AUTHENTICATED;
        }
        return !$this->errorCode;
    }
}
