<?php
if (empty($userId)) {
    $userId = User::model()->getUserId();
}
if(!empty($menuElements)):
?>
<ul class="b-horizontal-menu js-drop-down">
    <?php
    foreach($menuElements as $element):
		if(isset($element['params']['admin_only'])
            && $element['params']['admin_only']
            && !Admin::model()->isSiteAdminOrModerator($userId)
        ){
            continue;
        }
        switch($element['type']):
            case 'link':
                $link = Yii::app()->getBaseUrl(true) . $element['params']['link'];
                ?>
                <li>
                    <a href="<?php echo CHtml::encode($link); ?>">
                        <?php echo CHtml::encode(Yii::t('var', $element['title'])); ?>
                    </a>
                </li>
                <?php
                break;
            case 'user_profile':
            ?>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('user/view') . '/' . urlencode($userId); ?>">
                        <?php echo CHtml::encode(Yii::t('var', $element['title'])); ?>
                    </a>
                </li>
            <?php
                break;
            case 'user_admin_groups':
            ?>
                <li>
                    <a href="#" onclick="return false;">
                        <?php echo CHtml::encode(Yii::t('var', $element['title'])); ?>
                    </a>
                    <?php if (!empty($userAdminGroups)): ?>
                        <ul class="parent">
                            <?php foreach($userAdminGroups as $group): ?>
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('group/view') . '/' . urlencode($group['group_id']); ?>">
                                        <?php echo CHtml::encode($group['group_name']); ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php else: ?>
                        <ul><li><div class="gag"><?php echo Yii::t('var', 'Вы ещё не создавали групп'); ?></div></li></ul>
                    <?php endif; ?>
                </li>
            <?php
                break;
            case 'user_groups':
            ?>
                <li>
                    <a href="#" onclick="return false;">
                        <?php echo Yii::t('var', $element['title']); ?>
                    </a>
                    <?php if (!empty($userGroups)): ?>
                        <ul class="parent">
                            <?php foreach($userGroups as $group): ?>
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('group/view') . '/' . urlencode($group['group_id']); ?>">
                                        <?php echo CHtml::encode($group['group_name']); ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php else: ?>
                        <ul><li><div class="gag"><?php echo Yii::t('var', 'Вы пока не состоите ни в одной группе'); ?></div></li></ul>
                    <?php endif; ?>
                </li>
            <?php
                break;
            case 'user_update':
            ?>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('user/update') . '/' . urlencode($userId);?>">
                        <?php echo CHtml::encode(Yii::t('var', $element['title'])); ?>
                    </a>
                </li>
            <?php
                break;
        endswitch;
    endforeach;
    ?>
</ul>
<?php endif; ?>

