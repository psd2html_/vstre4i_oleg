<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Vstre4i.com',
    'theme'=>'vastre4i',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.services.*',
        'ext.eauth.custom_services.CustomVKontakteService',
        'ext.eauth.custom_services.CustomFacebookService',
        'ext.phpmailer.JPhpMailer',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			/*'password'=>'2275658134zarkon',*/
			'password'=>'olegvstre4izakon',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			//'ipFilters'=>array('localhost','::1'),
		),

	),

	// application components
	'components'=>array(
        'clientScript' => array(
            'scriptMap'=>array(
                'jquery.js'=>'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js',
                'jquery.min.js'=>'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
            )
        ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		
		/* 'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'gii/<controller:\w+>'=>'gii/<controller>',
				'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
			),
		), */
		
		//'db'=>array(
		//	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		//),

        'db' => require(dirname(__FILE__) . '/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
                array(
					'class' => 'CWebLogRoute',
					'showInFireBug' => true,
					'levels' => CLogger::LEVEL_INFO,
                ),
			),
		),
        'loid' => array(
            'class' => 'ext.lightopenid.loid',
        ),
		'wm'=>array(
            'class'=>'ext.wm.WmModule',
            'orderClass'=>'Orders',//Модель заказа
            'orderIdField'=>'id',//Id поле
            'orderSumField'=>'order_sum',//поле стоимости
            'orderSuccessFunction'=>'setSuccess',//функция модели при успешном платеже
            'orderSuccesUrl'=>false,//url успешного платежа (вообще-то бессмысленная вещь)
            'orderFailUrl'=>false,//url ошибочного платежа (вообще-то бессмысленная вещь)
            'orderMessage'=>false,//сообщение, которое будет выводиться пользователю при оплате (цель платежа), возможно использование "{$orderId}" в теле для вывода № заказа
            'csrfToken'=>false,//передавать или нет csrfToken
        ),
        'eauth' => array(
            'class' => 'ext.eauth.EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'services' => array( // You can change the providers and their classes.
/*                'google' => array(
                    'class' => 'GoogleOpenIDService',
                ),*/
                'yandex' => array(
                    'class' => 'YandexOAuthService',
                    'client_id' => '69b60a2936fd4a7888ca63b7fdb2fd59',
                    'client_secret' => '9d63bbd1a7e247f7a27479d61e883910'

                ),
                'twitter' => array(
                    'class' => 'TwitterOAuthService',
                    'key' => 'yhhO0MTE64Wp9nPMLTQTA',
                    'secret' => 'wivmAwya6POyekPMybzogn34LYEviMJeKucAO07NQ',
                ),
                'facebook' => array( //V
                    'class' => 'CustomFacebookService',
                    'client_id' => '197822860407002',
                    'client_secret' => '9d0b16a5dea1b22df0f25966f7a0aab1',
                ),
                'vkontakte' => array( //V
                    'class' => 'CustomVKontakteService',
                    'client_id' => '4062059',
                    'client_secret' => '4IeNlUIn0Fvv9Togqva9',
                ),
                'mailru' => array( //V
                    'class' => 'MailruOAuthService',
                    'client_id' => '725441',
                    'client_secret' => 'fdb0268dd93e736b93ce72058dce0355 ',
                ),
                'odnoklassniki' => array(
                    'class' => 'OdnoklassnikiOAuthService',
                    'client_id' => '206652672',
                    'client_public' => 'CBAGECFNABABABABA',
                    'client_secret' => '4900EFACC3539E14BC962152',
                    'title' => 'Однокл.',
                ),
            ),
        ),
		'Paypal' => array(
            'class'=>'application.components.Paypal',
            'apiUsername' => 'bt.sumrak-facilitator_api1.gmail.com',
            'apiPassword' => '1372794658',
            'apiSignature' => 'AC8Ef1CmZt1SdIDqcAVgfY0RsD79AsDCYgDUABqyYuw3e4.yewiFwbuG',
            'apiLive' => false,
            'returnUrl' => 'payment/paypalConfirm/', //regardless of url management component
            'cancelUrl' => 'payment/paypalCancel/', //regardless of url management component
            'currency' => 'USD',
        ),

		'ishop' =>    array(
			'class' =>    'ext.ishop.IShop',
			'options'   =>  array(
				'login'         =>  231277,
				'password'  =>      "password123"
			)
		),

        'request'=>array(
            'enableCookieValidation'=>true,
            'enableCsrfValidation'=>true,
			'class'=>'HttpRequest',
            'noCsrfValidationRoutes' => array('payment/WebmoneyResult','mail/getEmailUserForm','mail/getArchiveEmailForm','payment/sdpaysRequestHandler','user/banUser','user/unBanUser','ajax/getPopularGroups','ajax/getCategoryKeywords','city/getCityList','member/filterMembers'),
        ),
        'urlManager'=>array(
            'class'=>'application.components.UrlManager',
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(

                /*'<language:(ru|ua|kz|be)>/' => 'site/index',
                '<language:(ru|ua|kz|be)>/<action:(contact|login|logout|about|faq|terms)>/*' => 'site/<action>',
                '<language:(ru|ua|kz|be)>/<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<language:(ru|ua|kz|be)>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<language:(ru|ua|kz|be)>/<controller:\w+>/<action:\w+>/*'=>'<controller>/<action>',
				*/
                '<language:(ru)>/' => 'site/index',
				//'<action:(login|logout|about)>' => 'site/<action>',
				//'<controller:post>/<id:\d+>/<title:\w+>'=>'<controller>/view',
                '<language:(ru)>/<action:(contact|login|logout|terms)>/*' => 'site/<action>',
				'<language:(ru)>/<alias:\w+>' => 'page/view',
                '<language:(ru)>/<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<language:(ru)>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<language:(ru)>/<controller:\w+>/<action:\w+>/*'=>'<controller>/<action>',
            ),
        ),
	),
    'sourceLanguage'=>'en',
    'language'=>'ru',
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
        'languages'=>array('ru'=>'Русский', 'ua'=>'Українська', 'be'=>'Белорусский', 'kz'=>'Казахский'),
	),
    'aliases' => array(
        'xupload' => 'ext.xupload'
    ),
);
