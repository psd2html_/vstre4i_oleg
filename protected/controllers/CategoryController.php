<?php

class CategoryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */


	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = $this->loadModel($id);
        $translateModels = Translate::model()->findAllByAttributes(array('row_id'=>$id,'table_name'=>'category'));

        foreach($translateModels as $translate){
            $model->{$translate->language} = $translate->value;
        }
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Category;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Category']))
		{
			$model->attributes=$_POST['Category'];
			if($model->save()){
                foreach ($_POST['translate'] as $lang=>$translate){
                    $trModel = new Translate();
                    $trModel->row_id = $model->id;
                    $trModel->language = $lang;
                    $trModel->value = $translate;
                    $trModel->table_name = 'category';
                    $trModel->save(false);
                }
				$this->redirect(array('view','id'=>$model->id));
            }
		}
        $languages = Yii::app()->params['languages'];
        unset($languages['ru']);
        $trModels = array();
        if($languages){
            foreach($languages as $lang=>$value){
                $langModel = new Translate();
                $langModel->language = $lang;
                $trModels[$lang] = $langModel;
            }
        }
		$this->render('create',array(
			'model'=>$model,
            'trModels' => $trModels
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $translateModels = Translate::model()->findAllByAttributes(array('table_name'=>'category','row_id'=>$id));
        $languages = Yii::app()->params['languages'];
        unset($languages['ru']);
        $trModels = array();
        foreach($translateModels as  $translate){
            if(array_key_exists($translate->language,$languages)){
                unset($languages[$translate->language]);
                $trModels[$translate->language] = $translate;
            }
        }


        if($languages){
            foreach($languages as $lang=>$value){
                $langModel = new Translate();
                $langModel->row_id = $id;
                $langModel->language = $lang;
                $trModels[$lang] = $langModel;
            }
        }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Category']) && isset($_POST['translate']))
		{

            foreach ($_POST['translate'] as $lang=>$translate){
                $trModel = Translate::model()->findByAttributes(array('table_name'=>'category','row_id'=>$id,'language'=>$lang));
                if(!$trModel){
                    $trModel = new Translate();
                    $trModel->row_id = $id;
                }
                $trModel->language = $lang;
                $trModel->value = $translate;
                $trModel->table_name = 'category';
                $trModel->save();
            }
			$model->attributes=$_POST['Category'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
            'trModels' => $trModels,
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Category');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $model=new Category();
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Category']))
            $model->attributes=$_GET['Category'];
        $content = $this->renderPartial('admin',array(
            'model'=>$model,
        ),true);
        $this->render('application.views.admin.index',array('content'=>$content));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Category the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Category::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Category $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='category-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
