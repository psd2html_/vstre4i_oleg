<?php

class GroupController extends Controller
{

    private $config;
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','update','view','createGroup','joingroup','leavegroup','delgroup',
                    'createMeet','members','photos','addAlbum','updatingMeet', 'meet','creatingmeet','sentcomment','meetproposal','joinmeet','leavemeet',
                    'getusermeetsjson','uploadphoto','contactadmin',
                    //those actions must be replaced to section for registered users!
                    'deletewarning','deletemeetproposal','approvemeetproposal','delegategroup',
                    'delegateapprove','delegateagree', 'moderatorretire','makemoderatorrequest','satisfymoderatorrequest',
                    'moderatorapprove','moderatoragree','delegatewaiting','delegatecancel',
                ),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('activateGroup','GetPaymentTemplate','create',
                    'admin','delete',//admin and moderators are only allowed for this two actions
                    ),
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    private function _redirectNonAllowedUser()
    {
        $siteModeratorsModel = new SiteModerators();
        $userId = User::model()->getUserId();
        $isSiteModerator = $siteModeratorsModel->isSiteModerator($userId);
        $isAdmin = Admin::model()->isSiteAdmin($userId);
        if ( !$isAdmin && !$isSiteModerator ) {
            Yii::app()->request->redirect('/');
        }
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->useCommonKeywords = false;
        $model = $this->loadModel($id);
		if($model->type != 'group')
			throw new CHttpException(404,'The requested page does not exist.');
        $userId = User::model()->getUserId();
        $isSiteAdmin = Admin::model()->isSiteAdminOrModerator($userId);

        if (!$model->approved && !$isSiteAdmin) {
            $this->render('approvewaiting',array('model'=>$model));
        } else {
            $this->render('view',array('model'=>$model));
        }

	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Group;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
        //$isMeet = Yii::app()->request->getQuery('type',false);
		if(isset($_POST['Group']))
		{
			$model->attributes=$_POST['Group'];
            $model->date_created = Date('Y-m-d H:i:s');
            /* if($isMeet){
                $model->type = 'meetup';
            } else {
                $model->type = 'group';
            } */
            $model->type = 'meetup';
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create-meet2',array(
			'model'=>$model,
            //'isMeet' => $isMeet
		));
	}

    /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
    public function actionUpdate($id)
    {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/payment.js');
        $model=$this->loadModel($id);
        $ava = $model->picture;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Group']))
        {
            if ($_FILES['Group']['name']['picture']!='') {
                $uploaddir = 'images/group/';
               

			  // $filename = uniqid();
              //  $fileext = pathinfo($_FILES['Group']['name']['picture'], PATHINFO_EXTENSION);
             //   $uploadfile = $uploaddir.basename($filename.'.'.$fileext);
               // $file_from = $_FILES['Group']['tmp_name']["picture"];
            //    move_uploaded_file($file_from,$uploadfile);

				$rezName = UploadImg::model()->upload_image($uploaddir, 'Group', 'picture', 200, 200,  false, false);
				
                $_POST['Group']["picture"] = $rezName;
            } else {
                $_POST['Group']["picture"] = $ava;
            }


            $model->attributes=$_POST['Group'];
            if($model->save()){
				$link = '<a href="'. Yii::app()->createUrl('group/view', array('id' => $model->id)) . '">'.$model->name.'</a>';
				$noticeMessage = 'Некоторая информация о группе %s изменена';
				$noticeMessageGroup = 'Некоторая информация о группе %2$s изменена';
				$members = Member::model()->getGroupMembers($model->id);
				foreach($members as $member){
					Notice::addNoticeCheckSimilar($member['id'], 'notice', $noticeMessage, $noticeMessageGroup, $link);	
				}	
                $this->redirect(array('update','id'=>$model->id));
			}
        }

        $this->render('update',array(
            'model'=>$model,
            'group_id' => $id
        ));
    }

    public function actionMeet($id)
    {
		$this->useCommonKeywords = false;
        $model=$this->loadModel($id);
		
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Group']))
        {
            $model->attributes=$_POST['Group'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('meet',array(
            'model'=>$model,
        ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $this->_redirectNonAllowedUser();

		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

    public function actionMembers($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Group']))
        {
            $model->attributes=$_POST['Group'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('members',array(
            'model'=>$model,
        ));
    }

    public function actionUploadPhoto()
    {
        var_dump($_POST['album_id']);
    }

    public function actionPhotos($id)
    {
		$this->useCommonKeywords = false;
        if (isset($_FILES) && isset($_POST['album_id'])) {
            Photoalbum::model()->imagesUpload($_FILES,$_POST['album_id'],User::model()->getUserId());
        }

        $model= Group::model()->findByAttributes(array('type' => 'group', 'id' => $id));
        $albums = Photoalbum::model()->getGroupAlbums($model->id);

        $this->render('photos',array(
            'model'=>$model,
            'albums' => $albums
        ));
    }

    public function actionAddAlbum($id)
    {
        $album = new Photoalbum();
        $model= Group::model()->findByAttributes(array('type' => 'group', 'id' => $id));
        $userId = User::model()->getUserId();
		$isGroupAdmin = $model->isGroupAdmin($model->id, $userId);
		$isSiteAdmin = Admin::model()->isSiteAdminOrModerator($userId);
		if ($isGroupAdmin && !empty($_POST['Photoalbum']) || $isSiteAdmin && !empty($_POST['Photoalbum'])) {
            $album->setAttributes($_POST['Photoalbum']);
            $album->group_id = $model->id;
            $album->meet_id = 0;
            if ($album->save())
                $this->redirect($this->createUrl('photoalbum/view', array('id' => $album->id)));
        }
        $this->render('createAlbum', array('album' => $album, 'group' => $model));
    }

    public function actionCreatingMeet()
    {
        $request = Yii::app()->request;
        $meet   = $request->getParam('meet', false);
        $parent = (int)$request->getParam('parent', false);

        if ( ! $parent ) {
            echo 0;
            return 0;
        }

        if ( ! $meet ) {
            $name = $request->getParam('name', false);
            $city = $request->getParam('city', false);
            $date = $request->getParam('date', false);
            $hour = (int)$request->getParam('time_hour', 0);
            $seats = (int)$request->getParam('seats', 1);
            $minute  = (int)$request->getParam('time_minute', 0);
            $address = $request->getParam('address', false);
            $description = $request->getParam('description', false);

            $date = $date . ' ' . $hour . ':' . $minute;
            $date = date('Y:m:d H:i:s', strtotime($date));

            if ( $name && $address && $date && $city && $description ) {
                $meet = array(
                    'name' => $name,
                    'city' => $city,
                    'seats' => $seats,
                    'date_start' => $date,
                    'date_end' => $date,
                    'time_hour' => $hour,
                    'time_minute'  => $minute,
                    'address' => $address,
                    'description' => $description,
                );
            } else {
                echo 0;
                return 0;
            }
        }

        $anyTime = $request->getParam('any_time', false);
        if ($anyTime) {
            $meet['date_end'] = '';
        }

        $return = (int)Group::model()->createMeet($meet, $parent);
        echo $return;
        return $return;
    }

    public function actionUpdatingMeet($id)
    {
        $model = Group::model()->findByAttributes(array('id' => $id, 'type' => 'meetup'));
        $userId = User::model()->getUserId();
		$isAdmin = Admin::model()->isSiteAdmin($userId);
		if (empty($model) || !Group::model()->isGroupAdmin($model->parent, User::model()->getUserId()) && !$isAdmin)
            throw new HttpException(402);
        $meetData = $_GET['Group'];
        if (!empty($meetData)){
            $model->setAttributes($meetData);
            $time_hour = $meetData['time_hour'] ?  $meetData['time_hour'] : 0;
            $time_minute = $meetData['time_minute'] ?  $meetData['time_minute'] : 0;
			$date = $meetData['date'] . ' ' . $time_hour . ':' .  $time_minute;
            $date = date('Y-m-d H:i:s', strtotime($date));
            $model->date_start = $date;
            $model->date_end = $date;
            if ( isset($meetData['any_time']) && $meetData['any_time'] ) {
                $model->date_end = '';
            }
            if ( ! isset($meetData['seats']) ) {
                $model->seats = 1;
            }
            if ($model->save()){
				//notice
				$link = '<a href="'. Yii::app()->createAbsoluteUrl('group/meet', array('id' => $model->parent, 'mid' => $model->id)) . '">'.$model->name.'</a>';
				$noticeMessage = 'Изменена информация о встречи %s';
				$noticeMessageGroup = 'Изменена информация о встречи %2$s';
				$members = Member::model()->getGroupMembers($model->id);
				
				//email
				$mail = new JPhpMailer;
				$mail->setFrom(Yii::app()->params['adminEmail'], '');
				$mail->Subject = 'Изменена информация о встречи';
				$address = $model->address ? "Место проведения: $model->address;<br>" : '';
				$seats = $model->seats ? "Количество мест: $model->seats;<br>" : '';
				$start = $model->date_start == $model->date_end ? 'Начало: '.date('d.m.Y',strtotime($model->date_start)).' '.date('H:i',strtotime($model->date_start)).';<br>' : 'Начало: '.date('d.m.Y',strtotime($model->date_start)) . ' (любое время);<br>';
				$description = $model->description ? "Опись: $model->description<br>" : '';
				$content = "Изменена информация о встречи $link.<br>$address $seats $start $description";
				$mail->MsgHTML($this->message($content));
			
				//common
				foreach($members as $member){
					Notice::addNoticeCheckSimilar($member['id'], 'notice', $noticeMessage, $noticeMessageGroup, $link);	
					$mail->addAddress($member['email'], $member['name']);
				}	
				$mail->send();
				
                $this->redirect($this->createUrl('group/meet', array('id' => $model->parent, 'mid' => $model->id)));
				
			}
        } else
            $this->render('updateMeet', array('model' => $model));
    }

    public function actionCreateMeet($id)
    {

            $model=$this->loadModel($id);

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Group']))
            {
                $model->attributes=$_POST['Group'];
                if($model->save())
                    $this->redirect(array('view','id'=>$model->id));
            }

            $this->render('create-meet',array(
                'model'=>$model,
            ));

    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        /*$criteria = new CDbCriteria;
        $criteria->alias = 'member';
        $criteria->select = 'id, name, description, type, date_start, date_end, city_id, address, seats, date_created, picture, parent, appeal, background';
        $criteria->join = 'LEFT JOIN member ON member.group_id = group.id';
        $criteria->condition = 'favourite_type = 1';
        $sort=new CSort('Member');
        $sort->applyOrder($criteria);
        $sort->attributes = array(
            'member' => array(
                'asc' => 'member.confirm ASC',
                'desc' => 'member.confirm DESC'
            ),
        );*/

		$dataProvider=new CActiveDataProvider('Group'/*,array('criteria'=>$criteria,'sort'=>$sort)*/);
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

    /**
     * Manages all models.
     */
    public function actionAdmin($type = false)
    {
        $this->_redirectNonAllowedUser();

        $model=new Group();
        if($type){
            $model->search(true);
        } else {
            $model->search(false);
        }
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Group']))
            $model->attributes=$_GET['Group'];
        $content = $this->renderPartial('admin',array(
            'model'=>$model,
            'meet'=>$type
        ),true);
        $this->render('application.views.admin.index',array('content'=>$content));
    }

    public function actionSavePremoderationSettings()
    {
        $this->_redirectNonAllowedUser();

        $request = Yii::app()->request;
        $mode  = (int)$request->getParam('group_premoderation_mode', 0);
        $limit = (int)$request->getParam('group_premoderation_limit', 0);

        $success = Group::model()->savePremoderation($mode, $limit);

        Yii::app()->user->setFlash('success', Yii::t('var', 'Настройки премодерации сохранены.'));

        $this->redirect($this->createUrl('admin/grouppremoderation'));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Group the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Group::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Group $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='group-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionCreateGroup()
    {
        $request = Yii::app()->request;
        $model   = Group::model();
        $city = $request->getParam('city', false);
        $city_unknown = $request->getParam('city_new', false);
        $region = Region::model()->findByPk($request->getParam('region', false));
        if (empty($city) && !empty($city_unknown) && !empty($region)) {
            $newCity =new City();
            $newCity->region_id = $region->id;
            $newCity->country_id = $region->country_id;
            $newCity->name = $city_unknown;
            if ($newCity->save())
                $city = $newCity->name;
		}
        $name = $request->getParam('name', false);
        $userId  = $request->getParam('user_id', false);
        $keywords = $request->getParam('keywords', false);
        $description = $request->getParam('description', false);

        if ( ! $userId ) {
            $userId = User::model()->getUserId();
		}
		
        if ($city && $name && $description && $userId && $keywords) {
            $groupId = (int)$model->createGroup($city, $name, $description, $userId, $keywords);
			$mail = new JPhpMailer;
			$mail->setFrom(Yii::app()->params['adminEmail'], '');
			$mail->addAddress(User::model()->getUserEmail($userId), '');
			$mail->Subject = 'Заявка на создание группы';
			$content = "Ваша группа <b>$name</b> была успешно создана, но пока-что не доступна для просмотра другим пользователям.\r\n Группа будет доступна для просмотра после проверки модератором (обычно это занимает до 48 часов).";
			$mail->MsgHTML($this->message($content));$mail->send();
            
			$this->redirect(array('group/view','id'=>$groupId));
        } else {    
			$this->redirect(array('site/page','view'=>'create-group'));

		}
    }

    public function actionJoinGroup(){
        if ($_POST['id']) $id = $_POST['id'];
        if ($_POST['group_id']) $group_id = $_POST['group_id'];
        Group::model()->joinGroup($id,$group_id);
    }
    public function actionLeaveGroup(){
        if ($_POST['id']) $id = $_POST['id'];
        if ($_POST['group_id']) $group_id = $_POST['group_id'];
        Group::model()->leaveGroup($id,$group_id);
    }

    public function actionDeleteWarning($id)
    {
        $model = $this->loadModel($id);

        if ( ! $model->isGroupMainAdmin($model->id, User::model()->getUserId()) ) {
            $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('deletewarning',array(
            'model' => $model,
            'group_id' => $id
        ));
    }

    public function actionDelGroup(){
        if ($_POST['id']) $id = (int) $_POST['id'];
        if ($_POST['group_id']) $group_id = (int) $_POST['group_id'];
        Group::model()->delGroup($id,$group_id);
	    //$this->redirect(Yii::app()->homeUrl);
    }
    public function actionJoinMeet(){
        if ($_POST['id']) $id = $_POST['id'];
        if ($_POST['group_id']) $group_id = $_POST['group_id'];
        Group::model()->joinMeet($id,$group_id);
    }
    public function actionLeaveMeet(){
        if ($_POST['id']) $id = $_POST['id'];
        if ($_POST['group_id']) $group_id = $_POST['group_id'];
        Group::model()->leaveMeet($id,$group_id);
    }

    public function actionSentComment()
    {
        $request = Yii::app()->request;

        $targetId = (int)$request->getParam('group_id', 0);
        if (!$targetId) {
            $targetId = (int)$request->getParam('meet_id', 0);
        }
        $parentId = (int)Yii::app()->request->getParam('parent_id', 0);
        $userId   = (int)$request->getParam('sender_id', 0);
        $commentText = $request->getParam('text', '');

        $commentText = trim($commentText);

        $model = $this->loadModel($targetId);

        $success = 0;
        if ($commentText !== '') {
            $success = $model->sentComment($targetId, $commentText, $userId, $parentId);
        }

        //while meets have no settings, next if operator is needed
        if ($model->type == 'meetup') {
            $parent = $this->loadModel($model->parent);
            $commentNotice = $parent->comment_notice;
            $targetLink = Yii::app()->createAbsoluteUrl('group/meet/') . '/' . urlencode($model->parent)
                . '?mid=' . urlencode($model->id) . '#comments';
        } else {
            $commentNotice = $model->comment_notice;
            $targetLink = Yii::app()->createAbsoluteUrl('group/view/') . '/' . urlencode($model->id) . '#comments';
        }

        if ($success) {
            if ($model->type == 'group') {
                $admin = $model->getGroupAdmin($model->id);
                $typeWord = 'группе';
            } else {
                $admin = $model->getGroupAdmin($model->parent);
                $typeWord = 'событии';
            }

            if ($parentId) {
                $parentComment = Comment::model()->findByPk($parentId);
                $parentUser = User::model()->findByPk($parentComment->user_id);
                if ($parentUser->answer_comment_notice) {
                    //Notice::addNotice($parentUser->id, 'notice', $message);
					$link = '<br><a href="'. $targetLink . '">'.$model->name.'</a>';
                    $message = 'На ваш комментарий в ' . $typeWord . ' ' . $link . ' ответили.';
					$noticeMessage = 'На ваш комментарий в ' . $typeWord . ' %s ответили.';
					$noticeMessageGroup = 'На ваш комментарий в ' . $typeWord . ' %2$s ответили.';
					Notice::addNoticeCheckSimilar($parentUser->id, 'notice', $noticeMessage, $noticeMessageGroup, $link);
					$hostInfo = Yii::app()->request->hostInfo;
					$hostInfoA = explode('//',$hostInfo);
					$hostName = $hostInfoA[1];
					
					$subject = $message . ' '.$hostInfo;
                    $emailMessage = $message . '<br />'
                        . '<a href="'. $targetLink
                        . '" target="_blank">Перейти на сайт</a>';
                    //mail($parentUser->email, $subject, $emailMessage, "Content-type: text/html\r\n");
                
					$mail = new JPhpMailer;
					$mail->setFrom(Yii::app()->params['adminEmail'], '');
					$mail->addAddress($parentUser->email, '');
					$mail->Subject = $subject;
					$content = $emailMessage;
					$mail->MsgHTML($this->message($content));
					$mail->send();
				}
            }

            if ($commentNotice) {
				$hostInfo = Yii::app()->request->hostInfo;
				$hostInfoA = explode('//',$hostInfo);
				$hostName = $hostInfoA[1];
                $message = 'Оставлен комментарий в ' . $typeWord . ' ' . $model->name . '.';
                $subject = $message . ' '.$hostName;
                $emailMessage = $message . '<br />'
                    . '<a href="'. $targetLink
                    . '" target="_blank">Перейти на сайт</a>';
                //mail($admin['email'], $subject, $emailMessage, "Content-type: text/html\r\n");
				
				$mail = new JPhpMailer;
				$mail->setFrom(Yii::app()->params['adminEmail'], '');
				$mail->addAddress($admin['email'], '');
				$mail->Subject = $subject;
				$content = $emailMessage;
				$mail->MsgHTML($this->message($content));
				
				$moderators = Group::model()->getModerators($targetId);
				foreach($moderators as $moderator){
					$mail->addAddress($moderator['email'], $moderator['name']);
					Notice::addNoticeCheckSimilar($moderator['moderator_id'], 'notice', $noticeMessage, $noticeMessageGroup, $link);
				}					
				$mail->send();
            }
        }

        $this->redirect($targetLink);
    }

    public function actionDeleteMeetProposal()
    {
        $request = Yii::app()->request;
        $groupModel = Group::model();

        $groupId = (int)$request->getParam('proposal_id', 0);

        echo (int)$groupModel->deleteMeetProposal($groupId);
    }

    public function actionApproveMeetProposal()
    {
        $request = Yii::app()->request;
        $meet = $request->getParam('meet', false);
        $parent = $request->getParam('parent', false);

        if ($meet && $parent && isset($meet['id'])) {
            echo (int)Group::model()->createMeet($meet,$parent);
            Group::model()->deleteMeetProposal($meet['id']);
        } else {
            echo 0;
        }
    }

    public function actionMeetProposal()
    {
        $request = Yii::app()->request;
        $groupModel = Group::model();

        $name  = $request->getParam('name', false);
        $place = $request->getParam('place', false);
        $seats = (int)$request->getParam('seats', 1);
        $comment = $request->getParam('comment', false);

        $anyTime = (int)$request->getParam('any_time', 0);

        $hours = $request->getParam('time_hour', 0);
        $minutes = $request->getParam('time_minute', 0);
        $date = $request->getParam('date', 0) . ' ' . $hours . ':' . $minutes;
        $timestamp = strtotime($date);
        $date = date('Y/m/d H:i:s', $timestamp);

        $userId = (int)$request->getParam('user_id', 0);
        $groupId = (int)$request->getParam('group_id', 0);

        $success = $groupModel->meetProposal($name, $place, $seats, $date, $anyTime, $comment, $userId, $groupId);
        if (! $success) {
            Yii::app()->user->setFlash('error', Yii::t('var', 'Ошибка. Событие не предложено.'));
            return false;
        }

        $groupAdmin = $groupModel->getGroupAdmin($groupId);

        if ($groupAdmin['id'] && $groupAdmin['email']) {
            $username = User::model()->getUserName($userId);
            $group = $groupModel->getGroup($groupId);


            //Notice::addNotice($groupAdmin['id'], 'notice', $noticeMessage);
			$link = '<a href="'. Yii::app()->createUrl('group/update', array('id'=>$groupId, '#'=>'tab6')) . '">'.$group['name'].'</a>';
			$noticeMessage = $username . ' предложил новое событие в группе %s.';
			$noticeMessageGroup = 'Поступили предложения %d новых событий в группе %s.';
			Notice::addNoticeCheckSimilar($groupAdmin['id'], 'notice', $noticeMessage, $noticeMessageGroup, $link);
			$hostInfo = Yii::app()->request->hostInfo;
			$hostInfoA = explode('//',$hostInfo);
			$hostName = $hostInfoA[1];
            $subject = 'Предложено событие в группе ' . $group['name'] . ' '.$hostName;
			$noticeMessage = $username . ' предложил новое событие в группе ' . $link;
            $emailMessage = $noticeMessage . '<br />'
                . '<a href="'. Yii::app()->createAbsoluteUrl('group/view') . '/' . $groupId
                . '" target="_blank">Перейти на сайт</a>';
            //mail($groupAdmin['email'], $subject, $emailMessage, "Content-type: text/html\r\n");
			
			$mail = new JPhpMailer;
			$mail->setFrom(Yii::app()->params['adminEmail'], '');
			$mail->addAddress($groupAdmin['email'], '');
			$mail->Subject = $subject;
			$content = $emailMessage;
			$mail->MsgHTML($this->message($content));
			$moderators = Group::model()->getModerators($groupId);
			foreach($moderators as $moderator){
				$mail->addAddress($moderator['email'], $moderator['name']);
				Notice::addNoticeCheckSimilar($moderator['moderator_id'], 'notice', $noticeMessage, $noticeMessageGroup, $link);
			}					
			$mail->send();
        }
        return 1;
    }

    public function actionGetUserMeetsJson()
    {
        $result = '[';
        $i = 1;
        $dateReader = Group::model()->getUserMeetsJson($_GET['user_id']);
        foreach ($dateReader as $val) {
            $result .= '{ '
                . '"date": "' . CHtml::encode(strtotime($val['date'])) . '000", '
                . '"type": "meeting", '
                . '"title": "' . CHtml::encode($val['name']) . '", '
                . '"description": "' . CHtml::encode($val['description']) . '", '
                . '"url": "' . Yii::app()->createUrl('site/index') . '/group/meet/'
                    . urlencode($val['parent']) . '/?mid=' . urlencode($val['id']) . '", '
                . '"date_end": "' . CHtml::encode($val['date_end']) . '" '
                . '}';
            $result .= ',';
            $i++;
        }
        $len = strlen($result);
        $result = substr($result, 0, $len-1);
        $result .= ']';
        if ($result==']') $result="[]";
        echo $result;
    }



    public function actionGetPaymentTemplate($name,$group_id)
    {
        $methods = array('webmoney','paypal','qiwi','sms');
        if(in_array($name,$methods)){
            if($name == 'webmoney'){
                $webmoney = Admin::model()->findByAttributes(array('key'=>'webmoney'));
                $amount = Admin::model()->findByAttributes(array('key'=>'group_price_usa'));
                $this->renderPartial('application.views.group.payment.'.$name,array('group_id'=>$group_id,'amount'=>$amount->value,'webmoney'=>$webmoney->value));
            } else {
                $this->renderPartial('application.views.group.payment.'.$name,array('group_id'=>$group_id));
            }
        }
    }


    public function actionActivateGroup($code,$id)
    {
        if($code){
            $model = Payment::model()->findByAttributes(array('confirmation_code'=>$code),'status="0"');
            if($model){
                $model->group_id = $id;
                $model->status = 1;
                $model->save(false);
                Yii::app()->groupExtender->extendGroup($id);
                echo 1;
            } else {
                echo 0;
            }
        }
    }
    public function actionContactAdmin()
    {
        $request = Yii::app()->requsest;

        $adminId = (int)$request->getParam('admin_id');
        $userId  = (int)$request->getParam('user_id');
        $text    = $request->getParam('comment');

        if ($userId && $text) {
            $adminEmail = User::model()->getUserEmail($adminId);
            $userName   = User::model()->getUserName($userId);

            //Notice::addNotice($adminId, 'notice', $noticeMessage);

			$link = '<a href="'. Yii::app()->createUrl('user/view', array('id'=>$userId)) . '">'.$username.'</a>';
			$noticeMessage = '%s отправил Вам личное сообщение.';
			$noticeMessageGroup = '%2$s отправил Вам %1$d личных сообщения.';
			Notice::addNoticeCheckSimilar($adminId, 'notice', $noticeMessage, $noticeMessageGroup, $link);	
			
			
			
			$hostInfo = Yii::app()->request->hostInfo;
			$hostInfoA = explode('//',$hostInfo);
			$hostName = $hostInfoA[1];
            $subject = 'Новое сообщение. '.$hostName;

            $noticeMessage = $link . ' отправил вам личное сообщение.';
            $href = Yii::app()->createAbsoluteUrl('user/view') . '/' . $userId;
            $emailMessage = $noticeMessage . '<br />' . CHtml::encode($text)
                . '<br /><br /><a href="' . $href . '" target="_blank">Перейти на сайт</a>';
            //mail($adminEmail, $subject, $emailMessage, "Content-type: text/html\r\n");
			
			$mail = new JPhpMailer;
			$mail->setFrom(Yii::app()->params['adminEmail'], '');
			$mail->addAddress($adminEmail, '');
			$mail->Subject = $subject;
			$content = $emailMessage;
			$mail->MsgHTML($this->message($content));$mail->send();
        }
    }

    public function actionMakeModeratorRequest()
    {
        $request = Yii::app()->request;

        $groupId  = (int)$request->getParam('group_id', 0);
        $targetId = (int)$request->getParam('user_id', 0);
        $type     = $request->getParam('type', false);
        $senderId = User::model()->getUserId();

        $groupModel = Group::model();

        if ( $groupModel->isGroupMainAdmin($groupId, $senderId) ) {
            $adminApprove = true;
        } elseif ( $groupModel->isGroupModerator($groupId, $senderId) ) {
            $adminApprove = false;
        } else {
            return 0;
        }

        $requestId = $groupModel->makeModeratorRequest($type, $groupId, $targetId, $senderId);
        if ($adminApprove) {
            $groupModel->adminApproveModerator($type, $groupId, $targetId);
        }

        if ($requestId && $type == 'add') {
            $targetEmail = User::model()->getUserEmail($targetId);
            $senderName  = User::model()->getUserName($senderId);
            $group = Group::model()->getGroup($groupId);

            //Notice::addNotice($targetId, 'notice', $noticeMessage);
			$link = '<a href="'. Yii::app()->createUrl('group/view', array('id'=>$groupId, '#'=>'moderator')) . '">'.$group['name'].'</a>';
			$noticeMessage = $senderName . ' предложил вас на роль модератора группы %s';
			$noticeMessageGroup = $senderName . ' предложил вас на роль модератора группы %2$s';
			Notice::addNoticeCheckSimilar($targetId, 'notice', $noticeMessage, $noticeMessageGroup, $link);	

            $noticeMessage = $senderName . ' предложил вас на роль модератора группы ' . $link;
			$hostInfo = Yii::app()->request->hostInfo;
			$hostInfoA = explode('//',$hostInfo);
			$hostName = $hostInfoA[1];
            $subject = 'Вас предложили на роль модератора в группе ' . $group['name'] . '. '.$hostName;
            $href = Yii::app()->createAbsoluteUrl('group/moderatorapprove') . '/' . $groupId
                . '?rt=' . $groupModel->getModeratorRequestToken($requestId);
            $emailMessage = $noticeMessage . '<br />Вы можете согласиться или отклонить предложение.'
                . '<a href="' . $href . '" target="_blank">Пройдите по ссылке, чтобы сообщить своё решение.</a>';
            //mail($targetEmail, $subject, $emailMessage, "Content-type: text/html\r\n");

			$mail = new JPhpMailer;
			$mail->setFrom(Yii::app()->params['adminEmail'], '');
			$mail->addAddress($targetEmail, '');
			$mail->Subject = $subject;
			$content = $emailMessage;
			$mail->MsgHTML($this->message($content));$mail->send();
        }

        return $requestId;
    }

    public function actionSatisfyModeratorRequest()
    {
        $request = Yii::app()->request;

        $groupId = (int)$request->getParam('group_id', 0);
        $userId  = (int)$request->getParam('user_id', 0);
        $type    = $request->getParam('type', false);

        $groupModel = Group::model();

        if ( ! $groupModel->isGroupMainAdmin($groupId, User::model()->getUserId()) ) {
            return 0;
        }

        $success = $groupModel->satisfyModeratorRequest($type, $groupId, $userId);

        return $success;
    }

    public function actionModeratorApprove($id)
    {
        $model = $this->loadModel($id);

        $this->render('moderatorapprove',array(
            'model' => $model,
            'group_id' => $id,
        ));
    }

    public function actionModeratorAgree()
    {
        $userId   = User::model()->getUserId();
        $token    = Yii::app()->request->getParam('token', 0);
        $disagree = Yii::app()->request->getParam('disagree', 0);

        if ($disagree) {
            $success = Group::model()->deleteModRequestsByToken($userId, $token);
        } else {
            $success = Group::model()->moderatorAgree($userId, $token);
        }

        return $success;
    }

    public function actionModeratorRetire()
    {
        $model = Group::model();
        $request = Yii::app()->request;

        $userId  = User::model()->getUserId();
        $groupId = (int)$request->getParam('group_id', 0);

        if ( $groupId && $userId
            && $model->isGroupModerator($groupId, $userId)
        ) {
            $model->fireModerator($groupId, $userId);
        }

        $request->redirect(Yii::app()->createUrl('group/view') . '/' . $groupId);
    }

    public function actionDelegateGroup()
    {
        $request = Yii::app()->request;
        $model = Group::model();

        $groupId   = (int)$request->getParam('group_id', 0);
        $newUserId = (int)$request->getParam('user_id', 0);
        $oldUserId = User::model()->getUserId();

        if ( ! $model->isGroupMainAdmin($groupId, $oldUserId) ) {
            $this->redirect(array('view','id'=>$model->id));
        }

        if ($groupId && $newUserId) {
            $requestId = $model->makeDelegateRequest($groupId, $newUserId, $oldUserId);
            $requestToken = $model->getDelegateRequestToken($requestId);

            if ($requestId) {
                $newUserEmail = User::model()->getUserEmail($newUserId);
                $oldUserName  = User::model()->getUserName($oldUserId);
                $group = Group::model()->getGroup($groupId);


                //Notice::addNotice($newUserId, 'notice', $noticeMessage);
				$link = '<a href="'. Yii::app()->createUrl('group/view', array('id'=>$groupId, '#'=>'delegate')) . '">'.$group['name'].'</a>';
				$noticeMessage = $oldUserName . ' хочет передать вам права на управление группой %s';
				$noticeMessageGroup = $oldUserName . ' хочет передать вам права на управление группой %2$s';
				Notice::addNoticeCheckSimilar($newUserId, 'notice', $noticeMessage, $noticeMessageGroup, $link);	
				
                $noticeMessage = $oldUserName . ' хочет передать вам права на управление группой ' . $link;
				$hostInfo = Yii::app()->request->hostInfo;
				$hostInfoA = explode('//',$hostInfo);
				$hostName = $hostInfoA[1];
                $subject = 'Передача прав администратора в группе ' . $group['name'] . '. '.$hostName;
                $href = Yii::app()->createAbsoluteUrl('group/delegateapprove') . '/' . $groupId
                    . '?rt=' . $requestToken;
                $emailMessage = $noticeMessage . '<br />Вы можете согласиться или отклонить предложение.'
                    . '<a href="' . $href . '" target="_blank">Пройдите по ссылке, чтобы сообщить своё решение.</a>';
                //mail($newUserEmail, $subject, $emailMessage, "Content-type: text/html\r\n");

				$mail = new JPhpMailer;
				$mail->setFrom(Yii::app()->params['adminEmail'], '');
				$mail->addAddress($newUserEmail, '');
				$mail->Subject = $subject;
				$content = $emailMessage;
				$mail->MsgHTML($this->message($content));$mail->send();
            }
            echo CHtml::encode($requestToken);
            return $requestToken;
        } else {
            echo 0;
            return 0;
        }
    }

    public function actionDelegateApprove($id)
    {
        $model = $this->loadModel($id);

        $this->render('delegateapprove',array(
            'model' => $model,
            'group_id' => $id,
        ));
    }

    public function actionDelegateAgree()
    {
        $newAdminId   = User::model()->getUserId();
        $token    = Yii::app()->request->getParam('token', 0);
        $disagree = Yii::app()->request->getParam('disagree', 0);
        $groupModel = Group::model();

        $delegateRequest = $groupModel->getDelegateRequestByToken($newAdminId, $token);

        $success = 0;
        if (!$disagree) {
            $success = Group::model()->satisfyDelegateRequest($newAdminId, $token);
        }
        $groupModel->deleteDelegateRequestsByToken($newAdminId, $token);

        if ($success && $delegateRequest) {
            $oldAdminId = $delegateRequest['sender_id'];

            $newAdminEmail = User::model()->getUserEmail($newAdminId);
            $oldAdminEmail = User::model()->getUserEmail($oldAdminId);

            $newAdminName = User::model()->getUserName($newAdminId);
            $newAdminLink = Yii::app()->createAbsoluteUrl('user/view') . '/' . $newAdminId;

            $group = Group::model()->getGroup($delegateRequest['group_id']);

            $groupLink = Yii::app()->createAbsoluteUrl('group/view') . '/' . $group['id'];
            //to new user
            //Notice::addNotice($newAdminId, 'notice', $noticeMessage);
			$link = '<a href="'. Yii::app()->createUrl('group/view', array('id'=>$group['id'], '#'=>'delegate_agree')) . '">'.$group['name'].'</a>';
			$noticeMessage = 'Вы стали администратором группы %s';
			$noticeMessageGroup = 'Вы стали администратором группы %2$s';
			Notice::addNoticeCheckSimilar($newAdminId, 'notice', $noticeMessage, $noticeMessageGroup, $link);	
			
            $noticeMessage = 'Вы стали администратором группы ' . $link;
			$hostInfo = Yii::app()->request->hostInfo;
			$hostInfoA = explode('//',$hostInfo);
			$hostName = $hostInfoA[1];
            $subject = 'Вы стали администратором в группе ' . $group['name'] . '. '.$hostName;
            $emailMessage = $noticeMessage . '<br /><a href="' . $groupLink . '" target="_blank">'
                . 'Перейти на сайт.</a>';
            //mail($newAdminEmail, $subject, $emailMessage, "Content-type: text/html\r\n");

			$mail = new JPhpMailer;
			$mail->setFrom(Yii::app()->params['adminEmail'], '');
			$mail->addAddress($newAdminEmail, '');
			$mail->Subject = $subject;
			$content = $emailMessage;
			$mail->MsgHTML($this->message($content));$mail->send();
			
            //to old user
            //Notice::addNotice($oldAdminId, 'notice', $noticeMessage);
			$link = '<a href="'. Yii::app()->createUrl('group/view', array('id'=>$group['id'], '#'=>'youdelegated')) . '">'.$group['name'].'</a>';
			$noticeMessage = 'Вы передали права администратора в группе %s';
			$noticeMessageGroup = 'Вы передали права администратора в группе %2$s';
			Notice::addNoticeCheckSimilar($oldAdminId, 'notice', $noticeMessage, $noticeMessageGroup, $link);
			
            $noticeMessage = 'Вы передали права администратора в группе ' . $link
                . ' пользователю ' . $newAdminName;
            $subject = 'Вы передали права администратора в группе ' . $group['name'] . '. '.$hostName;
            $emailMessage = 'Вы передали права администратора в группе ' . $group['name']
                . ' пользователю <a href="' . $newAdminLink . '">' . $newAdminName . '</a>'
                . '<br /><a href="' . $groupLink . '" target="_blank">Перейти на сайт.</a>';
            //mail($oldAdminEmail, $subject, $emailMessage, "Content-type: text/html\r\n");
			
			$mail = new JPhpMailer;
			$mail->setFrom(Yii::app()->params['adminEmail'], '');
			$mail->addAddress($oldAdminEmail, '');
			$mail->Subject = $subject;
			$content = $emailMessage;
			$mail->MsgHTML($this->message($content));$mail->send();
        }

        echo (int)$success;
        return $success;
    }

    public function actionDelegateWaiting($id)
    {
        $model = $this->loadModel($id);

        if ( ! $model->isGroupMainAdmin($model->id, User::model()->getUserId()) ) {
            $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('delegatewaiting',array(
            'model' => $model,
            'group_id' => $id
        ));
    }

    public function actionDelegateCancel()
    {
        $model = Group::model();

        $groupId = (int)Yii::app()->request->getParam('group_id', 0);
        $userId = User::model()->getUserId();

        if ( ! $model->isGroupMainAdmin($groupId, $userId) ) {
            $this->redirect(array('view','id'=>$model->id));
        }

        $request = $model->getDelegateRequest($groupId);
        $success = $model->deleteDelegateRequests($groupId);

        if ($success && $request) {
            $targetEmail = User::model()->getUserEmail($request['target_id']);
            $adminName  = User::model()->getUserName($userId);
            $group = Group::model()->getGroup($groupId);

            $noticeMessage = $adminName . ' отменил заявку на передачу прав на управление группой ' . $group['name'];

            Notice::addNotice($request['target_id'], 'notice', $noticeMessage);

			$hostInfo = Yii::app()->request->hostInfo;
			$hostInfoA = explode('//',$hostInfo);
			$hostName = $hostInfoA[1];
            $subject = 'Заявка в группе ' . $group['name'] . ' отменена. '.$hostName;
            $groupLink = Yii::app()->createAbsoluteUrl('group/view') . '/' . $groupId;
            $userLink  = Yii::app()->createAbsoluteUrl('user/view') . '/' . $userId;
            $emailMessage = 'К сожалению, <a href="' . $userLink . '">' . $adminName . '</a>'
                . ' отменил заявку на передачу прав на управление группой ' . $group['name'] . '<br />'
                . '<a href="' . $groupLink . '" target="_blank">Перейти на сайт.</a>';
            //mail($targetEmail, $subject, $emailMessage, "Content-type: text/html\r\n");

			$mail = new JPhpMailer;
			$mail->setFrom(Yii::app()->params['adminEmail'], '');
			$mail->addAddress($targetEmail, '');
			$mail->Subject = $subject;
			$content = $emailMessage;
			$mail->MsgHTML($this->message($content));$mail->send();
        }

        echo (int)$success;
        return $success;
    }

}
