<?php

class MailController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

    public $mail = null;


    public function init(){
        $sql = 'SELECT * FROM admin as t WHERE t.key IN("mailer_username","mailer_host","mailer_password","mailer_port")';
        $query = Yii::app()->db->createCommand($sql);
        $dataReader = $query->query();
        $defaults = $this->setArrayIndexFromValue($dataReader->readAll(),'key');
        //var_dump($defaults);exit;
        $this->mail = new JPhpMailer;
        $this->mail->SMTPSecure = "ssl";
        $this->mail->IsSMTP();
        $this->mail->SMTPAuth = true;
        $this->mail->SMTPDebug  = 0;
        $this->mail->CharSet  = 'utf-8';
        $this->mail->Host = $defaults['mailer_host']['value'];
        $this->mail->Username = $defaults['mailer_username']['value'];
        $this->mail->Password = $defaults['mailer_password']['value'];
        $this->mail->Port = $defaults['mailer_port']['value'];
        parent::init();
    }

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','getEmailUserForm','sendToUser','sendEmailToAll','sendToAdress','getArchiveEmailForm','testTemplate'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $this->layout='//layouts/column2admin';
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        $this->layout='//layouts/column2admin';
		$model=new Mail;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Mail']))
		{
			$model->attributes=$_POST['Mail'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $this->layout='//layouts/column2admin';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Mail']))
		{
			$model->attributes=$_POST['Mail'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $this->layout='//layouts/column2admin';
		$dataProvider=new CActiveDataProvider('Mail');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $this->layout='//layouts/column2admin';
		$model=new Mail('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Mail']))
			$model->attributes=$_GET['Mail'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Mail the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Mail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Mail $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mail-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}



    public function actionGetEmailUserForm($id){
        if(Yii::app()->request->isAjaxRequest){
			$this->renderPartial('emailForm',array('user_id' => $id));
		}else{
			$this->layout='//layouts/column2admin';
			$this->render('emailForm',array('user_id' => $id));
		}
    }
    public function actionGetArchiveEmailForm($id){
		$db=Yii::app()->db;
		$sql='SELECT * FROM `mail` WHERE id = :id';
		$command=$db->createCommand($sql);
		$command->bindParam(":id",$id,PDO::PARAM_INT);	
		$resalt = $command->queryRow();
		$this->renderPartial('archiveEmailForm',array('letter' => $resalt));
	}
    
	public function actionSendEmailToAll(){
        $this->layout='//layouts/column2admin';
		
		if(isset($_POST['subject']) && isset($_POST['message'])){
			$mail = new JPhpMailer;
			$mail->Subject  = $_POST['subject'];
			$mail->MsgHTML($this->message($_POST['message']));
			$mail->SetFrom(Yii::app()->params->adminEmail, Yii::app()->name);
			$users = User::model()->findAll();
			foreach($users as $user){
				if($user->email && $user->status)
					$mail->AddAddress($user->email, ucfirst($user->name));
			}
			if($mail->Send())
				Yii::app()->user->setFlash('success', 'Успешно отправленно всем');
			else
				Yii::app()->user->setFlash('error', 'Не удалось отправить всем');
        }
		$this->render('sendEmailToAll');
    }



    public function actionSendToUser($id){
		if($_POST){
            $adminEmail = Admin::model()->findByAttributes(array('key'=>'admin_email'));
            $siteName = Admin::model()->findByAttributes(array('key'=>'site_name'));
            $userModel = User::model()->findByPk($id);
            $mail = new JPhpMailer;
            $mail->Subject  = $_POST['subject'];
            $mail->MsgHTML($this->message($_POST['message']));
            $mail->SetFrom($adminEmail->value, $siteName->value);
            $mail->AddAddress($userModel->email, ucfirst($userModel->name));
			$mail->Send();
			$this->redirect(array('user/admin'));

        }
    }
    public function actionSendToAdress(){
		if($_POST){
            $adminEmail = Admin::model()->findByAttributes(array('key'=>'admin_email'));
            $siteName = Admin::model()->findByAttributes(array('key'=>'site_name'));
            $userModel = User::model()->findByAttributes(array('email'=>$_POST['last_email']));
            //$userModel = User::model()->findByPk($id);
            $mail = new JPhpMailer;
            $mail->Subject  = $_POST['subject'];
			$mail->MsgHTML($this->message($_POST['message']));
			$mail->SetFrom($adminEmail->value, $siteName->value);
            $mail->AddAddress($_POST['last_email'], ucfirst($userModel->name));
			$mail->Send();
			$this->redirect(array('admin'));
        }
    }
	public function actionTestTemplate(){
		$message = 'На Вас жалоба. <a href="#">Просмотреть</a>';
		echo $this->renderPartial('//layouts/mail_template',array('message'=>$message),true);
	}
}
