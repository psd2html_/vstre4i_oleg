<?php

class MemberController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','sendrequest','appointmoderator','ismoderator','disrankmoderator','kickmember','acceptRequest','refuseRequest','removeFromBlacklist','AddToBlackList','filterMembers'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Member;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Member']))
		{
			$model->attributes=$_POST['Member'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Member']))
		{
			$model->attributes=$_POST['Member'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Member');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Member('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Member']))
			$model->attributes=$_GET['Member'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	public function actionFilterMembers()
	{
		/* $criteria=new CDbCriteria;
		$criteria->compare('name',$_GET['name'],true);
		$users = User::model()->findAll($criteria);
		if($user === null)
			echo '[]';
		else{
			$criteria=new CDbCriteria;
			$criteria->condition='country_id=:country_id';
			$criteria->params=array(':country_id'=>$_GET['country_id']);
			$dataProvider = new CActiveDataProvider('member', array(
				'criteria'=>$criteria,
			));
			
			return $this->renderPartial('admin',array(
				'dataProvider'=>$dataProvider
			),true);
		} */
		$request = Yii::app()->request;
		$midId = $request->getParam('mid', 0);
		$groupId = $request->getParam('groupId', 0);
		$user_id = User::model()->getUserId();
		$name = $request->getParam('name', '');
		$db = Yii::app()->db;
        $command = $db->createCommand();
		$command->select('user.id, user.name, user.avatar');
		$command->from('member');
		$command->join('user', 'user.id = member.user_id');
		if(!$midId){
			$command->where('`user`.`name` LIKE :name AND `member`.`group_id` = :groupId AND `member`.`confirm` = 1', array('name'=>$name.'%', 'groupId'=>$groupId));
		}else{
			$command->where('`user`.`name` LIKE :name AND `member`.`group_id` = :groupId AND `member`.`confirm` = 1', array('name'=>$name.'%', 'groupId'=>$midId));
		}
        $members =  $command->queryAll();
		$content = $this->renderPartial('_filterMembers',array(
				'dataReader'=>$members,
				'user_id'=>$user_id,
				'group_id'=>$groupId,
				'midId'=>$midId,
		),true);
		echo $content;
		
		//$command->where(array('like', 'name', $_GET['query'].'%'));
		//$command->where('name LIKE :name AND region_id=:region_id', array(':region_id'=>$_GET['region_id'], 'name'=>$_GET['query'].'%'));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Member the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Member::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Member $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='member-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionSendRequest()
    {
        $fields  = Yii::app()->request->getParam('fields', false);
        $groupId = (int)Yii::app()->request->getParam('group_id', false);
        $userId  = (int)Yii::app()->request->getParam('user_id', false);

        Member::model()->sendRequest($fields, $groupId, $userId);

        $groupAdmin = Group::model()->getGroupAdmin($groupId);
        $username = User::model()->getUserName($userId);

        $group = Group::model()->getGroup($groupId);

        if ( $groupAdmin['email'] && $userId ) {
			$link = '<br><a href="'. Yii::app()->createUrl('group/update', array('id'=>$groupId, '#'=>'tab3')) . '">'.$group['name'].'</a>';
            $noticeMessage = $username . ' подал заявку на вступление в группу %s';
            $noticeMessageGroup = '%d пользователя подали заявки на вступление в группу %s';
            Notice::addNoticeCheckSimilar($groupAdmin['id'], 'notice', $noticeMessage, $noticeMessageGroup, $link);

			//если включено уведомление то уведомляем модераторов и создателя
			if($group['member_notise']){
				$hostInfo = Yii::app()->request->hostInfo;
				$hostInfoA = explode('//',$hostInfo);
				$hostName = $hostInfoA[1];
				$subject = 'Заявка на вступление в группу ' . $group['name'] . " | $hostName";
				$emailMessage = $username . ' подал заявку на вступление в группу '.$group['name'].' <br />'
					. '<a href="'. Yii::app()->createAbsoluteUrl('group/view/') . '/' . $groupId
					. '" target="_blank">Перейти на сайт</a>';
				//mail($groupAdmin['email'], $subject, $emailMessage, "Content-type: text/html\r\n");
				$mail = new JPhpMailer;
				$mail->setFrom(Yii::app()->params['adminEmail'], '');
				$mail->addAddress($groupAdmin['email'], '');
				$mail->Subject = $subject;
				$content = $emailMessage;
				$mail->MsgHTML($this->message($content));
				$moderators = Group::model()->getModerators($groupId);
				foreach($moderators as $moderator){
					$mail->addAddress($moderator['email'], $moderator['name']);
					Notice::addNoticeCheckSimilar($moderator['moderator_id'], 'notice', $noticeMessage, $noticeMessageGroup, $link);
				}					
				$mail->send();
			}
	
			//уведомляем пользователя
			$mail = new JPhpMailer;
			$mail->setFrom(Yii::app()->params['adminEmail'], '');
			$mail->addAddress(User::model()->getUserEmail($userId), '');
			$mail->Subject = 'Вы подали заявку на вступление в группу '.$group['name'];
			$content = 'Ваша заявку на вступление в группу '.$group['name'].' находится на рассмотрении модератором.';
			$mail->MsgHTML($this->message($content));$mail->send();
        }
    }

    public function actionAppointModerator()
    {
        Member::model()->appointModerator($_GET['user_id'],$_GET['group_id']);
    }
    public function actionDisrankModerator()
    {
        Member::model()->disrankModerator($_GET['user_id'],$_GET['group_id']);
    }
    public function actionKickMember()
    {
        Member::model()->kickMember($_GET['user_id'],$_GET['group_id']);
    }

    public function actionAcceptRequest()
    {
        $model = Member::model();
        $requestId = (int)Yii::app()->request->getParam('request_id', 0);

        $success = $model->acceptRequest($requestId);

        if ($success) {
            $request = $model->getRequest($requestId);

            $group = Group::model()->getGroup($request['group_id']);
            $email = User::model()->getUserEmail($request['user_id']);

            if ( $group && $email ) {
                $noticeMessage_1 = 'Заявка на вступление в группу ' . $group['name'] . ' одобрена.';
                //Notice::addNotice($request['user_id'], 'notice', $noticeMessage);
				$link = '<a href="'. Yii::app()->createUrl('group/view', array('id'=>$group['id'])) . '">'.$group['name'].'</a>';
				$noticeMessage = 'Заявка на вступление в группу %s одобрена.';
				$noticeMessageGroup = 'Заявки (%d) на вступление в группу %s одобрены.';
				Notice::addNoticeCheckSimilar($request['user_id'], 'notice', $noticeMessage, $noticeMessageGroup, $link);

				$hostInfo = Yii::app()->request->hostInfo;
				$hostInfoA = explode('//',$hostInfo);
				$hostName = $hostInfoA[1];
                $subject = "Ваша заявка одобрена. $hostName";
                $emailMessage = $noticeMessage_1 . '<br />'
                    . '<a href="http://vstre4i.com'. Yii::app()->createUrl('group/view/') . '/' . (int)$group['id']
                    . '" target="_blank">Перейти на страницу группы</a>';
                //mail($email, $subject, $emailMessage, "Content-type: text/html\r\n");
				$mail = new JPhpMailer;
				$mail->setFrom(Yii::app()->params['adminEmail'], '');
				$mail->addAddress($email, '');
				$mail->Subject = $subject;
				$content = $emailMessage;
				$mail->MsgHTML($this->message($content));$mail->send();
            }
        }
    }

    public function actionRefuseRequest()
    {
        $requestId = (int)Yii::app()->request->getParam('request_id', 0);
        Member::model()->refuseRequest($requestId);
	    
		$request = $model->getRequest($requestId);
		$group = Group::model()->getGroup($request['group_id']);
		$email = User::model()->getUserEmail($request['user_id']);
		
		$mail = new JPhpMailer;
		$mail->setFrom(Yii::app()->params['adminEmail'], '');
		$mail->addAddress($email, '');
		$mail->Subject = "Ваша заявка на вступление в группу ".$group['name']." отклонена";
		$link = '<a href="'. Yii::app()->createUrl('group/view', array('id'=>$group['id'])) . '">'.$group['name'].'</a>';
		$content = "Ваша заявка на вступление в группу $link была отклрнена модератором группы.";
		$mail->MsgHTML($this->message($content));$mail->send();
    }

    public function actionAddToBlackList()
    {
        $requestId = (int)Yii::app()->request->getParam('request_id', 0);

        Member::model()->addToBlackList($requestId);
    }

    public function actionRemoveFromBlacklist()
    {
        Member::model()->removeFromBlacklist($_POST['request_id'],$_POST['user_id'],$_POST['group_id']);
    }
}
