<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
    public $banned = false;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','getvkuserid','update','groups','cgroups','registration','authorization','complaints','mycomplaints','answer'),
				'users'=>array('*'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','banUser','unBanUser','create','checkAnswer'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionGroups($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Group']))
        {
            $model->attributes=$_POST['Group'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('groups',array(
            'model'=>$model,
        ));
    }

    public function actionCgroups($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Group']))
        {
            $model->attributes=$_POST['Group'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('cgroups',array(
            'model'=>$model,
        ));
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = $this->loadModel($id);
        $user_id = User::model()->getUserId();
        $friends = array();
        if (($model->id == $user_id) && !empty($model->service) && in_array($model->service, array('facebook', 'vkontakte'))) {
            $service = Yii::app()->eauth->getIdentity($model->service);
            $service->authenticate();
            if ($model->service == 'facebook') {
                $facebookFriends = $service->makeSignedRequest('https://graph.facebook.com/me/friends');
                $serviceFriendsIds = array();
                foreach ($facebookFriends->data as $facebookFriend) {
                    $serviceFriendsIds[] = $facebookFriend->id;
                }

            } else {
                $serviceResponse = $service->makeSignedRequest('https://api.vk.com/method/friends.get.json', array(
                    'query' => array(
                        'uid' => $model->serv_id,
                    )
                ));
                $serviceFriendsIds = $serviceResponse->response;
            }
            $criteria = new CDbCriteria();
            $criteria->addCondition('service = :currentService');
            $criteria->params[':currentService'] = $model->service;
            $criteria->addInCondition('serv_id', $serviceFriendsIds);
            $friends = User::model()->findAll($criteria);
        }
		$this->render('view',array(
			'model'=> $model,
            'user_id' => $user_id,
            'friends' => $friends
		));
	}
	public function actionComplaints($id)
	{
        $model = $this->loadModel($id);
        $user_id = User::model()->getUserId();
		if ($model->id != $user_id){
			throw new CHttpException(403,'The requested page does not exist.');
		} 
		$complaints = Complaint::complaintsList($user_id);
		$this->render('view',array(
			'model'=> $model,
			'user_id' => $user_id,
			'complaints' => $complaints
		));
	}
	public function actionMycomplaints()
	{
        $user_id = User::model()->getUserId();
        $model = $this->loadModel($user_id);
		$complaints = Complaint::MyComplaintsList($user_id);
		$this->render('view',array(
			'model'=> $model,
			'user_id' => $user_id,
			'complaints' => $complaints
		));
	}
	public function actionAnswer($complaint)
	{
        $user_id = User::model()->getUserId();
		$db = Yii::app()->db;
        $command = $db->createCommand();
		$command->from('complaint');
		$command->where('complaint.id = '.(int)$complaint);
		$resalt = $command->queryRow();
        if($resalt['text_id'] != 'user'){
			$groupModel = Group::model();
			$groupsIds = $groupModel->getAllUsersGroupsIds($user_id);
			$meetsIds  = $groupModel->getMeetsIdsWhereUserIsAdmin($groupsIds);
			$ids = array_merge($groupsIds, $meetsIds);
		}
		if($resalt['user_id'] != $user_id){
			if($resalt['text_id'] == 'user' && $resalt['item_id'] != $user_id)
				throw new CHttpException(403,'The requested page does not exist.');
			if($resalt['text_id'] != 'user' && !in_array($resalt['item_id'], $ids))
				throw new CHttpException(403,'The requested page does not exist.');
		}
		$model = $this->loadModel($user_id);
		$complaint = Complaint::model()->getFormatedComplaintRecords($complaint);
		//fb($complaint, 'complaint');exit;
		$this->render('view',array(
			'model'=> $model,
			'user_id' => $user_id,
			'complaint' => $complaint
		));
	}
	public function actionCheckAnswer($complaint)
	{
		//$user_id = User::model()->getUserId();
		$db = Yii::app()->db;
        $command = $db->createCommand();
		$command->from('complaint');
		$command->where('complaint.id = '.(int)$complaint);
		$resalt = $command->queryRow();
		$user_id = $resalt['user_id'];
        //fb($user_id,$complaint);exit;
		$model = $this->loadModel($user_id);
		$complaint = Complaint::model()->getFormatedComplaintRecords($complaint);
		//fb($complaint, 'complaint');exit;
		$this->render('view',array(
			'model'=> $model,
			'user_id' => $user_id,
			'complaint' => $complaint
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		//$this->layout='//layouts/column2admin';		
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
            $attributes = array('name', 'email', 'city', 'city_id', 'region_id', 'country_id', 'pass', 'reg_date', 'birthday', 'sex','answer_comment_notice');

            $model->name  = $_POST['User']['name'];
            $model->email = $_POST['User']['email'];
            $model->city  = $_POST['User']['city'];
            $model->country_id  = $_POST['User']['country_id'];
            if($model->country_id)
				$model->region_id  = $_POST['User']['region_id'];
			$model->city_id = $_POST['User']['city_id'];
			if($model->city && $model->city_id && $model->region_id){
				$criteria = new CDbCriteria();
				$criteria->condition='name=:name AND region_id=:region_id';
				$criteria->params=array(':name'=>$model->city,':region_id'=>$model->region_id);
				$city_model = City::model()->find($criteria);
				if($city_model === null){
					$model->city  = '';
				}else{
					$model->city = $city_model->name;
				}
			}
			if($model->city && !$model->city_id && $model->region_id){
				$criteria = new CDbCriteria();
				$criteria->condition='name=:name AND region_id=:region_id';
				$criteria->params=array(':name'=>$model->city,':region_id'=>$model->region_id);
				
				$city_model = City::model()->find($criteria);
				if($city_model === null){
					$model->city  = '';	
				}else{
					$model->city_id = $city_model->id;
				}	
			}
            $model->sex   = $_POST['User']['sex'];
            $model->birthday = $_POST['User']['birthday']
                ? date('Y-m-d', strtotime($_POST['User']['birthday']))
                :  '';
            $model->reg_date = date('Y-m-d');
            $model->answer_comment_notice = $_POST['User']['answer_comment_notice'];
            //$model->pass  = $_POST['User']['pass'];

            if ($_FILES['User']['name']['avatar']) {

                //print_r($model->uploadAvatar($id, $_FILES['User']['name']['avatar'])); die;

			    $model->avatar =   $_POST['User']["avatar"] = $model->uploadAvatar($id, $_FILES['User']['name']['avatar']);
                $attributes[] = 'avatar';
            }
            if($model->save(true, $attributes)) {
                $this->redirect('admin');
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{	
        $model = $this->loadModel($id);
        $userId = $model->getUserId();//сразу перенаправляет и не дает возможности сохранить пароль
		$isAdmin = Admin::model()->isSiteAdmin($userId);

        if ($userId !== $id && !$isAdmin) {
            $this->redirect(array('view','id'=>$userId));
        };

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['User'])) {
          /*  $avatar = $_POST['User']['avatar'];

            if ($avatar != '') {
                $uploadedFile=CUploadedFile::getInstance($model,'avatar');
                $fileName = "$model->id";
                $model->avatar = $fileName;
            }*/
			if(empty($_POST['User']['serv_id']))
				$attributes  = array('name', 'email', 'city', 'city_id', 'region_id', 'country_id', 'birthday', 'sex', 'pass' ,'answer_comment_notice');
			else
				$attributes  = array('name', 'email', 'city', 'city_id', 'region_id', 'country_id', 'birthday', 'sex' ,'answer_comment_notice');

            $model->name  = $_POST['User']['name'];
            $model->email = $_POST['User']['email'];
            $model->city  = $_POST['User']['city'];
            $model->country_id  = $_POST['User']['country_id'];
            if($model->country_id)
				$model->region_id  = $_POST['User']['region_id'];
			$model->city_id = $_POST['User']['city_id'];
			if($model->city && $model->city_id && $model->region_id){
				$criteria = new CDbCriteria();
				$criteria->condition='name=:name AND region_id=:region_id';
				$criteria->params=array(':name'=>$model->city,':region_id'=>$model->region_id);
				$city_model = City::model()->find($criteria);
				if($city_model === null){
					$model->city  = '';
				}else{
					$model->city = $city_model->name;
				}
			}
			if($model->city && !$model->city_id){
				$criteria = new CDbCriteria();
				$criteria->condition='name=:name';
				$criteria->params=array(':name'=>$model->city);
				
				$city_model = City::model()->find($criteria);
				if($city_model === null){
					if($model->region_id && $model->country_id){
						$newCity = new City;
						$newCity->region_id = $model->region_id;
						$newCity->country_id = $model->country_id;
						$newCity->name =  $model->city;
						if($newCity->save());
							$model->city  = $newCity->name;
					}else{
						$model->city  = '';
					}
					
				}else{
					$model->city_id = $city_model->id;
				}
				
			}
            $model->sex   = $_POST['User']['sex'];
            $model->birthday = $_POST['User']['birthday']
                ? date('Y-m-d', strtotime($_POST['User']['birthday']))
                : '';
            $model->answer_comment_notice = $_POST['User']['answer_comment_notice'];
            //$model->pass  = $_POST['User']['pass'];

            if ($_FILES['User']['name']['avatar']) {

                //print_r($model->uploadAvatar($id, $_FILES['User']['name']['avatar'])); die;

			    $model->avatar =   $_POST['User']["avatar"] = $model->uploadAvatar($id, $_FILES['User']['name']['avatar']);
                $attributes[] = 'avatar';
            }

            if($model->save(true, $attributes)) {
				if(Yii::app()->user->hasFlash('error') && $model->email){
					Yii::app()->user->getFlash('error');
				}
                $this->redirect(array('view','id'=>$model->id));
            }

/*			if($model->save())
                if ($avatar != '') $uploadedFile->saveAs('images/user/'.$fileName.'.jpg');
				$this->redirect(array('view','id'=>$model->id));*/
		}


        /*if (isset($_POST['Hotels'])) {
            $model->attributes = $_POST['Hotels'];
            $model->pic2 = CUploadedFile::getInstance($model, 'pic2');
            if ($model->save()) {
                $mod = 0644;
                if (($model->pic2)) {
                    $dir = str_replace('framework', '', Yii::getFrameworkPath());
                    $file = $dir . '/images/hotels/';
                    $ti = md5(time() . $model->pic2) . substr($model->pic2, strlen($model->pic2) - 4, strlen($model->pic2));
                    $file.= $ti;
                    $model->pic2->saveAs($file);
                    @chmod($file, $mod);
                    $model->image = $ti;
                    $model->save();
                }
                $this->redirect(array('view', 'id' => $model->id));
            }
        }*/


		$this->render('update',array('model'=>$model,));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
            if(isset($_REQUEST['type']) && $_REQUEST['type'] == 'banned'){
                $model=new User();
                $model->search(true);
                $this->banned = true;
            } else {
                $model=new User();
                $model->search(false);
            }
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['User']))
                $model->attributes=$_GET['User'];
            $content = $this->renderPartial('admin',array(
                'model'=>$model,
            ),true);
            $this->render('application.views.admin.index',array('content'=>$content));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    public function actionGetVKUserID()
    {
        echo (int)User::model()->getVKUserID($_GET['user_id']);
    }

    public function actionBanUser($id){
        $model = $this->loadModel($id);
        $model->status = "0";
        $model->save();
    }

    public function actionUnBanUser($id){
        $model = $this->loadModel($id);
        $model->status = "1";
        $model->save();
    }

    public function actionRegistration(){
        if ($_GET['name']!='' && $_GET['email']!='' && $_GET['password']!='') {
        $row = User::model()->registration($_GET['name'],$_GET['email'],$_GET['password']);
            if ($row > 0) {
                $identity=new UserIdentity($_GET['name'],$_GET['password']);
                if($identity->authenticate()) {
                    Yii::app()->user->login($identity);
                }
                echo '<a class="button button-blue">
                    <span class="create-group" style="margin-top: 0px;">'.Yii::t('var', 'Создать группу').'</span>
                </a><input type="hidden" name="user_id" value="'.CHtml::encode($row).'">';
            }
        }

    }

    public function actionAuthorization()
    {
        if ($_GET['name']!='' && $_GET['password']!='') {
            $row = User::model()->authorization($_GET['name'],$_GET['password']);
            if ($row !== null) {
                $identity=new UserIdentity($_GET['name'],$_GET['password']);
                if($identity->authenticate())
                    Yii::app()->user->login($identity);
                echo '<a class="button button-blue">
                    <span class="create-group" style="margin-top: 0px;">'.Yii::t('var', 'Создать группу').'</span>
                </a><input type="hidden" name="user_id" value="'.CHtml::encode($row->id).'">';
            }
        }
    }
}
