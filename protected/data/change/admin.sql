-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.38-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица vstre4i.admin
DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vstre4i.admin: 24 rows
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`id`, `key`, `value`) VALUES
	(1, 'smsdostup_md5', '1e248c7fa52f0698a21a34d450f86097'),
	(3, 'group_price_usa', '3'),
	(4, 'webmoney', 'Z303342120238'),
	(5, 'webmoney_secret_key', '8C836742-C86E-4281-9D15-956E349336C8'),
	(7, 'qiwi_login', 'login'),
	(8, 'qiwi_password', 'password'),
	(9, 'paypal_login', 'bt.sumrak-facilitator_api1.gmail.com'),
	(10, 'paypal_password', '1372794658'),
	(11, 'mailer_host', 'smtp.gmail.com'),
	(12, 'mailer_username', '1sim2sim@gmail.com'),
	(13, 'mailer_password', 'j&w3D!bFfU`cbG'),
	(14, 'mailer_port', '465'),
	(15, 'site_name', 'Социальная сеть встреч, встречи в реале фото, виртуальные встречи по интересам'),
	(16, '', ''),
	(17, 'admin_email', 'admin@vstre4i.kz.com'),
	(18, 'paypal_signature', 'AC8Ef1CmZt1SdIDqcAVgfY0RsD79AsDCYgDUABqyYuw3e4.yewiFwbuG'),
	(19, 'paypal_return_url', 'payment/paypalConfirm'),
	(20, 'paypal_cancel_url', 'payment/paypalCancel/'),
	(21, 'paypal_api_live', '0'),
	(22, 'paypal_live', '0'),
	(23, 'group_premoderation_mode', '1'),
	(24, 'group_premoderation_limit', '11'),
	(25, 'keywords', 'соцыальная сеть, развллечение, отдых, хобби'),
	(26, 'description', 'описание соцыальной сети');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
