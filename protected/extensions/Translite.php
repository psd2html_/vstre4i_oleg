<?php
/**
 * Translite class file.
 *
 * @author Evgeniy Verbitskiy <everbslab@gmail.com>
 * @version 0.0.2
 * @package helpers
 *  
 *  Класс предназначен для облегчения перевода русских ссылок в транслит для дальнейшей их записи в базу 
 * 
 *  @example
 *  
 *  $sefurl = Translite::rusencode('моя новая ссылка 1');
 *  -- преобразует русский текст в транслит вида "moya_novaya_ssylka_1".
 *  
 *    
 */
class Translite
{
	public static $rustable =array(
		"Є"=>"YE","І"=>"I","Ѓ"=>"G","і"=>"i","№"=>"#","є"=>"ye","ѓ"=>"g",
		"А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
		"Е"=>"E","Ё"=>"YO","Ж"=>"ZH",
		"З"=>"Z","И"=>"I","Й"=>"J","К"=>"K","Л"=>"L",
		"М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R",
		"С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"X",
		"Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'",
		"Ы"=>"Y","Ь"=>"","Э"=>"E","Ю"=>"YU","Я"=>"YA",
		"а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
		"е"=>"e","ё"=>"yo","ж"=>"zh",
		"з"=>"z","и"=>"i","й"=>"j","к"=>"k","л"=>"l",
		"м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
		"с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"x",
		"ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"shh","ъ"=>"",
		"ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
		"—"=>"-","«"=>"","»"=>"","…"=>""
	);
    
    
    /* Функция перевода русского текста в транслит
     * 
     * @var string $str - исходная строка
     * @var string $spacechar - строка-разделитель пробелов
     * 
     */
    public static function rusencode($str = null, $spacechar = '_')
    {
    	if ($str)
    	{
    		$str = strtolower(strtr($str, self::$rustable));
            $str = preg_replace('~[^-a-z0-9_]+~u', $spacechar, $str);
            $str = trim($str, $spacechar);
            return $str;
    	} else {
    		return;
    	}
    }
}	 
?>
