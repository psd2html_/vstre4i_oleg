<?php
class CustomFacebookService extends FacebookOAuthService
{
	/**
	 * https://developers.facebook.com/docs/authentication/permissions/
	 */
	protected $scope = 'user_birthday,user_hometown,user_friends,email';

	/**
	 * http://developers.facebook.com/docs/reference/api/user/
	 * @see FacebookOAuthService::fetchAttributes()
	 */
	/* protected function fetchAttributes() {
		$this->attributes = (array) $this->makeSignedRequest('https://graph.facebook.com/me');
	} */
	protected function fetchAttributes() {
	//echo 555;exit;
		$info = (object) $this->makeSignedRequest('https://graph.facebook.com/me', array(
			'query' => array(
				//'uids' => $this->uid,
				//'fields' => '', // uid, first_name and last_name is always available
				'fields' => 'name,hometown,gender,birthday,email,locale,location',
				'scope' => 'user_birthday,user_hometown,user_friends,email,hometown'
			),
		));
        $name = $info->username;
        if(empty($info->username)) {

            $name = $info->name;
        }
		//var_dump($info);exit;
		$this->attributes['id'] = $info->id;
		$this->attributes['name'] =$name;
		$this->attributes['url'] = $info->link;
        $this->attributes['email'] = $info->email;
        $this->attributes['picture'] = "http://graph.facebook.com/{$info->id}/picture?type=large";
		if($info->gender == 'male')
			$this->attributes['gender'] = 1;
		elseif($info->gender == 'female')
			$this->attributes['gender'] = 2;
		else
			$this->attributes['gender'] = 0;
		if($info->locale){
			switch ($info->locale) {
				case 'ru_RU'  : $countryName = '������'; break;
				case 'uk_UA'  : $countryName = '�������'; break;
				case 'be_BY'  : $countryName = '��������'; break;
				default: $countryName = '���������';
			}
			$this->attributes['country'] = $countryName;
		}
	}
}
