<?php
/**
 * An example of extending the provider class.
 *
 * @author Maxim Zemskov <nodge@yandex.ru>
 * @link http://github.com/Nodge/yii-eauth/
 * @license http://www.opensource.org/licenses/bsd-license.php
 */

require_once dirname(dirname(__FILE__)).'/services/VKontakteOAuthService.php';

class CustomVKontakteService extends VKontakteOAuthService {

	protected $scope = 'email,friends';

	protected function fetchAttributes() {
		$info = (array)$this->makeSignedRequest('https://api.vk.com/method/users.get.json', array(
			'query' => array(
				'uids' => $this->uid,
				//'fields' => '', // uid, first_name and last_name is always available
				'fields' => 'nickname, sex, bdate, city, country, timezone, photo, photo_medium, photo_big, photo_rec',
				'scope' => 'email,friends'
			),
		));

		$info = $info['response'][0];
			
		
		$this->attributes['id'] = $info->uid;
		$this->attributes['name'] = $info->first_name.' '.$info->last_name;
		$this->attributes['url'] = 'http://vk.com/id'.$info->uid;

		if (!empty($info->nickname))
			$this->attributes['username'] = $info->nickname;
		else
			$this->attributes['username'] = 'id'.$info->uid;

		$this->attributes['gender'] = $info->sex  == 'M' || $info->sex == '�' ? 1 : 2;

		if($info->country){
			$countryName = '';
			switch ($info->country) {
				case '1'  : $countryName = '������'; break;
				case '2'  : $countryName = '�������'; break;
				case '3'  : $countryName = '��������'; break;
				default: $countryName = '���������';
			}
			$this->attributes['country'] = $countryName;
			if($info->city){
				//getCityById need token,
				//sig for getCities realy not required,
				$cityInfo = (array)$this->makeSignedRequest('https://api.vk.com/method/getCities', array(
					'query' => array(
						'cids' => $info->city
					),
				));
				$cityInfo = $cityInfo['response'][0];
				$this->attributes['city'] = $cityInfo->name;
				//print_r($cityInfo->name);exit;
			}
		}
		$this->attributes['timezone'] = timezone_name_from_abbr('', $info->timezone*3600, date('I'));;

		$this->attributes['birthday'] = $info->bdate;
		
		$this->attributes['photo'] = $info->photo;
		$this->attributes['photo_medium'] = $info->photo_medium;
		$this->attributes['photo_big'] = $info->photo_big;
		$this->attributes['photo_rec'] = $info->photo_rec;
        if(!empty($info->photo_big))
			$this->attributes['picture'] = $info->photo_big;
	}
}
