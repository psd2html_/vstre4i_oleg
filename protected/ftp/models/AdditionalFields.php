<?php

/**
 * This is the model class for table "additional_fields".
 *
 * The followings are the available columns in table 'additional_fields':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $type
 * @property integer $required
 * @property integer $group_id
 */
class AdditionalFields extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AdditionalFields the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'additional_fields';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, required, group_id', 'required'),
			array('required, group_id', 'numerical', 'integerOnly'=>true),
			array('name, description', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, description, required, group_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'required' => 'Required',
			'group_id' => 'Group',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('required',$this->required);
		$criteria->compare('group_id',$this->group_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function deleteAdditionalField($id){
        $connection = Yii::app()->db;
        $command = $connection->createCommand("DELETE FROM `additional_fields` WHERE `id`=$id");
        return $rowCount=$command->execute();
    }
    public function addAdditionalField($name,$description,$required,$group_id){
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `additional_fields` (`name`,`description`,`required`,`group_id`) VALUES ('".$name."','".$description."',$required,$group_id);");
        return $rowCount=$command->execute();
    }
    public function getGroupAdditionalFields($group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `id`,`name`,`required` FROM `additional_fields` WHERE `group_id`=$group_id");
        return $result=$command->query();
    }
    public function getUserAdditionalField($user_id,$group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `id`,`field_id`,`value` FROM `group_additional_fields` WHERE `group_id`=$group_id AND `user_id`=$user_id");
        return $result=$command->query();
    }
}