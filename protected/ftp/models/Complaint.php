<?php

/**
 * This is the model class for table "complaint".
 *
 * The followings are the available columns in table 'complaint':
 * @property integer $id
 * @property integer $item_id
 * @property string $date
 * @property string $text
 * @property string $text_id
 */
class Complaint extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Complaint the static model class
	 */

    public $name = false;
    public $type = false;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'complaint';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_id, date, text, text_id', 'required'),
			array('item_id', 'numerical', 'integerOnly'=>true),
			array('text', 'length', 'max'=>500),
			array('text_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, item_id, date, text, text_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'group' => array(self::BELONGS_TO, 'Group', '','on'=>'t.item_id=g.id', 'joinType'=>'INNER JOIN', 'alias'=>'g')
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item_id' => 'Item',
			'date' => 'Date',
			'text' => 'Text',
			'text_id' => 'Text',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        $criteria->with  = array('group');
		$criteria->compare('id',$this->id);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('text_id',$this->text_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function sentComplaint($mid,$text,$text_id,$user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `complaint` VALUES ('',$mid,'".date('Y-m-d')."','".$text."','".$text_id."',$user_id);");
        $rowCount=$command->execute();
    }
}