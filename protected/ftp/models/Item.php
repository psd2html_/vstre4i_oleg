<?php
class Item extends CActiveRecord
{
    public $image;
    // ... other attributes

    public function rules()
    {
        return array(
            array('avatar', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'),
        );
    }
}