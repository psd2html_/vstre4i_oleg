<?php

/**
 * This is the model class for table "keyword".
 *
 * The followings are the available columns in table 'keyword':
 * @property integer $id
 * @property integer $cat_id
 * @property string $name
 * @property integer $parent
 */
class Keyword extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Keyword the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'keyword';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cat_id, name', 'required'),
			array('cat_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cat_id, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cat_id' => 'Cat',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cat_id',$this->cat_id);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function getKeywordName($keyword)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `id` FROM `keyword` WHERE `name`='".$keyword."'");
        return $value = $command->queryScalar();
    }
    public function getUserInterests($user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `keyword`.`id` as `id`,`keyword`.`name` as `name` FROM `interest`,`keyword` WHERE `interest`.`keyword_id`=`keyword`.`id` AND `interest`.`user_id`=$user_id");
        $value = $command->query();
        return $value;
    }
    public function insertUserInterest($user_id,$keyword_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `interest` VALUES ('',$user_id,$keyword_id);");
        $rowCount=$command->execute();
    }
    public function removeUserInterest($user_id, $kid)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("DELETE FROM `interest` WHERE `user_id`=$user_id AND `keyword_id`=$kid");
        $value = $command->query();
        return $value;
    }
    public function getInterestsCategory()
    {
        $result = '';
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `id`,`name` FROM `category` ORDER BY `name`");
        $dataReader = $command->query();
        foreach ($dataReader as $val) {
            $result .= '<li class="keywords-list-cat">'.$val['name'].'</li>';
        }
        return $result;
    }
    public function getCategoryInterestsList($cat)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `keyword`.`id`,`keyword`.`name` FROM `category`,`keyword` WHERE `keyword`.`cat_id`=`category`.`id` AND `category`.`name`='$cat' ORDER BY `name`");
        return $value = $command->query();
    }
}