<?php

/**
 * This is the model class for table "photoalbum".
 *
 * The followings are the available columns in table 'photoalbum':
 * @property integer $id
 * @property string $name
 * @property integer $meet_id
 * @property string $description
 */
class Photoalbum extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Photoalbum the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'photoalbum';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, meet_id, description, group_id',  'required'),
			array('meet_id, group_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('description', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, meet_id, description, group_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'meet_id' => 'Meet',
			'description' => 'Description',
            'group_id' => 'Group',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('meet_id',$this->meet_id);
		$criteria->compare('description',$this->description,true);
        $criteria->compare('group_id',$this->group_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>'id DESC',
            )
		));
	}
    public function getGroupAlbums($group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `id`,`name` FROM `photoalbum` WHERE `group_id`=$group_id");
        return $value=$command->query();
    }
    public function getAlbumPhotos($album_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `album_id`,`link` FROM `photo` WHERE `album_id`=$album_id");
        return $value=$command->query();
    }
    public function getGroupPhotos($group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `photo`.`album_id` as `album_id`,`photo`.`link` as `link` FROM `photo`,`photoalbum` WHERE `photo`.`album_id`= `photoalbum`.`id` AND `photoalbum`.`group_id`=$group_id");
        return $value=$command->query();
    }
    public function getRandomPhoto($group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `photo`.`link` as `link`,`photo`.`album_id` as `album_id` FROM `photo`,`photoalbum` WHERE `photoalbum`.`id`=`photo`.`album_id` AND `photoalbum`.`group_id`=$group_id ORDER BY rand()");
        $value = $command->queryRow();
        return $value;
    }
    public function imagesUpload($file,$album_id,$user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `photo` VALUES ('',$album_id,$user_id,'".Date('Y-m-d H:i:s')."',$file);");
        $rowCount=$command->execute();
    }
}