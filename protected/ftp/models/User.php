<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property integer $serv_id
 * @property string $service
 * @property string $name
 * @property string $reg_date
 * @property string $email
 * @property string $pass
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, reg_date', 'required'),
			array('serv_id, sex', 'numerical', 'integerOnly'=>true),
			array('service, city, birthday', 'length', 'max'=>50),
			array('name, email', 'length', 'max'=>100),
			array('pass', 'length', 'max'=>32),
            array('about', 'length', 'max'=>500),
            array('avatar', 'length', 'max'=>200, 'on'=>'insert,update'),
            array('avatar', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, serv_id, service, name, reg_date, email, pass, city, birthday, sex, avatar, about', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'serv_id' => 'Serv',
			'service' => 'Service',
			'name' => 'Имя',
			'reg_date' => 'Reg Date',
			'email' => 'Email',
			'pass' => 'Пароль',
            'city' => 'Город',
            'birthday' => 'День рождения',
            'sex' => 'Пол',
            'avatar' => 'Аватара',
            'about' => 'Обо мне',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($banned)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('serv_id',$this->serv_id);
		$criteria->compare('service',$this->service,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('reg_date',$this->reg_date,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('pass',$this->pass,true);
        $criteria->compare('city',$this->city,true);
        $criteria->compare('birthday',$this->birthday,true);
        $criteria->compare('sex',$this->sex,true);
        $criteria->compare('avatar',$this->avatar,true);
        $criteria->compare('about',$this->about,true);
        if($banned){
            $criteria->addCondition('status="0"');
        }
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>'id DESC',
            )

        ));
	}
    public function serviceRegistrationUser($id,$serviceName,$name) {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `user` VALUES ('','".$id."','".$serviceName."','".$name."','".Date('Y-m-d H:i:s')."','','','','','','','');");
        $rowCount=$command->execute();
    }
    public function registrationUser($id,$serviceName,$name) {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `user` VALUES ('','".$id."','".$serviceName."','".$name."','".Date('Y-m-d H:i:s')."','','','','','','','');");
        $rowCount=$command->execute();
    }
    public function getPicture($id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `avatar` FROM `user` WHERE `id`=".$id);
        return $value=$command->queryScalar();
    }
    public function getId($id,$service)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `id` FROM `user` WHERE `serv_id`=".$id." AND `service`='".$service."'");
        $value = $command->queryScalar();
        if ($value) return $value; else return 0;
    }
    public function getName($id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `name` FROM `user` WHERE `id`=".$id);
        $value = $command->queryScalar();
        return $value;
    }
    public function getAbout($id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `about` FROM `user` WHERE `id`=".$id);
        $value = $command->queryScalar();
        return $value;
    }
    public function getUserId()
    {
        if (!Yii::app()->user->isGuest) {
            if (isset(Yii::app()->user->service)) {
                $user_id = Yii::app()->user->id;
                $service = Yii::app()->user->service;
            } else {
                $user_id = Yii::app()->user->id;
                $service = '';
            }
        } else {
            $user_id = 0;
            $service = '';
        }
        return User::model()->getId($user_id,$service);
    }
    public function getUserServiceID($user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `serv_id` FROM `user` WHERE `id`=".$user_id);
        $value = $command->queryScalar();
        if ($value) return $value; else return 0;
    }
    public function getUserService($user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `service` FROM `user` WHERE `id`=".$user_id);
        $value = $command->queryScalar();
        if ($value) return $value; else return 0;
    }
    public function getVKUserID($user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `id` FROM `user` WHERE `serv_id` IN (".implode(',',$user_id).") AND `service`='vkontakte'");
        $value = $command->queryScalar();
        return $value;
    }
    public function getUserEmail($user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `email` FROM `user` WHERE `id`=$user_id");
        $value = $command->queryScalar();
        return $value;
    }
}