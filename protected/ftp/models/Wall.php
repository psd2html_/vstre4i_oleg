<?php

/**
 * This is the model class for table "wall".
 *
 * The followings are the available columns in table 'wall':
 * @property integer $id
 * @property integer $user_id
 * @property string $text
 * @property string $date
 * @property integer $sender_id
 */
class Wall extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wall the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'wall';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, text, date, sender_id', 'required'),
			array('user_id, sender_id', 'numerical', 'integerOnly'=>true),
			array('text', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, text, date, sender_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'text' => 'Text',
			'date' => 'Date',
			'sender_id' => 'Sender',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('sender_id',$this->sender_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function getWallRecords($user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `wall`.`id` as `id`,`wall`.`text` as `text`,`wall`.`date` as `date`,`user`.`name` as `name` FROM `wall`,`user` WHERE `wall`.`user_id`=`user`.`id` AND `wall`.`user_id`=".$user_id);
        $value = $command->query();
        return $value;
    }
    public function sentRecord($sender_id,$text,$user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `wall` VALUES ('',".$user_id.",'".$text."','".Date('Y-m-d')."',".$sender_id.");");
        $rowCount=$command->execute();
    }
}