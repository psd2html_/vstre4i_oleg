<?php
/* @var $this AdditionalFieldsController */
/* @var $model AdditionalFields */

$this->breadcrumbs=array(
	'Additional Fields'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AdditionalFields', 'url'=>array('index')),
	array('label'=>'Create AdditionalFields', 'url'=>array('create')),
	array('label'=>'View AdditionalFields', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AdditionalFields', 'url'=>array('admin')),
);
?>

<h1>Update AdditionalFields <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>