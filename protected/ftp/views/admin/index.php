<?php
/* @var $this AdminController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Admins',
);

?>

<div class="center admin">
    <nav class="side-left-menu" action="<?php CController::createUrl('admin/saveSettings')?>">
        <ol>
            <?php $c_id  = Yii::app()->controller->id;?>
            <?php $a_id = Yii::app()->controller->action->id; ?>
            <li <?php if( $c_id == 'admin' && $a_id == 'index'){echo 'class="active"';}?> ><a href="<?php echo CController::createUrl('admin/index',array()) ?>">Общие настройки</a></li>
            <li <?php if( $c_id == 'admin' && $a_id == 'payment'){echo 'class="active"';}?> ><a href="<?php echo CController::createUrl('admin/payment',array()) ?>">Платежные системы</a></li>
            <li <?php if($c_id == 'country' || $c_id == 'city'){echo 'class="active"';}?> ><a href="<?php echo CController::createUrl('country/admin')?>">Города и страны</a></li>
            <li <?php if($c_id == 'photo' || $c_id == 'photoalbum'){echo 'class="active"';}?> ><a href="<?php echo CController::createUrl('photo/admin')?>">Фотографии</a></li>
            <li <?php if($c_id == 'user'){echo 'class="active"';}?> ><a href="<?php echo CController::createUrl('user/admin')?>">Пользователи</a></li>
            <li <?php if($c_id == 'page'){echo 'class="active"';}?> ><a href="<?php echo CController::createUrl('page/admin')?>">Страницы</a></li>
            <li <?php if( $c_id == 'admin' && $a_id == 'smtp'){echo 'class="active"';}?> ><a href="<?php echo CController::createUrl('admin/smtp',array()) ?>">Настройки Smtp</a></li>
            <li <?php if( $c_id == 'Complaint'){echo 'class="active"';}?> ><a href="<?php echo CController::createUrl('complaint/admin',array()) ?>">Жалобы</a></li>



        </ol>
    </nav>
        <?php echo $content; ?>
