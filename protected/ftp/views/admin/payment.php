<ul class="tab-container">
    <li <?php if($name == 'paypal'){echo 'class="active"'; }?>><a href="<?php echo CController::createUrl('admin/payment',array('name'=>'paypal'))?>" title="Paypal">Paypal</a></li>
    <li <?php if($name == 'webmoney'){echo 'class="active"'; }?>><a href="<?php echo CController::createUrl('admin/payment',array('name'=>'webmoney'))?>" title="Webmoney">Webmoney</a></li>
    <li <?php if($name == 'qiwi'){echo 'class="active"'; }?>><a href="<?php echo CController::createUrl('admin/payment',array('name'=>'qiwi'))?>" title="Qiwi">Qiwi</a></li>
    <li <?php if($name == 'sms'){echo 'class="active"'; }?>><a href="<?php echo CController::createUrl('admin/payment',array('name'=>'sms'))?>" title="SMS">SMS</a></li>

</ul>
<?php echo CHtml::beginForm('','POST',array('id'=>'payment-settings-form'))?>
    <?php echo $content; ?>
    <?php echo CHtml::submitButton('Сохранить'); ?>
<?php echo CHtml::endForm()?>