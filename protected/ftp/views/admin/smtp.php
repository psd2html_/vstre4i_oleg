<ul class="tab-container">
    <li class="active"><a href="<?php echo CController::createUrl('admin/smtp')?>" title="Smtp">Smtp</a></li>

</ul>
<?php echo CHtml::beginForm('','POST',array('id'=>'payment-settings-form'))?>
<div class="content-block">
    <?php ?>
    <?php echo CHtml::label('Хост','mailer_host')?>
    <?php echo CHtml::textField('smtp[mailer_host]',$data['mailer_host']['value'],array('id'=>'mailer_host'))?>
    <br/>
    <?php echo CHtml::label('Имя пользователя','mailer_username')?>
    <?php echo CHtml::textField('smtp[mailer_username]',$data['mailer_username']['value'],array('id'=>'mailer_username'))?>
    <br/>
    <?php echo CHtml::label('Пароль','mailer_password')?>
    <?php echo CHtml::textField('smtp[mailer_password]',$data['mailer_password']['value'],array('id'=>'mailer_password'))?>
    <br/>
    <?php echo CHtml::label('Порт','mailer_port')?>
    <?php echo CHtml::textField('smtp[mailer_port]',$data['mailer_port']['value'],array('id'=>'mailer_port'))?>

</div>
<?php echo CHtml::submitButton('Сохранить'); ?>
<?php echo CHtml::endForm()?>