
<ul class="tab-container">
    <li class="active"><a href="<?php echo CController::createUrl('complaint/admin')?>" title="Жалобы">Жалобы</a></li>
</ul>
<div class="content-block">
<?php
/* @var $this ComplaintController */
/* @var $model Complaint */

$this->breadcrumbs=array(
	'Complaints'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Complaint', 'url'=>array('index')),
	array('label'=>'Create Complaint', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#complaint-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'complaint-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
          'name'=>'item_id',
           'value'=>'
           $data->group->type == "group"?CHtml::link($data->item_id,Yii::app()->createUrl("group/$data->item_id")):
                 CHtml::link($data->item_id,Yii::app()->createUrl("group/meet/4/?mid=$data->item_id"));',
           'type'=>'html'
        ),
        array(
            'name'=>'name',
            'value' => '$data->group->name'
        ),
		'date',
		'text',
		array(
            'name'=>'type',
            'value' => '$data->group->type'
                ),
        'user_id',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>