<?php
/* @var $this GroupController */
/* @var $model Group */
/* @var $form CActiveForm */

Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerCssFile(Yii::app()->request->baseUrl . '/css/colorpicker.css') //css
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/eye.js') //js
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/utils.js') //js
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/layout.js') //js
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/colorpicker.js'); //js

?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'group-form',
        'enableAjaxValidation'=>false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>150)); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'description'); ?>
        <?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>1000)); ?>
        <?php echo $form->error($model,'description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'date_start'); ?>
        <?php echo $form->textField($model,'date_start',array('size'=>60,'maxlength'=>25)); ?>
        <?php echo $form->error($model,'date_start'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'date_end'); ?>
        <?php echo $form->textField($model,'date_end',array('size'=>60,'maxlength'=>25)); ?>
        <?php echo $form->error($model,'date_end'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'address'); ?>
        <?php echo $form->textArea($model,'address',array('rows'=>6, 'cols'=>50)); ?>
        <?php echo $form->error($model,'address'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'seats'); ?>
        <?php echo $form->textField($model,'seats'); ?>
        <?php echo $form->error($model,'seats'); ?>
    </div>

    <script>
        $(document).ready(function(){
            $('#Group_background').ColorPicker({
                onSubmit: function(hsb, hex, rgb, el) {
                    $(el).val(hex);
                    $(el).ColorPickerHide();
                },
                onBeforeShow: function () {
                    $(this).ColorPickerSetColor(this.value);
                }
            })
                .bind('keyup', function(){
                    $(this).ColorPickerSetColor(this.value);
                });
        });
    </script>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->