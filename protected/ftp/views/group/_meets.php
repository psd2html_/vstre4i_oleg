<?
if (isset(Yii::app()->user->service)) {
    $user_id = User::model()->getId(Yii::app()->user->id,Yii::app()->user->service);
} else {
    $user_id = Yii::app()->user->id;
}
if (!Yii::app()->user->id) {
    $user_id = 0;
}
?>
<div class="content-block" style="padding-bottom: 0;">
    <h3 class="align-center"><a href="<? echo 'http://'.$_SERVER['SERVER_NAME'].Yii::app()->createUrl('site/index').'/group/meet/'.Group::model()->getMeetGroupID($data->id).'/?mid='.$data->id; ?>"><? echo CHtml::encode($data->name); ?></a></h3>
    <div class="event-date f-left"><? echo date('d.m.Y',strtotime(CHtml::encode($data->date_start))) ?> <span class="time"><? echo date('H:i',strtotime(CHtml::encode($data->date_start))) ?></span></div>
    <div class="clear"></div>
    <div class="avatars">
        <?
        $members = Member::model()->getGroupMembers($data->id);
        foreach($members as $val) {
            $avatar = $val['avatar'];
            if ($avatar && $avatar != '') $avatar = $avatar; else $avatar = 'no-pic.png';
            echo '<a href="'.Yii::app()->createUrl('site/index').'/user/'.$val['id'].'"><img class="f-left" src="/images/user/'.$avatar.'" /></a>';
        }
        ?>
    </div>
    <div class="clear"></div>
    <p><? echo CHtml::encode($data->description); ?></p>
    <? if ($user_id!=0) { ?>
    <a mid="<? echo CHtml::encode($data->id); ?>" onclick="$('#complaint').arcticmodal();" title="пожаловаться на событие" class="show-all-participants complain-event align-center f-right">пожаловаться на событие</a>
    <? } ?>
</div>