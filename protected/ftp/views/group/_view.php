<div class="event border-radius-5 odd">
    <div class="event-img f-left"><img style="height: 68px;width: 93px;" src="<?php $pic = CHtml::encode($data->picture); if ($pic != NULL && $pic != '') echo CHtml::encode($data->picture); else echo '/images/group/no-pic.png'; ?>" alt="event"></div>
    <div class="event-name f-left"><a href="<? echo Yii::app()->createUrl('site/index').'/group/'.CHtml::encode($data->id); ?>"><? echo CHtml::encode($data->name); ?></a></div>
    <div class="event-qty f-left">Участников: <? $model=new Member; echo ' '.$model->getGroupMembersCount($data->id) ?></div>
    <div class="event-date f-left">
        <?
        $meet = Group::model()->getNextMeet($data->id);
        if ($meet != 0) {
            echo 'ближайшая встреча '.date('d.m.Y',strtotime($meet));
        } else {
            echo 'ближайших встреч нет';
        }
        ?>
    </div>
</div>