<?php
/* @var $this GroupController */
/* @var $model Group */

$this->breadcrumbs=array(
    'Groups'=>array('index'),
    'Create',
);

?>

<div class="center">
    <div class="white-block">
        <h1 class="sharp align-center"><? echo Yii::t('var', 'Создание встречи');?></h1>
        <form id="create-meet-form" method="post" onsubmit="return false;">
            <lable for="name">Название</lable><input type="text" name="meet[name]" value=""><br>
            <lable for="description">Описание</lable><input type="text" name="meet[description]" value=""><br>
            <lable for="date_start">Дата начала</lable><input type="text" name="meet[date_start]" value=""><br>
            <lable for="date_end">Дата окончания</lable><input type="text" name="meet[date_end]" value=""><br>
            <lable for="city">Город</lable><input type="text" name="meet[city]" value=""><br>
            <lable for="address">Адресс</lable><input type="text" name="meet[address]" value=""><br>
            <lable for="seats">Мест</lable><input type="text" name="meet[seats]" value=""><br>
            <button onclick="createMeet()">Создать</button>
        </form>
        <script>
            function createMeet(){
                $.ajax({
                    type: 'POST',
                    url: '<? echo Yii::app()->createUrl('group/creatingmeet');?>',
                    data: $('#create-meet-form').serialize()+'&parent=<? echo $model->id; ?>&YII_CSRF_TOKEN=<? echo Yii::app()->request->csrfToken;?>',
                    success: function(data){
                        window.location.href="<? echo Yii::app()->createUrl('site/index').'/group/meet/'.$model->id.'/?mid='; ?>"+data;
                    }
                });
            }
        </script>
    </div>
</div>