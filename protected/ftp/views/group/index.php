<?php
/* @var $this GroupController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Groups',
);

$this->menu=array(
	array('label'=>'Create Group', 'url'=>array('create')),
	array('label'=>'Manage Group', 'url'=>array('admin')),
);
?>

<h1>Groups</h1>

    <select onchange="changeGroupListView(this)">
        <option value="" selected="selected">
            Выберите вид
        </option>
        <option value="table">
            Таблица
        </option>
        <option value="list">
            Список
        </option>
    </select>
    <select onchange="changeGroupListView(this)">
        <option value="" selected="selected">
            Сортировка
        </option>
        <option value="create-up">
            Дата создания (возр)
        </option>
        <option value="create-down">
            Дата создания (убыв)
        </option>
        <option value="meet">
            Ближайшие встречи
        </option>
        <option value="member-count">
            Кол-во участников
        </option>
        <option value="meet-count">
            Кол-во встреч
        </option>
    </select>

<form id="search-form" onsubmit="return false;" style="width:100%;">
    <input id="keyword" name="keyword" type="text">
    <input id="city" name="city" type="text">
    <button id="search" onclick="getGroupsClick()">Искать</button>
    <div id="categories" style="width:100%;">
        <?
        $criteria2 = new CDbCriteria;
        $dataProvider2=new CActiveDataProvider('Category',array('criteria'=>$criteria2));
        $this->widget('zii.widgets.CListView', array(
            'dataProvider'=>$dataProvider2,
            'itemView'=>'_categories',
            'template'=>'{items}',
            'itemsCssClass'=>'clearfix port-det port-thumb'
        ));
        ?>
    </div>
</form>

<script>
    function changeGroupListView(sel){
        window.location.href='<? echo Yii::app()->createUrl('group/index'); ?>/type/'+sel.options[sel.selectedIndex].value;
    }
</script>
<script type="text/javascript">

    function getGroups(){
        $.ajax({
            type: 'GET',
            url: '<? echo Yii::app()->createUrl('ajax/groupList'); ?>',
            data: {city : YMaps.location.city},
            success: function(data){
                $('.items').html(data);
            }
        });
    }

    function getGroupsClick(){
        $.ajax({
            type: 'GET',
            url: '<? echo Yii::app()->createUrl('ajax/groupListClick'); ?>',
            data: $('#search-form').serialize(),
            success: function(data){
                $('.items').html(data);
            }
        });
    }

    $(document).ready(function(){

        var map = new YMaps.Map(), center;
        if (YMaps.location) {
            $('#maps').html('Ваш город: '+YMaps.location.city);
            $('city').val(YMaps.location.city);
        }

        getGroups();

        $.widget( "custom.catcomplete", $.ui.autocomplete, {
            _renderMenu: function( ul, items ) {
                var that = this,
                    currentCategory = "";
                $.each( items, function( index, item ) {
                    if ( item.category != currentCategory ) {
                        ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                        currentCategory = item.category;
                    }
                    that._renderItemData( ul, item );
                });
            }
        });

        var data = [
            <? echo GroupKeyword::model()->getAllKeywords();?>
        ];

        $( "#keyword" ).catcomplete({
            delay: 0,
            source: data
        });

        var data_city = [
            <? echo City::model()->getAllGroupCities();?>
        ];

        $( "#city" ).catcomplete({
            delay: 0,
            source: data_city
        });

        $(document).on('click','.keyword-table input.keyword',function(){
            $.ajax({
                type: 'GET',
                url: '<? echo Yii::app()->createUrl('ajax/getFilteredGroups'); ?>',
                data: $('#search-form,#keywords-form').serialize(),
                success: function(data){
                    $('.items .items').html(data);
                }
            });
        });

    });

</script>
<style>
    .ui-autocomplete-category {
        font-weight: bold;
        padding: .2em .4em;
        margin: .8em 0 .2em;
        line-height: 1.5;
    }
    .keyword-table {
        float: left;
        margin-right: 15px !important;
        width: 195px;
    }
</style>
<?
if (isset($_GET['type'])) $type = $_GET['type']; else $type = 'table';
if ($type=='table') $itemView = '_view'; else $itemView = '_view_list';


$criteria2 = new CDbCriteria;
$criteria2->condition = "type = 'group'";
$dataProvider2=new CActiveDataProvider('Group',array('criteria'=>$criteria2));
$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider2,
    'itemView'=>$itemView,
    'sortableAttributes'=>array('name','member'),
    'template'=>'{items}'
));
?>