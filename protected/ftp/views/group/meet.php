<?php
/* @var $this GroupController */
/* @var $model Group */

$meet=Group::model()->findByPk($_GET['mid']);

Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerCssFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css') //css
    ->registerScriptFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/jquery-ui.js');

if ($model->background && $model->background!='')
    echo "<style>body { background: none repeat scroll 0 0 #".$model->background." !important; }</style>";

$this->breadcrumbs=array(
    'Groups'=>array('index'),
    $model->name,
);

$this->menu=array(
    array('label'=>'List Group', 'url'=>array('index')),
    array('label'=>'Create Group', 'url'=>array('create')),
    array('label'=>'Update Group', 'url'=>array('update', 'id'=>$model->id)),
    array('label'=>'Delete Group', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage Group', 'url'=>array('admin')),
);

if (isset(Yii::app()->user->service)) {
    $user_id = User::model()->getId(Yii::app()->user->id,Yii::app()->user->service);
} else {
    $user_id = Yii::app()->user->id;
}
if (!Yii::app()->user->id) {
    $user_id = 0;
}

$isgroupadmin = Group::model()->isGroupAdmin($model->id,$user_id);
$ismember = Member::model()->isMember($user_id,$model->id);
?>


<div class="center event-page">
    <aside id="#SideLeft" class="f-left" style="width: 187px; margin: 0 7px 10px 0;">
        <div class="sidebar-left">
            <div class="group-img">
                <?
                if ($model->picture != '' || $model->picture != NULL) $picture = $model->picture; else $picture = '/images/group/no-pic.png';
                ?>
                <img src="<? echo $picture; ?>" style="height: 189px;width: 186px;">
            </div>
            <? if ($isgroupadmin == 1) { ?>
                <a onclick="window.location.href='<? echo Yii::app()->createUrl('site/index').'/group/update/'.$model->id ?>'" title="настройки" class="complain align-center">Настройки</a>
            <? }else { ?>
            <? if ($user_id!=0) { ?>
                <a mid="<? echo $model->id ?>" onclick="$('#complaint').arcticmodal();" title="пожаловаться на группу" class="complain align-center">пожаловаться на группу</a>
                <? } ?>
            <? } ?>
        </div>
        <div class="sidebar-left">
            <div class="group-info">
                <ul>
                    <li><? echo City::model()->getCity($model->city_id).', '.Country::model()->getCountryByCityID($model->city_id);?></li>
                    <li>Группа создана: <? echo date('d.m.Y',strtotime($model->date_created));?></li>
                    <li>Участники: <? echo Member::model()->getGroupMembersCount($model->id);?></li>
                    <li>Будущие события: <? echo Group::model()->getFutureMeetsCount($model->id);?></li>
                    <li>Прошедшие события: <? echo Group::model()->getPastMeetsCount($model->id);?></li>
                    <li><span style="float:left;display:block;">Календарь группы: </span><span class="calend-show"></span></li>
                    <script>
                        $(document).ready(function(){
                            $('.calend-show').on('click',function(){
                                $('.calend').show();
                            })
                        });
                    </script>
                </ul>
                <div class="key-words">
                    <ul>
                        <? $dataReader = GroupKeyword::model()->getGroupKeywords($model->id);
                        $i = 0;
                        foreach($dataReader as $row) {
                            echo '<li><a href="#" title="#">'.$row['name'].'</a></li>';
                            $i++;
                        } ?>
                    </ul>
                </div>
                <div class="organizers">Организаторы:
                    <ul>
                        <?
                        $dataReader = Member::model()->getModeratorsList($model->id);
                        foreach($dataReader as $row) {
                            echo '<li><a href="'.Yii::app()->createUrl('site/index').'/user/'.$row['user_id'].'">'.$row['name'].'</a></li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <select id="country-select">
            <option value="choose">Свяжитесь с нами</option>
            <option value="all">(057) 025 25 25</option>
            <option value="program">skype: vstre4i</option>
            <option value="education">e-mail: vstre4i@mail.ru</option>
        </select>
        <div class="sidebar-right" style="margin-top: 10px;">
            <h2 class="sidebar-right-title align-center sharp sharp-white">Случайные группы</h2>
            <?
            $dataReader = Group::model()->getRandomGroup($model->id);
            if (count($dataReader)>0)
                foreach($dataReader as $row) {
                    echo '<a href="'.Yii::app()->createUrl('site/index').'/group/'.$row['id'].'"><img style="width:187px;height:189px;" src="'.(($row['picture']!=''&&$row['picture']!=NULL)?('/images/group/no-pic.pg'):('/images/group/no-pic.png')).'">'.$row['name'].'</a>';
                }
            else echo 'Похожей группы не найдено';
            ?>
            <div class="sidebar-right-content"></div>
        </div>
    </aside>

    <section id="group-content" class="f-left">
        <div class="content-block-wrap">
            <h2 class="sharp align-center sharp-white"><?php echo $meet->name; ?></h2>
            <div class="content-block">
                <div class="data sharp"><? echo date('d.m.Y',strtotime($meet->date_start)); ?></div >
                <div class="place sharp"><? echo City::model()->getCity($meet->city_id).', '.$meet->address; ?></div >
                <div class="time sharp"><? echo date('H:m',strtotime($meet->date_start)); ?></div >
            </div>
        </div>
        <div class="content-block-wrap">
            <div class="content-block">
                <?php echo $meet->description; ?>
            </div>
        </div>

        <div class="content-block-wrap calend">
            <h2 class="sharp align-center"><? echo Yii::t('var', 'Календарь');?></h2>
            <div class="content-block">
                <?

                Yii::import('application.extensions.calendar.classes.calendar', true);

                $month = isset($_GET['m']) ? $_GET['m'] : NULL;
                $year  = isset($_GET['y']) ? $_GET['y'] : NULL;

                $calendar = Calendar::factory($month, $year);

                $events = array();

                $dataReader = Group::model()->getGroupMeetsID($model->id);
                foreach ($dataReader as $val) {
                    $events[] = $calendar->event()
                        ->condition('timestamp', strtotime($val['date_start']))
                        ->title('Hello All')
                        ->output('<a href="'.Yii::app()->createUrl('site/index').'/group/meet/'.$model->id.'/?mid='.$val['id'].'">'.date('H:i',strtotime($val['date_start'])).'<br>'.$val['name'].'</a>');
                }

                $calendar->standard('today')->standard('prev-next');

                foreach ($events as $val) {
                    $calendar->attach($val);
                }
                ?>
                <table class="calendar">
                    <thead>
                    <tr class="navigation">
                        <th class="prev-month"><a href="<?php echo htmlspecialchars($calendar->prev_month_url()) ?>"><?php echo $calendar->prev_month() ?></a></th>
                        <th colspan="5" class="current-month"><?php echo $calendar->month() ?> <?php echo $calendar->year ?></th>
                        <th class="next-month"><a href="<?php echo htmlspecialchars($calendar->next_month_url()) ?>"><?php echo $calendar->next_month() ?></a></th>
                    </tr>
                    <tr class="weekdays">
                        <?php foreach ($calendar->days() as $day): ?>
                            <th><?php echo $day ?></th>
                        <?php endforeach ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($calendar->weeks() as $week): ?>
                        <tr>
                            <?php foreach ($week as $day): ?>
                                <?php
                                list($number, $current, $data) = $day;

                                $classes = array();
                                $output  = '';

                                if (is_array($data))
                                {
                                    $classes = $data['classes'];
                                    $title   = $data['title'];
                                    $output  = empty($data['output']) ? '' : '<ul class="output"><li>'.implode('</li><li>', $data['output']).'</li></ul>';
                                }
                                ?>
                                <td class="day <?php echo implode(' ', $classes) ?>">
                                    <span class="date" title="<?php echo implode(' / ', $title) ?>"><?php echo $number ?></span>
                                    <div class="day-content">
                                        <?php echo $output ?>
                                    </div>
                                </td>
                            <?php endforeach ?>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
                <style>
                    .calendar {width:100%; border-collapse:collapse;}
                    .calendar tr.navigation th {padding-bottom:20px;}
                    .calendar th.prev-month {text-align:left;}
                    .calendar th.current-month {text-align:center; font-size:1.5em;}
                    .calendar th.next-month {text-align:right;}
                    .calendar tr.weekdays th {text-align:left;}
                    .calendar td {width:14%; height:100px; vertical-align:top; border:1px solid #CCC;}
                    .calendar td.today {background:#FFD;}
                    .calendar td.prev-next {background:#EEE;}
                    .calendar td.prev-next span.date {color:#9C9C9C;}
                    .calendar td.holiday {background:#DDFFDE;}
                    .calendar span.date {display:block; padding:4px; line-height:12px; background:#EEE;}
                    .calendar div.day-content {}
                    .calendar ul.output {margin:0; padding:0 4px; list-style:none;}
                    .calendar ul.output li {margin:0; padding:5px 0; line-height:1em; border-bottom:1px solid #CCC;}
                    .calendar ul.output li:last-child {border:0;}

                        /* Small Calendar */
                    .calendar.small {width:auto; border-collapse:separate;}
                    .calendar.small tr.navigation th {padding-bottom:5px;}
                    .calendar.small tr.navigation th a {font-size:1.5em;}
                    .calendar.small th.current-month {font-size:1em;}
                    .calendar.small tr.weekdays th {text-align:center;}
                    .calendar.small td {width:auto; height:auto; padding:4px 8px; text-align:center; border:0; background:#EEE;}
                    .calendar.small span.date {display:inline; padding:0; background:none;}
                </style>
            </div>
        </div>
        <? if ($user_id!=0) { ?>
        <div class="content-block-wrap">
            <div class="content-block">
                <label for="comment" class="f-left sharp">Комментарий:</label>
                <div class="clear"></div>
                <?
                $dataReader = Group::model()->getMeetComments($meet->id);

                foreach($dataReader as $row) {
                    echo "<div>".$row['name'].': '.$row['text']."</div>";
                }
                ?>
                <form id="comments-form" onsubmit="return false;">
                    <textarea id="comment" class="f-left" name="text"></textarea>
                    <input onclick="sentComment(<? echo $meet->id.','.$user_id; ?>)" type="image" src="/images/send-comment.png" name="send-comment" class="f-right"/>
                </form>
            </div>
        </div>
        <? } ?>
    </section>

    <aside id="#SideRight" class="f-right" style="width: 200px; margin: 0 2px 10px 7px;">

        <?
        if ($user_id!=0 && $ismember) {
        ?>
        <div class="sidebar-right">
            <h2 class="sidebar-right-title align-center sharp sharp-white" style="margin-bottom: 0;">Вы участвуете?</h2>
            <div class="sidebar-right-content sidebar-right-content-participants">
                <p class="sharp">Всего мест: <? echo $meet->seats;?></p>
                <p class="sharp">Осталось мест: <? $free_seats = $meet->seats-Member::model()->getRequestsCount($meet->id); echo $free_seats; ?></p>
                <? if ($free_seats>0 && Member::model()->isMeetMember($meet->id,$user_id)==NULL) { ?>
                    <input type="button" name="yes" class="yes" onclick="joinMeet(<? echo $user_id.', '.$meet->id;?>)" />
                <? } ?>
                <? if (Member::model()->isMeetMember($meet->id,$user_id)!=NULL) { ?>
                    <input type="button" name="no" class="no" onclick="leaveMeet(<? echo $user_id.', '.$meet->id;?>)" />
                <? } ?>
            </div>
        </div>
        <?
        }
        ?>

        <div class="sidebar-right">
            <h2 class="sidebar-right-title align-center sharp sharp-white" style="margin-bottom: 0;">Участники</h2>
            <div class="sidebar-right-content sidebar-right-content-participants">
                <div class="group-participants">
                    <?
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'group_id = '.$meet->id.' AND confirm=1';

                    $dataProvider=new CActiveDataProvider('Member',array('criteria'=>$criteria));
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider'=>$dataProvider,
                        'itemView'=>'member_list',
                        'template'=>'{items}'
                    ));
                    ?>
                </div>
            </div>

            <a href="<? echo Yii::app()->createUrl('site/index').'/group/members/'.$model->id.'/?mid='.$meet->id; ?>" title="посмотреть всех участников группы" class="show-all-participants align-center">просмотреть всех</a>
        </div>
        <div class="sidebar-right" style="margin-top: 10px;">
            <h2 class="sidebar-right-title align-center sharp sharp-white">Поделиться встречей</h2>
            <script type="text/javascript" src="//yandex.st/share/share.js"
                    charset="utf-8"></script>
            <div class="yashare-auto-init" data-yashareL10n="ru"
                 data-yashareType="none" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir"></div>
        </div>
    </aside>
</div>





<script>
    function sentComment(meet_id,user_id) {
        $.ajax({
            type: 'POST',
            url: '<? echo Yii::app()->createUrl('group/sentComment');?>',
            data: $('#comments-form').serialize() + "&user_id=" + user_id + "&meet_id=" + meet_id + "&YII_CSRF_TOKEN=<? echo Yii::app()->request->csrfToken;?>",
            success: function(data){
                window.location.reload();
            }
        });
    }
</script>

<script>
    function joinMeet(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<? echo Yii::app()->createUrl('group/joinMeet');?>',
            data: { id: user_id, group_id: group_id,YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>' },
            success: function(data){
                window.location.reload();
            }
        });
    }
    function leaveMeet(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<? echo Yii::app()->createUrl('group/leaveMeet');?>',
            data: { id: user_id, group_id: group_id,YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>' },
            success: function(data){
                window.location.reload();
            }
        });
    }
</script>
<script>
    function addMeetRequest(){
        $.ajax({
            type: 'POST',
            url: '<? echo Yii::app()->createUrl('member/addMeetRequest');?>',
            data: { id: id, group_id: <? echo $model->id; ?>,YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>' },
            success: function(data){
                window.location.reload();
            }
        });
    }
    function removeMeetRequest(){
        $.ajax({
            type: 'POST',
            url: '<? echo Yii::app()->createUrl('member/removeMeetRequest');?>',
            data: { id: id, group_id: <? echo $model->id; ?>,YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>' },
            success: function(data){
                window.location.reload();
            }
        });
    }
</script>






<div style="display: none;">
    <div class="box-modal" id="exampleModal">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="addit-fields-form">
            <table>
                <?
                $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                foreach($dataReader as $row) {
                    echo "<tr><td>".$row['name']."</td><td><input type='text' value='' name='fields[".$row['id']."]'></td></tr>";
                }
                ?>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
        </form>
        <script src="http://jquery.bassistance.de/validate/jquery.validate.js"></script>
        <script>
            $().ready(function() {

                $("#addit-fields-form").validate({
                    rules: {
                        <?
                            $i = 0;
                            $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                            foreach($dataReader as $row) {
                                if ($row['required']==1) {
                                    echo (($i>0)?',':'').$row['id'].': "required"';
                                    $i++;
                                }
                             }
                        ?>
                    },
                    messages: {
                        <?
                                $i = 0;
                                $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                                foreach($dataReader as $row) {
                                    if ($row['required']==1) {
                                        echo (($i>0)?',':'').$row['id'].': "Обязательное поле"';
                                        $i++;
                                    }
                                 }
                            ?>
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('member/sendRequest'); ?>',
                            data: $('#addit-fields-form').serialize()+'&group_id=<? echo $model->id; ?>&user_id=<? echo User::model()->getUserId(); ?>',
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="complaint">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="complaint-form">
            <table>
                <tr><td>Введите текст жалобы</td><td><input type="text" name="text" value=""></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
        </form>
        <script src="http://jquery.bassistance.de/validate/jquery.validate.js"></script>
        <script>
            $().ready(function() {

                $("#complaint-form").validate({
                    rules: {
                        text: "required"
                    },
                    messages: {
                        text: "Обязательное поле"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('complaint/sentComplaint'); ?>',
                            data: $('#complaint-form').serialize()+'&text_id=group&item_id=<? echo $model->id; ?>&user_id=<? echo User::model()->getUserId(); ?>',
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="mailto">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="mailto-form">
            <table>
                <tr><td>Введите имя друга</td><td><input type="text" name="name" value=""></td></tr>
                <tr><td>Введите email друга</td><td><input type="text" name="email" value=""></td></tr>
                <tr><td>Введите комментарий</td><td><textarea type="text" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
            <input type="hidden" name="group_id" value="<? echo $model->id; ?>">
        </form>
        <script src="http://jquery.bassistance.de/validate/jquery.validate.js"></script>
        <script>
            $().ready(function() {

                $("#mailto-form").validate({
                    rules: {
                        name: "required",
                        email: "required",
                        comment: "required"
                    },
                    messages: {
                        name: "Обязательное поле",
                        email: "Обязательное поле",
                        comment: "Обязательное поле"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('ajax/mailto'); ?>',
                            data: $('#mailto-form').serialize(),
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<style>
    .box-modal {
        width:540px;
        margin: 0 auto;
        background-color: #ffffff;
        padding: 20px;
    }
    #sidebar {
        display: none;
    }
    .span-5 {
        width:0px
    }
    .span-19 {
        width: 950px;
    }
</style>