<?php
/* @var $this GroupController */
/* @var $data Group */

if (isset(Yii::app()->user->service)) {
    $user_id = User::model()->getId(Yii::app()->user->id,Yii::app()->user->service);
} else {
    $user_id = Yii::app()->user->id;
}
if (!Yii::app()->user->id) {
    $user_id = 0;
}

?>

<h1 class="page-title sharp align-center">Все участники группы</h1>
<div class="center vse-uchastniki-admin<? if (Group::model()->isGroupAdmin($model->id,$user_id)) echo ' vse-uchastniki-admin'; else echo ' vse-uchastniki'; ?>">
    <div style="width:100%;float:left">
        <form action="" method="post" style="width: 384px;">
            <input name="search-field-orange" type="text" class="search-field-orange f-left" />
            <input type="image" src="/images/search-icon-orange.png" name="search-btn" class="search-btn-magnifier ">
        </form>
    </div>
    <?
    if (isset($_GET['mid'])) $dataReader = Member::model()->getGroupMembers($_GET['mid']);
    else $dataReader = Member::model()->getGroupMembers($model->id);
    if (!Group::model()->isGroupAdmin($model->id,$user_id)) {
        echo '<div style="width:100%;float: left">';
        foreach($dataReader as $row) {
            $avatar = $row['avatar'];
            if ($avatar && $avatar != '') ; else $avatar = 'no-pic.png';
            ?>
            <a class="participants f-left white-block align-center" title="#" href="<? echo Yii::app()->createUrl('site/index').'/user/'.$row['id']; ?>">
                <img src="/images/user/<? echo $avatar; ?>" alt="event">
                <p class="participants-name f-left"><? echo $row['name']; ?><br /></p>
            </a>
            <?
        }
        echo '</div>';
    } else {
        echo '<div style="width:100%;float: left">';
        foreach($dataReader as $row) {
            $avatar = $row['avatar'];
            if ($avatar && $avatar != '') ; else $avatar = 'no-pic.png';
            ?>
            <div class="participants white-block">
                <div class="participants-img f-left align-center"><a href="<? echo Yii::app()->createUrl('site/index').'/user/'.$row['id']; ?>"><img style="width:150px;height:150px;" src="/images/user/<? echo $avatar; ?>" alt="event"></a></div>
                <div class="participants-name sharp f-left align-center"><? echo $row['name']; ?><br /><span>г. Харьков</span></div>
                <div class="participants-action f-left align-center">
                    <ul>
                        <li>
                            <a class="sharp-blue" title="#" onclick="$('#mailto').arcticmodal();">Написать сообщение</a>
                        </li>
                        <?
                        if (Member::model()->isModerator($row['id'],$model->id)>0) {
                            ?>
                            <li>
                                <a class="sharp-blue" title="#" onclick="disrankModerator(<? echo $row['id'].','.$model->id; ?>)">Разжаловать</a>
                            </li>
                        <?
                        } else { ?>
                            <li>
                                <a class="sharp-blue" title="#" onclick="appointModerator(<? echo $row['id'].','.$model->id; ?>)">Назначить модератором</a>
                            </li>
                        <? }
                        ?>
                        <li>
                            <a class="sharp-blue" title="#" onclick="kickMember(<? echo $row['id'].','.$model->id; ?>)">Удалить из группы</a>
                        </li>
                    </ul>
                </div>
            </div>
            <?
        }
        echo '</div>';
    }
    ?>
</div>


<script>
    function appointModerator(user_id,group_id){
        $.ajax({
            type: 'GET',
            url: '<? echo Yii::app()->createUrl('member/appointModerator');?>',
            data: { user_id: user_id, group_id: group_id,YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>' },
            success: function(data){
                window.location.reload();
            }
        });
    }
    function disrankModerator(user_id,group_id){
        $.ajax({
            type: 'GET',
            url: '<? echo Yii::app()->createUrl('member/disrankModerator');?>',
            data: { user_id: user_id, group_id: group_id,YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>' },
            success: function(data){
                window.location.reload();
            }
        });
    }
    function kickMember(user_id,group_id){
        $.ajax({
            type: 'GET',
            url: '<? echo Yii::app()->createUrl('member/kickMember');?>',
            data: { user_id: user_id, group_id: group_id,YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>' },
            success: function(data){
                window.location.reload();
            }
        });
    }
</script>
<div style="display: none;">
    <div class="box-modal" id="mailto">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="mailto-form">
            <table>
                <tr><td>Введите имя друга</td><td><input type="text" name="name" value=""></td></tr>
                <tr><td>Введите email друга</td><td><input type="text" name="email" value=""></td></tr>
                <tr><td>Введите комментарий</td><td><textarea type="text" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
            <input type="hidden" name="group_id" value="<? echo $model->id; ?>">
        </form>
        <script src="http://jquery.bassistance.de/validate/jquery.validate.js"></script>
        <script>
            $().ready(function() {

                $("#mailto-form").validate({
                    rules: {
                        name: "required",
                        email: "required",
                        comment: "required"
                    },
                    messages: {
                        name: "Обязательное поле",
                        email: "Обязательное поле",
                        comment: "Обязательное поле"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('ajax/mailto'); ?>',
                            data: $('#mailto-form').serialize(),
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>