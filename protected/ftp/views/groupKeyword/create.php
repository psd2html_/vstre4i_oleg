<?php
/* @var $this GroupKeywordController */
/* @var $model GroupKeyword */

$this->breadcrumbs=array(
	'Group Keywords'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List GroupKeyword', 'url'=>array('index')),
	array('label'=>'Manage GroupKeyword', 'url'=>array('admin')),
);
?>

<h1>Create GroupKeyword</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>