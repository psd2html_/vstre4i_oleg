<?php /* @var $this Controller */

if (isset(Yii::app()->user->service)) {
    $user_id = User::model()->getId(Yii::app()->user->id,Yii::app()->user->service);
} else {
    $user_id = Yii::app()->user->id;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/select2.css" type="text/css" media="screen, projection" />
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" />
    <![endif]-->
    <script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>

    <script src="//yandex.st/jquery/1.9.1/jquery.min.js"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <?
    Yii::app()->clientScript
        ->registerCoreScript('jquery')
        ->registerCssFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css')
        ->registerCssFile(Yii::app()->request->baseUrl . '/css/jquery.arcticmodal-0.3.css')
        ->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.arcticmodal-0.3.min.js');
    ?>

    <script src="http://api-maps.yandex.ru/1.1/index.xml?key=AD_SnFEBAAAANqE5NAIA58bRRSnaURTZs_EPQ6kyeIPJTt0AAAAAAAAAAAD3QAIiHNeBNRVtarQF6GoD1TRnOg==~ALPWnFEBAAAAUreqOgMAyQDSCLoNb-JOIEvHvfOZpQVYWKoAAAAAAAAAAAB6cfLrZVKMXgnTN0lbuk7-GOMdkA==" type="text/javascript"></script>

    <script type="text/javascript" src="/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

    <link rel="stylesheet" href="/fancybox/source/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/source/jquery.fancybox.pack.js?v=2.1.4"></script>

    <link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>

    <link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/select2.js"></script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" type="text/css" media="screen" />
    <script src="http://jquery.bassistance.de/validate/jquery.validate.js"></script>


    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.eventCalendar.js"></script>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/eventCalendar.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/eventCalendar_theme_responsive.css">

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/functions.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/payment.js"></script>



</head>

<body>


<div id="wrapper">
    <header id="header">
        <div class="top-header-container box-shadow">
            <div class="center">
                <div class="log-in f-right">
                    <ul>
                        <?
                        if (!Yii::app()->user->id) {
                        ?>
                        <li class="f-left"><a class="white" href="<? echo Yii::app()->createUrl('site/registration');?>"><? echo Yii::t('var', 'Регистрация');?></a>|</li>
                        <li class="f-left"><a class="white" href="<? echo Yii::app()->createUrl('site/login'); ?>"><? echo Yii::t('var', 'Вход');?></a></li>
                        <?
                        } else {
                        ?>
                        <li class="f-left"><a class="white" href="<? echo Yii::app()->createUrl('user/view').'/'.$user_id;?>"><? echo Yii::app()->user->name; ?></a>|</li>
                            <li class="f-left"><a class="white" href="<? echo Yii::app()->createUrl('site/logout'); ?>"><? echo Yii::t('var', 'Выход');?></a></li>
                        <?
                        }
                        ?>
                    </ul>
                </div>
                <?php
                    $this->widget('application.components.widgets.LanguageSelector');
                ?>
            </div>
        </div>
        <div class="center">
            <a href="/"><div class="logo f-left"></div></a>
            <form id="top-search" action="<? echo Yii::app()->createUrl('site/index')?>" method="post" class="search-form-header f-left">
                <input name="search" type="text" class="search f-left input-border" />
                <input name="YII_CSRF_TOKEN" type="hidden" value="<? echo Yii::app()->request->csrfToken;?>"/>
                <button type="button" name="search-btn" class="search-btn input-border" onclick="$('#top-search').submit();"><span class="white"><? echo Yii::t('var', 'Поиск');?></span></button>
            </form>
            <button type="button" name="create-group" class="create-group f-right input-border" onclick="window.location.href='<? echo Yii::app()->createUrl('site/index').'/site/page/view/create-group';?>'"><span class="white"><? echo Yii::t('var', 'Создать группу');?></span></button>
        </div>
    </header><!-- #header-->
    <div id="content">
        <?php echo $content; ?>
    </div><!-- #content-->
</div><!-- #wrapper -->
<div class="email-form" style="display: none"></div>
<footer id="footer">
    <div class="center">
        <nav class="footer-links f-left">
            <ul>
                <li><a class="white" href="<? echo Yii::app()->createUrl('site/index').'/about/'; ?>"><? echo Yii::t('var', 'О нас');?></a></li>
                <li><a class="white" href="<? echo Yii::app()->createUrl('site/index').'/contact/'; ?>"><? echo Yii::t('var', 'Контакты');?></a></li>
                <li><a class="white" href="<? echo Yii::app()->createUrl('site/index').'/faq/'; ?>"><? echo Yii::t('var', 'Помощь');?></a></li>
            </ul>
        </nav>

        <div class="copyright f-right">
            <p class="white">Copyright © 2013 "Vstre4i"<br />
                <? echo Yii::t('var', 'Все права защищены');?>.</p>
         <!--  <p class="white"><? echo Yii::t('var', 'Сайт разработан "KlymStyle"');?></p>-->
        </div>
    </div>
</footer><!-- #footer -->
</body>
</html>