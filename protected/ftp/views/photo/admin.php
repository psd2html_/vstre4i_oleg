
<ul class="tab-container">
    <li class="active"><a href="<?php echo CController::createUrl('photo/admin')?>" title="Фотографии">Фотографии</a></li>
    <li ><a href="<?php echo CController::createUrl('photoalbum/admin')?>" title="Альбомы">Альбомы</a></li>
</ul>
<div class="content-block">

    <?php
/* @var $this PhotoController */
/* @var $model Photo */

$this->breadcrumbs=array(
	'Photos'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#photo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php $assetsDir = '/images/album/'?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'photo-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'album_id',
		'user_id',
		'date',
		'link',
        array('name'=>'image',
              'value'=>'(!empty($data->link))?CHtml::image(Yii::app()->assetManager->publish(YiiBase::getPathOfAlias("webroot.images.album")."/".$data->album_id."/".$data->link),"",array("style"=>"width:75px;height:75px;")):"no image"',
              'type'=>'html'),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>
