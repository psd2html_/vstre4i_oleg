<ul class="tab-container">
    <li ><a href="<?php echo CController::createUrl('photo/admin')?>" title="Фотографии">Фотографии</a></li>
    <li class="active"><a href="<?php echo CController::createUrl('album/admin')?>" title="Альбомы">Альбомы</a></li>
</ul>
<div class="content-block">
<?php
/* @var $this PhotoalbumController */
/* @var $model Photoalbum */


$this->menu=array(
	array('label'=>'Create Photoalbum', 'url'=>array('create')),
);

$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#photoalbum-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'photoalbum-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'meet_id',
		'description',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>