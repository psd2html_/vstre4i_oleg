<?php
/* @var $this PhotoalbumController */
/* @var $model Photoalbum */

$this->breadcrumbs=array(
	'Photoalbums'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Photoalbum', 'url'=>array('index')),
	array('label'=>'Create Photoalbum', 'url'=>array('create')),
	array('label'=>'View Photoalbum', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Photoalbum', 'url'=>array('admin')),
);
?>

<h1>Update Photoalbum <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>