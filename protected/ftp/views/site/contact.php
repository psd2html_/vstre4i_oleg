<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);
?>



<div class="center">
    <div class="white-block">
        <h1 class="sharp align-center"><? echo Yii::t('var', 'Контакты');?></h1>
        <?php if(Yii::app()->user->hasFlash('contact')): ?>

            <div class="flash-success">
                <?php echo Yii::app()->user->getFlash('contact'); ?>
            </div>

        <?php else: ?>

            <h3 style="font-size: 20px;margin-bottom: 20px;"><? echo Yii::t('var', 'Напишите нам');?></h3>
            <p class="note"><? echo Yii::t('var', 'Поля со');?> <span class="red">*</span> <? echo Yii::t('var', 'обязательные для заполнения.');?></p>

            <div class="form">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'contact-form',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                    ),
                )); ?>

                <table class="contact-table">
                    <tr>
                        <td>
                            <? echo Yii::t('var', 'Имя');?> <span class="red">*</span><br>
                        </td>
                        <td>
                            <?php echo $form->textField($model,'name',array('class'=>'input-border')); ?>
                            <?php echo $form->error($model,'name'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <? echo Yii::t('var', 'Email');?> <span class="red">*</span><br>
                        </td>
                        <td>
                            <?php echo $form->textField($model,'email',array('class'=>'input-border')); ?>
                            <?php echo $form->error($model,'email'); ?>
                        </td>
                    </tr>
                    <tr style="height:130px;">
                        <td style="vertical-align:top;">
                            <? echo Yii::t('var', 'Сообщение');?> <span class="red">*</span><br>
                        </td>
                        <td>
                            <?php echo $form->textArea($model,'body',array('class'=>'input-border', 'rows'=>6, 'cols'=>50)); ?>
                            <?php echo $form->error($model,'body'); ?>
                        </td>
                    </tr>
                    <?php if(CCaptcha::checkRequirements()): ?>
                    <tr style="height:90px;">
                        <td style="vertical-align:bottom;">
                            <? echo Yii::t('var', 'Защитный код');?> <span class="red">*</span><br>
                        </td>
                        <td>
                            <?php $this->widget('CCaptcha'); ?><br>
                            <?php echo $form->textField($model,'verifyCode',array('class'=>'input-border')); ?>
                            <?php echo $form->error($model,'verifyCode'); ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <tr style="height:90px;">
                        <td></td>
                        <td style="vertical-align: middle">
                            <?php echo CHtml::submitButton(Yii::t('var', 'Отправить'),array('class'=>'align-center white registration create input-border','style'=>'height:32px')); ?>
                        </td>
                    </tr>
                </table>

                <input id="ContactForm_subject" type="hidden" name="ContactForm[subject]" maxlength="128" size="60" value="Обратная связь">

                <?php $this->endWidget(); ?>

            </div><!-- form -->

        <?php endif; ?>
    </div>
</div>
