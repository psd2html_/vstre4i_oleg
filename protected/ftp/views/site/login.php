<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>



<div class="center">
    <div class="white-block">
        <h1 class="sharp align-center"><? echo Yii::t('var', 'Авторизация');?></h1>
        <p><? echo Yii::t('var', 'Пожалуйста, заполните следующие поля');?></p>
        <div class="form">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'login-form',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
            )); ?>

            <p class="note" style="font-size: 14px;"><? echo Yii::t('var', 'Поля со');?> <span class="red">*</span> <? echo Yii::t('var', 'обязательные для заполнения.');?></p>

            <table class="registration-table">
                <tr>
                    <td>
                        <? echo Yii::t('var', 'Имя');?> <span class="red">*</span><br>
                    </td>
                    <td>
                        <?php echo $form->textField($model,'username',array('class'=>'input-border')); ?><br>
                        <?php echo $form->error($model,'username'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <? echo Yii::t('var', 'Пароль');?> <span class="red">*</span><br>
                    </td>
                    <td>
                        <?php echo $form->passwordField($model,'password',array('class'=>'input-border')); ?><br>
                        <?php echo $form->error($model,'password'); ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <?php echo $form->checkBox($model,'rememberMe'); ?>
                        <? echo Yii::t('var', 'Запомнить меня');?>
                        <?php echo $form->error($model,'rememberMe'); ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <?php echo CHtml::submitButton(Yii::t('var', 'Войти'),array('class'=>'align-center white registration create input-border','style'=>'height:32px')); ?>
                    </td>
                </tr>
            </table>

            <h2 style="margin:10px 0px;"><? echo Yii::t('var', 'Вы можете авторизироваться через соц.сети:');?></h2>
            <?php Yii::app()->eauth->renderWidget(); ?>

            <?php $this->endWidget(); ?>
        </div><!-- form -->
    </div>
</div>
