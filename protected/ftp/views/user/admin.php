<ul class="tab-container">
    <li <?php if(!$this->banned) echo 'class="active"'?>><a href="<?php echo CController::createUrl('user/admin')?>" title="Все">Все</a></li>
    <li <?php if($this->banned) echo 'class="active"'?>><a href="<?php echo CController::createUrl('user/admin',array('type'=>'banned'))?>" title="Забаненные">Забаненные</a></li>
</ul>
<div class="content-block">
    <?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create User', 'url'=>array('create')),
);


    $this->widget('zii.widgets.CMenu', array(
        'items'=>$this->menu,
        'htmlOptions'=>array('class'=>'operations'),
    ));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search($this->banned),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'serv_id',
		'service',
		'name',
		'reg_date',
		'email',
        'city',
        'sex',
        'birthday',
        'about',

        array(
            'name'=>'status',
            'value'=>'$data->status?"":"banned"'
        ),
		/*
		'pass',
		*/
		array(
			'class'=>'CButtonColumn',
            'template'=>'{update}{delete}{ban}{unban}{mail}',
            'buttons'=>array
            (
                'ban' => array
                (
                    'label'=>'Ban',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/ban.png',
                    'visible' => '$data->status=="1"',
                    'click'=>"function(){
                                    $.fn.yiiGridView.update('user-grid', {
                                        type:'POST',
                                        url:$(this).attr('href'),
                                        success:function(data) {
                                              $.fn.yiiGridView.update('user-grid');
                                        }
                                    })
                                    return false;
                              }
                     ",
                    'url'=>'Yii::app()->controller->createUrl("banUser",array("id"=>$data->primaryKey))',
                ),
                'unban' => array
                (
                    'label'=>'UnBan',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/unban.png',
                    'visible' => '$data->status=="0"',
                    'click'=>"function(){
                                    $.fn.yiiGridView.update('user-grid', {
                                        type:'POST',
                                        url:$(this).attr('href'),
                                        success:function(data) {
                                              $.fn.yiiGridView.update('user-grid');
                                        }
                                    })
                                    return false;
                              }
                     ",
                    'url'=>'Yii::app()->controller->createUrl("unBanUser",array("id"=>$data->primaryKey))',
                ),
                'mail' => array
                (
                    'label'=>'mail',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/mail.png',
                    'click'=>"function(){
                                    var id = $(this).parent().parent().find(':first-child').html();
                                    var url = '".Yii::app()->getBaseUrl(true)."/mail/getEmailUserForm?id='+id;
                                    $('.email-form').load(url,{},function(cont){
                                    try{
                                        CKEDITOR.replace( 'Mail_text');
                                        } catch (e){

                                        }
                                        $(this).show();
                                    });
                                    return false;
                              }
                     ",
                    'url'=>'Yii::app()->controller->createUrl("mail/getEmailUserForm",array("id"=>$data->id))',
                ),
            )
		),
	),
)); ?>
</div>
