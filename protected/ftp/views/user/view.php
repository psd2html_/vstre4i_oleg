<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name,
);

$avatar = $model->avatar;
if ($model->sex == 1) $sex = 'Муж.';
if ($model->sex == 2) $sex = 'Жен.';

if (isset(Yii::app()->user->service)) {
    $user_id = User::model()->getId(Yii::app()->user->id,Yii::app()->user->service);
} else {
    $user_id = Yii::app()->user->id;
}
if (!Yii::app()->user->id) {
    $user_id = 0;
}

$email = User::model()->getUserEmail($model->id);
?>

<div class="center">
    <aside id="#SideLeft" class="f-left" style="width: 237px; margin: 0 45px 10px 0;">
        <div class="avatar white-block">
            <img src="/images/user/<? if ($avatar && $avatar != '') echo $avatar.'.jpg'; else echo 'no-pic.png';?>" alt="учасник">
        </div>


        <!-- Только если не личная -->
        <? if ($user_id != $model->id) { ?>

            <?
            if ($email != '') {
            ?>
            <input onclick="$('#mailto').arcticmodal();" type="image" src="/images/send-message.png" name="send-message" class="send-msg"/>
            <?
            }
            ?>

            <div class="sidebar-right">
                <h2 class="sidebar-right-title align-center sharp" style="margin-bottom: 0;">Участвует в:</h2>
                <div class="sidebar-right-content sidebar-left-content-participants">
                    <div class="group-participants">
                        <?
                        $dataReader = Group::model()->getUserGroupList($model->id);
                        foreach ($dataReader as $value) {
                            if ($value['picture'] != '' || $value['picture'] != NULL) $picture = $value['picture']; else $picture = '/images/group/no-pic.png';
                            echo '<a href="'.Yii::app()->createUrl('site/index').'/group/'.$value['id'].'" title="'.$value['id'].'">
                                <img src="'.$picture.'" alt="participants" style="width: 60px; height: 50px;"/>
                            </a>';
                        }
                        ?>
                    </div>

                    <a href="#" title="посмотреть всех участников группы" class="show-all-participants align-center">просмотреть всех</a>
                </div>
            </div>

            <div class="sidebar-right">
                <h2 class="sidebar-right-title align-center sharp" style="margin-bottom: 0;">Создатель групп:</h2>
                <div class="sidebar-right-content sidebar-left-content-participants">
                    <div class="group-participants">
                        <?
                        $dataReader = Group::model()->getUserAdminGroupList($model->id);
                        foreach ($dataReader as $value) {
                            if ($value['picture'] != '' || $value['picture'] != NULL) $picture = $value['picture']; else $picture = '/images/group/no-pic.png';
                            echo '<a href="'.Yii::app()->createUrl('site/index').'/group/'.$value['id'].'" title="'.$value['id'].'">
                                <img src="'.$picture.'" alt="participants" style="width: 60px; height: 50px;"/>
                            </a>';
                        }
                        ?>
                    </div>
                    <a href="#" title="посмотреть всех участников группы" class="show-all-participants align-center">просмотреть все</a>
                </div>
            </div>
        <? } else { ?>
            <a class="complain align-center" title="пожаловаться на группу" onclick="window.location.href='<? echo Yii::app()->createUrl('site/index').'/user/update/'.$user_id ?>'">Настройки</a>
            <div id="eventCalendarDefault"></div>
            <script>
                $(document).ready(function() {
                    $("#eventCalendarDefault").eventCalendar({
                        eventsjson: '<? echo Yii::app()->createUrl('group/getusermeetsjson',array('user_id'=>$user_id)); ?>' // link to events json
                    });
                });
            </script>
        <? } ?>

    </aside>

    <section id="group-content" class="f-left">

        <? if ($user_id == $model->id) { ?>
            <div class="content-block-wrap">
                <div class="content-block">
                    <div class="slider align-center">
                        <img src="/images/slider-emulator.jpg" alt="slider" />
                        <a class="prev" href="#"></a>
                        <a class="next" href="#"></a>
                    </div>
                </div>
            </div>

            <div class="content-block-wrap">
                <div class="content-block content-block-interest">
                    <h3 class="sharp">Мои интересы:</h3>
                    <?
                    $dataReader = Keyword::model()->getUserInterests($model->id);
                    foreach ($dataReader as $value) {
                        echo '<div class="interest align-center sharp f-left" kid="'.$value['id'].'">
                            '.$value['name'].'
                            <input type="image" src="/images/inerest-icon.png" class="remove-interest" name="remove-interest" />
                        </div>';
                    }
                    ?>
                    <div class="clear"></div>
                    <button onclick="$('#interest').arcticmodal();" type="button" name="add-interest" class="add-interest"><span>Добавить интерес</span></button>
                </div>
            </div>
        <? } else { ?>
            <!-- если не личная -->
            <div class="content-block-wrap">
                <div class="content-block">
                    <h3 class="sharp align-center"><?php echo $model->name; ?></h3>
                    <?
                    if ($model->city) echo '<p class="sharp">Город: '.City::model()->getCity($model->city).'</p>';
                    if ($model->birthday) echo '<p class="sharp">Дата рождения: '.date('d-m-Y',strtotime($model->birthday)).'</p>';
                    if ($model->about) echo ' <p class="sharp">О себе: <p style="font-size: 12px;">'.$model->about.'</p></p>';
                    ?>
                    <p class="sharp">Интересы:
                    <p style="font-size: 12px;">
                        <?
                        $i = 0;
                        $dataReader = Keyword::model()->getUserInterests($model->id);
                        foreach ($dataReader as $value) {
                            if ($i == 0) {
                                echo $value['name'];
                            } else {
                                echo ', '.$value['name'];
                            }
                            $i++;
                        }
                        ?>
                    </p>
                    </p>
                </div>
            </div>

            <div class="content-block-wrap">
                <h2 class="sharp" style="padding-left: 30px;">Стена</h2>
                <div class="content-block">
                    <?
                    $dataReader = Wall::model()->getWallRecords($model->id);

                    foreach($dataReader as $row) {
                        echo "<div>".$row['name'].': '.$row['text']."</div>";
                    }
                    ?>
                    <? if ($user_id!=0) { ?>
                    <form id="wall-form" onsubmit="return false;">
                        <textarea id="comment" class="f-left" name="text"></textarea><br>
                        <input class="f-right" type="image" src="/images/send-comment.png" onclick="sentRecord(<? echo $model->id.','.User::model()->getUserId(); ?>)">
                    </form>
                    <? } ?>
                </div>
            </div>
        <? } ?>
    </section>

</div>



<style>
    .box-modal {
        width:500px;
        margin: 0 auto;
        background-color: #ffffff;
        padding: 20px;
    }
</style>
<script>
    function sentRecord(user_id,sender_id) {
        $.ajax({
            type: 'POST',
            url: '<? echo Yii::app()->createUrl('wall/sentRecord');?>',
            data: $('#wall-form').serialize() + "&user_id=" + user_id + "&sender_id=" + sender_id + "&YII_CSRF_TOKEN=<? echo Yii::app()->request->csrfToken;?>",
            success: function(data){
                window.location.reload();
            }
        });
    }

    $(document).ready(function(){
        $('.remove-interest').on('click',function(){
            $.ajax({
                type: 'GET',
                url: '<? echo Yii::app()->createUrl('keyword/RemoveUserInterest');?>',
                data: {user_id: <? echo $model->id; ?>, kid: $(this).parent().attr('kid')},
                success: function(data){
                    window.location.reload();
                }
            });
        });
    });

</script>
<div style="display: none;">
    <div class="box-modal" id="mailto">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="mailto-form">
            <table>
                <tr><td>Введите Ваше сообщение:</td><td><textarea name="message"></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
            <input type="hidden" name="group_id" value="<? echo $model->id; ?>">
            <input type="hidden" name="user_id" value="<? echo $user_id; ?>">
            <input type="hidden" name="email" value="<? echo $email; ?>">
        </form>
        <script src="http://jquery.bassistance.de/validate/jquery.validate.js"></script>
        <script>
            $().ready(function() {

                $("#mailto-form").validate({
                    rules: {
                        message: "required"
                    },
                    messages: {
                        message: "Обязательное поле"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('ajax/mailto'); ?>',
                            data: $('#mailto-form').serialize(),
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="interest">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="interest-form">
            <span class="all-cats">Все категории</span>
            <ul class="keywords-list">
                <?
                echo Keyword::model()->getInterestsCategory();
                ?>
            </ul>
            <script>
                $(document).ready(function(){
                    $(document).on('click','.keywords-list-cat',function(){
                        var name = $(this).html();
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('keyword/getcategoryinterestslist'); ?>',
                            data: {cat: $(this).html()},
                            success: function(data){
                                $('.keywords-list').html(data);
                            }
                        });
                    });
                    $(document).on('click','.all-cats',function(){
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('keyword/getcategorylist'); ?>',
                            data: {cat: $(this).html()},
                            success: function(data){
                                $('.keywords-list').html(data);
                            }
                        });
                    });
                    $(document).on('click','.keywords-list-keyword',function(){
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('keyword/insertuserinterest'); ?>',
                            data: {user_id:<? echo User::model()->getUserId(); ?>,keyword:$(this).html()},
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    });
                });
            </script>
        </form>

    </div>
</div>
