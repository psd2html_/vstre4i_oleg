<?php

/**
 * This is the model class for table "admin".
 *
 * The followings are the available columns in table 'admin':
 * @property integer $id
 * @property string $key
 * @property string $value
 */
class Admin extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Admin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('key, value', 'required'),
			array('key, value', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, key, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'key' => 'Название',
			'value' => 'Значение',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('key',$this->key,true);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function isSiteAdmin($userId)
    {
        $db = Yii::app()->db;

        $isAdmin = $db->createCommand()
            ->select()
            ->from('admin_superusers')
            ->where('`user_id` = ' . (int)$userId)
            ->queryRow();

        return (bool)$isAdmin;
    }

    public function isSiteModerator($userId)
    {
        $db = Yii::app()->db;

        $isModerator = $db->createCommand()
            ->select()
            ->from('admin_moderators')
            ->where('`user_id` = ' . (int)$userId)
            ->queryRow();

        return (bool)$isModerator;
    }

    public function isSiteAdminOrModerator($userId)
    {
        $db = Yii::app()->db;

        $isAdmin = $db->createCommand()
            ->select()
            ->from('admin_superusers')
            ->where('`user_id` = ' . (int)$userId)
            ->queryRow();

        $isModerator = $db->createCommand()
            ->select()
            ->from('admin_moderators')
            ->where('`user_id` = ' . (int)$userId)
            ->queryRow();

        return $isAdmin || $isModerator;
    }
    public static function getMainMetaTags()
    {
			$sql = 'SELECT t.key, t.value FROM admin as t WHERE t.key IN("site_name","keywords","description")';
			$query = Yii::app()->db->createCommand($sql);
			$dataReader = $query->query();
			$result = array();
			foreach($dataReader->readAll() as $row){
				$result[$row['key']] = $row['value'];
			}
			return $result;
    }
    public static function getMainKeywords()
    {
			$sql = "SELECT value FROM admin WHERE admin.key = 'keywords'";
			$query = Yii::app()->db->createCommand($sql);	
			$data = $query->queryScalar();
			return $data;
    }
    public static function getMainDescription()
    {
			$sql = "SELECT value FROM admin WHERE admin.key = 'description'";
			$query = Yii::app()->db->createCommand($sql);	
			$data = $query->queryScalar();
			return $data;
    }

}
