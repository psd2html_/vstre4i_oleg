<?php

/**
 * This is the model class for table "city".
 *
 * The followings are the available columns in table 'city':
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property integer $parent
 * @property string $language
 */
class City extends CActiveRecord
{

    const STATUS_APPROVED = 1;
    const STATUS_NOT_APPROVED = 0;

    public $value;
    public $ua;
    public $kz;
    public $be;
    public $ru;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'city';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('country_id, name, region_id', 'required'),
			array('country_id, approved', 'numerical', 'integerOnly'=>true),
			array('approved', 'default', 'setOnEmpty' => true, 'value'=> self::STATUS_NOT_APPROVED),
			array('name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, country_id, region_id, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
    public function relations()
    {
        return array(
            'translate' => array(self::HAS_MANY, 'Translate', 'row_id', 'on'=>'translate.table_name ="city"'),
            'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
            'region' => array(self::BELONGS_TO, 'Region', 'region_id')

        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'country_id' => 'Страна',
			'region_id' => 'Регион',
			'approved' => 'Статус',
			'name' => 'Название',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($id = false)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        $criteria->with = array('country','translate');
		if($id > 0){
			$criteria->condition='t.country_id=:country_id';
			$criteria->params=array(':country_id'=>$id);
		}
//        $criteria->together = true;
		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.country_id',$this->country_id);
		$criteria->compare('t.region_id',$this->region_id);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('t.approved',$this->approved);
        $criteria->compare('translate.language', $this->ua);
        $criteria->compare('translate.language', $this->kz);
        $criteria->compare('translate.language', $this->be);
		$criteria->order = 't.approved ASC, t.id ASC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes'=>array(
                    'ua'=>array(
                        'asc'=>'translate.language',
                        'desc'=>'translate.language DESC',
                    ),
                    'be'=>array(
                        'asc'=>'translate.language',
                        'desc'=>'translate.language DESC',
                    ),
                    'kz'=>array(
                        'asc'=>'translate.language',
                        'desc'=>'translate.language DESC',
                    ),
                    'name' => array(
                        'asc'=>'t.name',
                        'desc'=>'t.name DESC',
                    ),
                    'country' => array(
                        'asc'=>'country.name',
                        'desc' => 'country.name DESC'
                    ),
                    'region' => array(
                        'asc'=>'region.name',
                        'desc' => 'region.name DESC'
                    ),
                    'approved' => array(
                        'asc'=>'approved.name',
                        'desc' => 'approved.name DESC'
                    ),
                    'id' => array(
                        'asc' => 't.id',
                        'desc' => 't.id DESC'
                    ),
                ),
			)
		  )
		);
	}

    public function getCity($cityId)
    {
        $db = Yii::app()->db;

        $cityName = $db->createCommand()
            ->select('name')
            ->from($this->tableName())
            ->where('`id` = ' . (int)$cityId)
            ->queryScalar();

        return $cityName;
    }

    public function getCityID($cityName)
    {
        $db = Yii::app()->db;

        $cityId = $db->createCommand()
            ->select('id')
            ->from($this->tableName())
            ->where('`name` = :cityName', array(':cityName'=>$cityName))
            ->queryScalar();

        if ($cityId == NULL) {
            $cityId = 0;
        }

        return $cityId;
    }
    public function getCityInfo($cityName, $countryId)
    {
        $db = Yii::app()->db;

        $cityInfo = $db->createCommand()
            ->select('id, region_id')
            ->from($this->tableName())
            ->where('`name` = :cityName AND `country_id` = :countryId AND `approved` = 1', array(':cityName'=>$cityName, ':countryId'=>$countryId))
            ->queryRow();

        if ($cityInfo == NULL) {
            $cityInfo = array();
        }

        return $cityInfo;
    }


    public function getCityIdsList($countryName)
    {
        $db = Yii::app()->db;

        $cityIds = $db->createCommand()
            ->select('city.id')
            ->from($this->tableName())
            ->join('country', 'country.id = city.country_id')
            ->where('country.name = :countryName', array(':countryName'=>$countryName))
            ->queryAll();

        foreach ($cityIds as $key => $cityId) {
            $cityIds[$key] = $cityId['id'];
        }

        return $cityIds;
    }

    public function getCityIdsListByCountryId($countryId)
    {
        $countryName = Country::model()->getCountryName($countryId);

        $cityIds = $this->getCityIdsList($countryName);

        return $cityIds;
    }

    public function getAllGroupCities()
    {
        $db = Yii::app()->db;

        $dataReader = $db->createCommand()
            ->selectDistinct('city.name, city.id, country.name AS country')
            ->from($this->tableName())
            ->join('group', 'group.city_id = city.id')
            ->leftJoin('country', 'country.id = city.country_id')
            ->order('country.name, city.name')
            ->queryAll();

        $result = '';
        foreach($dataReader as $row) {
            $result .= '{ label: "' . CHtml::encode($row['name']) . '", category: "' . CHtml::encode($row['country']) . '" },';
        }
        return $result;
    }

    public function getCityList($countryName)
    {
        $db = Yii::app()->db;

        $cities = $db->createCommand()
            ->select('city.id, city.name')
            ->from($this->tableName())
            ->join('country', 'country.id = city.country_id')
            ->where('country.name = :countryName', array(':countryName'=>$countryName))
            ->queryAll();

        return $cities;
    }
	static function cityList($region_id) {
		if($region_id){
			$criteria = new CDbCriteria();
			$criteria->select='id, name';
			$criteria->condition='region_id=:region_id';
			$criteria->params=array(':region_id'=>$region_id);
			$regions = CHtml::listData(self::model()->findAll($criteria), 'id', 'name');
			return  $regions;
		}
		return array();
	}

    public function getCountryCitiesIds($countryId)
    {
        $db = Yii::app()->db;

        $cities = $db->createCommand()
            ->select('id')
            ->from('city')
            ->where('`country_id` = ' . (int)$countryId)
            ->queryAll();

        foreach ($cities as $key => $city) {
            $cities[$key] = (int)$city['id'];
        }

        return $cities;
    }
	
	
    public function scopes()
    {
        return array(
            'sorted'=>array(
                'order'=>'name'
            ),
        );
    }
}
