<?php

/**
 * This is the model class for table "city".
 *
 * The followings are the available columns in table 'city':
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property integer $parent
 * @property string $language
 */
class City extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return City the static model class
	 */


    public $value;
    public $ua;
    public $kz;
    public $be;
    public $ru;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'city';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('country_id, name', 'required'),
			array('country_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, country_id, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
    public function relations()
    {
        return array(
            'translate' => array(self::HAS_MANY, 'Translate', 'row_id', 'on'=>'translate.table_name ="city"'),
            'country' => array(self::BELONGS_TO, 'Country', 'country_id')

        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'country_id' => 'Country',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        $criteria->with = array('country','translate');
        $criteria->together = true;
		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.country_id',$this->country_id);
		$criteria->compare('t.name',$this->name,true);
        $criteria->compare('translate.language', $this->ua);
        $criteria->compare('translate.language', $this->kz);
        $criteria->compare('translate.language', $this->be);

		$criteria->compare('id',$this->id);
		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('name',$this->name,true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes'=>array(
                    'ua'=>array(
                        'asc'=>'translate.language',
                        'desc'=>'translate.language DESC',
                    ),
                    'be'=>array(
                        'asc'=>'translate.language',
                        'desc'=>'translate.language DESC',
                    ),
                    'kz'=>array(
                        'asc'=>'translate.language',
                        'desc'=>'translate.language DESC',
                    ),
                    'name' => array(
                        'asc'=>'t.name',
                        'desc'=>'t.name DESC',
                    ),
                    'country' => array(
                        'asc'=>'country.name',
                        'desc' => 'country.name DESC'
                    ),
                    'id' => array(
                        'asc' => 't.id',
                        'desc' => 't.id DESC'
                    )
                ),
		)));
	}

    public function getCity($cityId)
    {
        $db = Yii::app()->db;

        $cityName = $db->createCommand()
            ->select('name')
            ->from($this->tableName())
            ->where('`id` = ' . (int)$cityId)
            ->queryScalar();

        return $cityName;
    }

    public function getCityID($cityName)
    {
        $db = Yii::app()->db;

        $cityId = $db->createCommand()
            ->select('id')
            ->from($this->tableName())
            ->where('`name` = :cityName', array(':cityName'=>$cityName))
            ->queryScalar();

        if ($cityId == NULL) {
            $cityId = 0;
        }

        return $cityId;
    }

    public function getCityIdsList($countryName)
    {
        $db = Yii::app()->db;

        $cityIds = $db->createCommand()
            ->select('city.id')
            ->from($this->tableName())
            ->join('country', 'country.id = city.country_id')
            ->where('country.name = :countryName', array(':countryName'=>$countryName))
            ->queryAll();

        foreach ($cityIds as $key => $cityId) {
            $cityIds[$key] = $cityId['id'];
        }

        return $cityIds;
    }

    public function getCityIdsListByCountryId($countryId)
    {
        $countryName = Country::model()->getCountryName($countryId);

        $cityIds = $this->getCityIdsList($countryName);

        return $cityIds;
    }

    public function getAllGroupCities()
    {
        $db = Yii::app()->db;

        $dataReader = $db->createCommand()
            ->selectDistinct('city.name, city.id, country.name AS country')
            ->from($this->tableName())
            ->join('group', 'group.city_id = city.id')
            ->leftJoin('country', 'country.id = city.country_id')
            ->order('country.name, city.name')
            ->queryAll();

        $result = '';
        foreach($dataReader as $row) {
            $result .= '{ label: "' . CHtml::encode($row['name']) . '", category: "' . CHtml::encode($row['country']) . '" },';
        }
        return $result;
    }

    public function getCityList($countryName)
    {
        $db = Yii::app()->db;

        $cities = $db->createCommand()
            ->select('city.id, city.name')
            ->from($this->tableName())
            ->join('country', 'country.id = city.country_id')
            ->where('country.name = :countryName', array(':countryName'=>$countryName))
            ->queryAll();

        return $cities;
    }

    public function getCountryCitiesIds($countryId)
    {
        $db = Yii::app()->db;

        $cities = $db->createCommand()
            ->select('id')
            ->from('city')
            ->where('`country_id` = ' . (int)$countryId)
            ->queryAll();

        foreach ($cities as $key => $city) {
            $cities[$key] = (int)$city['id'];
        }

        return $cities;
    }
}