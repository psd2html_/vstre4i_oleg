<?php

/**
 * This is the model class for table "complaint".
 *
 * The followings are the available columns in table 'complaint':
 * @property integer $id
 * @property integer $item_id
 * @property string $date
 * @property string $text
 * @property string $text_id
 */
class Complaint extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Complaint the static model class
	 */

    public $name = false;
    public $type = false;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'complaint';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_id, date, text, text_id', 'required'),
			array('item_id', 'numerical', 'integerOnly'=>true),
			array('text', 'length', 'max'=>500),
			array('text_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, item_id, date, text, text_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'group' => array(self::BELONGS_TO, 'Group', 'item_id'),
            'meet' => array(self::BELONGS_TO, 'Group', 'item_id'),
            'user' => array(self::BELONGS_TO, 'User', 'item_id')
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item_id' => 'Item',
			'date' => 'Date',
			'text' => 'Text',
			'text_id' => 'Text',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria=new CDbCriteria;
		$with = $this->text_id ? $this->text_id : 'user';
        $criteria->with  = array($with);
		$criteria->compare('id',$this->id);
		//$criteria->compare('t.item_id',$this->item_id);
		$criteria->compare('t.date',$this->date,true);
		//$criteria->compare('t.text',$this->text,true);
		$criteria->compare('t.text_id',$this->text_id,true);
		$criteria->compare('t.parent_id',0);
		$criteria->compare('t.`status`',0);
		$criteria->compare($with.'.name',$this->item_id,true);
		$criteria->order = 't.id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function sentComplaint($mid, $text, $textId, $userId)
    {
        $db = Yii::app()->db;

        $columns = array(
            'item_id' => (int)$mid,
            'date'    => date('Y-m-d'),
            'text'    => htmlspecialchars($text),
            'text_id' => $textId,
            'user_id' => (int)$userId,
        );
        $success = $db->createCommand()
            ->insert('complaint', $columns);

        return $success;
    }
	static function complaintsList($userId){
		$db = Yii::app()->db;
		$groupModel = Group::model();
		$groupsIds = $groupModel->getAllUsersGroupsIds($userId);
		$meetsIds  = $groupModel->getMeetsIdsWhereUserIsAdmin($groupsIds);
		$ids = array_merge($groupsIds, $meetsIds);
		$incondition = implode(',', array_values($ids));
        $command = $db->createCommand();
		$command->select('complaint.*, user.id AS sender_id, user.name AS sender_name');
		$command->from('complaint');
		$command->join('user', 'complaint.user_id = user.id');
		$command->where('complaint.item_id IN (' . $incondition .') AND parent_id = 0 AND complaint.text_id <> "user" OR complaint.item_id = '.(int)$userId.'  AND complaint.text_id = "user" AND parent_id = 0');
		$command->order('complaint.date ASC');
		$complaints = $command->queryAll();
        return $complaints;
	}
	static function MyComplaintsList($userId){
		$db = Yii::app()->db;
		$groupModel = Group::model();
		$groupsIds = $groupModel->getAllUsersGroupsIds($userId);
		$meetsIds  = $groupModel->getMeetsIdsWhereUserIsAdmin($groupsIds);
		$ids = array_merge($groupsIds, $meetsIds);
		$incondition = implode(',', array_values($ids));
        $command = $db->createCommand();
		$command->select('complaint.*, user.id AS sender_id, user.name AS sender_name');
		$command->from('complaint');
		$command->join('user', 'complaint.user_id = user.id');
		$command->where('complaint.user_id = '.(int)$userId.' AND parent_id = 0');
		//$command->where('complaint.user_id = '.(int)$userId.' AND parent_id = 0 AND complaint.item_id NOT IN (' . $incondition .')');
		$command->order('complaint.date ASC');
		$complaints = $command->queryAll();
        return $complaints;
	}
	static function getCountOfAnswers($id, $user_id){
		$db = Yii::app()->db;
        $command = $db->createCommand();
		$command->select('count(*)');
		$command->from('complaint');
		$command->where('complaint.parent_id = '.(int)$id.' AND user_id = '.(int)$user_id);
		$count = $command->queryScalar();
        return $count;
		
		
	}
	static function getCountOfAnswersMy($id, $user_id){
		$db = Yii::app()->db;
        $command = $db->createCommand();
		$command->select('count(*)');
		$command->from('complaint');
		$command->where('complaint.parent_id = '.(int)$id.' AND user_id <> '.(int)$user_id);
		$count = $command->queryScalar();
        return $count;
		
		
	}
    public function getComplaintRecords($complaint_id)
    {
        $db = Yii::app()->db;

        $complaint = $db->createCommand()
            ->select('complaint.*, user.name AS sender_name')
            ->from($this->tableName())
            ->join('user', 'complaint.user_id = user.id')
            ->where('complaint.id = '.(int)$complaint_id.' OR complaint.parent_id = '.(int)$complaint_id)
            ->order('date ASC')
            ->queryAll();

        return $complaint;
    }

    public function getFormatedComplaintRecords($complaint_id)
    {
        $records = $this->getComplaintRecords($complaint_id);
        if (!$records) {
            return $records;
        }

        $levels = array();
        foreach ($records as $record) {
            $depth = $record['depth'];
            if (!isset($levels[$depth])) {
                $levels[$depth] = array();
            }
            $levels[$depth][$record['id']] = $record;
        }

        for ($i = count($levels) - 1; $i > 0; $i--) {
            foreach ($levels[$i] as $comment) {
                $parentId = $comment['parent_id'];
                if (!isset($levels[$i-1][$parentId]['children'])) {
                    $levels[$i-1][$parentId]['children'] = array();
                }
                $levels[$i-1][$parentId]['children'][$comment['id']] = $comment;
            }
        }

        $result = $levels[0];

        return $result;
    }
    public function sentRecord($senderId, $text, $itemId, $text_id, $parentId = 0)
    {
        $db = Yii::app()->db;

        $depth = 0;
        if ($parentId) {
            $depth = $db->createCommand()
                ->select('depth')
                ->from($this->tableName())
                ->where('id = ' . (int)$parentId)
                ->queryScalar();
            $depth++;
        }

        $columns = array(
            'item_id'   => (int)$itemId,
            'text'      => htmlspecialchars(trim($text)),
            'user_id' => (int)$senderId,
            'parent_id' => (int)$parentId,
            'text_id'     => htmlspecialchars(trim($text_id)),
            'depth'     => (int)$depth,
            'date'     => date('Y-m-d', time()),
        );
        $rowCount = $db->createCommand()
            ->insert($this->tableName(), $columns);

        return $rowCount;
    }
	static function getType($type){
		$types=array(
			'user'=>'На пользователя',
			'group'=>'На группу',
			'meet'=>'На встречу',
		);
		return $types[$type];
	
	}
}
