<?php

/**
 * This is the model class for table "country".
 *
 * The followings are the available columns in table 'country':
 * @property integer $id
 * @property string $name
 * @property string $language
 */
class Country extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Country the static model class
	 */

    public $ua = false;
    public $kz = false;
    public $be = false;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'country';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'translate' => array(self::HAS_MANY, 'Translate', 'row_id','condition'=>'translate.table_name="country"'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
        $criteria->with = array('translate');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getCountryByCityID($cityId)
    {
        $db = Yii::app()->db;

        $country = $db->createCommand()
            ->select('country.name')
            ->from($this->tableName())
            ->join('city', 'city.country_id = country.id')
            ->where('city.id = ' . (int)$cityId)
            ->queryScalar();

        return $country;
    }

    public function getCountries()
    {
        $db = Yii::app()->db;

        $countries = $db->createCommand()
            ->select('country.id, country.name')
            ->from($this->tableName())
            ->queryAll();

        return $countries;
    }

    public function getCountryId($countryName)
    {
        $db = Yii::app()->db;

        $id = $db->createCommand()
            ->select('id')
            ->from('country')
            ->where('name=:name', array(':name'=>$countryName))
            ->queryScalar();

        return (int)$id;
    }

    public function getCountryName($countryId)
    {
        $db = Yii::app()->db;

        $name = $db->createCommand()
            ->select('name')
            ->from('country')
            ->where('id=:id', array(':id'=>$countryId))
            ->queryScalar();

        return $name;
    }

}