<?php

/**
 * This is the model class for table "menus".
 *
 * The followings are the available columns in table 'menus':
 * @property integer $id
 * @property string $name
 * @property string $language
 */
class Menus extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Menu the static model class
	 */

    public $ua = false;
    public $kz = false;
    public $be = false;
    public $alias;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'menus_elements';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('menu_id', 'required'),
			array('title', 'length', 'max'=>255),
			array('menu_id, page_id, order_number', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('name, page_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'translate' => array(self::HAS_MANY, 'Translate', 'row_id','condition'=>'translate.table_name="menus"'),
			'page' => array(self::BELONGS_TO, 'Page', 'page_id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'menu_id' => 'Тип меню',
			'type' => 'Тип параметра',
			'title' => 'Название',
			'params' => 'Атрибуты',
			'page_id' => 'Страница',
			'order_number' => 'Порядок',
			'alias' => 'Страница',
		);
	}


    public function getMenus()
    {
        $db = Yii::app()->db;

        $command = $db->createCommand()
            ->select()
            ->from($this->tableName())
            ->where('menu_id = :menu_id', array('menu_id'=>1))
            ->order('menu_id ASC');

        $menuElements = $command->queryAll();

        $menus = array();
        foreach($menuElements as $element) {
            if (!isset($menus[$element['menu_id']])) {
                $menus[$element['menu_id']] = array();
            }
            $menus[$element['menu_id']][] = $element;
        }

        return $menus;
    }

    public function saveMenus($elements)
    {
        $allowedFields = array(
            'menu_id' => 'int',
            'type' => 'string',
            'title' => 'string',
            'params' => 'string',
            'order_number' =>'int',
        );

        $db = Yii::app()->db;

        $rowsAffected = 0;
        foreach($elements as $elementId => $element) {
            $columns = array_intersect_key($element, $allowedFields);
            if (empty($columns) && !$elementId) {
                continue;
            }

            foreach($columns as $fieldName => $fieldValue) {
                settype($columns[$fieldName], $allowedFields[$fieldName]);
            }

            $effect = $db->createCommand()
                ->update($this->tableName(), $columns, 'id = ' . (int)$elementId);
            $rowsAffected += $effect;
        }

        return $rowsAffected;
    }
	
	public function search($type='bottom')
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        $criteria->with  = array('page');
		$criteria->condition='menu_id=:menu_id';
		$criteria->params=array(':menu_id'=>2);
		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.menu_id',$this->menu_id);
		$criteria->compare('t.type',$this->type);
		$criteria->compare('t.title',$this->title);
		$criteria->compare('t.params',$this->params);
		$criteria->compare('t.order_number',$this->order_number);
		//$criteria->compare('page.name',$this->page->name);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}
