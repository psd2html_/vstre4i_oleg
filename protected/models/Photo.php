<?php

/**
 * This is the model class for table "photo".
 *
 * The followings are the available columns in table 'photo':
 * @property integer $id
 * @property integer $album_id
 * @property integer $user_id
 * @property string $date
 * @property string $link
 */
class Photo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Photo the static model class
	 */
    public $image;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'photo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('album_id, user_id, date, link', 'required'),
			array('album_id, user_id', 'numerical', 'integerOnly'=>true),
			array('link', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, album_id, user_id, date, link', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'album_id' => 'Album',
			'user_id' => 'User',
			'date' => 'Date',
			'link' => 'Link',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('album_id',$this->album_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('link',$this->link,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>'id DESC',
            )
		));
	}

    public function photoUpload($albumId, $userId, $filename)
    {
        $db = Yii::app()->db;

        $columns = array(
            'album_id' => (int)$albumId,
            'user_id' => (int)$userId,
            'date' => Date('Y-m-d'),
            'link' => $filename,
        );
        $rowCount = $db->createCommand()
            ->insert($this->tableName(), $columns);

        return $rowCount;
    }

    public function getRandomPhoto()
    {
        $db = Yii::app()->db;

        $photo = $db->createCommand()
            ->select('photo.link, group.name, group.id as group_id, photoalbum.id as album_id')
            ->from($this->tableName())
            ->join('photoalbum', 'photo.album_id = photoalbum.id')
            ->join('group', 'photoalbum.group_id = group.id')
            ->order('RAND()')
            ->queryRow();

        return $photo;
    }

    public function getAllPhotos($page = 0)
    {
        $offset = (int)$page * 50;
        $limit = $offset + 50;

        $db = Yii::app()->db;

        $photos = $db->createCommand()
            ->select()
            ->from($this->tableName())
            ->limit($limit, $offset)
            ->queryAll();

        return $photos;
    }
}