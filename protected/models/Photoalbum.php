<?php

/**
 * This is the model class for table "photoalbum".
 *
 * The followings are the available columns in table 'photoalbum':
 * @property integer $id
 * @property string $name
 * @property integer $meet_id
 * @property string $description
 */
class Photoalbum extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Photoalbum the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'photoalbum';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, meet_id, description, group_id',  'required'),
			array('meet_id, group_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('description', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, meet_id, description, group_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'group'=> array(self::HAS_ONE, 'Group', array('id' => 'group_id'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'meet_id' => 'ID встречи',
			'description' => 'Описание',
            'group_id' => 'ID групы',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('meet_id',$this->meet_id);
		$criteria->compare('description',$this->description,true);
        $criteria->compare('group_id',$this->group_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>'id DESC',
            )
		));
	}

    public function getGroupAlbums($groupId)
    {
        $db = Yii::app()->db;

        $groupAlbums = $db->createCommand()
            ->select()
            ->from($this->tableName())
            ->where('group_id = ' . (int)$groupId)
            ->queryAll();

        return $groupAlbums;
    }

    public function getAlbumPhotos($albumId)
    {
        $db = Yii::app()->db;

        $albumPhotos = $db->createCommand()
            ->select('album_id, link')
            ->from('photo')
            ->where('album_id = ' . (int)$albumId)
            ->queryAll();

        return $albumPhotos;
    }

    public function getGroupPhotos($groupId)
    {
        $db = Yii::app()->db;

        $groupPhotos = $db->createCommand()
            ->select('photo.album_id, photo.link')
            ->from('photo')
            ->join('photoalbum', 'photo.album_id = photoalbum.id')
            ->where('photoalbum.group_id = ' . (int)$groupId)
            ->queryAll();

        return $groupPhotos;
    }

    public function getRandomPhoto($groupId)
    {
        $db = Yii::app()->db;

        $photo = $db->createCommand()
            ->select('photo.link, photo.album_id')
            ->from('photo')
            ->join('photoalbum', 'photoalbum.id = photo.album_id')
            ->where('photoalbum.group_id = ' . (int)$groupId)
            ->order('RAND()')
            ->queryRow();

        return $photo;
    }

    public function getRandomAlbumPhoto($albumId)
    {
        $db = Yii::app()->db;

        $photo = $db->createCommand()
            ->select('photo.link')
            ->from('photo')
            ->join('photoalbum', 'photoalbum.id = photo.album_id')
            ->where('photo.album_id = ' . (int)$albumId)
            ->order('RAND()')
            ->queryScalar();

        return $photo;
    }
    public function getAlbumsGroupId($albumId)
    {
        $db = Yii::app()->db;

        $group_id = $db->createCommand()
            ->select('group_id')
            ->from($this->tableName())
            ->where('id = ' . (int)$albumId)
            ->queryScalar();
        return $group_id;
    }

    public function imagesUpload($file, $albumId, $userId)
    {
        $db = Yii::app()->db;

        $columns = array(
            'album_id' => (int)$albumId,
            'user_id' => (int)$userId,
            'date' => Date('Y-m-d H:i:s'),
            'link' => $file,
        );
        $rowCount = $db->createCommand()
            ->insert('photo', $columns);

        return $rowCount;
    }
	
	 public function createAlbum($name, $groupId, $meetId, $date)
    {
        $db = Yii::app()->db;

        $columns = array(
            'name' => $name,
            'meet_id' => (int)$meetId,
            'description' => '',
            'group_id' => (int)$groupId,
            'date' => $date,
        );

        $rowCount = $db->createCommand()
            ->insert($this->tableName(), $columns);

        return $rowCount;
    }

}
