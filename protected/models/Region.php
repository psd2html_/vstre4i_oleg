<?php

/**
 * This is the model class for table "region".
 *
 * The followings are the available columns in table 'region':
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property integer $parent
 * @property string $language
 */
class Region extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return City the static model class
     */


    public $value;
    public $ua;
    public $kz;
    public $be;
    public $ru;
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'region';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('country_id, name', 'required'),
            array('country_id', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>100),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, country_id, name', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'translate' => array(self::HAS_MANY, 'Translate', 'row_id', 'on'=>'translate.table_name ="region"'),
            'country' => array(self::BELONGS_TO, 'Country', 'country_id')

        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'country_id' => 'Country',
            'name' => 'Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->with = array('country','translate');
        $criteria->together = true;
        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.country_id',$this->country_id);
        $criteria->compare('t.name',$this->name,true);
        $criteria->compare('translate.language', $this->ua);
        $criteria->compare('translate.language', $this->kz);
        $criteria->compare('translate.language', $this->be);

        $criteria->compare('id',$this->id);
        $criteria->compare('country_id',$this->country_id);
        $criteria->compare('name',$this->name,true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes'=>array(
                    'ua'=>array(
                        'asc'=>'translate.language',
                        'desc'=>'translate.language DESC',
                    ),
                    'be'=>array(
                        'asc'=>'translate.language',
                        'desc'=>'translate.language DESC',
                    ),
                    'kz'=>array(
                        'asc'=>'translate.language',
                        'desc'=>'translate.language DESC',
                    ),
                    'name' => array(
                        'asc'=>'t.name',
                        'desc'=>'t.name DESC',
                    ),
                    'country' => array(
                        'asc'=>'country.name',
                        'desc' => 'country.name DESC'
                    ),
                    'id' => array(
                        'asc' => 't.id',
                        'desc' => 't.id DESC'
                    )
                ),
            )));
    }


    public function getRegionList($countryName)
    {
        $db = Yii::app()->db;

        $regions = $db->createCommand()
            ->select('region.id, region.name')
            ->from($this->tableName())
            ->join('country', 'country.id = region.country_id')
            ->where('country.name = :countryName', array(':countryName'=>$countryName))
            ->queryAll();

        return $regions;
    }

    public function getCountryRegionsIds($countryId)
    {
        $db = Yii::app()->db;

        $regions = $db->createCommand()
            ->select('id')
            ->from('region')
            ->where('`country_id` = ' . (int)$countryId)
            ->queryAll();

        foreach ($regions as $key => $region) {
            $regions[$key] = (int)$region['id'];
        }

        return $regions;
    }
	
    static function getStatCountryRegions($countryId=false)
    {
        $db = Yii::app()->db;

        $regions = $db->createCommand();
        $regions->select('region.id, region.name');
        $regions->from('region');
        if($countryId > 1) {  
			$regions->where('region.country_id = :countryId', array(':countryId'=>$countryId));
        }
		$regions = $regions->queryAll();
		$list = array();
		foreach ($regions as $key => $region) {
             $list[$region['id']] = $region['name'];
        }

        return $list;
    }
    public function getRegionByCityID($cityId)
    {
        $db = Yii::app()->db;

        $country = $db->createCommand()
            ->select('region.name')
            ->from($this->tableName())
            ->join('city', 'city.region_id = region.id')
            ->where('city.id = ' . (int)$cityId)
            ->queryScalar();

        return $country;
    }
	static function regionsList($country_id) {
		if($country_id){
			$criteria = new CDbCriteria();
			$criteria->select='id, name';
			$criteria->condition='country_id=:country_id';
			$criteria->params=array(':country_id'=>$country_id);
			$countries = CHtml::listData(self::model()->findAll($criteria), 'id', 'name');
			return  $countries;
		}
		return array();
	}
    public function getRegionName($regionId)
    {
        $db = Yii::app()->db;

        $name = $db->createCommand()
            ->select('name')
            ->from('region')
            ->where('id=:id', array(':id'=>$regionId))
            ->queryScalar();

        return $name;
    }
}
