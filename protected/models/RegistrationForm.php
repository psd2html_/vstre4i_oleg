<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class RegistrationForm extends CFormModel
{
    public $username;
    public $password;
    public $email;
    public $terms;

    private $_identity;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('username, email, password, terms', 'required'),
           /* array('username','unique','className' => 'User','attributeName' => 'name','message' => 'Пользователь с таким e-mail уже существует'),*/
            array('email','unique','className' => 'User','attributeName' => 'email','message' => 'Пользователь с таким e-mail уже существует'),
            // password needs to be authenticated
            array('password', 'authenticate'),
            array('username','logauth'),
            array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => 'Некоректные символы. Используйте, пожалуйста, латинский алфавит и цифры (A-z0-9)'),

        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'rememberMe'=>'Remember me next time',
        );
    }

    public function logauth()
    {
        die();
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute,$params)
    {
        if(!$this->hasErrors())
        {
            $this->_identity=new UserIdentity($this->username,$this->password);
            if(!$this->_identity->authenticate())
                $this->addError('password','Incorrect username or password.');
        }
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {
        if($this->_identity===null)
        {
            $this->_identity=new UserIdentity($this->username,$this->password);
            $this->_identity->authenticate();
        }
        if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
        {
            $duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
            Yii::app()->user->login($this->_identity,$duration);
            return true;
        }
        else
            return false;
    }
    public function registration()
    {
        $model = new User();

        $username = $_POST['RegistrationForm']['username'];
        $email = $_POST['RegistrationForm']['email'];
        $password = md5($_POST['RegistrationForm']['password']);

        $model->name = $username;
        $model->email = $email;
        $model->pass = $password;
        $model->status = '1';
        $model->reg_date = Date('Y-m-d H:i:s');

        if ($model->validate() && $model->save()) {
            $identity=new UserIdentity($username,$password);
            if($identity->authenticate())
                Yii::app()->user->login($identity);
            return Yii::app()->db->getLastInsertId();
        }
    }
}
