<?php


class UploadImg extends CActiveRecord
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'photo';
	}

    public function upload_from_soc($folder, $path, $pname='Photo', $spname='link', $h=300, $w=300, $pfilename = false, $need_large=true, $inside=false) {

        if(strlen($path) ==0 ) {
            return false;
        }

        $fileext = pathinfo($path, PATHINFO_EXTENSION);
        if(empty($fileext)) {

            $fileext = 'png';
        }

        $name = 'image'.time().'.'.$fileext;

        //$p =  $_SERVER['DOCUMENT_ROOT'].'/images/'.$name;

		$p =  Yii::app()->basePath .'/../images/'.$name;

        $r = file_get_contents($path);
          file_put_contents($p,$r);


        $_FILES[$pname]['tmp_name'][$spname] = $p;
        $_FILES[$pname]['name'][$spname] = $name;
        $_FILES[$pname]['size'][$spname] = filesize($p);
        $_FILES[$pname]['type'][$spname] = 'image/jpeg';





        UploadImg::model()->upload_image($folder,  $pname, $spname, $h, $w, $pfilename, $need_large, $inside);

        unlink($p);


    }



	
	
    public function upload_image($folder,  $pname='Photo', $spname='link', $h=300, $w=300, $pfilename = false, $need_large=true, $inside=false)
    {
        //$CI = & get_instance();  
        $meta_info = 'original';
        
        //$path = $CI->config->item('gallery_root');
        $height = $h;
        $width = $w;
        
        $or_height = 1000;;
        $or_width = 1000;
        $fileext = pathinfo($_FILES[$pname]['name'][$spname], PATHINFO_EXTENSION);



        if(empty($fileext)) {

            $fileext = 'png';

        }
      
        if ($fileext != 'png' && $fileext != 'jpeg' && $fileext !=
            'gif' && $fileext != 'jpg') 
        {
					
                    echo '3 - '.  CHtml::encode($fileext);
            
        } 
		
        else {
            
            
            
            $ext = '';
            if ($fileext == 'gif') {
                $ext = 'gif';
            }
            if ($fileext == 'jpeg') {
                $ext = 'jpeg';
            }
            if ($fileext == 'png') {
                $ext = 'png';
            }
            if ($fileext == 'jpg') {
                $ext = 'jpg';
            }
			
			

            if(!is_dir($folder))
                mkdir($folder, 0777, true);
            

			  if(!$pfilename){

				$fname = md5(uniqid(rand(), true));
			}
			else {
				$fname = $pfilename;
			}
			
			
            $iname = $fname . '.' . $ext;
			
//			$path = $_SERVER['DOCUMENT_ROOT'].'/'.$folder;
			$path =  Yii::app()->basePath .'/../'.$folder;
            $filename = $path . $iname;


			
			if(!$pfilename){
				while (file_exists($filename)) {
					$iname = md5(uniqid(rand(), true)) . '.' . $ext;
					$filename = $path . $iname;
				}
			}

            $sname = $iname;
            $iname = $meta_info . "-" . $iname;
            $filename = $path . $iname;


        if(is_uploaded_file($_FILES[$pname]['tmp_name'][$spname])) {
            move_uploaded_file($_FILES[$pname]['tmp_name'][$spname], $filename);
        } else {

            copy($_FILES[$pname]['tmp_name'][$spname], $filename);

        }







            $rez = $this->img_resize($filename, $path . "mini_" . $sname, $height, $width);
            
                if($need_large)
                {
                   $rez = $this->img_resize($filename, $path . $sname, $or_height, $or_width);
                   
                   // 
                }
            if($rez)
            {
               // unlink($filename);
            }

            return $sname;
        } 
		/*else {
            //echo $img_obj['error'];
            return false;
        }*/
    }

    public function img_resize($src, $dest, $width, $height, $rgb = 0xffffff, $quality =
        100)
    {
        if (!file_exists($src))
            return false;

        $size = getimagesize($src);

        if ($size === false)
            return false;

        // Определяем исходный формат по MIME-информации, предоставленной
        // функцией getimagesize, и выбираем соответствующую формату
        // imagecreatefrom-функцию.
        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
        $icfunc = "imagecreatefrom" . $format;
        if (!function_exists($icfunc))
            return false;

        $x_ratio = $width / $size[0];
        $y_ratio = $height / $size[1];

        $ratio = min($x_ratio, $y_ratio);
        $use_x_ratio = ($x_ratio == $ratio);

        $new_width = $use_x_ratio ? $width : floor($size[0] * $ratio);
        $new_height = !$use_x_ratio ? $height : floor($size[1] * $ratio);
        $new_left = $use_x_ratio ? 0 : floor(($width - $new_width) / 2);
        $new_top = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

        $isrc = $icfunc($src);
        $idest = imagecreatetruecolor($width, $height);

        imagefill($idest, 0, 0, $rgb);
        imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0, $new_width, $new_height,
            $size[0], $size[1]);

        imagejpeg($idest, $dest, $quality);

        imagedestroy($isrc);
        imagedestroy($idest);

        return true;

    }
    
    public function delete_image($file, $folder='')
    {
        $CI = & get_instance();
        if($file != $CI->config->item('gallery_no_image'))
          
        $path = $CI->config->item('gallery_root');
//        $path = $_SERVER['DOCUMENT_ROOT'].$path;
		$path =  Yii::app()->basePath .'/..'.$path;
        return unlink($path.$folder.$file);
        
    }


}

?>
