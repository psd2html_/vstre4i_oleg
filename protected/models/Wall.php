<?php

/**
 * This is the model class for table "wall".
 *
 * The followings are the available columns in table 'wall':
 * @property integer $id
 * @property integer $user_id
 * @property string $text
 * @property string $date
 * @property integer $sender_id
 */
class Wall extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wall the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'wall';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, text, date, sender_id', 'required'),
			array('user_id, sender_id', 'numerical', 'integerOnly'=>true),
			array('text', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, text, date, sender_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'user' => array(self::BELONGS_TO, 'User','user_id'),
            'sender' => array(self::BELONGS_TO, 'User','sender_id')

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'text' => 'Text',
			'date' => 'Date',
			'sender_id' => 'Sender',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        $criteria->with = array('user','sender');
        $criteria->together = true;
		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('sender_id',$this->sender_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getWallRecords($userId)
    {
        $db = Yii::app()->db;

        $wall = $db->createCommand()
            ->select('wall.*, user.name AS sender_name')
            ->from($this->tableName())
            ->join('user', 'wall.sender_id = user.id')
            ->where('wall.user_id = ' . (int)$userId)
            ->order('date ASC')
            ->queryAll();

        return $wall;
    }

    public function getFormatedWallRecords($userId)
    {
        $records = $this->getWallRecords($userId);

        if (!$records) {
            return $records;
        }

        $levels = array();
        foreach ($records as $record) {
            $depth = $record['depth'];
            if (!isset($levels[$depth])) {
                $levels[$depth] = array();
            }
            $levels[$depth][$record['id']] = $record;
        }

        for ($i = count($levels) - 1; $i > 0; $i--) {
            foreach ($levels[$i] as $comment) {
                $parentId = $comment['parent_id'];
                if (!isset($levels[$i-1][$parentId]['children'])) {
                    $levels[$i-1][$parentId]['children'] = array();
                }
                $levels[$i-1][$parentId]['children'][$comment['id']] = $comment;
            }
        }

        $result = $levels[0];

        return $result;
    }

    public function sentRecord($senderId, $text, $userId, $parentId = 0)
    {
        $db = Yii::app()->db;

        $depth = 0;
        if ($parentId) {
            $depth = $db->createCommand()
                ->select('depth')
                ->from($this->tableName())
                ->where('id = ' . (int)$parentId)
                ->queryScalar();
            $depth++;
        }

        $columns = array(
            'user_id'   => (int)$userId,
            'text'      => trim($text),
            'sender_id' => (int)$senderId,
            'parent_id' => (int)$parentId,
            'depth'     => (int)$depth,
        );
        $rowCount = $db->createCommand()
            ->insert($this->tableName(), $columns);

        return $rowCount;
    }
}