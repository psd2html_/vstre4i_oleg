<?php

/**
 * This is the model class for table "group".
 *
 * The followings are the available columns in table 'group':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $type
 * @property string $date_start
 * @property string $date_end
 * @property integer $city_id
 * @property string $address
 * @property integer $seats
 * @property string $date_created
 * @property string $picture
 * @property integer $parent
 * @property string $appeal
 * @property string $background
 */
class Group extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Group the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, type, city_id, date_created', 'required'),
			array('city_id, seats, parent', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>150),
			array('description', 'length', 'max'=>1000),
			array('type', 'length', 'max'=>7),
			array('appeal', 'length', 'max'=>50),
			array('background', 'length', 'max'=>200),
			array('date_start, date_end, address, picture', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, description, type, date_start, date_end, city_id, address, seats, date_created, picture, parent, appeal, background', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'type' => 'Type',
			'date_start' => 'Date Start',
			'date_end' => 'Date End',
			'city_id' => 'City',
			'address' => 'Address',
			'seats' => 'Seats',
			'date_created' => 'Date Created',
			'picture' => 'Picture',
			'parent' => 'Parent',
			'appeal' => 'Appeal',
			'background' => 'Background',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('date_start',$this->date_start,true);
		$criteria->compare('date_end',$this->date_end,true);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('seats',$this->seats);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('picture',$this->picture,true);
		$criteria->compare('parent',$this->parent);
		$criteria->compare('appeal',$this->appeal,true);
		$criteria->compare('background',$this->background,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function createGroup($city,$name,$description,$appeal,$user_id,$keywords)
    {
        $city = City::model()->getCityID($city);
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `group` VALUES ('','".$name."','".$description."','group','','',$city,'','','".Date('Y-m-d H:i:s')."','','','".$appeal."','');");
        $rowCount=$command->execute();
        $group_id = $connection->lastInsertID;
        $command = $connection->createCommand("INSERT INTO `member` VALUES ('',$user_id,$group_id,'',1,".date('Y-m-d').");");
        $rowCount=$command->execute();
        $command = $connection->createCommand("INSERT INTO `group_admin` VALUES ('',$user_id,$group_id,1);");
        $rowCount=$command->execute();
        $command = $connection->createCommand("INSERT INTO `group_admin` VALUES ('',$user_id,$group_id,1);");
        $rowCount=$command->execute();
        $keywords = explode(',',$keywords);
        foreach ($keywords as $value) {
            $command = $connection->createCommand("INSERT INTO `group_keyword` VALUES ('',$group_id,$value);");
            $rowCount=$command->execute();
        }
        return $group_id;
    }
    public function joinGroup($id,$group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `member` (`user_id`,`group_id`,`member_type`,`confirm`) VALUES ($id,$group_id,'',0);");
        return $rowCount=$command->execute();
    }
    public function leaveGroup($id,$group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("DELETE FROM `member` WHERE `user_id`=$id AND `group_id`=$group_id");
        return $rowCount=$command->execute();
    }
    public function joinMeet($id,$group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `member` (`user_id`,`group_id`,`member_type`,`confirm`) VALUES ($id,$group_id,'',1);");
        return $rowCount=$command->execute();
    }
    public function leaveMeet($id,$group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("DELETE FROM `member` WHERE `user_id`=$id AND `group_id`=$group_id");
        return $rowCount=$command->execute();
    }
    public function isGroupAdmin($group_id){
        $connection = Yii::app()->db;
        $user_id = User::model()->getUserId();
        $command = $connection->createCommand("SELECT `admin` FROM `group_admin` WHERE `group_id`=$group_id AND `user_id`=$user_id");
        return $rowCount=$command->queryScalar();
    }
    public function getFilteredGroups($city,$keyword,$keywords)
    {
        if ($city!='') $city = " AND `city`.`name`='".$city."'";
        if ($keyword!='') $keyword = " AND `group`.`name` LIKE '%".$keyword."%'";
        if (count($keywords)>0) $keywords = " AND `keyword`.`name` IN ('".implode('\',\'', $keywords)."')"; else $keywords = '';
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT distinct `group`.`id` as `id`,`group`.`name` as `name`,`group`.`picture` as `picture`
        FROM `group`,`group_keyword`,`city`,`keyword`
        WHERE `group`.`city_id`=`city`.`id`
        AND `group_keyword`.`field_id`=`keyword`.`id`
        ".$keyword.$keywords."
        AND `group`.`id`=`group_keyword`.`group_id`
        AND `group`.`type`='group'
        ".$city);
        return $result=$command->query();
    }
    public function getRandomGroup($group_id)
    {
        $keywords = array();
        $keys = GroupKeyword::model()->getGroupKeywords($group_id);
        foreach($keys as $row) {
            $keywords[] = $row['id'];
        }
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT distinct `group`.`id` as `id`,`group`.`name` as `name`,`group`.`description` as `description`,`group`.`picture` as `picture`
                                                FROM `group`,`group_keyword`,`keyword`
                                                WHERE `group_keyword`.`group_id` = `group`.`id`
                                                AND `group_keyword`.`field_id` = `keyword`.`id`
                                                AND `group_keyword`.`field_id` IN (".implode(',', $keywords).")
                                                AND `group`.`id` !=$group_id
                                                AND `group`.`type`='group'
                                                ORDER BY Rand()
                                                LIMIT 1");
        return $rowCount=$command->query();
    }
    public function getRandomPaidGroup($group_id)
    {
        $keywords = array();
        $keys = GroupKeyword::model()->getGroupKeywords($group_id);
        foreach($keys as $row) {
            $keywords[] = $row['id'];
        }
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT distinct `group`.`id` as `id`,`group`.`name` as `name`,`group`.`description` as `description`,`group`.`picture` as `picture`
                                                FROM `group`,`group_keyword`,`keyword`,`paid_group`
                                                WHERE `group_keyword`.`group_id` = `group`.`id`
                                                AND `group_keyword`.`field_id` = `keyword`.`id`
                                                AND `group_keyword`.`field_id` IN (".implode(',', $keywords).")
                                                AND `group`.`id` !=$group_id
                                                AND `paid_group`.`group_id` = `group`.`id`
                                                AND `paid_group`.`date_to`>'".date('Y-m-d H:i:s')."'
                                                AND `group`.`type`='group'
                                                ORDER BY Rand()
                                                LIMIT 2");
        return $rowCount=$command->query();
    }
    public function createMeet($meet,$parent)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `group` VALUES ('','".$meet['name']."','".$meet['description']."','meetup','".$meet['date_start']."','".$meet['date_end']."',".$meet['city'].",'".$meet['address']."',".$meet['seats'].",'".Date('Y-m-d H:i:s')."','',".$parent.",'','');");
        $rowCount=$command->execute();
        return Yii::app()->db->getLastInsertId();
    }
    public function getMeetComments($meet_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `comment`.`text` as `text`,`comment`.`date` as `date`,`user`.`id` as `id`,`user`.`name` as `name` FROM `comment`,`user` WHERE `comment`.`user_id`=`user`.`id` AND `comment`.`meet_id`=$meet_id");
        $value = $command->query();
        return $value;
    }
    public function sentComment($meet_id,$text,$user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `comment` VALUES ('',".$user_id.",".$meet_id.",'".$text."','".Date('Y-m-d H:i:s')."');");
        return $rowCount=$command->execute();
    }
    public function getUserGroupList($user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `group`.`id` as `id`,`group`.`picture` as `picture`,`group`.`name` as `name` FROM `group`,`member` WHERE `member`.`group_id`=`group`.`id` AND `member`.`user_id`=$user_id");
        $value = $command->query();
        return $value;
    }
    public function getUserAdminGroupList($user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `group`.`id` as `id`,`group`.`picture` as `picture`,`group`.`name` as `name` FROM `group`,`member`,`group_admin` WHERE `member`.`group_id`=`group`.`id` AND `member`.`user_id`=$user_id AND `group_admin`.`group_id`=`group`.`id` AND `group_admin`.`user_id`=$user_id");
        $value = $command->query();
        return $value;
    }
    public function meetProposal($place,$date,$comment,$user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `meet_proposal` VALUES ('','".$place."','".$date."','".$comment."',".$user_id.",'".Date('Y-m-d')."');");
        return $rowCount=$command->execute();
    }
    public function getUserMeetsJson($user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `group`.`id` as `id`,`group`.`name` as `name`,`group`.`description` as `description`,`group`.`date_start` as `date`,`group`.`parent` as `parent` FROM `group`,`member` WHERE `group`.`id`=`member`.`group_id` AND `member`.`user_id`=$user_id AND `group`.`type`='meetup'");
        return $value=$command->query();
    }
    public function getMeetGroupID($meet_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `parent` FROM `group` WHERE `id`=$meet_id");
        return $value=$command->queryScalar();
    }
    public function getNextMeet($group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `date_start` FROM `group` WHERE `parent`=$group_id AND `date_created`>'".date('Y-m-d H:i:s')."' ORDER BY `date_start`");
        return $value=$command->queryScalar();
    }
    public function getGroupMeetsID($group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `id`,`date_start`,`name` FROM `group` WHERE `parent`=$group_id");
        return $value=$command->query();
    }
}