<?php

class GroupController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','creategroup','joingroup','leavegroup','createmeet','members','photos','meet','creatingmeet','sentcomment','meetproposal','joinmeet','leavemeet','getusermeetsjson','uploadphoto'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Group;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Group']))
		{
			$model->attributes=$_POST['Group'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

    /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Group']))
		{
			$model->attributes=$_POST['Group'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

    public function actionMeet($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Group']))
        {
            $model->attributes=$_POST['Group'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('meet',array(
            'model'=>$model,
        ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

    public function actionMembers($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Group']))
        {
            $model->attributes=$_POST['Group'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('members',array(
            'model'=>$model,
        ));
    }

    public function actionUploadPhoto()
    {
        var_dump($_POST['album_id']);
    }

    public function actionPhotos($id)
    {
        if (isset($_FILES) && isset($_POST['album_id'])) {

            Photoalbum::model()->imagesUpload($_FILES,$_POST['album_id'],User::model()->getUserId());
        }

        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Group']))
        {
            $model->attributes=$_POST['Group'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('photos',array(
            'model'=>$model,
        ));
    }

    public function actionCreatingMeet()
    {
        echo Group::model()->createMeet($_POST['meet'],$_POST['parent']);
    }

    public function actionCreateMeet($id)
    {

            $model=$this->loadModel($id);

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Group']))
            {
                $model->attributes=$_POST['Group'];
                if($model->save())
                    $this->redirect(array('view','id'=>$model->id));
            }

            $this->render('create-meet',array(
                'model'=>$model,
            ));

    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        /*$criteria = new CDbCriteria;
        $criteria->alias = 'member';
        $criteria->select = 'id, name, description, type, date_start, date_end, city_id, address, seats, date_created, picture, parent, appeal, background';
        $criteria->join = 'LEFT JOIN member ON member.group_id = group.id';
        $criteria->condition = 'favourite_type = 1';
        $sort=new CSort('Member');
        $sort->applyOrder($criteria);
        $sort->attributes = array(
            'member' => array(
                'asc' => 'member.confirm ASC',
                'desc' => 'member.confirm DESC'
            ),
        );*/

		$dataProvider=new CActiveDataProvider('Group'/*,array('criteria'=>$criteria,'sort'=>$sort)*/);
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Group('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Group']))
			$model->attributes=$_GET['Group'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Group the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Group::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Group $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='group-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    public function actionCreateGroup()
    {
        if ($_GET['city']) $city = $_GET['city'];
        if ($_GET['name']) $name = $_GET['name'];
        if ($_GET['description']) $description = $_GET['description'];
        if ($_GET['appeal']) $appeal = $_GET['appeal'];
        if ($_GET['user_id']) $user_id = $_GET['user_id'];
        if ($_GET['keywords']) $keywords = $_GET['keywords'];
        echo Group::model()->createGroup($city,$name,$description,$appeal,$user_id,$keywords);
    }
    public function actionJoinGroup(){
        if ($_POST['id']) $id = $_POST['id'];
        if ($_POST['group_id']) $group_id = $_POST['group_id'];
        Group::model()->joinGroup($id,$group_id);
    }
    public function actionLeaveGroup(){
        if ($_POST['id']) $id = $_POST['id'];
        if ($_POST['group_id']) $group_id = $_POST['group_id'];
        Group::model()->leaveGroup($id,$group_id);
    }
    public function actionJoinMeet(){
        if ($_POST['id']) $id = $_POST['id'];
        if ($_POST['group_id']) $group_id = $_POST['group_id'];
        Group::model()->joinMeet($id,$group_id);
    }
    public function actionLeaveMeet(){
        if ($_POST['id']) $id = $_POST['id'];
        if ($_POST['group_id']) $group_id = $_POST['group_id'];
        Group::model()->leaveMeet($id,$group_id);
    }
    public function actionSentComment()
    {
        Group::model()->sentComment($_POST['meet_id'],$_POST['text'],$_POST['user_id']);
    }
    public function actionMeetProposal()
    {
        Group::model()->meetProposal($_GET['place'],$_GET['date'],$_GET['comment'],$_GET['user_id']);
    }
    public function actionGetUserMeetsJson()
    {
        $result = '[';
        $i = 1;
        $dateReader = Group::model()->getUserMeetsJson($_GET['user_id']);
        foreach ($dateReader as $val) {
            $result .= '{ "date": "'.strtotime($val['date']).'000", "type": "meeting", "title": "'.$val['name'].'", "description": "'.$val['description'].'", "url": "'.Yii::app()->createUrl('site/index').'/group/meet/'.$val['parent'].'/?mid='.$val['id'].'" }';

                $result .= ',';

            $i++;
        }
        $len = strlen($result);
        $result = substr($result, 0, $len-1);
        $result .= ']';
        echo $result;
    }
}
