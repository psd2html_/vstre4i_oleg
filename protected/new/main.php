<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Vstre4i.kz',
    'theme'=>'vastre4i',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.services.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'2275658134zarkon',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		//'db'=>array(
		//	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		//),
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=vstre4i',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		// uncomment the following to use a MySQL database
		/*
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=testdrive',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		*/
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
        'loid' => array(
            'class' => 'ext.lightopenid.loid',
        ),
        'eauth' => array(
            'class' => 'ext.eauth.EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'services' => array( // You can change the providers and their classes.
/*                'google' => array(
                    'class' => 'GoogleOpenIDService',
                ),*/
                'yandex' => array(
                    'class' => 'YandexOpenIDService',
                ),
                'twitter' => array(
                    'class' => 'TwitterOAuthService',
                    'key' => 'yhhO0MTE64Wp9nPMLTQTA',
                    'secret' => 'wivmAwya6POyekPMybzogn34LYEviMJeKucAO07NQ',
                ),
                'facebook' => array(
                    'class' => 'FacebookOAuthService',
                    'client_id' => '303928756406490',
                    'client_secret' => '8caedbb43189a170b926058cd3adb6bc',
                ),
                'vkontakte' => array(
                    'class' => 'VKontakteOAuthService',
                    'client_id' => '3305404',
                    'client_secret' => 'woRyzyZlZVDCINgCBiXD',
                ),
                'mailru' => array(
                    'class' => 'MailruOAuthService',
                    'client_id' => '706856',
                    'client_secret' => '0a9f927663eecaae9387bdd72cd8271c',
                ),
                'odnoklassniki' => array(
                    'class' => 'OdnoklassnikiOAuthService',
                    'client_id' => '...',
                    'client_public' => '...',
                    'client_secret' => '...',
                    'title' => 'Однокл.',
                ),
            ),
        ),
        'request'=>array(
            'enableCookieValidation'=>true,
            'enableCsrfValidation'=>true,
        ),
        'urlManager'=>array(
            'class'=>'application.components.UrlManager',
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(
                '<language:(ru|ua|kz|be)>/' => 'site/index',
                '<language:(ru|ua|kz|be)>/<action:(contact|login|logout|about|faq|terms)>/*' => 'site/<action>',
                '<language:(ru|ua|kz|be)>/<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<language:(ru|ua|kz|be)>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<language:(ru|ua|kz|be)>/<controller:\w+>/<action:\w+>/*'=>'<controller>/<action>',
            ),
        ),
	),
    'sourceLanguage'=>'en',
    'language'=>'ua',
    'language'=>'ru',
    'language'=>'kz',
    'language'=>'be',
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
        'languages'=>array('ru'=>'Русский', 'ua'=>'Українська', 'kz'=>'Казахский', 'be'=>'Белорусский'),
	),
    'aliases' => array(
        'xupload' => 'ext.xupload'
    ),
);