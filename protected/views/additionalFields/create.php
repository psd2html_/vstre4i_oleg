<?php
/* @var $this AdditionalFieldsController */
/* @var $model AdditionalFields */

$this->breadcrumbs=array(
	'Additional Fields'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AdditionalFields', 'url'=>array('index')),
	array('label'=>'Manage AdditionalFields', 'url'=>array('admin')),
);
?>

<h1>Create AdditionalFields</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>