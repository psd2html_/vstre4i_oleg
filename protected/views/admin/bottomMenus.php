<ul class="tab-container">
    <li><a href="<?php echo CController::createUrl('admin/menus')?>" title="Верхнее меню">Верхнее меню</a></li>
    <li class="active"><a href="<?php echo CController::createUrl('admin/bottomMenus', array('type'=>'bottom'))?>" title="Нижнее меню">Нижнее меню</a></li>
</ul>
<div class="b-admin-content">
	<?php
	$columnTitles = array(
		'id'      => 'ID',
		'menu_id' => 'ID меню',
		'type'    => 'Тип',
		'title'   => 'Заголовок',
		'params'  => 'Параметры',
		'order_number' => 'Порядок',
	);
	?>
	<div class="b-admin-content">
		<?php if(!empty($menus)): ?>
			<?php echo CHtml::beginForm('saveMenus'); ?>
				<?php foreach($menus as $menu): ?>
					<?php if(!empty($menu)): ?>
						<table class="b-admin-table">
							<thead>
								<tr>
									<?php foreach(array_keys(current($menu)) as $title): ?>
										<th>
											<?php
											if ( isset($columnTitles[$title]) ):
												echo CHtml::encode($columnTitles[$title]);//Yii::t('var', $columnTitles[$title]);
											else:
												echo CHtml::encode($title);
											endif;
											?>
										</th>
									<?php endforeach; ?>
								</tr>
							</thead>
							<tbody>
								<?php foreach($menu as $element): ?>
									<tr>
										<?php foreach($element as $key => $field): ?>
											<td>
												<?php
												if($key == 'id'):
													echo CHtml::encode($field);
												else:
												?>
													<input type="text"
														   name="elements[<?php echo CHtml::encode($element['id']); ?>][<?php echo CHtml::encode($key); ?>]"
														   value="<?php echo CHtml::encode($field); ?>" />
												<?php endif;?>
											</td>
										<?php endforeach; ?>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					<?php endif; ?>
				<?php endforeach; ?>
				<div class="row submit">
					<?php echo CHtml::submitButton('Сохранить');//Yii::t('var', 'Сохранить'); ?>
				</div>
			<?php echo CHtml::endForm(); ?>
		<?php endif; ?>
	</div>
</div>
<script>
	function showTab(tab) {
		$(".tabs").addClass('no-display');
		$(".tabs[tab=tab"+tab+"]").removeClass('no-display');
	}

	$(document).ready(function(){
		var hash = window.location.hash ? window.location.hash : '#tab1';
		$( hash + '-cell' ).addClass('active');
		showTab(parseInt(hash.substr(-1)));
		$('.tab-container li').on('click',function(){
			if ( ! $(this).hasClass('active') && ! $(this).hasClass('gag') ) {
				$('.tab-container li').removeClass('active');
				$(this).addClass('active');
			}
		});
	});
</script>
