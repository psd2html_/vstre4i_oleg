<ul class="tab-container">
    <li class="active"><a href="<?php echo CController::createUrl('admin/index')?>" title="Настройки">Настройки</a></li>
    <li><a href="<?php echo CController::createUrl('admin/metaTags')?>" title="Метатеги">Метатеги</a></li>
</ul>
<div class="b-admin-content">
    <?php
    /* @var $this UserController */
    /* @var $model User */

    /* $this->breadcrumbs=array(
        'Users'=>array('index'),
        'Manage',
    ); */
    ?>

    <?php echo CHtml::form('','POST')?>
    <table class="b-form-table">
        <tr>
            <td><?php echo CHtml::label('Имя сайта','site_name'); ?></td>
            <td><?php echo CHtml::textField('default[site_name]',$data['site_name']['value']); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::label('Email админа','admin_email'); ?></td>
            <td><?php echo CHtml::textField('default[admin_email]',$data['admin_email']['value']); ?></td>
        </tr>
    </table>
    <div class="row submit">
        <?php echo CHtml::submitButton('Сохранить')?>
    </div>
    <?php CHtml::endForm(); ?>
</div><!-- .b-admin-content-->
