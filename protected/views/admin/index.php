<?php
/* @var $this AdminController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Административная панель',
);
$userId = User::model()->getUserId();
$isAdmin = Admin::model()->isSiteAdmin($userId);
$siteModeratorsModel = new SiteModerators();
$isSiteModerator = $siteModeratorsModel->isSiteModerator($userId);

$c_id  = Yii::app()->controller->id;
$a_id = Yii::app()->controller->action->id;

if ($isSiteModerator) {
    if ($c_id == 'admin' && $a_id == 'index') {
        $this->redirect('complaint/admin');
    }
    $this->breadcrumbs = array(
        'Административная панель (модератор)',
    );
}

?>


<div class="center admin">
    <nav class="side-left-menu" action="<?php CController::createUrl('admin/saveSettings')?>">
        <ol>
            <?php if($isAdmin): ?>
                <li <?php if( $c_id == 'admin' && $a_id == 'index' || $c_id == 'admin' && $a_id == 'metaTags'){echo 'class="active"';}?> >
                    <a href="<?php echo CController::createUrl('admin/index',array()) ?>">
                        <?php echo Yii::t('var','Общие настройки')?>
                    </a>
                </li>
                <li <?php if( $c_id == 'admin' && $a_id == 'menus'){echo 'class="active"';}?> >
                    <a href="<?php echo CController::createUrl('admin/menus',array()) ?>">
                        <?php echo Yii::t('var','Управление меню')?>
                    </a>
                </li>
                <li <?php if( $c_id == 'admin' && $a_id == 'payment'){echo 'class="active"';}?> >
                    <a href="<?php echo CController::createUrl('admin/payment',array()) ?>">
                        <?php echo Yii::t('var','Платежные системы')?>
                    </a>
                </li>
                <li <?php if($c_id == 'country' || $c_id == 'city'){echo 'class="active"';}?> >
                    <a href="<?php echo CController::createUrl('country/admin')?>">
                        <?php echo Yii::t('var','Города и страны')?>
                    </a>
                </li>
                <li <?php if($c_id == 'photo' || $c_id == 'photoalbum'){echo 'class="active"';}?> >
                    <a href="<?php echo CController::createUrl('photo/admin')?>">
                        <?php echo Yii::t('var','Фотографии')?>
                    </a>
                </li>
                <li <?php if($c_id == 'user'){echo 'class="active"';}?> >
                    <a href="<?php echo CController::createUrl('user/admin')?>">
                        <?php echo Yii::t('var','Пользователи')?>
                    </a>
                </li>
                <li <?php if( $c_id == 'admin' && $a_id == 'moderators'){echo 'class="active"';}?> >
                    <a href="<?php echo CController::createUrl('admin/moderators') ?>">
                        <?php echo Yii::t('var','Модераторы сайта')?>
                    </a>
                </li>
                <li <?php if($c_id == 'page'){echo 'class="active"';}?> >
                    <a href="<?php echo CController::createUrl('page/admin')?>">
                        <?php echo Yii::t('var','Страницы')?>
                    </a>
                </li>
                <li <?php if( $c_id == 'admin' && $a_id == 'smtp'){echo 'class="active"';}?> >
                    <a href="<?php echo CController::createUrl('admin/smtp',array()) ?>">
                        <?php echo Yii::t('var','Настройки Smtp')?>
                    </a>
                </li>
            <?php endif; ?>
            <li <?php if( $c_id == 'complaint'){echo 'class="active"';}?> >
                <a href="<?php echo CController::createUrl('complaint/admin',array()) ?>">
                    <?php echo Yii::t('var','Жалобы')?>
                </a>
            </li>
            <li <?php if( $c_id == 'group' && $a_id == 'admin'){echo 'class="active"';}?> >
                <a href="<?php echo CController::createUrl('group/admin',array()) ?>">
                    <?php echo Yii::t('var','Группы и Встречи')?>
                </a>
            </li>
            <li <?php if( $c_id == 'admin' && $a_id == 'grouppremoderation'){echo 'class="active"';}?> >
                <a href="<?php echo CController::createUrl('admin/grouppremoderation',array()) ?>">
                    <?php echo Yii::t('var','Премодерация групп')?>
                </a>
            </li>
            <?php if($isAdmin): ?>
                <li <?php if( $c_id == 'category' || $c_id == 'keyword'){echo 'class="active"';}?> >
                    <a href="<?php echo CController::createUrl('keyword/admin',array()) ?>">
                        <?php echo Yii::t('var','Ключевые слова')?>
                    </a>
                </li>
                <li <?php if( $c_id == 'wall' || $c_id == 'comment'){echo 'class="active"';}?> >
                    <a href="<?php echo CController::createUrl('wall/admin',array()) ?>">
                        <?php echo Yii::t('var','Комментарии')?>
                    </a>
                </li>
                <li <?php if( $c_id == 'mail'){echo 'class="active"';}?> >
                    <a href="<?php echo CController::createUrl('mail/sendEmailToAll',array()) ?>">
                        <?php echo Yii::t('var','Рассылка')?>
                    </a>
                </li>
            <?php endif; ?>
        </ol>
    </nav>
        <?php echo $content; ?>

