<?php

?>
<div class="b-admin-content">
    <div class="b-moderators-search">
        <div><strong>Найти пользователя по имени и <br />предложить ему стать модератором сайта:</strong></div>
        <div class="b-moderators-search__button">Найти</div>
        <div class="b-moderators-search__names">
            <input type="text" class="b-moderators-search__input" size="40"
                   placeholder="Введите имя пользователя" value="" />
            <div class="b-moderators-search__answer"></div>
        </div><!-- .b-moderators-search__names-->
    </div><!-- .b-moderators-search-->

    <?php
    $moderators = $model->getModerators();
    if (!empty($moderators)) {
        echo '<br /><h4><strong>Модераторы:</strong></h4><br />';
    }
    foreach ($moderators as $moderator):
        $moderatorPic = $moderator['user_avatar']
            ?  CHtml::encode($moderator['user_avatar'])
            : 'no-pic.png';
        ?>
        <div class="b-moderator">
            <?php
            $moderatorLink = Yii::app()->createUrl('site/index') . '/user/' . urlencode($moderator['user_id']);
            $fireLinkId = 'fire-' . urlencode($moderator['user_id']);
            $nameId = 'modName-' . urlencode($moderator['user_id']);
            ?>
            <div class="b-moderator__info">
                <a href="<?php echo $moderatorLink; ?>" target="_blank">
                    <img src="/images/user/mini_<?php echo CHtml::encode($moderatorPic); ?>" class="b-moderator__pic" />
                    <br />
                    <span id="<?php echo CHtml::encode($nameId); ?>"><?php echo CHtml::encode($moderator['user_name']); ?></span>
                </a>
            </div><!-- .b-moderator__info-->
            <div class="b-moderator__button">
                <span class="span-link b-moderator__fire"
                    id="<?php echo CHtml::encode($fireLinkId); ?>">Снять с <br />должности</span>
            </div><!-- .b-moderator__button-->
            <div style="clear: both;"></div>
        </div><!-- .b-moderator-->
    <?php endforeach; ?>

    <?php
    $requests = $model->getRequests();
    if (!empty($requests)):
        echo '<br /><hr style="margin: 0;" />';
        echo '<div class="b-moderators-requests">';
        echo '<h4 class="b-moderators-requests__caption">'
            . '<strong>Отправленные заявки:</strong></h4>'
            .'<h4 class="b-moderators-requests__caption">'
            . 'У пользователей есть 24 часа чтобы ответить на заявку.</h4>';
    endif;
    foreach($requests as $request):
        $userPic = $request['user_avatar']
            ? CHtml::encode($request['user_avatar'])
            : 'no-pic.png';
        $denyRequestLinkId = 'denyRequest-' . CHtml::encode($request['user_id']);
        ?>
        <div class="b-moderator">
            <?php $userLink = Yii::app()->createUrl('site/index') . '/user/' . urlencode($request['user_id']); ?>
            <div class="b-moderator__info">
                <a href="<?php echo $userLink; ?>" target="_blank">
                    <img src="/images/user/mini_<?php echo CHtml::encode($userPic); ?>" class="b-moderator__pic" />
                    <br />
                    <?php echo CHtml::encode($request['user_name']); ?>
                </a>
            </div><!-- .b-moderator__info-->
            <div class="b-moderator__button">
                <span class="span-link b-moderator__deny-request"
                      id="<?php echo CHtml::encode($denyRequestLinkId); ?>">Отменить заявку</span>
            </div><!-- .b-moderator__button-->
            <div style="clear: both;"></div>
        </div><!-- .b-moderator-->
    <?php
    endforeach;
    if (!empty($requests)):
        echo '</div><!-- .b-moderators-requests-->';
    endif;
    ?>

</div><!-- .b-admin-content-->
<script>
    function makeModeratorRequest(linkTag)
    {
        var id = $(linkTag).attr('id').split('-');
        var userId = id[1];

        var url = '<?php echo Yii::app()->createUrl('admin/makeModeratorRequest'); ?>';
        var CsrfToken = '<?php echo Yii::app()->request->csrfToken;?>';

        window.location.href = url + '?YII_CSRF_TOKEN=' + CsrfToken + '&user_id=' + userId;
    }
    $().ready(function() {
        $('.b-moderator__deny-request').click(function(){
            var id = $(this).attr('id').split('-');
            var userId = id[1];

            var url = '<?php echo Yii::app()->createUrl('admin/denyModeratorRequest'); ?>';
            var CsrfToken = '<?php echo Yii::app()->request->csrfToken;?>';

            window.location.href = url + '?YII_CSRF_TOKEN=' + CsrfToken + '&user_id=' + userId;
        });
        $('.b-moderator__fire').click(function(){
            var id = $(this).attr('id').split('-');
            var userId = id[1];
            var name = $('#modName-' + userId).text();

            var url = '<?php echo Yii::app()->createUrl('admin/fireModerator'); ?>';
            var CsrfToken = '<?php echo Yii::app()->request->csrfToken;?>';

            if (confirm('Снять с должности пользователя "' + name + '"?')) {
                window.location.href = url + '?YII_CSRF_TOKEN='
                    + CsrfToken + '&user_id=' + userId;
            }
        });
        $('.b-moderator__make-request-link').click(function(){
            makeModeratorRequest(this);
        });
        $('.b-moderators-search__button').click(function(){
            $('.b-moderators-search__input').keyup();
        });
        var searchAnswerTag = $('.b-moderators-search__answer');
        $('.b-moderators-search').focusout(function(){
            $('.b-moderators-search__answer').delay(100).fadeOut('fast');
        });
        $('.b-moderators-search__input').focusin(function(){
            if (searchAnswerTag.text()) {
                searchAnswerTag.fadeIn('fast');
            }
        }).keyup(function(){
            $.ajax({
                type: 'GET',
                url : '<?php echo Yii::app()->createUrl('ajax/findUsers'); ?>',
                data: {
                    YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>',
                    username: $(this).val()
                },
                success: function(data){
                    var answer = '';
                    if (data != 0) {
                        var users = JSON.parse(data);
                        $(users).each(function(index,user){
                            if ($(user.name).html()) {
                                user.name = $(user.name).html();
                            }
                            answer += '<div class="b-moderators-search__answer-unit">'
                                + '<span class="span-link b-moderator__add-link b-moderators-search__add"'
                                + ' id="makeModReq-' + parseInt(user.id) + '">Предложить</span>'
                                + '<div class="b-moderators-search__answer-unit-name">'
                                + '<a href="<?php echo Yii::app()->createUrl('user/view'); ?>/'
                                + parseInt(user.id) + '" target="_blank">' + user.name + '</a></div>'
                                + '</div>';
                        });
                    }
                    if (answer === '') {
                        answer = '<div class="b-moderators-search__answer-unit">Нет результатов</div>';
                    } else if (users.length > 14) {
                        answer += '<div class="b-moderators-search__answer-unit">'
                            + '<div class="b-moderators-search__answer-unit-name">...</div></div>';
                    }
                    $('.b-moderators-search__answer').html(answer).fadeIn('fast');
                    $('.b-moderators-search__add').click(function(){
                        makeModeratorRequest(this);
                    });
                }
            });
        });

    });
</script>

