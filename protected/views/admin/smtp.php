<ul class="tab-container">
    <li class="active"><a href="<?php echo CController::createUrl('admin/smtp')?>" title="Smtp">Smtp</a></li>

</ul>
<div class="b-admin-content">
    <?php echo CHtml::beginForm('','POST',array('id'=>'payment-settings-form'))?>
    <table class="b-form-table">
        <tr>
            <td>
                <?php echo CHtml::label('Хост','mailer_host')?>
            </td>
            <td>
                <?php echo CHtml::textField('smtp[mailer_host]',$data['mailer_host']['value'],array('id'=>'mailer_host'))?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo CHtml::label('Имя пользователя','mailer_username')?>
            </td>
            <td>
                <?php echo CHtml::textField('smtp[mailer_username]',$data['mailer_username']['value'],array('id'=>'mailer_username'))?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo CHtml::label('Пароль','mailer_password')?>
            </td>
            <td>
                <?php echo CHtml::textField('smtp[mailer_password]',$data['mailer_password']['value'],array('id'=>'mailer_password'))?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo CHtml::label('Порт','mailer_port')?>
            </td>
            <td>
                <?php echo CHtml::textField('smtp[mailer_port]',$data['mailer_port']['value'],array('id'=>'mailer_port'))?>
            </td>
        </tr>
    </table>
    <div class="row submit"><?php echo CHtml::submitButton('Сохранить'); ?></div>
    <?php echo CHtml::endForm()?>
</div><!-- .b-admin-content-->
