<ul class="tab-container">
    <li><a href="<?php echo CController::createUrl('admin/menus')?>" title="Верхнее меню">Верхнее</a></li>
    <li class="active"><a href="<?php echo CController::createUrl('bottomMenus/admin')?>" title="Нижнее меню">Нижнее</a></li>
</ul>
<div class="b-admin-content">
<?php
/* @var $this CityController */
/* @var $model City */

$this->menu=array(
	array('label'=>'Создать пункт меню', 'url'=>array('create')),
);
    $this->widget('zii.widgets.CMenu', array(
        'items'=>$this->menu,
        'htmlOptions'=>array('class'=>'operations b-admin-buttons'),
    ));

?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'menus-grid',
	'summaryText'=>'',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array('name'=>'title',
              'value' => '$data->title',
             // 'header' => 'Название'
        ),
		array('name'=>'page_id',
              'value' => '$data->page->alias',
              //'header' => 'Страница'
        ),
		array('name'=>'order_number',
              'value' => '$data->order_number',
              //'header' => 'order_number'
        ),
		array(
			'class'=>'CButtonColumn',
            'template'=>'{delete}{update}',
		),
	),
)); ?>
</div>
