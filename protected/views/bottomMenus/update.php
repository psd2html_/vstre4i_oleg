<?php
/* @var $this MenuController */
/* @var $model Menu */

$this->breadcrumbs=array(
	'Меню'=>array('admin'),
	'Обновить',
);
?>

<h1>Обновить пункт меню</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
