<?php
/* @var $this MenuController */
/* @var $model Menu */

$this->breadcrumbs=array(
	'Меню'=>array('admin'),
	$model->title,
);
?>

<h1>Просмотр меню</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            'name' => 'title',
            'label' =>'Название'
        ),
	),
)); ?>
