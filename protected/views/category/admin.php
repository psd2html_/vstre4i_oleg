<ul class="tab-container">
    <li><a href="<?php echo CController::createUrl('keyword/admin')?>" title="Слова">Слова</a></li>
    <li class="active"><a href="<?php echo CController::createUrl('category/admin')?>" title="Категории">Категории</a></li>
</ul>
<div class="b-admin-content">
    <?php

/* @var $this CategoryController */
/* @var $model Category */
$this->menu=array(
	array('label'=>Yii::t('var','Create Category'), 'url'=>array('create')),
);
    $this->widget('zii.widgets.CMenu', array(
        'items'=>$this->menu,
        'htmlOptions'=>array('class'=>'operations b-admin-buttons'),
    ));


?>

<?php $data = $model->search();

?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'category-grid',
	'dataProvider'=>$data,
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
            'name'=>'name',
            'header' => Yii::t('var','name')
        ),
        array(
            'name'=>'ua',
            'value'=> '@$data->translate[0]->language == "ua"?@$data->translate[0]->value:(@$data->translate[1]->language == "ua"?@$data->translate[1]->value:@$data->translate[2]->value)',
            'header' => Yii::t('var','ua')
        ),
        array(
            'name'=>'kz',
            'value'=> '@$data->translate[0]->language == "kz"?@$data->translate[0]->value:(@$data->translate[1]->language == "kz"?@$data->translate[1]->value:@$data->translate[2]->value)',
            'header' => Yii::t('var','kz')
        ),
        array(
            'name'=>'be',
            'value'=> '@$data->translate[0]->language == "be"?@$data->translate[0]->value:(@$data->translate[1]->language == "be"?@$data->translate[1]->value:@$data->translate[2]->value)',
            'header' => Yii::t('var','be')
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

</div>
