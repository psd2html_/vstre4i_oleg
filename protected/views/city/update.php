<?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs=array(
	'Города'=>array('admin'),
	$model->name=>array('view','id'=>$model->id),
	'Редактировать',
);
?>

<h1>Обновить город: <?php echo CHtml::encode($model->name); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'trModels' => $trModels)); ?>
