<?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs=array(
	'Города'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List City', 'url'=>array('index')),
	array('label'=>'Create City', 'url'=>array('create')),
	array('label'=>'Update City', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete City', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>(int) $model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage City', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('var','View City #').CHtml::encode($model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
            'name' => 'country_id',
            'label' => Yii::t('var','country_id')
        ),
        array(
            'name' => 'name',
            'label' => Yii::t('var','name')
        ),
        array(
            'name' => 'ua',
            'label' => Yii::t('var','ua')
        ),
        array(
            'name' => 'kz',
            'label' => Yii::t('var','be')
        ),
        array(
            'name' => 'country_id',
            'label' => Yii::t('var','country_id')
        ),
	),
)); ?>
