<?php
/* @var $this CommentController */
/* @var $model Comment */
?>

<h1><?php echo Yii::t('var','View Comment #').' '.CHtml::encode($model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
            'name' => 'user_id',
            'header' => Yii::t('var','user_id')
        ),
        array(
            'name' => 'meet_id',
            'header' => Yii::t('var','meet_id')
        ),
        array(
            'name' => 'text',
            'header' => Yii::t('var','text')
        ),array(
            'name' => 'date',
            'header' => Yii::t('var','date')
        ),
	),
)); ?>
