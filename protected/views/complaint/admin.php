
<ul class="tab-container">
    <li class="active"><a href="<?php echo CController::createUrl('complaint/admin')?>" title="<?php echo Yii::t('var','Жалобы')?>"><?php echo Yii::t('var','Жалобы')?></a></li>
</ul>
<div class="b-admin-content">
<?php
/* @var $this ComplaintController */
/* @var $model Complaint */

?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'complaint-grid',
	'summaryText'=>'Количество жалоб: {count}',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		/*'id',*/
        array(
            'name' => 'text',
            'header' => Yii::t('var','text')
        ),
        /*array(
            'name'=>'name',
            'value' => 'CHtml::encode($data->group->name)',
            'header' => Yii::t('var','name')

        ),*/
      array(
            'header' => Yii::t('var','type'),
            'name' => 'text_id',
			'value'=>function($data, $i){				
				return Complaint::getType($data->text_id);
			},
            'type' => 'html',
			'filter'=>CHtml::dropDownList('Complaint[text_id]', $model->text_id,  
                array(
					'user'=>'На пользователей',
					'group'=>'На группы',
					'meet'=>'На встречи',
				), array('empty' => 'Выбор...')
            ),
        ), 
		array(
			'name'=>'item_id',
			'value'=>function($data, $i){				
				return $data->{$data->text_id}->name;
			},
           'type'=>'html',
           'header' => Yii::t('var','name'),
        ),
        /*array(
            'name'=>'type',
            'value' => '$data->group->type',
            'header' => Yii::t('var','type')
                ),
        array(
            'name'=>'user_id',
            'header' => Yii::t('var','user_id')
        ),*/
		array(
			'class'=>'CButtonColumn',
            'template'=>'{view}{delete}{update}',
            'buttons' => array(
				'view' => array(
					'url' => function($data, $i){
						$id = $data->parent_id > 0 ? $data->parent_id :  $data->id;
						return Yii::app()->createAbsoluteUrl('user/checkAnswer', array('complaint'=>$id, '#' => 'comment-'.$data->id)); 
					}
				),
            ),
		),
        array(
            'name' => 'date',
            'header' => Yii::t('var','date'),
			'filter'=>false,
        ),
	),
)); ?>
</div>
