<?php
/* @var $this ComplaintController */
/* @var $model Complaint */

$this->breadcrumbs=array(
	'Жалобы'=>array('admin'),
	'Создать',
);

$this->menu=array(
	array('label'=>'List Complaint', 'url'=>array('index')),
	array('label'=>'Manage Complaint', 'url'=>array('admin')),
);
?>

<h1>Создать жолобу</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
