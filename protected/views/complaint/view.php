<?php
/* @var $this ComplaintController */
/* @var $model Complaint */

$this->breadcrumbs=array(
	'Жалобы'=>array('admin'),
	$model->id,
);

?>

<h1><?php echo Yii::t('var','View Complaint #').' '.CHtml::encode($model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            'name'=>'item_id',
            'label'=>Yii::t('var','text_id_gr'),
        ),
		array(
            'name' => 'date',
            'label'=>Yii::t('var','date'),
        ),
        array(
            'name' => 'text',
            'label'=>Yii::t('var','text'),
        ),
	),
)); ?>
