<?php
/* @var $this CountryController */
/* @var $model Country */

$this->breadcrumbs=array(
	'Страны'=>array('admin'),
	'Обновить',
);

$this->menu=array(
	array('label'=>'List Country', 'url'=>array('index')),
	array('label'=>'Create Country', 'url'=>array('create')),
	array('label'=>'View Country', 'url'=>array('view', 'id'=>(int)$model->id)),
	array('label'=>'Manage Country', 'url'=>array('admin')),
);
?>

<h1>Обновление страны №<?php echo CHtml::encode($model->id); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'trModels' => $trModels)); ?>
