<?php
/* @var $this CountryController */
/* @var $model Country */

$this->breadcrumbs=array(
	'Страны'=>array('admin'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Country', 'url'=>array('index')),
	array('label'=>'Create Country', 'url'=>array('create')),
	array('label'=>'Update Country', 'url'=>array('update', 'id'=>(int)$model->id)),
	array('label'=>'Delete Country', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>(int)$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Country', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('var','View Country #').CHtml::encode($model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
            'name' =>'name',
            'label' => Yii::t('var','Название')
        ),
        array(
            'name' => 'ua',
            'label' => Yii::t('var','ua')
        ),
        array(
            'name' => 'kz',
            'label' => Yii::t('var','ua')
        ),
        array(
            'name' => 'be',
            'label' => Yii::t('var','ua')
        ),
	),
)); ?>
