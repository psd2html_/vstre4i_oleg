<?php
/* @var $this GroupController */
/* @var $model Group */
/* @var $form CActiveForm */

Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/eye.js') //js
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/utils.js') //js
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/layout.js') //js
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/jscolor.js'); //js

?>

<div class="form b-group-admin-form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'group-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    )); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,Yii::t('var','Название'),array('class'=>'b-group-admin-form__label')); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>150,'class'=>'input-border search b-group-admin-form__field')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,Yii::t('var','Описание'),array('class'=>'b-group-admin-form__label')); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>1000,'class'=>'input-border search b-group-admin-form__field')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,Yii::t('var','Город'),array('class'=>'b-group-admin-form__label')); ?>
		<?php echo $form->textField($model,'city_id',array('size'=>60,'maxlength'=>1000,'class'=>'input-border search b-group-admin-form__field')); ?>
		<?php echo $form->error($model,'city_id'); ?>
	</div>

	<div class="row" style="height: 111px;">
		<?php echo $form->labelEx($model,Yii::t('var','Адрес'),array('class'=>'b-group-admin-form__label'),array('style'=>'display: table-cell;float: right;vertical-align: middle;')); ?>
		<?php echo $form->textArea($model,'address',array('rows'=>6, 'cols'=>50,'class'=>'input-border b-group-admin-form__field')); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row" style="height: 25px; padding: 15px 0;">
		<?php echo $form->labelEx($model,Yii::t('var','Картинка'),array('class'=>'b-group-admin-form__label')); ?>
		<?php echo $form->fileField($model,'picture',array('rows'=>6, 'cols'=>50,'class'=>'input-border search b-group-admin-form__field')); ?>
		<?php echo $form->error($model,'picture'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,Yii::t('var','Фон'),array('class'=>'b-group-admin-form__label')); ?>
		<?php echo $form->textField($model,'background',array('size'=>60,'maxlength'=>200,'class'=>'input-border search color b-group-admin-form__field')); ?>
		<?php echo $form->error($model,'background'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,Yii::t('var','Скрыть группу'),array('class'=>'b-group-admin-form__label')); ?>
        <?php echo $form->checkBox($model,'hided',array('class'=>'b-group-admin-form__checkbox')); ?>
        <?php echo $form->error($model,'hided'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'Уведомлять о	поступлении заявок на вступление в группу',array('class'=>'b-group-admin-form__label')); ?>
        <?php echo $form->checkBox($model,'member_notise',array('class'=>'b-group-admin-form__checkbox')); ?>
        <?php echo $form->error($model,'member_notise'); ?>
    </div>
	
    <div class="row">
        <?php echo $form->labelEx($model,Yii::t('var','Получать уведомления об комментариях'),array('class'=>'b-group-admin-form__label')); ?>
        <?php echo $form->checkBox($model,'comment_notice',array('class'=>'b-group-admin-form__checkbox')); ?>
        <?php echo $form->error($model,'comment_notice'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,Yii::t('var','Получать уведомления об фотографиях'),array('class'=>'b-group-admin-form__label')); ?>
        <?php echo $form->checkBox($model,'photo_notice',array('class'=>'b-group-admin-form__checkbox')); ?>
        <?php echo $form->error($model,'photo_notice'); ?>
    </div>


    <br />

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : Yii::t('var','Сохранить'), array('class'=>'b-group-admin-form__button')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
