<?php
/* @var $this GroupController */
/* @var $data Group */
?>
<?
$required = CHtml::encode($data->required);
?>

<tr>
    <td><?php echo CHtml::encode($data->name); ?></td>
    <td><?php echo CHtml::encode($data->description); ?></td>
    <td><? if ($required == 1) echo 'Да'; else echo 'Нет';?></td>
    <td class="b-add-fields__delete"><button onclick="deleteAdditionalField(<? echo CHtml::encode($data->id); ?>);">Удалить</button></td>
</tr>