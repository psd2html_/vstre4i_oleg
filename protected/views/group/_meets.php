<?php
$user_id = User::model()->getUserId();

$groupId = Group::model()->getMeetGroupID($data->id);
?>
<div class="content-block" style="padding-bottom: 0;">
    <h3 class="align-center" style="position: relative">
        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].Yii::app()->createUrl('site/index').'/group/meet/'.urlencode($groupId).'/?mid='.CHtml::encode($data->id); ?>"><? echo CHtml::encode($data->name); ?></a>
        <?php if (Group::isGroupAdminS($groupId, $user_id)): ?>
            <a class="edit-meet" href="<?php echo $this->createUrl('group/updatingMeet', array('id' => $data->id)) ?>">Редактировать</a>
        <?php endif; ?>
    </h3>
    <div class="data"><?php echo CHtml::encode(date('d.m.Y',strtotime($data->date_start))); ?></div>
    <div class="time">
        <?php
        if ($data->date_end == '0000-00-00 00:00:00') {
            echo 'Любое время';
        } else {
            echo CHtml::encode(date('H:i',strtotime($data->date_start)));
        }
        ?>
    </div>
    <div class="place"><?php
        $city = City::model()->getCity($data->city_id);
        $address = 'г. ' . CHtml::encode($city) . ', ' . CHtml::encode($data->address);
        ?>
        <a href="http://www.google.ru/maps/search/<?php echo urlencode($address); ?>"
           target="_blank" class="place-link"><?php echo CHtml::encode($address); ?></a>
    </div>

    <div class="clear"></div>
    <div class="avatars">
        <?php
        $members = Member::model()->getGroupMembers($data->id);
        foreach($members as $member):
            $avatar = $member['avatar'];
            if (!$avatar) {
                $avatar = 'no-pic.png';
            }
        ?>
            <div class="b-avatar">
                <a href="<?php echo Yii::app()->createUrl('site/index').'/user/'.urlencode($member['id']);?>" target="_blank">
                    <img src="/images/user/mini_<?php echo CHtml::encode($avatar); ?> " />
                    <?php echo CHtml::encode($member['name']); ?>
                </a>
            </div>
        <?php endforeach;// class="f-left"?>
    </div>
    <div class="clear"></div>
    <p><?php echo CHtml::encode($data->description); ?></p>
    <? if ($user_id!=0 && false) { ?>
    <a mid="<?php echo CHtml::encode($data->id); ?>" onclick="$('#complaint').arcticmodal();" title="пожаловаться на событие" class="show-all-participants complain-event align-center f-right">пожаловаться на событие</a>
    <? } ?>
</div>
