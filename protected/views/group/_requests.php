<?php
/* @var $this GroupController */
/* @var $data Group */
?>

<div class="request">
    <?php
    $picture = User::model()->getPicture($data->user_id);
    if ($picture == '' || $picture == NULL) {
        $picture = '/images/user/mini_no-pic.png';
    } else {
        $picture = "/images/user/mini_".$picture;
    }
    ?>
	
	<div class="b-request">
		<a href="<?php echo Yii::app()->createUrl('site/index').'/user/'.urlencode($data->user_id); ?>" target="_blank">
			<?php
            $userName = User::model()->getName($data->user_id);
            echo CHtml::encode($userName);
            ?><br />
			<img src="<?php echo CHtml::encode($picture); ?>" width="100">
		</a>
  
        <div>
            <button onclick="acceptRequest(<?php echo (int)$data->id; ?>)" class="b-requests__button">
                <?php echo Yii::t('var', 'Принять'); ?>
            </button>
            <button onclick="refuseRequest(<?php echo (int)$data->id; ?>)" class="b-requests__button">
                <?php echo Yii::t('var', 'Отклонить'); ?>
            </button>
            <button onclick="addToBlackList(<?php echo (int)$data->id; ?>)" class="b-requests__button">
                <?php echo Yii::t('var', 'В чёрный список'); ?>
            </button>
        </div>
        <?php
        $dataReader = AdditionalFields::model()->getUserAdditionalField($data->user_id,$data->group_id);
        if(count($dataReader)>0):
        ?>
            <br />Вопрос/ответ:<br />
        <?php endif; ?>

        <div style="height: 100px; overflow-y: scroll">
            <table>
            <?php foreach($dataReader as $row): ?>
                <tr><td><?php echo CHtml::encode($row['value']); ?></td></tr>
            <?php endforeach; ?>
            </table>
		</div>
    </div><!-- .b-request-->
    <!--<b><?php /*echo CHtml::encode($data->getAttributeLabel('id')); */?>:</b>
    <?php /*echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); */?>
    <br />-->
</div>

