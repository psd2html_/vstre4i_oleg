<div class="event border-radius-5 odd" onclick="window.location.href='<? echo Yii::app()->createUrl('site/index').'/group/'.urlencode($data->id); ?>'">
    <div class="event-img f-left"><img style="height: 68px;width: 93px;" src="<?php $pic = CHtml::encode($data->picture); if ($pic != NULL && $pic != '') echo '/images/group/mini_'.CHtml::encode($data->picture); else echo '/images/group/mini_no-pic.png'; ?>" alt="event"></div>
    <div class="event-name f-left"><a href="<?php echo Yii::app()->createUrl('site/index').'/group/'.urlencode($data->id); ?>"><? echo CHtml::encode($data->name); ?></a></div>
    <div class="event-qty f-left">Участников: <?php $model=new Member; echo ' '.CHtml::encode($model->getGroupMembersCount($data->id)); ?></div>
    <div class="event-date f-left">
        <?
        $meet = Group::model()->getNextMeet($data->id);
        if ($meet != 0) {
            echo 'ближайшая встреча '.CHtml::encode(date('d.m.Y',strtotime($meet)));
        } else {
            echo 'ближайших встреч нет';
        }
        ?>
    </div>
</div>