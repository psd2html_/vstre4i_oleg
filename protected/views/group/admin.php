<ul class="tab-container">
    <li <?php if(!$meet)echo 'class="active"'?>><a href="<?php echo CController::createUrl('group/admin')?>" title="<?php echo Yii::t('vat','Группы')?>"><?php echo Yii::t('vat','Группы')?></a></li>
    <li <?php if($meet) echo 'class="active"'?>><a href="<?php echo CController::createUrl('group/admin',array('type'=>'meet'))?>" title="<?php echo Yii::t('vat','Встречи')?>"><?php echo Yii::t('vat','Встречи')?></a></li>
</ul>
<div class="b-admin-content">
    <?php
	
$user_id = User::model()->getUserId();
$this->breadcrumbs=array(
	'Групы',
);
    /* @var $this UserController */
    /* @var $model User */
if($meet){
    /* $this->menu=array(
        array('label'=>Yii::t('var','Create Meet'), 'url'=>Yii::app()->createUrl('group/create')),
    ); */
} else {
    $this->menu=array(
        array('label'=>Yii::t('var','Create Group'), 'url'=>Yii::app()->createUrl('site/index').'/site/page/view/create-group'),
    );
}

    $this->widget('zii.widgets.CMenu', array(
        'items'=>$this->menu,
        'htmlOptions'=>array('class'=>'operations b-admin-buttons'),
    ));


$groupColumns = array(
    /*'id',*/
    array(
        'name' => 'name',
        'header' => Yii::t('var','name')
    ),
    array(
        'name' => 'description',
        'header' => Yii::t('var','description')
    ),
    array(
        'name' => 'city_id',
        'header' => Yii::t('var','city'),
        'value' => 'Country::model()->getCountryByCityID($data->city_id)." -> ".City::model()->getCity($data->city_id);',
    ),
    /*array(
        'name' => 'address',
        'header' => Yii::t('var','address')
    ),*/
    array(
        'name' => 'date_created',
        'header' => Yii::t('var','date_created')
    ),
/*    array(
        'name' => 'picture',
        'header' => Yii::t('var','picture')
    ),
    array(
        'name' => 'background',
        'header' => Yii::t('var','background')
    ),*/
    array(
        'class'=>'CButtonColumn',
    ),
);

$meetColumns = array(
    /*'id',*/
    array(
        'name' => 'name',
        'header' => Yii::t('var','name')
    ),
    array(
       'name' => 'description',
        'header' => Yii::t('var','description')
    ),
    array(
        'name' => 'seats',
        'header' => Yii::t('var','seats')
    ),
    array(
        'header' => Yii::t('var','Количество участников'),
        'value' => 'Member::model()->getRequestsCount($data->id)',
    ),
    array(
        'name' => 'parent',
        'header' => Yii::t('var','parentGroup'),
        'value' => 'CHtml::link(Group::model()->getGroupName($data->id),Yii::app()->createUrl("group/$data->id"))',
        'type'=>'html',
    ),
    array(
        'name' => 'city_id',
        'header' => Yii::t('var','city'),
        'value' => 'Country::model()->getCountryByCityID($data->city_id)." -> ".City::model()->getCity($data->city_id);',
    ),
    /*array(
        'name' => 'address',
        'header' => Yii::t('var','address')
    ),*/
    array(
        'name' => 'date_created',
        'header' => Yii::t('var','date_created')
    ),
/*    array(
        'name' => 'date_start',
        'header' => Yii::t('var','date_start')
    ),
    array(
        'name' => 'date_end',
        'header' => Yii::t('var','date_end')
    ),*/
    /* array(
        'class'=>'CButtonColumn',
    ), */
	array(
		'class'=>'CButtonColumn',
		'template'=>'{view}{delete}{update}',
		'buttons' => array(
			'view' => array(
				'url' => function($data, $i){
					//Yii::log(print_r($i));
					//Yii::log(print_r($n));
					//Yii::log($n);
					return Yii::app()->createAbsoluteUrl('group/meet', array('id'=>$data->parent)).'/?mid='.CHtml::encode($data->id);
				},
			),
			'update' => array(
				'url' => function($data, $i){
					//Yii::log(print_r($i));
					//Yii::log(print_r($n));
					//Yii::log($n);
					return Yii::app()->createAbsoluteUrl('group/updatingMeet', array('id'=>$data->id));
				},
			),
		),
	),
);

//$groupId = Group::model()->getMeetGroupID($data->id);
if(!$meet){
    $columns = $groupColumns;
} else {
    $columns = $meetColumns;
}
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'summaryText'=>'Всего: {count}',
	'id'=>'group-grid',
	'dataProvider'=>$model->search($meet),
	'filter'=>$model,
	'columns'=> $columns
)); ?>
</div>
