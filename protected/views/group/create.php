<?php
/* @var $this GroupController */
/* @var $model Group */

$this->breadcrumbs=array(
	'Групы'=>array('admin'),
	'Создать',
);

$this->menu=array(
	array('label'=>'List Group', 'url'=>array('index')),
	array('label'=>'Manage Group', 'url'=>array('admin')),
);
?>

<h1>Create Group</h1>

<?php echo $this->renderPartial('_formCreate', array('model'=>$model,'isMeet'=>$isMeet)); ?>
