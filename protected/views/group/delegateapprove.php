<?php
/* @var $this GroupController */
/* @var $model Group */

Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerCssFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css') //css
    ->registerScriptFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/jquery-ui.js');

$this->setPageTitle('Стать администратором - ' . $model->name . ' - '. Yii::app()->name);

if ($model->background && $model->background!='')
    echo "<style>body { background: none repeat scroll 0 0 #".CHtml::encode($model->background)." !important; }</style>";

$this->breadcrumbs=array(
    $model->name => Yii::app()->createUrl('group/view') . '/' . urlencode($model->id),
	'Стать администратором',
);

$groupCity = City::model()->getCity($model->city_id);
$groupCountry = Country::model()->getCountryByCityID($model->city_id);

$user_id = User::model()->getUserId();

if (!$user_id) {
    Yii::app()->request->redirect(Yii::app()->createUrl('login/view'));
}

$groupLink = Yii::app()->createUrl('group/view') . '/' . urlencode($model->id);

$requestToken = Yii::app()->request->getParam('rt', 0);
$delegateRequest = $model->getDelegateRequest($model->id);
if (empty($delegateRequest)) {
    Yii::app()->request->redirect($groupLink);
}
$senderId = $delegateRequest['sender_id'];

$isgroupadmin = Group::model()->isGroupAdmin($model->id,$user_id);
$ismember = Member::model()->isMember($user_id,$model->id);
?>

<div class="center">
    <aside id="#SideLeft" class="f-left" style="width: 187px; margin: 0 7px 10px 0;">
        <div class="sidebar-left">
            <div class="group-img">
                <?
                if ($model->picture != '' || $model->picture != NULL) {
                    $picture = $model->picture;
                } else {
                    $picture = 'no-pic.png';
                }
                ?>
                <img src="/images/group/<?php echo CHtml::encode($picture); ?>" style="">
            </div>
            <? if ($isgroupadmin == 1) { ?>
                <a onclick="window.location.href='<? echo Yii::app()->createUrl('site/index').'/group/update/'.urlencode($model->id); ?>'"
                   title="<?php echo Yii::t('var', 'настройки'); ?>" class="complain align-center b-settings-button">
                    <?php echo Yii::t('var', 'Настройки');?>
                </a>
            <? } ?>

        </div>
        <div class="sidebar-left">
            <div class="group-info">
                <ul>
                    <li><?php echo CHtml::encode($groupCity) . ', ' . CHtml::encode($groupCountry);?></li>
                    <li><?php echo Yii::t('var', 'Группа создана');?>:
                        <?php echo CHtml::encode(date('d.m.Y',strtotime($model->date_created)));?></li>
                    <li><?php echo Yii::t('var', 'Участники');?>:
                        <?php echo CHtml::encode(Member::model()->getGroupMembersCount($model->id));?></li>
                    <li><?php echo Yii::t('var', 'Будущие события');?>:
                        <?php echo CHtml::encode(Group::model()->getFutureMeetsCount($model->id));?></li>
                    <li><?php echo Yii::t('var', 'Прошедшие события');?>:
                        <?php echo CHtml::encode(Group::model()->getPastMeetsCount($model->id));?></li>
                    <li><span class="calend-span"><?php
                            echo Yii::t('var', 'Календарь группы');
                            ?>: </span><span class="calend-link"></span></li>
                    <script>
                        $(document).ready(function(){
                            $('.calend-link').click(function(){
                                window.open('<?php echo $groupLink; ?>?calend=1', '_blank');
                            });
                        });
                    </script>
                </ul>
                <div class="key-words">
                    <?php echo Yii::t('var', 'Ключевые слова'); ?>:
                    <ul>
                        <?php $dataReader = GroupKeyword::model()->getGroupKeywords($model->id);
                        $i = 0;
                        foreach($dataReader as $row) {
                            $keywordLink = Yii::app()->createUrl('site/index')
                                . '/?key=' . urlencode($row['name'])
                                . '&cat_id=' . urlencode($row['cat_id']);
                            echo '<li><a href="' . $keywordLink . '">'
                                . CHtml::encode($row['name']) . '</a></li>';
                            $i++;
                        } ?>
                    </ul>
                </div>
                <div class="organizers"><? echo Yii::t('var', 'Организаторы'); ?>:
                    <ul>
                        <?
                        $dataReader = Member::model()->getGroupAdmin($model->id);
                        foreach ($dataReader as $row) {
                            echo '<li><a href="'.Yii::app()->createUrl('site/index').'/user/'.urlencode($row['user_id']).'">'
                                . CHtml::encode($row['name']) . '</a></li>';
                        }
                        $dataReader = Member::model()->getModeratorsList($model->id);
                        foreach($dataReader as $row) {
                            echo '<li><a href="'.Yii::app()->createUrl('site/index').'/user/'.urlencode($row['user_id']).'">'
                                . CHtml::encode($row['name']) . '</a></li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </aside>
    <section id="group-content" class="f-left">
        <div class="content-block-wrap">
            <h2 class="sharp align-center"><?php echo CHtml::encode($model->name); if ($isgroupadmin == 1) echo ' '.Yii::t('var', '(вы - администратор)') ?></h2>
            <div class="content-block">
                <p class="group-description">
                    <?php
                    $senderLink = Yii::app()->createUrl('user/view') . '/' . urlencode($senderId);
                    $message = 'Администратор (<a href="' . $senderLink . '" target="_blank">'
                        . User::model()->getUserName($senderId) . '</a>)'
                        . ' группы <a href="' . $groupLink . '" target="_blank">'
                        . CHtml::encode($model->name) . '</a> предлагает вам свои права на управление группой.'
                        . '<br/ >(Вы станете новым администратором группы)';
                    ?>
                    <table>
                        <tr>
                            <td colspan="2">
                                <?php echo $message; ?>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="agree-button"><?php echo Yii::t('var', 'Согласиться');?></div>
                            </td>
                            <td>
                                <div class="disagree-button"><?php echo Yii::t('var', 'Отказаться');?></div>
                            </td>
                        </tr>
                    </table>
                </p><!-- .group-description-->
                <script>
                    $().ready(function(){
                        $('.agree-button').click(function(){
                            $.ajax({
                                type: 'POST',
                                url: '<?php echo Yii::app()->createUrl('group/delegateagree');?>',
                                data: {
                                    token: '<?php echo $requestToken; ?>',
                                    YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>'
                                },
                                success: function(data){
                                    if (data) {
                                        window.location.href = '<?php echo $groupLink; ?>';
                                    } else {
                                        alert('Ошибка запроса! Попробуйте ответить позже.');
                                    }
                                }
                            });
                            return false;
                        });
                        $('.disagree-button').click(function(){
                            $.ajax({
                                type: 'POST',
                                url: '<?php echo Yii::app()->createUrl('group/delegateagree');?>',
                                data: {
                                    disagree: 1,
                                    token: '<?php echo $requestToken; ?>',
                                    YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>'
                                },
                                success: function(data){
                                    if (data) {
                                        window.location.href = '<?php echo $groupLink; ?>';
                                    } else {
                                        alert('Ошибка запроса! Попробуйте ответить позже.');
                                    }
                                }
                            });
                            return false;
                        });
                    });
                </script>
            </div>
        </div>
    </section>
    <aside id="#SideRight" class="f-right" style="width: 187px; margin: 0 0 10px 7px;">
        <div class="sidebar-right">
            <h2 class="sidebar-right-title align-center sharp" style="margin-bottom: 0;"><? echo Yii::t('var', 'Участники');?></h2>
            <div class="sidebar-right-content sidebar-right-content-participants">
                <div class="group-participants">
                    <?
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'group_id = '.(int)$model->id.' AND confirm=1';

                    $dataProvider=new CActiveDataProvider('Member',array('criteria'=>$criteria));
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider'=>$dataProvider,
                        'itemView'=>'member',
                        'template'=>'{items}'
                    ));
                    ?>
                </div>
                <a href="<? echo Yii::app()->createUrl('site/index').'/group/members/'.urlencode($model->id); ?>" title="посмотреть всех участников группы" class="show-all-participants align-center"><? echo Yii::t('var', 'просмотреть всех');?></a>
            </div>
        </div>
    </aside>
</div>





<script>
    function joinGroup(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('group/joinGroup');?>',
            data: {
                id: user_id,
                group_id: group_id,
                YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
            },
            success: function(data){
                window.location.reload();
            }
        });
    }
    function leaveGroup(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('group/leaveGroup');?>',
            data: {
                id: user_id,
                group_id: group_id,
                YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
            },
            success: function(data){
                window.location.reload();
            }
        });
    }

    function delGroup(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('group/delGroup');?>',
            data: {
                id: user_id,
                group_id: group_id,
                YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
            },
            success: function(data){
                //window.location.reload();
                window.location.href = '/';
            }
        });
    }
</script>
<!--  <p>Ваши друзья из социальных сетей</p>
        --><?/*
        include_once 'facebook/facebook.php';
        $facebook = new Facebook(array(
            'appId' => '591018247595529',
            'secret' => '96f74c40b923be3eb8536699065f89d7',
            'cookie' => true
        ));

        $access_token = $facebook->getAccessToken();
        $friends = $facebook->api('/me/friends?token='.$access_token);*/

/*$session = $facebook->getSession();
if ($session) {
    $uid = $facebook->getUser();
    $me = $facebook->api('/me');
}
print_r($me);*/
?>


<div style="display: none;">
    <div class="box-modal" id="exampleModal">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="addit-fields-form">
            <table>
            <?
            $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
            foreach($dataReader as $row) {
                echo "<tr><td>".CHtml::encode($row['name'])."</td><td><input type='text' value='' name='fields[".CHtml::encode($row['id'])."]'></td></tr>";
            }
            ?>
                <tr><td></td><td><input class="submit" type="submit" value="<? echo Yii::t('var', 'Отправить');?>"></td></tr>
            </table>
        </form>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js"></script>
        <script>
            $().ready(function() {

            $("#addit-fields-form").validate({
            rules: {
                <?
                    $i = 0;
                    $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                    foreach($dataReader as $row) {
                        if ($row['required']==1) {
                            echo (($i>0)?',':'').CHtml::encode($row['id']).': "required"';
                            $i++;
                        }
                     }
                ?>
            },
            messages: {
                <?
                        $i = 0;
                        $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                        foreach($dataReader as $row) {
                            if ($row['required']==1) {
                                echo (($i>0)?',':'').CHtml::encode($row['id']).': "Обязательное поле"';
                                $i++;
                            }
                         }
                    ?>
            },
                submitHandler: function(form) {
                    $.ajax({
                        type: 'GET',
                        url: '<?php echo Yii::app()->createUrl('member/sendRequest'); ?>',
                        data: $('#addit-fields-form').serialize()+'&group_id=<?php echo (int)$model->id; ?>&user_id=<?php echo (int)$user_id; ?>',
                        success: function(data){
                            window.location.reload();
                        }
                    });
                }
            });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="complaint">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="complaint-form">
            <table>
                <tr>
                    <td><br /><?php echo Yii::t('var', 'Введите текст жалобы');?>:<br /><br /></td>
                </tr>
                <tr>
                    <td>
                        <textarea style="float:right;height:auto;width:auto;padding:5px;font-size: 12px;" cols="60" rows="9" name="text"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input class="submit" type="submit" value="<? echo Yii::t('var', 'Отправить');?>">
                    </td>
                </tr>
            </table>
            <input type="hidden" name="mid" value="<?php echo (int)$model->id; ?>">
            <input type="hidden" name="text_id" value="group">
        </form>

        <script>
            $(document).ready(function() {

                $('.complain-event').on('click',function(){
                    $('input[name=mid]').val($(this).attr('mid'));
                });

                $("#complaint-form").validate({
                    rules: {
                        text: "required"
                    },
                    messages: {
                        text: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('complaint/sentComplaint'); ?>',
                            data: $('#complaint-form').serialize()+'&user_id=<?php echo (int)$user_id; ?>'+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="mailto">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="mailto-form">
            <table>
                <tr><td><? echo Yii::t('var', 'Введите имя друга');?></td><td><input type="text" name="name" value=""></td></tr>
                <tr><td><? echo Yii::t('var', 'Введите email друга');?></td><td><input type="text" name="email" value=""></td></tr>
                <tr><td style="vertical-align: middle;"><? echo Yii::t('var', 'Введите комментарий');?></td><td><textarea type="text" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="<? echo Yii::t('var', 'Отправить');?>"></td></tr>
            </table>
            <input type="hidden" name="group_id" value="<?php echo CHtml::encode($model->id); ?>">
        </form>
        <script>
            $(document).ready(function() {

                $("#mailto-form").validate({
                    rules: {
                        name: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        comment: "required"
                    },
                    messages: {
                        name: "<?php echo Yii::t('var', 'Обязательное поле');?>",
                        email: "<?php echo Yii::t('var', 'Введите правильный email');?>",
                        comment: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('ajax/mailto'); ?>',
                            data: $('#mailto-form').serialize()+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="meet-proposal">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="meet-proposal-form">
            <table>
                <tr>
                    <td><?php echo Yii::t('var', 'Введите название события');?></td>
                    <td><input type="text" name="name" value=""></td>
                </tr>
                <tr>
                    <td><?php echo Yii::t('var', 'Введите место');?></td>
                    <td><input type="text" name="place" value=""></td>
                </tr>
                <tr>
                    <td><?php echo Yii::t('var', 'Выберите дату');?></td>
                    <td>
                        <input type="text" name="date" placeholder="<?php echo Yii::t('var', 'Нажмите, чтобы выбрать');?>" value="" class="b-jui-datepicker">
                    </td>
                </tr>
                <tr>
                    <td><?php echo Yii::t('var', 'Выберите время');?></td>
                    <td>
                        <span>Час:</span>
                        <select name="time_hour" class="b-meet-time-hour">
                            <option value="0">0</option>
                        </select>
                        <span>Минуты:</span>
                        <select name="time_minute" class="b-meet-time-minute">
                            <option value="0">0</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <span>Любое время:</span>
                        <input name="any_time" class="b-any-time-input" type="checkbox" value="1" style="width: auto;"/>
                    </td>
                </tr>
                <tr>
                    <td><?php echo Yii::t('var', 'Введите комментарий');?></td>
                    <td><textarea type="comment" name="comment" value=""></textarea></td>
                </tr>
                <tr><td></td><td><input class="submit" type="submit" value="<?php echo Yii::t('var', 'Отправить');?>"></td></tr>
            </table>
            <input type="hidden" name="user_id" value="<?php echo CHtml::encode($user_id); ?>">
            <input type="hidden" name="group_id" value="<? echo CHtml::encode($model->id); ?>">
        </form>
        <script>
            $().ready(function() {
                $('.b-any-time-input').change(function(){
                    var timeFields = $('.b-meet-time-hour, .b-meet-time-minute');
                    if ( $(this).prop('checked') ) {
                        timeFields.attr('disabled', 'disabled');
                    } else {
                        timeFields.removeAttr('disabled');
                    }
                });
                $("#meet-proposal-form").validate({
                    rules: {
                        name: "required",
                        place: "required",
                        date: "required",
                        comment: "required"
                    },
                    messages: {
                        name: "<?php echo Yii::t('var', 'Обязательное поле');?>",
                        place: "<?php echo Yii::t('var', 'Обязательное поле');?>",
                        date: "<?php echo Yii::t('var', 'Обязательное поле');?>",
                        comment: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('group/meetproposal'); ?>',
                            data: $('#meet-proposal-form').serialize()+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
    <?php if ( $isgroupadmin == 1 ): ?>
        <div class="box-modal" id="meet-create">
            <div class="box-modal_close arcticmodal-close"></div>
            <form action="" method="get" id="meet-create-form">
                <table>
                    <tr>
                        <td><?php echo Yii::t('var', 'Введите название события');?></td>
                        <td><input type="text" name="name" value=""></td>
                    </tr>
                    <tr>
                        <td><?php echo Yii::t('var', 'Введите место');?></td>
                        <td><input type="text" name="address" value=""></td>
                    </tr>
                    <tr>
                        <td><?php echo Yii::t('var', 'Выберите дату');?></td>
                        <td>
                            <input type="text" name="date" placeholder="<?php echo Yii::t('var', 'Нажмите, чтобы выбрать');?>"
                                   value="" class="b-jui-datepicker">
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo Yii::t('var', 'Выберите время');?></td>
                        <td>
                            <span>Час:</span>
                            <select name="time_hour" class="b-meet-time-hour">
                                <option value="0">0</option>
                            </select>
                            <span>Минуты:</span>
                            <select name="time_minute" class="b-meet-time-minute">
                                <option value="0">0</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <span>Любое время:</span>
                            <input name="any_time" class="b-any-time-input" type="checkbox" value="1" style="width: auto;"/>
                        </td>
                    </tr>
                    <tr>
                        <td><? echo Yii::t('var', 'Введите комментарий');?></td>
                        <td><textarea name="description"></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input class="submit" type="submit" value="<? echo Yii::t('var', 'Отправить');?>"></td>
                    </tr>
                </table>
                <input type="hidden" name="city" value="<?php echo CHtml::encode($model->city_id); ?>">
                <input type="hidden" name="user_id" value="<?php echo CHtml::encode($user_id); ?>">
                <input type="hidden" name="parent" value="<?php echo CHtml::encode($model->id); ?>">
            </form>
            <script>
                $().ready(function() {
                    $("#meet-create-form").validate({
                        rules: {
                            name   : "required",
                            address: "required",
                            date   : "required",
                            description: "required"
                        },
                        messages: {
                            name   : "<?php echo Yii::t('var', 'Обязательное поле');?>",
                            address: "<?php echo Yii::t('var', 'Обязательное поле');?>",
                            date   : "<?php echo Yii::t('var', 'Обязательное поле');?>",
                            description: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                        },
                        submitHandler: function(form) {
                            $.ajax({
                                type: 'GET',
                                url: '<?php echo Yii::app()->createUrl('group/creatingmeet'); ?>',
                                data: $('#meet-create-form').serialize()+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                                success: function(data){
                                    window.location.reload();
                                }
                            });
                        }
                    });
                });
            </script>
        </div><!-- #meet-create-->
    <?php endif; ?>
</div>
<div style="display: none;">
    <div class="box-modal" id="contact-admin">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="contact-admin-form">
            <table>
                <tr>
                    <td><?php echo Yii::t('var', 'Введите ваше сообщение');?></td>
                    <td><textarea type="comment" name="comment" value=""></textarea></td>
                </tr>
                <tr><td></td><td><input class="submit" type="submit" value="<?php echo Yii::t('var', 'Отправить');?>"></td></tr>
            </table>
            <input type="hidden" name="user_id" value="<?php echo CHtml::encode($user_id); ?>">
        </form>
        <script>
            $().ready(function() {
                $("#contact-admin-form").validate({
                    rules: {
                        place: "required",
                        date: "required",
                        comment: "required"
                    },
                    messages: {
                        place: "<?php echo Yii::t('var', 'Обязательное поле');?>",
                        date: "<?php echo Yii::t('var', 'Обязательное поле');?>",
                        comment: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('group/contactadmin'); ?>',
                            data: $('#contact-admin-form').serialize()+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<style>
    .box-modal {
        width:540px;
        margin: 0 auto;
        background-color: #ffffff;
        padding: 20px;
    }
    #sidebar {
        display: none;
    }
    .span-5 {
        width:0px
    }
    .span-19 {
        width: 950px;
    }
</style>
<script>
    function sentComment(meet_id,user_id) {
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('group/sentComment');?>',
            data: $('#comments-form').serialize() + "&user_id=" + parseInt(user_id)
                + "&meet_id=" + parseInt(meet_id)
                + "&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
            success: function(data){
                window.location.reload();
            }
        });
    }
</script>
<script>
    $(document).ready(function(){
        $('#сontact-us').on('change',function(){
            $('#contact-admin').arcticmodal();
        });

        var height = $('.group-img img').height();
        var width = $('.group-img img').width();
        var left,top;
        if (width > height) {
            $('.group-img img').css({'height':'192px'});
            width = $('.group-img img').width();
            left = (width - 185.5)/2;
            $('.group-img img').css({'left':'-'+left+'px'});
        }
        else {
            $('.group-img img').css({'width':'185.5px'});
            height = $('.group-img img').height();
            top = (height - 192)/2;
            $('.group-img img').css({'top':'-'+top+'px'});
        }


    });
</script>