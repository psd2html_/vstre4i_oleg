<?
$picture = User::model()->getPicture($data->user_id);
$name = User::model()->getUserName($data->user_id);
if ($picture == '' || $picture == NULL) {
    $picture = '/images/user/mini_no-pic.png';
} else {
    $picture = "/images/user/mini_".$picture;
}
if ($data->user_id != User::model()->getUserId()):
?>
    <div class="b-delegate-item">
        <div class="b-delegate-item__avatar">
            <a href="<?php echo Yii::app()->createUrl('site/index').'/user/'.urlencode($data->user_id); ?>" title="#">
                <img src="<?php echo CHtml::encode($picture); ?>" alt="<?=$name;?>" style="width: 70px; height: 60px;"/>
            </a>
        </div><!-- .b-delegate-item__avatar-->
        <div class="b-delegate-item__right">
            <div class="b-delegate-item__name">
                <a href="<?php echo Yii::app()->createUrl('site/index').'/user/'.urlencode($data->user_id); ?>" title="#">
                    <?php echo CHtml::encode($name); ?>
                </a>
            </div><!-- .b-delegate-item__name-->
            <div class="b-delegate-item__delegate">
                <a href="#" onclick="if(confirm('Передать права пользователю <?php echo CHtml::encode($name); ?>?')){delegate(currentGroupId,<?php echo (int)$data->user_id; ?>);return false;}">
                    Передать права
                </a>
            </div><!-- .b-delegate-item__delegate-->
        </div><!-- .b-delegate-item__right-->
    </div><!-- .b-delegate-item-->
<?php endif; ?>
