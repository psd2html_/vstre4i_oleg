<?php
/* @var $this GroupController */
/* @var $model Group */

Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerCssFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css') //css
    ->registerScriptFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/jquery-ui.js');

$this->setPageTitle('Удалить группу - ' . $model->name . ' - '. Yii::app()->name);

if ($model->background && $model->background!='')
    echo "<style>body { background: none repeat scroll 0 0 #".CHtml::encode($model->background)." !important; }</style>";

$this->breadcrumbs=array(
    $model->name => Yii::app()->createUrl('group/view') . '/' . $model->id,
	'Удалить группу',
);

$groupCity = City::model()->getCity($model->city_id);
$groupCountry = Country::model()->getCountryByCityID($model->city_id);

$user_id = User::model()->getUserId();
$isGroupMainAdmin = Group::model()->isGroupMainAdmin($model->id,$user_id);
$ismember = Member::model()->isMember($user_id,$model->id);
$groupLink = Yii::app()->createUrl('group/view') . '/' . urlencode($model->id);
if (!$isGroupMainAdmin) {
    Yii::app()->request->redirect($groupLink);
}
if ($model->isGroupDelegated($model->id)) {
    Yii::app()->request->redirect(Yii::app()->createUrl('group/delegatewaiting') . '/' . urlencode($model->id));
}
?>

<div class="center">
    <aside id="#SideLeft" class="f-left" style="width: 187px; margin: 0 7px 10px 0;">
        <div class="sidebar-left">
            <div class="group-img">
                <?
                if ($model->picture != '' || $model->picture != NULL) {
                    $picture = $model->picture;
                } else {
                    $picture = 'no-pic.png';
                }
                ?>
                <img src="/images/group/<?php echo CHtml::encode($picture); ?>" style="">
            </div>
            <? if ($isGroupMainAdmin == 1) { ?>
                <a onclick="window.location.href='<? echo Yii::app()->createUrl('site/index').'/group/update/'.urlencode($model->id); ?>'"
                   title="<?php echo Yii::t('var', 'Управление группой'); ?>" class="complain align-center b-settings-button">
                    <? echo Yii::t('var', 'Управление группой');?>
                </a>
            <? } ?>

        </div>
        <div class="sidebar-left">
            <div class="group-info">
                <ul>
                    <li><?php echo CHtml::encode($groupCity) . ', ' . CHtml::encode($groupCountry);?></li>
                    <li><?php echo Yii::t('var', 'Группа создана');?>:
                        <?php echo CHtml::encode(date('d.m.Y',strtotime($model->date_created)));?></li>
                    <li><?php echo Yii::t('var', 'Участники');?>:
                        <?php echo CHtml::encode(Member::model()->getGroupMembersCount($model->id));?></li>
                    <li><?php echo Yii::t('var', 'Будущие события');?>:
                        <?php echo CHtml::encode(Group::model()->getFutureMeetsCount($model->id));?></li>
                    <li><?php echo Yii::t('var', 'Прошедшие события');?>:
                        <?php echo CHtml::encode(Group::model()->getPastMeetsCount($model->id));?></li>
                    <li><span class="calend-span"><?php
                            echo Yii::t('var', 'Календарь группы');
                            ?>: </span><span class="calend-link"></span></li>
                    <script>
                        $(document).ready(function(){
                            $('.calend-link').click(function(){
                                window.open('<?php echo $groupLink; ?>?calend=1', '_blank');
                            });
                        });
                    </script>
                </ul>
                <div class="key-words">
                    <?php echo Yii::t('var', 'Ключевые слова'); ?>:
                    <ul>
                        <?php $dataReader = GroupKeyword::model()->getGroupKeywords($model->id);
                        $i = 0;
                        foreach($dataReader as $row) {
                            $keywordLink = Yii::app()->createUrl('site/index')
                                . '/?key=' . urlencode($row['name'])
                                . '&cat_id=' . urlencode($row['cat_id']);
                            echo '<li><a href="' . $keywordLink . '">'
                                . CHtml::encode($row['name']) . '</a></li>';
                            $i++;
                        } ?>
                    </ul>
                </div>
                <div class="organizers"><? echo Yii::t('var', 'Организаторы');?>:
                    <ul>
                        <?
                        $dataReader = Member::model()->getGroupAdmin($model->id);
                        foreach ($dataReader as $row) {
                            echo '<li><a href="'.Yii::app()->createUrl('site/index').'/user/'.urlencode($row['user_id']).'">'.CHtml::encode($row['name']).'</a></li>';
                        }
                        $dataReader = Member::model()->getModeratorsList($model->id);
                        foreach($dataReader as $row) {
                            echo '<li><a href="'.Yii::app()->createUrl('site/index').'/user/'.urlencode($row['user_id']).'">'.CHtml::encode($row['name']).'</a></li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </aside>
    <section id="group-content" class="f-left">
        <div class="content-block-wrap">
            <h2 class="sharp align-center"><?php echo CHtml::encode($model->name); if ($isGroupMainAdmin == 1) echo ' '.Yii::t('var', '(вы - администратор)') ?></h2>
            <div class="content-block">
                <p class="group-description">

                <table>
                    <tr>
                        <td colspan="2">
                            Вы уверены, что хотите удалить группу? <strong>Это действие нельзя отменить.</strong><br /><br />
                            Вы можете <strong>передать права</strong>
                            <?php
                            $moderators = Group::model()->getModerators($model->id);
                            if (!empty($moderators)) {
                                echo ' одному из <strong>модераторов</strong>:<br />';
                            } else {
                                echo ' одному из <strong>участников</strong>:<br />';
                            }
                            foreach ($moderators as $moderator):
                                if ( ! $moderator['avatar'] ) {
                                    $moderatorPic = '/images/user/mini_no-pic.png';
                                } else {
                                    $moderatorPic = '/images/user/mini_' . $moderator['avatar'];
                                }
                                $moderatorLink = Yii::app()->createUrl('user/view') . '/' . urlencode($moderator['user_id']);
                                ?>
                                <div class="b-delegate-item">
                                    <div class="b-delegate-item__avatar">
                                        <a href="<?php echo $moderatorLink; ?>">
                                            <img src="<?php echo CHtml::encode($moderatorPic); ?>" class="b-delegate-item__avatar-img" alt=""/>
                                        </a>
                                    </div><!-- .b-delegate-item__avatar-->
                                    <div class="b-delegate-item__right">
                                        <div class="b-delegate-item__name">
                                            <a href="<?php echo $moderatorLink; ?>"
                                               id="user-link-<?php echo CHtml::encode($moderator['user_id']); ?>"><?php
                                                echo CHtml::encode($moderator['name']); ?></a>
                                        </div><!-- .b-delegate-item__name-->
                                        <div class="b-delegate-item__delegate">
                                            <a href="#" class="b-delegate-item__delegate-link"
                                               id="delegate-<?php echo CHtml::encode($moderator['user_id']); ?>">Передать права</a>
                                        </div><!-- .b-delegate-item__delegate-->
                                    </div><!-- .b-delegate-item__right-->
                                </div><!-- .b-delegate-item-->
                            <?php
                            endforeach;
                            echo empty($moderators) ? '' : '<br />' ;
                            ?>
                            <div class="b-moderators-search">
                                <?php echo empty($moderators) ? '' : '<div><strong>или одному из участников:</strong></div>'; ?>
                                <div class="b-moderators-search__button">Найти</div>
                                <input type="text" class="b-moderators-search__input" size="40"
                                       placeholder="Введите имя участника группы" value="" />
                                <div class="b-moderators-search__answer"></div>
                            </div>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="b-delete-group__divider">
                                <div class="b-delete-group__divider-text">ИЛИ</div>
                            </div>
                            <input type="checkbox" value="1" class="del-group-agree" id="del-group-agree"/>
                            <label for="del-group-agree">Я понимаю, что эту группу невозможно будет восстановить</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="del-group-button"><?php echo Yii::t('var', 'Удалить группу');?></div>
                        </td>
                        <td>
                            <div class="del-group-button-cancel"><?php echo Yii::t('var', 'Отмена');?></div>
                        </td>
                    </tr>
                </table>
                </p>
                <?php
                $waitingLink = Yii::app()->createUrl('group/delegatewaiting') . '/' . urlencode($model->id);
                ?>
                <script>
                    function delegate(linkTag)
                    {
                        var userId = $(linkTag).attr('id').split('-')[1];
                        var userName = $('#user-link-' + userId).text();
                        if (confirm('Передать права пользователю ' + userName + '?')) {
                            $.ajax({
                                type: 'GET',
                                url: '<?php echo Yii::app()->createUrl('group/delegategroup'); ?>',
                                data: {
                                    group_id: <?php echo (int)$model->id; ?>,
                                    user_id: userId,
                                    YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
                                },
                                success: function(data){
                                    if (data) {
                                        window.location.href = '<?php echo $waitingLink; ?>?rt=' + data;
                                    } else {
                                        alert('Запрос не отправлен, попробуйте ещё раз.');
                                    }
                                }
                            });
                        }
                    }
                    $().ready(function(){
                        $('.b-delegate-item__delegate-link').click(function(){
                            delegate(this);
                            return false;
                        });

                        var searchAnswerTag = $('.b-moderators-search__answer');
                        $('.b-moderators-search__input').focusout(function(){
                            $('.b-moderators-search__answer').delay(100).slideUp('fast');
                        }).focusin(function(){
                            if (searchAnswerTag.text()) {
                                searchAnswerTag.slideDown('fast');
                            }
                        }).keyup(function(){
                            $.ajax({
                                type: 'GET',
                                url : '<?php echo Yii::app()->createUrl('ajax/findMembers'); ?>',
                                data: {
                                    YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>',
                                    username: $(this).val(),
                                    group_id: <?php echo (int)$model->id;?>
                                },
                                success: function(data){
                                    var answer = '';
                                    if (data != 0) {
                                        $(JSON.parse(data)).each(function(index,member){
                                            if ($(member.name).html()) {
                                                member.name = $(member.name).html();
                                            }
                                            answer += '<div class="b-moderators-search__answer-unit">'
                                                + '<span class="span-link b-delegate-item__delegate-link'
                                                + ' b-moderators-search__add" id="delegate-' + member.id + '">'
                                                + 'Передать права</span>'
                                                + '<div class="b-moderators-search__answer-unit-name">'
                                                + '<a href="<?php echo Yii::app()->createUrl('user/view'); ?>/' + parseInt(member.id) + '"'
                                                + ' id="user-link-' + parseInt(member.id) + '" target="_blank">'
                                                + member.name + '</a></div>'
                                                + '</div>';
                                        });
                                    }
                                    if (answer === '') {
                                        answer = '<div class="b-moderators-search__answer-unit">Нет результатов</div>';
                                    }
                                    $('.b-moderators-search__answer').html(answer).slideDown('fast');
                                    $('.b-delegate-item__delegate-link').click(function(){
                                        delegate(this);
                                        return false;
                                    });
                                }
                            });
                        });
                        $('.del-group-button').click(function(){
                            if ($('.del-group-agree').prop('checked') && confirm('Удалить группу?')) {
                                delGroup(<?php echo (int)User::model()->getUserId().', '.(int)$model->id;?>);
                            }
                            return false;
                        });
                        $('.del-group-button-cancel').click(function(){
                            window.location.href = '<?php echo Yii::app()->createUrl('group/view') . '/' . urlencode($model->id); ?>';
                            return false;
                        });
                    });
                </script>
            </div>
        </div>
    </section>
    <aside id="#SideRight" class="f-right" style="width: 187px; margin: 0 0 10px 7px;">
        <div class="sidebar-right">
            <h2 class="sidebar-right-title align-center sharp" style="margin-bottom: 0;"><? echo Yii::t('var', 'Участники');?></h2>
            <div class="sidebar-right-content sidebar-right-content-participants">
                <div class="group-participants">
                    <?
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'group_id = ' . (int)$model->id . ' AND confirm = 1';

                    $dataProvider=new CActiveDataProvider('Member',array('criteria'=>$criteria));
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider'=>$dataProvider,
                        'itemView'=>'member',
                        'template'=>'{items}'
                    ));
                    ?>
                </div>
                <a href="<? echo Yii::app()->createUrl('site/index').'/group/members/'.urlencode($model->id); ?>" title="посмотреть всех участников группы" class="show-all-participants align-center"><? echo Yii::t('var', 'просмотреть всех');?></a>
            </div>
        </div>
    </aside>
</div>





<script>
    function joinGroup(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('group/joinGroup');?>',
            data: {
                id: user_id,
                group_id: group_id,
                YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
            },
            success: function(data){
                window.location.reload();
            }
        });
    }
    function leaveGroup(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('group/leaveGroup');?>',
            data: {
                id: user_id,
                group_id: group_id,
                YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
            },
            success: function(data){
                window.location.reload();
            }
        });
    }

    function delGroup(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('group/delGroup');?>',
            data: {
                id: user_id,
                group_id: group_id,
                YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
            },
            success: function(data){
                //window.location.reload();
                window.location.href = '/';
            }
        });
    }
</script>
<!--  <p>Ваши друзья из социальных сетей</p>
        --><?/*
        include_once 'facebook/facebook.php';
        $facebook = new Facebook(array(
            'appId' => '591018247595529',
            'secret' => '96f74c40b923be3eb8536699065f89d7',
            'cookie' => true
        ));

        $access_token = $facebook->getAccessToken();
        $friends = $facebook->api('/me/friends?token='.$access_token);*/

/*$session = $facebook->getSession();
if ($session) {
    $uid = $facebook->getUser();
    $me = $facebook->api('/me');
}
print_r($me);*/
?>


<div style="display: none;">
    <div class="box-modal" id="exampleModal">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="addit-fields-form">
            <table>
            <?
            $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
            foreach($dataReader as $row) {
                echo '<tr><td>'.CHtml::encode($row['name']).'</td>'
                    . '<td><input type="text" value="" name="fields['.CHtml::encode($row['id']).']"></td></tr>';
            }
            ?>
                <tr><td></td><td><input class="submit" type="submit" value="<?php echo Yii::t('var', 'Отправить');?>"></td></tr>
            </table>
        </form>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js"></script>
        <script>
            $().ready(function() {
                $("#addit-fields-form").validate({
                    rules: {
                        <?
                        $i = 0;
                        $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                        foreach($dataReader as $row) {
                            if ($row['required']==1) {
                                echo (($i>0)?',':'').CHtml::encode($row['id']).': "required"';
                                $i++;
                            }
                         }
                        ?>
                    },
                    messages: {
                        <?
                        $i = 0;
                        $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                        foreach($dataReader as $row) {
                            if ($row['required']==1) {
                                echo (($i>0)?',':'').CHtml::encode($row['id']).': "Обязательное поле"';
                                $i++;
                            }
                         }
                        ?>
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('member/sendRequest'); ?>',
                            data: $('#addit-fields-form').serialize()+'&group_id=<?php echo (int)$model->id; ?>&user_id=<?php echo (int)$user_id; ?>',
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="complaint">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="complaint-form">
            <table>
                <tr>
                    <td><br /><?php echo Yii::t('var', 'Введите текст жалобы');?>:<br /><br /></td>
                </tr>
                <tr>
                    <td>
                        <textarea style="float:right;height:auto;width:auto;padding:5px;font-size: 12px;" cols="60" rows="9" name="text"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input class="submit" type="submit" value="<?php echo Yii::t('var', 'Отправить');?>">
                    </td>
                </tr>
            </table>
            <input type="hidden" name="mid" value="<?php echo CHtml::encode($model->id); ?>">
            <input type="hidden" name="text_id" value="group">
        </form>

        <script>
            $(document).ready(function() {

                $('.complain-event').on('click',function(){
                    $('input[name=mid]').val($(this).attr('mid'));
                });

                $("#complaint-form").validate({
                    rules: {
                        text: "required"
                    },
                    messages: {
                        text: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('complaint/sentComplaint'); ?>',
                            data: $('#complaint-form').serialize()+'&user_id=<?php echo (int)$user_id; ?>'+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="mailto">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="mailto-form">
            <table>
                <tr><td><? echo Yii::t('var', 'Введите имя друга');?></td><td><input type="text" name="name" value=""></td></tr>
                <tr><td><? echo Yii::t('var', 'Введите email друга');?></td><td><input type="text" name="email" value=""></td></tr>
                <tr><td style="vertical-align: middle;"><?php echo Yii::t('var', 'Введите комментарий');?></td><td><textarea type="text" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="<? echo Yii::t('var', 'Отправить');?>"></td></tr>
            </table>
            <input type="hidden" name="group_id" value="<?php echo CHtml::encode($model->id); ?>">
        </form>
        <script>
            $(document).ready(function() {

                $("#mailto-form").validate({
                    rules: {
                        name: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        comment: "required"
                    },
                    messages: {
                        name: "<?php echo Yii::t('var', 'Обязательное поле');?>",
                        email: "<?php echo Yii::t('var', 'Введите правильный email');?>",
                        comment: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('ajax/mailto'); ?>',
                            data: $('#mailto-form').serialize()+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="contact-admin">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="contact-admin-form">
            <table>
                <tr><td><? echo Yii::t('var', 'Введите ваше сообщение');?></td><td><textarea type="comment" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="<? echo Yii::t('var', 'Отправить');?>"></td></tr>
            </table>
            <input type="hidden" name="user_id" value="<? echo $user_id; ?>">
            <input type="hidden" name="email" value="<? echo $user_id; ?>">
        </form>
        <script>
            $().ready(function() {

                $("#contact-admin-form").validate({
                    rules: {
                        place: "required",
                        date: "required",
                        comment: "required"
                    },
                    messages: {
                        place: "<? echo Yii::t('var', 'Обязательное поле');?>",
                        date: "<? echo Yii::t('var', 'Обязательное поле');?>",
                        comment: "<? echo Yii::t('var', 'Обязательное поле');?>"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('group/contactadmin'); ?>',
                            data: $('#contact-admin-form').serialize()+"&YII_CSRF_TOKEN=<? echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<style>
    .box-modal {
        width:540px;
        margin: 0 auto;
        background-color: #ffffff;
        padding: 20px;
    }
    #sidebar {
        display: none;
    }
    .span-5 {
        width:0px
    }
    .span-19 {
        width: 950px;
    }
</style>
<script>
    function sentComment(meet_id,user_id) {
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('group/sentComment');?>',
            data: $('#comments-form').serialize() + "&user_id=" + parseInt(user_id)
                + "&meet_id=" + parseInt(meet_id)
                + "&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
            success: function(data){
                window.location.reload();
            }
        });
    }
</script>
<script>
    $(document).ready(function(){
        $('#сontact-us').on('change',function(){
            $('#contact-admin').arcticmodal();
        });

        var height = $('.group-img img').height();
        var width = $('.group-img img').width();
        var left,top;
        if (width > height) {
            $('.group-img img').css({'height':'192px'});
            width = $('.group-img img').width();
            left = (width - 185.5)/2;
            $('.group-img img').css({'left':'-'+left+'px'});
        }
        else {
            $('.group-img img').css({'width':'185.5px'});
            height = $('.group-img img').height();
            top = (height - 192)/2;
            $('.group-img img').css({'top':'-'+top+'px'});
        }


    });
</script>