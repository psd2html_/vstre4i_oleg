<?
$picture = User::model()->getPicture($data->user_id);
if (!$picture) {
    $picture = '/images/user/mini_no-pic.png';
} else {
    $picture = "/images/user/mini_".$picture;
}
?>
<a href="<? echo Yii::app()->createUrl('site/index').'/user/'.urlencode($data->user_id); ?>" title="#">
    <img class="f-left" src="<?php echo CHtml::encode($picture); ?>" alt="<?php echo CHtml::encode(User::model()->getName($data->user_id)); ?>" style="width: 70px; height: 60px;"/>
    <p class="f-left" style="margin: 20px; width: 55px;"><?php echo CHtml::encode(User::model()->getName($data->user_id)); ?></p>
    <p class="clear"></p>
</a>
