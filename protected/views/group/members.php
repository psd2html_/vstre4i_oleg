<?php
/* @var $this GroupController */
/* @var $data Group */

$user_id = User::model()->getUserId();
$this->setPageTitle('Участники - ' . $model->name . ' - ' . Yii::app()->name);
$this->breadcrumbs = array(
    $model->name => Yii::app()->createUrl('group/view') . '/' . urlencode($model->id),
    'Участники'
);
?>

<h1 class="page-title sharp align-center">Все участники группы</h1>
<div class="center vse-uchastniki-admin<?php if (Group::model()->isGroupAdmin($model->id,$user_id)) echo ' vse-uchastniki-admin'; else echo ' vse-uchastniki'; ?>">
    <div style="width:100%;float:left">
        <form action="" method="post" style="width: 384px;">
            <input id="searchInput" name="name" type="text" class="search-field-orange f-left" />
            <input id="searchBtn" type="image" src="/images/search-icon-orange.png" name="search-btn" class="search-btn-magnifier " />
        </form>
    </div>
   <div id="UsersList" style="width:100%;float: left">
    <?
    if (isset($_GET['mid'])) 
		$dataReader = Member::model()->getGroupMembers($_GET['mid']);
    else 
		$dataReader = Member::model()->getGroupMembers($model->id);
     
	if (!Group::model()->isGroupAdmin($model->id,$user_id)) {
        foreach($dataReader as $row) {
            $avatar = $row['avatar'];
            if ($avatar && $avatar != '') ; else $avatar = 'no-pic.png';
            ?>
            <a class="participants f-left white-block align-center"
               title="#" href="<?php echo Yii::app()->createUrl('site/index').'/user/'.urlencode($row['id']); ?>">
                <img src="/images/user/mini_<?php echo CHtml::encode($avatar); ?>" alt="event">
                <p class="participants-name f-left"><?php echo CHtml::encode($row['name']); ?><br /></p>
            </a>
            <?
        }
    } else {
        foreach($dataReader as $row) {
            $avatar = $row['avatar'];
            if ($avatar && $avatar != '') ; else $avatar = 'no-pic.png';
            ?>
            <div class="participants white-block">
                <div class="participants-img f-left align-center"><a href="<? echo Yii::app()->createUrl('site/index').'/user/'.urlencode($row['id']); ?>"><img style="width:150px;height:150px;" src="/images/user/mini_<?php echo CHtml::encode($avatar); ?>" alt="event"></a></div>
                <div class="participants-name sharp f-left align-center"><?php echo CHtml::encode($row['name']); ?><br /><span><?php echo CHtml::encode($row['city']); ?></span></div>
                <div class="participants-action f-left align-center">
                    <ul>
                        <li>
                            <a class="sharp-blue" title="#" onclick="$('#mailto').arcticmodal();">Написать сообщение</a>
                        </li>
                        <?
                        if (Member::model()->isModerator($row['id'],$model->id)>0) {
                            ?>
                            <li>
                                <a class="sharp-blue" title="#"
                                   onclick="disrankModerator(<?php echo (int)$row['id'].','.(int)$model->id; ?>)">Разжаловать</a>
                            </li>
                        <?
                        } else { ?>
                            <li>
                                <a class="sharp-blue" title="#"
                                   onclick="appointModerator(<?php echo (int)$row['id'].','.(int)$model->id; ?>)">Назначить модератором</a>
                            </li>
                        <? }
                        ?>
                        <li>
                            <a class="sharp-blue" title="#"
                               onclick="kickMember(<?php echo (int)$row['id'].','.(int)$model->id; ?>)">Удалить из группы</a>
                        </li>
                    </ul>
                </div>
            </div>
            <?
        }
    }
    ?>
	</div>
</div>


<script>
    function appointModerator(user_id,group_id){
        $.ajax({
            type: 'GET',
            url: '<?php echo Yii::app()->createUrl('member/appointModerator');?>',
            data: {
                user_id: user_id,
                group_id: group_id,
                YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
            },
            success: function(data){
                window.location.reload();
            }
        });
    }
    function disrankModerator(user_id,group_id){
        $.ajax({
            type: 'GET',
            url: '<?php echo Yii::app()->createUrl('member/disrankModerator');?>',
            data: {
                user_id: user_id,
                group_id: group_id,
                YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
            },
            success: function(data){
                window.location.reload();
            }
        });
    }
    function kickMember(user_id,group_id){
        $.ajax({
            type: 'GET',
            url: '<?php echo Yii::app()->createUrl('member/kickMember');?>',
            data: {
                user_id: user_id,
                group_id: group_id,
                YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
            },
            success: function(data){
                window.location.reload();
            }
        });
    }
</script>
<div style="display: none;">
    <div class="box-modal" id="mailto">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="mailto-form">
            <table>
                <tr><td>Введите имя друга</td><td><input type="text" name="name" value=""></td></tr>
                <tr><td>Введите email друга</td><td><input type="text" name="email" value=""></td></tr>
                <tr><td>Введите комментарий</td><td><textarea type="text" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
            <input type="hidden" name="group_id" value="<?php echo CHtml::encode($model->id); ?>">
        </form>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js"></script>
        <script>
		
			yii_csrf_token = '<?php echo Yii::app()->request->csrfToken; ?>';
            $().ready(function() {

                $("#mailto-form").validate({
                    rules: {
                        name: "required",
                        email: "required",
                        comment: "required"
                    },
                    messages: {
                        name: "Обязательное поле",
                        email: "Обязательное поле",
                        comment: "Обязательное поле"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('ajax/mailto'); ?>',
                            data: $('#mailto-form').serialize(),
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
				var data = {};
				var name = '';
				var mid = <?= Yii::app()->request->getParam('mid', 0)?>;
				var groupId = <?= $model->id ?>;
				if(!mid){
					data = {name: name, groupId : groupId};
				}else{
					data = {name: name, groupId : groupId, mid : mid};
				}
					groupId = mid;
				/* $('#searchInput').keyup(function(){
				}); */

				$('#searchBtn').on('click', function(evt){
					evt.preventDefault();
					name = $('#searchInput').val() || '';
					if(!name) 
						return false;
					$('#UsersList').load( "<?= Yii::app()->createUrl('member/filterMembers');?>", $.extend(data, {name: name}) );
				})
            });
        </script>
    </div>
</div>
