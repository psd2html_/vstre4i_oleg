<?php echo CHtml::beginForm('https://merchant.webmoney.ru/lmi/payment.asp','POST'); ?>

<div class="row">
    <?php echo CHtml::hiddenField('LMI_PAYMENT_DESC','Group 2 Top. vstre4i.kz') ?>
    <?php echo CHtml::hiddenField('LMI_PAYMENT_NO','3') ?>
    <?php echo CHtml::hiddenField('LMI_PAYEE_PURSE',$webmoney) ?>
    <?php echo CHtml::hiddenField('LMI_SUCCESS_URL',Yii::app()->getBaseUrl(true).'/payment/webmoneySuccess') ?>
    <?php echo CHtml::hiddenField('LMI_RESULT_URL',Yii::app()->getBaseUrl(true).'/payment/webmoneyResult') ?>
    <?php echo CHtml::hiddenField('LMI_FAIL_URL',Yii::app()->getBaseUrl(true).'/payment/webmoneyFail') ?>
    <?php echo CHtml::hiddenField('LMI_PAYMENT_AMOUNT',$amount) ?>
    <?php echo CHtml::hiddenField('LMI_SIM_MODE','0') ?>
    <?php echo CHtml::hiddenField('FIELD_USER_ID',Yii::app()->user->id); ?>
    <?php echo CHtml::hiddenField('FIELD_GROUP_ID',false) ?>


</div>

<div class="row submit">
    <?php echo CHtml::submitButton('Оплатить'); ?>
</div>

<?php echo CHtml::endForm(); ?>
</div>