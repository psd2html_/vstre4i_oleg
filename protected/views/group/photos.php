<?php
/* @var $this GroupController */
/* @var $data Group */

$user_id = User::model()->getUserId();

if (! Member::model()->isMember($user_id, $model->id)) {
    $this->redirect(Yii::app()->createUrl('group/view') . '/' . urlencode($model->id));
}

$this->setPageTitle('Фотографии - ' . $model->name . ' - ' . Yii::app()->name);
$description = $model->description;
if($description){
	Yii::app()->clientScript->registerMetaTag(CHtml::encode($description), 'description');
}
$keywords = '';
foreach(GroupKeyword::model()->getGroupKeywords($model->id, false) as $word){
	$keywords .= $word.', ';
}
$keywords = substr($keywords,0,-2);
if($keywords){
	Yii::app()->clientScript->registerMetaTag(CHtml::encode($keywords).', '.Admin::getMainKeywords(), 'keywords');
}

$this->breadcrumbs = array(
    $model->name => Yii::app()->createUrl('group/view') . '/' . urlencode($model->id),
    'Фотографии'
);

?>

    <div class="center">
        <h1 class="page-title sharp align-center">Фотографии группы <?php echo CHtml::encode($model->name); ?></h1>
        <a href="<?php echo $this->createUrl('group/addAlbum', array('id' => $model->id))?>" class="add-album-button">Добавить альбом</a>

        <?php $i = 1;
        foreach($albums as $row) {
            $pic = Photoalbum::model()->getRandomAlbumPhoto($row['id']);
            if ($pic=='') {
                $pic = '/images/group/mini_no-pic.png';
            } else {
                $pic = CHtml::encode('/images/album/'.$row['id'].'/mini_'.$pic);
            }
            ?>
            <div class="photo-container f-left<? if ($i>3) echo ' hide'; ?>">
                <a href="<?php echo Yii::app()->createUrl('site/index').'/photoalbum/'.CHtml::encode($row['id']); ?>"><img src="<?php echo CHtml::encode($pic);?>" alt="photo-group" /></a>
                <div class="photo-info align-center"><span class="photo-title"><?php echo CHtml::encode($row['name']); ?></span><br /><span class="photo-date"><?php echo CHtml::encode(date("d.m.Y", strtotime($row['date']) )); ?></span></div>
            </div>
			
            <?php
            $i++;
        } ?>
        <a class="show-all align-center" style="margin-top: 10px" title="Показать все фотоальбомы группы" href="#" onclick="showAllAlbums()"><?php echo Yii::t('var', 'показать все альбомы');?></a>

        <h2 class="latest-photos-title"><?php echo Yii::t('var', 'Последние фото со встреч');?>:</h2>
        <div class="latest-photos">
            <?
            $dataReader = Photoalbum::model()->getGroupPhotos($model->id);
            foreach($dataReader as $row) {
                echo '<a class="album-image fancybox" href="/images/album/'.urlencode($row['album_id']).'/'.urlencode($row['link']).'"><img src="/images/album/'.CHtml::encode($row['album_id']).'/mini_'.CHtml::encode($row['link']).'" alt="photo" style="height: 200px"></a>';
            } ?>
        </div>

    </div>
	<?php
	$cs = Yii::app()->clientScript;
	$cs->registerScript('showAlbom', "
		function showAllAlbums(){
			$('.photo-container').removeClass('hide');
		}
	", CClientScript::POS_END);
	?>
