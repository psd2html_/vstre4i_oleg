<?php
/* @var $this PhotoalbumController */
/* @var $model Photoalbum */
/* @var $form CActiveForm */


$this->breadcrumbs=array(
    Group::model()->getGroupName($model->parent) => Yii::app()->createUrl('group/view') . '/' . urlencode($model->parent),
    'Редаткирование события'
);

Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerCssFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css') //css
    ->registerScriptFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/jquery-ui.js')
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.ui.datepicker-ru.js')
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/now-date-input.js');
	

?>

<div class="center">
    <form action="<?php $this->createUrl('group/updatingMeet', array('id' => $model->id))?>" method="get" id="meet-proposal-form" style="width: 550px; margin:0 auto;">
        <table style="border-spacing: 5px">
            <tr>
                <td><? echo Yii::t('var', 'Введите название события');?>:</td>
                <td><?php echo CHtml::activeTextField($model, 'name', array('class'=> 'select-input', 'style' => 'margin-top: 10px'))?></td>
            </tr>
            <tr>
                <td><? echo Yii::t('var', 'Введите место');?>:</td>
                <td><?php echo CHtml::activeTextField($model, 'address', array('class'=> 'select-input ', 'style' => 'margin-top: 10px'))?></td>
            </tr>
            <tr>
                <td><? echo Yii::t('var', 'Количество мест');?>:</td>
                <td><?php echo CHtml::activeTextField($model, 'seats', array('class'=> 'select-input ', 'style' => 'margin-top: 10px'))?></td>
            </tr>
            <tr>
                <td><? echo Yii::t('var', 'Выберите дату');
				$placeholder = $model->date_start ? 'placeholder="'.date('d.m.Y', strtotime($model->date_start)).'"' : 'placeholder="'.Yii::t('var', 'Нажмите, чтобы выбрать').'"';
				?></td>
                <td>
					<!--<input id="bJuiDate" class="select-input b-jui-datepicker" style="margin-top: 10px; margin-bottom: 10px" type="input" name="Group[date]" <?//=$placeholder?> value="<?//= date('d.m.Y', strtotime($model->date_start)) ?>" />-->
					<?php
					$this->widget('zii.widgets.jui.CJuiDatePicker',array(
						'name'=>'Group[date]',
						// additional javascript options for the date picker plugin
						'options'=>array(
							'showAnim'=>'fold',
						),
						'htmlOptions'=>array(
							'class'=>'select-input',
							'style'=>"margin-top: 10px; margin-bottom: 10px",
						),
						'value'=>date('d.m.Y', strtotime($model->date_start)),
					));
					?>
					
					
                </td>
            </tr>
            <tr>
                <td><? echo Yii::t('var', 'Выберите время');?></td>
                <td>
                    <span>Час:</span>
                    <?php $hoursMinutes = Group::getHoursMinutes();?>
                    <?php 
					$attrList = array('class' => 'b-meet-time-hour select-input');
					if($model->date_end == '0000-00-00 00:00:00')
						$attrList['disabled'] = 'disabled';
					echo CHtml::dropDownList('Group[time_hour]', date('G', strtotime($model->date_start)), $hoursMinutes['hours'], $attrList)?>
                    <?php 
					$attrList['class'] = 'b-meet-time-minute select-input';
					echo CHtml::dropDownList('Group[time_minute]', intval(date('i', strtotime($model->date_start))), $hoursMinutes['minutes'], $attrList)?>

                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <span>Любое время:</span>
                    <?php 
					echo CHtml::checkBox('Group[any_time]', false, array('class'=>'b-any-time-input'))?>
                </td>
            </tr>
            <tr><td style="vertical-align: middle"><? echo Yii::t('var', 'Введите комментарий');?></td><td><textarea class="select-input" style="height: 95px; margin-top: 10px" type="comment" name="comment" value=""><?php echo CHtml::encode($model->description)?></textarea></td></tr>
            <tr><td><input class="registration input-border" type="submit" value="<? echo Yii::t('var', 'Отправить');?>"></td><td></td></tr>
        </table>
    </form>
</div>
<br>
<script>
    $('.b-any-time-input').change(function(){
        var timeFields = $('.b-meet-time-hour, .b-meet-time-minute');
        if ( $(this).prop('checked') ) {
            timeFields.attr('disabled', 'disabled');
        } else {
            timeFields.removeAttr('disabled');
        }
    });
</script>
<?php
	if($model->date_end == '0000-00-00 00:00:00'){
		$cs = Yii::app()->clientScript;
		$cs->registerScript('checkedAnytime', "
			$('#Group_any_time').prop('checked', true);
		", CClientScript::POS_READY);
	}
