<?php
/* @var $this GroupKeywordController */
/* @var $model GroupKeyword */

$this->breadcrumbs=array(
	'Group Keywords'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List GroupKeyword', 'url'=>array('index')),
	array('label'=>'Create GroupKeyword', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#group-keyword-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'group-keyword-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'group_id',
		'field_id',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
