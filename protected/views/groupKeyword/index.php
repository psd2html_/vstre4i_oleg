<?php
/* @var $this GroupKeywordController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Group Keywords',
);

$this->menu=array(
	array('label'=>'Create GroupKeyword', 'url'=>array('create')),
	array('label'=>'Manage GroupKeyword', 'url'=>array('admin')),
);
?>

<h1>Group Keywords</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
