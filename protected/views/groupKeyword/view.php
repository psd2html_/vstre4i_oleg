<?php
/* @var $this GroupKeywordController */
/* @var $model GroupKeyword */

$this->breadcrumbs=array(
	'Group Keywords'=>array('index'),
	(int)$model->id,
);

$this->menu=array(
	array('label'=>'List GroupKeyword', 'url'=>array('index')),
	array('label'=>'Create GroupKeyword', 'url'=>array('create')),
	array('label'=>'Update GroupKeyword', 'url'=>array('update', 'id'=>(int)$model->id)),
	array('label'=>'Delete GroupKeyword', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>(int)$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage GroupKeyword', 'url'=>array('admin')),
);
?>

<h1>View GroupKeyword #<?php echo CHtml::encode($model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'group_id',
		'field_id',
	),
)); ?>
