<?php
/* @var $this KeywordController */
/* @var $model Keyword */
$this->breadcrumbs=array(
	'Ключевые слова'=>array('admin'),
	'Просмотр',
);
?>

<h1><?php echo Yii::t('var','View Keyword #')." ".CHtml::encode($model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
            'name' => 'cat_id',
            'label' => Yii::t('var','cat_id')
        ),
        array(
            'name' => 'name',
            'label' => Yii::t('var','name')
        ),
        array(
            'name' => 'ua',
            'label' => Yii::t('var','ua')
        ),
        array(
            'name' => 'kz',
            'label' => Yii::t('var','kz')
        ),
        array(
            'name' => 'be',
            'label' => Yii::t('var','be')
        ),
	),
)); ?>
