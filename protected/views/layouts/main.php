<?php /* @var $this Controller */
$user_id = User::model()->getUserId();
//fb($this->route);
//fb(Yii::app()->request->hostInfo);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?php 
	if($this->useCommonKeywords)
		$this->renderPartial("/layouts/_mataTags", array('place' => 'main'));	
	?>
    <meta name="google-site-verification" content="jEdKvw8MLr7WLGkTed4Jx8Mskn44wlq_5B5xV7XrVyI" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
    <![endif]-->
    <!--[if lt IE 7]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie7.css" media="screen, projection" />
    <![endif]-->

    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style2.css" type="text/css" media="screen, projection" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/select2.css" type="text/css" media="screen, projection" />
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" />
    <![endif]-->
    <script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
    <?
    Yii::app()->clientScript
        ->registerCoreScript('jquery')
        ->registerCssFile(Yii::app()->request->baseUrl . '/css/jquery-ui.css')
        ->registerCssFile(Yii::app()->request->baseUrl . '/css/jquery.arcticmodal-0.3.css')
        ->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.arcticmodal-0.3.min.js');
    ?> 
<!--https://tech.yandex.ru/maps/keys/get/-->
     <!--<script src="http://api-maps.yandex.ru/1.1/index.xml?key=ADOjblQBAAAAsYJqQwMA6r6-YQOtRLr-6P7K2tB2wJhuMc8AAAAAAAAAAAAg_1fHeyg4rqtEpzKZOKW5F_hQDg=="
	type="text/javascript"></script>-->

    <script type="text/javascript" src="/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

    <link rel="stylesheet" href="/fancybox/source/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/source/jquery.fancybox.pack.js?v=2.1.4"></script>

    <link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5"></script>

    <link rel="stylesheet" href="/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/select2.js"></script>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" type="text/css" media="screen" />
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js"></script>


    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.eventCalendar.js"></script>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/eventCalendar.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/eventCalendar_theme_responsive.css">

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/functions.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.10.3.custom.min.js"></script>
    <!--<script src="<?php //echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.min.js"></script>-->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/payment.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.dropdownPlain.js"></script>



</head>

<body>

<div id="wrapper">
    <header id="header">
        <div class="top-header-container box-shadow">
            <div class="center">
                <div class="log-in f-right">
                    <ul>
                        <?php if (!Yii::app()->user->id): ?>
                            <li class="f-left"><a class="white" href="<?php echo Yii::app()->createUrl('site/registration');?>"><?php echo Yii::t('var', 'Регистрация');?></a>|</li>
                            <li class="f-left"><a class="white" href="<?php echo Yii::app()->createUrl('site/login'); ?>"><?php echo Yii::t('var', 'Вход');?></a></li>
                        <?php else: ?>
                            <li class="f-left"><a class="white" href="<?php echo Yii::app()->createUrl('user/view').'/'.urlencode($user_id);?>"><?php echo CHtml::encode(Yii::app()->user->name); ?></a>|</li>
                            <li class="f-left"><a class="white" href="<?php echo Yii::app()->createUrl('site/logout'); ?>"><?php echo Yii::t('var', 'Выход');?></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
                <?php
                   // $this->widget('application.components.widgets.LanguageSelector');
                ?>
            </div>
        </div>
        <div class="center">
            <a href="/"><div class="logo f-left"></div></a>
            <form id="top-search" action="<?php echo Yii::app()->createUrl('site/index')?>" method="post" class="search-form-header f-left">
                <input name="search" type="text" class="search f-left input-border" />
                <input name="YII_CSRF_TOKEN" type="hidden" value="<? echo Yii::app()->request->csrfToken;?>"/>
                <button type="button" name="search-btn" class="search-btn input-border" onclick="$('#top-search').submit();"><span class="white"><?php echo Yii::t('var', 'Поиск');?></span></button>
            </form>
            <button type="button" name="create-group" class="create-group f-right input-border" onclick="window.location.href='<?php echo Yii::app()->createUrl('site/index').'/site/page/view/create-group';?>'"><span class="white"><? echo Yii::t('var', 'Создать группу');?></span></button>
        </div><!-- .center-->
    </header><!-- #header-->
    <div id="content">

        <?php if ( Yii::app()->user->id ): ?>
            <div class="b-top-user-menu">
                <?php $this->widget('application.components.widgets.MenusWidget', array('id'=>'1')); ?>
            </div>
        <?php endif; ?>

        <div class="b-breadcrumbs">
            <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                'links'=>$this->breadcrumbs,
            )); ?>
        </div><!-- .b-breadcrumbs-->

        <?php echo $content; ?>
    </div><!-- #content-->
</div><!-- #wrapper -->
<div class="email-form myform" ></div>

<div id="b-search-loading__icon" ><div ></div></div>
<div id="b-search-loading__overlay" ></div>

<?php
// Notice::addNotice('tery', 'notice', 'hello');
// Yii::app()->user->setFlash('sc',User::model()->getUserId());
$flashMessages = Yii::app()->user->getFlashes();
$userNotices = Notice::popNotices( User::model()->getUserId() );
if ($flashMessages || ! empty($userNotices) ) :
?>
<div class="b-messages">
    <div class="b-messages__buttons">
        <div class="b-messages__close-all">Закрыть все сообщения</div>
    </div><!-- .b-messages__buttons-->
    <?php if ($flashMessages): ?>
        <div class="b-flashes">
            <ul class="b-flashes__list">
                <?php foreach($flashMessages as $type => $message): ?>
                    <li class="b-flashes__list-item">
                        <div class="b-flashes__message b-flashes__<?php echo  CHtml::encode($type); ?>">
                            <div class="b-flashes__message-close"></div>
                            <?php echo  $message; ?>
                        </div><!-- .b-flashes__message-->
                    </li><!-- .b-flashes__list-item-->
                <?php endforeach; ?>
            </ul><!-- .b-flashes__list-->
        </div><!-- .b-flashes-->
    <?php endif; ?>
    <?php if ( ! empty($userNotices) ): ?>
        <div class="b-flashes">
            <ul class="b-flashes__list">
                <?php foreach($userNotices as $notice): ?>
                    <li class="b-flashes__list-item">
                        <div class="b-flashes__message b-flashes__<?php echo CHtml::encode($notice['type']); ?>">
                            <div class="b-flashes__message-close"></div>
                            <?php 
							if($notice['link'] && $notice['count'] > 1)
								echo sprintf($notice['message'], $notice['count'], $notice['link']); 
							elseif($notice['link'])
								echo sprintf($notice['message'], $notice['link'] ); 
							else
								echo $notice['message'];
							?>
                        </div><!-- .b-flashes__message-->
                    </li><!-- .b-flashes__list-item-->
                <?php endforeach; ?>
            </ul><!-- .b-flashes__list-->
        </div><!-- .b-flashes-->
    <?php endif; ?>
</div><!-- .b-messages-->
<?php endif; ?>
 
<footer id="footer">
    <div class="center">
        <nav class="footer-links f-left">
            <ul>
				<?php $this->widget('application.components.widgets.MenusWidget', array('id'=>2)); ?>
            </ul>
        </nav>

        <div class="copyright f-right">
            <p class="white">Copyright © 2013 "Vstre4i"<br />
                <?php echo Yii::t('var', 'Все права защищены');?>.</p>
            <!--<p class="white"><?php echo Yii::t('var', 'Сайт разработан "KlymStyle"');?></p>-->
        </div>
    </div>
</footer><!-- #footer -->
<!-- Yandex.Metrika counter -->
<!--
<script type="text/javascript">
(function (d, w, c) {
	(w[c] = w[c] || []).push(function() {
    	try {
        	w.yaCounter26917950 = new Ya.Metrika({id:26917950,
                	webvisor:true,
                	clickmap:true,
                	trackLinks:true,
                	accurateTrackBounce:true});
    	} catch(e) { }
	});

	var n = d.getElementsByTagName("script")[0],
    	s = d.createElement("script"),
    	f = function () { n.parentNode.insertBefore(s, n); };
	s.type = "text/javascript";
	s.async = true;
	s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

	if (w.opera == "[object Opera]") {
    	d.addEventListener("DOMContentLoaded", f, false);
	} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>-->
<!-- /Yandex.Metrika counter -->
<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56354244-1', 'auto');
  ga('send', 'pageview');

</script>-->
</body>
</html>
