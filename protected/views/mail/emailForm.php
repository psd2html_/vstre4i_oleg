<?php 
$this->breadcrumbs=array(
	'Пользователи'=>array('admin'),
	'Написать',
);?>

<div class="form">
	<?php echo CHtml::beginForm(CController::createUrl('mail/sendToUser',array('id'=>$user_id)),'POST',array('id'=>'send-email-form'))?>
		<?php echo CHtml::label('Заголовок','subject')?>
		<?php echo CHtml::textField('subject')?>
		<br />
		<br />
		<?php echo CHtml::textArea('message','aa',array('id'=>'Mail_text'))?>
		<br />
		<?php echo CHtml::submitButton('Отправить'); ?>
	<?php echo CHtml::endForm();?>
</div>
