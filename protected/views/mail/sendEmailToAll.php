<ul class="tab-container">
    <li class="active"><a href="<?php echo CController::createUrl('mail/sendEmailToAll'); ?>" title="Рассылка">Рассылка</a></li>
    <li><a href="<?php echo CController::createUrl('mail/admin')?>" title="Архив сообщений">Архив </a></li>
</ul>
<div class="b-admin-content">
	<?php 
	$this->breadcrumbs=array(
		'Рассылка',
	);
	?>
	<div class="form sendEmailtoAll">
	<?php 
		echo CHtml::beginForm(CController::createUrl('mail/sendEmailToAll'),'POST',array('id'=>'payment-settings-form'))?>
		<?php echo CHtml::label('Тема','subject')?>
		<?php echo CHtml::textField('subject')?>
		<br>
		<br>
		<?php echo CHtml::textArea('message','aa',array('id'=>'Mail_text'))?>
		<br>
		<?php echo CHtml::submitButton('Отправить', array('confirm'=>'Вы действительно хотите отправить это письмо всем пользователям?')); ?>
	<?php echo CHtml::endForm();?>
	</div>
</div>
<script type="text/javascript">
	CKEDITOR.config.width = '600px'; 
	CKEDITOR.config.height = '400px'; 
</script>
