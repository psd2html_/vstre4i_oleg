<?  
	if (!Group::model()->isGroupAdmin($group_id,$user_id)) {
        foreach($dataReader as $row) {
            $avatar = $row['avatar'];
            if ($avatar && $avatar != '') ; else $avatar = 'no-pic.png';
            ?>
            <a class="participants f-left white-block align-center"
               title="#" href="<?php echo Yii::app()->createUrl('site/index').'/user/'.urlencode($row['id']); ?>">
                <img src="/images/user/mini_<?php echo CHtml::encode($avatar); ?>" alt="event">
                <p class="participants-name f-left"><?php echo CHtml::encode($row['name']); ?><br /></p>
            </a>
            <?
        }
    } else {
        foreach($dataReader as $row) {
            $avatar = $row['avatar'];
            if ($avatar && $avatar != '') ; else $avatar = 'no-pic.png';
            ?>
            <div class="participants white-block">
                <div class="participants-img f-left align-center"><a href="<? echo Yii::app()->createUrl('site/index').'/user/'.urlencode($row['id']); ?>"><img style="width:150px;height:150px;" src="/images/user/mini_<?php echo CHtml::encode($avatar); ?>" alt="event"></a></div>
                <div class="participants-name sharp f-left align-center"><?php echo CHtml::encode($row['name']); ?><br /><span><?php echo CHtml::encode($row['city']); ?></span></div>
                <div class="participants-action f-left align-center">
                    <ul>
                        <li>
                            <a class="sharp-blue" title="#" onclick="$('#mailto').arcticmodal();">Написать сообщение</a>
                        </li>
                        <?
                        if (Member::model()->isModerator($row['id'],$model->id)>0) {
                            ?>
                            <li>
                                <a class="sharp-blue" title="#"
                                   onclick="disrankModerator(<?php echo (int)$row['id'].','.(int)$model->id; ?>)">Разжаловать</a>
                            </li>
                        <?
                        } else { ?>
                            <li>
                                <a class="sharp-blue" title="#"
                                   onclick="appointModerator(<?php echo (int)$row['id'].','.(int)$model->id; ?>)">Назначить модератором</a>
                            </li>
                        <? }
                        ?>
                        <li>
                            <a class="sharp-blue" title="#"
                               onclick="kickMember(<?php echo (int)$row['id'].','.(int)$model->id; ?>)">Удалить из группы</a>
                        </li>
                    </ul>
                </div>
            </div>
            <?
        }
    }
?>
