<ul class="tab-container">
    <li class="active"><a href="<?php echo CController::createUrl('page/admin')?>" title="<?php echo Yii::t('var','Страницы')?>"><?php echo Yii::t('var','Страници')?></a></li>
</ul>
<div class="b-admin-content">
<?php
/* @var $this PageController */
/* @var $model Page */
$this->menu=array(
	array('label'=>Yii::t('var','Create Page'), 'url'=>array('create')),
);


$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations b-admin-buttons'),
));

?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'page-grid',
	'summaryText'=>'Всего {count} страниц',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		array(
            'name' => 'name',
            'header' => Yii::t('var','name')
        ),
		'alias',
		'keywords',
		array(
			'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array
            (
                /* 'move' => array
                (
                    'label'=>'alias',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/ban.png',
                    'visible' => '1',
                    'url'=>'Yii::app()->createUrl("site/$data->alias")',
                ), */
				'view' =>array(
					'url' => function($data, $i){
						return Yii::app()->createAbsoluteUrl('page/view', array('alias'=>$data->alias));
					},
				)
            )
		),
	),
)); ?>
</div>
