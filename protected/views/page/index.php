<?php
/* @var $this PageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Страницы',
);

?>

<h1>Страницы</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'summaryText'=>'Всего {count} страниц',
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
