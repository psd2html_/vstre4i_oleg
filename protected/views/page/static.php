<?php
/* @var $this SiteController */

$this->pageTitle=Yii::t('var', $model->name) . ' - ' . Yii::app()->name;
$this->breadcrumbs=array(
    Yii::t('var', $model->name),
);
?>

<div class="center">
    <div class="white-block">
        <h1 class="sharp align-center"><? echo Yii::t('var', $model->name);?></h1>
        <?php echo $model->text?>
    </div>
</div>