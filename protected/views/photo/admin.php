
<ul class="tab-container">
    <li class="active"><a href="<?php echo CController::createUrl('photo/admin')?>" title="<?php echo Yii::t('var','Фотографии')?>"><?php echo Yii::t('var','Фотографии')?></a></li>
    <li ><a href="<?php echo CController::createUrl('photoalbum/admin')?>" title="<?php echo Yii::t('var','Альбомы')?>"><?php echo Yii::t('var','Альбомы')?></a></li>
</ul>
<div class="b-admin-content">
<?php
/* @var $this PhotoController */
/* @var $model Photo */

?>
<?php
$assetsDir = '/images/album/';

$photos = $model->getAllPhotos();
foreach ($photos as $photo):
    $deleteLink = Yii::app()->createUrl('photo/delete') . '/' . urlencode($photo['id']);
    $albumLink  = Yii::app()->createUrl('photoalbum'. '/' . urlencode($photo['album_id']));
    $userLink   = Yii::app()->createUrl('user/view') . '/' . urlencode($photo['user_id']);
    $imageTag = CHtml::link(
        CHtml::image(
            Yii::app()->assetManager->publish(
                YiiBase::getPathOfAlias("webroot.images.album")."/" . CHtml::encode($photo['album_id'] . "//mini_" . $photo['link'])),
            '',
            array('class' => 'b-admin-photos__image')

        ),
        '/images/album/' . CHtml::encode($photo['album_id'] . '/original-' . $photo['link']),
        array('class' => 'b-admin-photos__image-link fancybox','rel' => 'adminView')
    );
?>
    <div id="photoBlock<?=$photo['id']?>" class="b-admin-photos__photo">
        <?php echo $imageTag; ?>
        <div class="b-admin-photos__buttons">
            <a href="<?php echo $userLink; ?>" target="_blank"><img src="/images/user.png" alt="Пользователь" /></a>
            <a href="<?php echo $albumLink; ?>" target="_blank"><img src="/images/album.png" alt="В фотоальбом" /></a>
			<a href="#"><img src="/images/delete.png" alt="Удалить" onclick="deleteItem(<?=$photo['id']?>)" /></a>
			<?/*= CHtml::linkButton('<img src="/images/delete.png" alt="Удалить" />', array(
				'submit'=>array(
					'photo/delete',
					'id' => $photo['id'],
					'ajax'=>'photoGrig'
				),
				'params'=>array(
					Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
				),
				'confirm'=>"Точно удалить?"
			)) */?>
        </div>
    </div><!-- .b-admin-photos__photo-->
<?php endforeach; ?>


<?php
if (false):
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'photo-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
            'name' => 'album_id',
            'header' => Yii::t('var','album_id')
        ),
        array(
            'name' => 'user_id',
            'header' => Yii::t('var','user_id')
        ),
        array(
            'name' => 'date',
            'header' => Yii::t('var','date')
        ),
        array(
            'name' => 'link',
            'header' => Yii::t('var','link')
        ),
        array('name'=>'image',
              'value'=>'(!empty($data->link))?CHtml::link(CHtml::image(Yii::app()->assetManager->publish(YiiBase::getPathOfAlias("webroot.images.album")."/".$data->album_id."/".$data->link),"",array("style"=>"width:75px;height:75px;")),"/images/album/$data->album_id/$data->link"):"no image"',
              'type'=>'html',
              'header' => Yii::t('var','image'),
        ),
		array(
			'class'=>'CButtonColumn',
            'template'=>'{move}{delete}',
            'buttons'=>array
            (
                'move' => array
                (
                    'label'=>'alias',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/ban.png',
                    'visible' => '1',
                    'url'=>'Yii::app()->createUrl("photoalbum/$data->album_id")',
                ),
            )
        ),
	),
));
endif; 

$cs = Yii::app()->clientScript;
$cs->registerScript('deleteItem', "
	function deleteItem(id){
		if(!confirm('Вы действительно хотите удалить изображение?'))
			return false;
		$.ajax({
			type: 'POST',
			url: '".$this->createUrl('photo/delete')."?id='+id,
			data: { ".Yii::app()->request->csrfTokenName.":'".Yii::app()->request->csrfToken."'}
		}).done(function() {
			console.log($(this));
			blockId = '#photoBlock'+id;
			$(blockId).remove();
			//parent.remove();
		});
	}
", CClientScript::POS_END); 
?>
</div>

<script>
    window.onload = function() {
        $(".fancybox").fancybox();
    };
</script>
