<?php
/* @var $this PhotoalbumController */
/* @var $model Photoalbum */
/* @var $form CActiveForm */
?>

<div class="">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<table class="b-form-table">
	<tr>
		<td><?php echo $form->label($model,'id'); ?></td>
        <td><?php echo $form->textField($model,'id'); ?></td>
	</tr>
	<tr>
        <td><?php echo $form->label($model,'name'); ?></td>
        <td><?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?></td>
	</tr>
	<tr>
        <td><?php echo $form->label($model,'meet_id'); ?></td>
        <td><?php echo $form->textField($model,'meet_id'); ?></td>
	</tr>
	<tr>
        <td><?php echo $form->label($model,'description'); ?></td>
        <td><?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>200)); ?></td>
	</tr>
</table>
	<div class="row submit">
		<?php echo CHtml::submitButton('Поиск'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->