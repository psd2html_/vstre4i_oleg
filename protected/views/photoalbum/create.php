<?php
/* @var $this PhotoalbumController */
/* @var $model Photoalbum */

$this->breadcrumbs=array(
	'Photoalbums'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Photoalbum', 'url'=>array('index')),
	array('label'=>'Manage Photoalbum', 'url'=>array('admin')),
);
?>

<h1>Создать альбом</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
