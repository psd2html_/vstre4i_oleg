<?php
/* @var $this PhotoalbumController */
/* @var $model Photoalbum */

$this->breadcrumbs=array(
	'Photoalbums'=>array('index'),
	(int)$model->name=>array('view','id'=>(int)$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Photoalbum', 'url'=>array('index')),
	array('label'=>'Create Photoalbum', 'url'=>array('create')),
	array('label'=>'View Photoalbum', 'url'=>array('view', 'id'=>(int)$model->id)),
	array('label'=>'Manage Photoalbum', 'url'=>array('admin')),
);
?>

<h1> <?php echo Yii::t('var','Обновить альбом ').CHtml::encode($model->id); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>