<?php
/* @var $this PhotoalbumController */
/* @var $model Photoalbum */


$this->breadcrumbs=array(
	'Группа - '.$model->group->name => Yii::app()->createUrl('group/view') . '/' . urlencode($model->group_id),
	CHtml::encode($model->name)
);

$this->setPageTitle('Альбом ' . $model->name . ' - ' . Yii::app()->name);
$description = $model->description;
if($description){
	Yii::app()->clientScript->registerMetaTag(CHtml::encode($description), 'description');
}else{
	Yii::app()->clientScript->registerMetaTag(Admin::getMainDescription(), 'description');
}
Yii::app()->clientScript->registerMetaTag(CHtml::encode($model->name).', '.Admin::getMainKeywords(), 'keywords');
?>
<div class="center">
    <h1 class="page-title"> <? echo Yii::t('var','Альбом')." ".CHtml::encode($model->name); ?></h1>
    <?php $this->widget('ext.EFineUploader.EFineUploader',
        array(
            'id'=>'photoUpload',
            'config'=>array(
                'text' => array(
                    "uploadButton"=> 'Загрузить файл',
                    "cancelButton"=> 'Отмена',
                    "retryButton"=> 'Попробовать ещё раз',
                    "deleteButton"=> 'Удалить',
                    "failUpload"=> 'Ошибка загрузки',
                    "dragZone"=> 'Переместите сюда файлы для загрузки',
                    "dropProcessing"=> 'Загрузка...',
                    "formatProgress"=> "{percent}% из {total_size}",
                    "waitingForResponse"=> "Загрузка..."
                ),
                'autoUpload'=>true,
                'request'=>array(
                    'endpoint'=> $this->createUrl('photo/upload'),
                    'params'=>array('YII_CSRF_TOKEN'=>Yii::app()->request->csrfToken, 'album_id' => $model->id),
                    'inputName' => 'Photo[link]'
                ),
                'multiple' => true,
                'retry'=>array('enableAuto'=>true,'preventRetryResponseProperty'=>true),
                'chunking'=>array('enable'=>true,'partSize'=>100),//bytes
                'callbacks'=>array(
                    'onComplete'=>'js:function(id, name, response){
                          $(".album-photos").prepend("<a class=\"fancybox\" rel=\"album'.$model->id.'\" href=\"/images/album/'.urlencode($model->id).'/" + response.filename + "\"> <img src=\"/images/album/'.urlencode($model->id).'/mini_" + response.filename + "\" style=\"width:128px;height:128px;float:left;\"></a>")
                    }',
//                    'onError'=>"js:function(id, name, errorReason){
//                            alert(errorReason)
//                    }",
                ),
                'validation'=>array(
                    'allowedExtensions'=>array('jpg','jpeg', 'png', 'gif'),
                    'sizeLimit'=>2 * 1024 * 1024,//maximum file size in bytes
                ),
            )
        ));

    ?>
    <div class="album-photos">
        <?
        $dataReader = Photoalbum::model()->getAlbumPhotos((int)$model->id);
        foreach($dataReader as $row) {
            echo '<a class="fancybox" href="/images/album/'.urlencode($row['album_id']).'/'.urlencode($row['link']).'"><img src="/images/album/'.urlencode($row['album_id']).'/mini_'.urlencode($row['link']).'" style="width:128px;height:128px;float:left;"></a>';
        }
        ?>
    </div>
</div>

<script>
    window.onload = function() {
        $(".fancybox").fancybox();
    };
</script>
