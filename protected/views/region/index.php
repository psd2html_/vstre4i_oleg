<?php
/* @var $this regionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'regions',
);

$this->menu=array(
	array('label'=>'Create region', 'url'=>array('create')),
	array('label'=>'Manage region', 'url'=>array('admin')),
);
?>

<h1>regions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
