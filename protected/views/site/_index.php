<?php
	$request = Yii::app()->request;

	$country = $request->getParam('country', false);
	$countryId = (int)Country::model()->getCountryId($country);
	$city = $request->getParam('city', '');

	$keywordsCategory = $request->getParam('category', false);
	$key = $request->getParam('key', false);
	$keyword = $request->getParam('keyword', false);
	$chosenKeywords = $request->getParam('keywords', array());

	$pageNumber = (int)$request->getParam('page_number', 1);

	if ($key) {
		//if (isset($_GET['city'])) $city = $_GET['city']; else $city = '';
		//if (isset($_GET['keyword'])) $keyword = $_GET['keyword']; else $keyword = '';
		//if (isset($_GET['keywords'])) $keywords = $_GET['keywords']; else $keywords = array();
		$keys = array($key);
		if (isset($_GET['order'])) {
			$groups = Group::model()->getFilteredGroups($countryId,$city,$keyword,$keys,$_GET['order']);
		} else {
			$groups = Group::model()->getFilteredGroups($countryId,$city,$keyword,$keys);
		}
		if (isset($_GET['view'])) {
			$view = $_GET['view'];
		} else {
			$view = 'tab';
		}
		foreach ($groups as $row):
			if ($view == 'list'): ?>
				<?php $groupLink = Yii::app()->createUrl('site/index').'/group/' . urlencode($row['id']); ?>
				<div class="event border-radius-5 odd" onclick="window.location.href='<?php echo $groupLink; ?>'">
					<div class="event-img f-left">
						<?php
						$pic = $row['picture'];
						if ($pic != NULL && $pic != '') {
							$eventImg = '/images/group/' . CHtml::encode($row['picture']);
						} else {
							$eventImg = '/images/group/no-pic.png';
						}
						?>
						<img style="height: 68px;width: 93px;" src="<?php echo $eventImg; ?>" alt="event" />
					</div>
					<div class="event-name f-left">
						<a href="<?php echo Yii::app()->createUrl('site/index').'/group/'.urlencode($row['id']); ?>"><?php
							echo CHtml::encode($row['name']); ?></a>
					</div>
					<div class="event-qty f-left">
						Участников: <?php $model=new Member; echo ' ' . $model->getGroupMembersCount($row['id']) ?>
					</div>
					<div class="event-date f-left">
						<?php
						$meet = Group::model()->getNextMeet($row['id']);
						if ($meet != 0) {
							echo Yii::t('var', 'ближайшая встреча ') . CHtml::encode(date('d.m.Y', strtotime($meet)));
						} else {
							echo Yii::t('var', 'ближайших встреч нет');
						}
						?>
					</div>
				</div>
			<?php else: ?>
				<?php $eventLink =  Yii::app()->createUrl('site/index').'/group/'.urlencode($row['id']); ?>
				<div class="event border-radius-5 even align-center f-left" onclick="window.location.href='<?php echo $eventLink; ?>'">
					<div class="event-img f-left">
						<?php
						$pic = $row['picture'];
						if ($pic != NULL && $pic != '') {
							$eventImg ='/images/group/' . CHtml::encode($row['picture']);
						} else {
							$eventImg ='/images/group/no-pic.png';
						}
						?>
						<img style="height: 167px;width: 167px;" src="<?php echo $eventImg; ?>" alt="event" />
					</div>
					<div class="event-name f-left">
						<a href="<?php echo Yii::app()->createUrl('site/index').'/group/'.CHtml::encode($row['id']); ?>">
							<?php echo CHtml::encode($row['name']); ?>
						</a>
					</div>
					<div class="event-qty f-left">
						Участников: <?php $model=new Member; echo ' '.$model->getGroupMembersCount($row['id']) ?>
					</div>
					<div class="event-date f-left">
						<?php
						$meet = Group::model()->getNextMeet($row['id']);
						if ($meet != 0) {
							echo Yii::t('var', 'ближайшая встреча ').CHtml::encode(date('d.m.Y',strtotime($meet)));
						} else {
							echo Yii::t('var', 'ближайших встреч нет');
						}
						?>
					</div>
				</div>
			<?php endif;
		endforeach;
	} else {
		try {
			$criteria = new CDbCriteria();
			$criteria->select = 't.id, t.name, t.picture, t.city_id';
			if (!$city && $countryId) {
				$citiesInCountry = City::model()->getCountryCitiesIds($countryId);
				$criteria->addInCondition('t.city_id', $citiesInCountry);
			}
			if ($city != '') {
				$city_id = City::model()->getCityID($city);
				$criteria->addCondition('t.city_id = ' . (int)$city_id);
			}
			if (count($chosenKeywords)) {
				$keywordIds = Keyword::model()->getKeywordIds($chosenKeywords);
				$groupsWithKeywords = Group::model()->getGroupsWithKeywords($keywordIds);
				$criteria->addInCondition('t.id', $groupsWithKeywords);
			} elseif ($keywordsCategory && $keywordsCategory != 'all') {
				$keywordIds = Keyword::model()->getCategoryKeywordIds($keywordsCategory);
				$groupsWithKeywords = Group::model()->getGroupsWithKeywords($keywordIds);
				$criteria->addInCondition('t.id', $groupsWithKeywords);
			}
			if ($keyword != '') {
				$criteria->join = ' JOIN group_keyword ON group_keyword.group_id = t.id'
					. ' JOIN keyword ON keyword.id = group_keyword.field_id';
				if (is_array($criteria->params)) {
					$criteria->params[':keyword'] = '%' . $keyword . '%';
				} else {
					$criteria->params = array(':keyword' => '%' . $keyword . '%');
				}
				$keywordCondition = 't.name LIKE :keyword OR t.description LIKE :keyword'
					. ' OR keyword.name LIKE :keyword';
				$criteria->addCondition($keywordCondition);
			}

			//$criteria->alias = 'group';

			$criteria->addCondition('type = "group"');
			//$criteria->with = array('group_keyword');
			//if (isset($_GET['cat'])) {
			//    $cat = $_GET['cat'];
			//    $cats = array();
			//    foreach ($cat as $key => $value) {
			//        array_push($cats,$key);
			//    }
			//    $criteria->addInCondition('keyword.cat_id', $cats);
			//}
			//if ($key) {
			//    $criteria->addInCondition('keyword.name', array($key));
			//}

			//$criteria->distinct = true;

			$limit = 8;//15
			$maxPages = 8;//15
			$offset = ($pageNumber - 1) * $limit;
			$offset = $offset > ($limit * $maxPages) ? $limit * $maxPages : $offset;

			$criteria->group = 't.id';
			$criteria->offset = $offset;
			$criteria->limit = $limit;
			$dataProvider = new CActiveDataProvider('Group', array('criteria' => $criteria,));
			$dataProvider->setPagination(false);
			$groups = array();
			foreach ($dataProvider->data as $data) {
				$groups[] = $data->id;
			}
			if ($keywordsCategory && $keywordsCategory != 'all') {
				$keywords = Keyword::model()->getCategoryKeywords($keywordsCategory);
			} else {
				$keywords = array();
				//$keywords = GroupKeyword::model()->getGroupsKeywordsList($groups);
			}
			?>
				<div class="keywords-container" style="display: <?php echo count($keywords) ? 'block' : 'none'; ?>">
					<div class="keywords-notice">
						В данный момент отображаются все группы, относящиеся к вашему поисковому запросу.<br />
						Нажмите на слово ниже для отображения групп только содержащих выбранное ключевое слово.
					</div><!-- .keywords-notice-->
					<div class="keywords-box border-radius-5 white-block">
			<?php
			foreach($keywords as $row) {
				$inputClass = '';
				$inputName = '';
				if (in_array($row['name'], $chosenKeywords)) {
					$inputClass = 'active';
					$inputName = 'keywords[]';
				}
				echo '<div class="keywords f-left ' . CHtml::encode($inputClass) . '"'
					. ' id="keywords-block-' . CHtml::encode($row['id']) . '">' . CHtml::encode($row['name']) . '</div>';
				echo '<input name="' . CHtml::encode($inputName) . '" type="hidden"'
					. ' id="keywords-input-' . CHtml::encode($row['id']) . '"'
					. ' class="keywords-input ' . CHtml::encode($inputClass) . '"'
					. ' value="' . CHtml::encode($row['name']) . '" />';
			}
			?>
						<button name="reset-btn-orange" class="reset-btn-orange">
							<span><?php echo Yii::t('var', 'Сбросить'); ?></span>
						</button>
					</div>
				</div>

			<?php

			if ($_GET['view'] == 'list') {
				$view = '_view';
			} else {
				$view = '_view_tab';
			}

			$this->widget('zii.widgets.CListView', array(
					'dataProvider' => $dataProvider,
					'itemView' => 'application.views.group.' . $view,
					'template' => '{items}'
			));

			$pagesCount = (int)($dataProvider->getTotalItemCount() / $limit);
			//echo 'pagg['. $pagesCount.']';
			if ($pagesCount > 1) {
				echo '<div class="clear"></div><div class="b-search-pagination">'
					. '<ul class="b-search-pagination__list">';
				if ($pageNumber != 1) {
					echo '<li class="b-search-pagination__item">'
						. '<a href="#" class="b-search-pagination__previous">&#9668;</a></li>';
				}
				for ($i = 1; $pagesCount >= $i; $i++) {
					echo '<li class="b-search-pagination__item">';
					if ($i == $pageNumber) {
						echo '<span class="b-search-pagination__item-current">'
							. (int)$i . '</span>';
					} else {
						echo '<a href="#" class="b-search-pagination__item-link" id="search-pag-' . (int)$i . '">'
							. (int)$i . '</a>';
					}
					echo '</li>';
				}
				if ($pageNumber != $pagesCount) {
					echo '<li class="b-search-pagination__item">'
						. '<a href="#" class="b-search-pagination__next">&#9658;</a></li>';
				}
				echo '</ul></div>';
			}

		} catch (CDbException $ex) {
			echo Yii::t('var', 'По Вашему запросу группы не найдены.');
			?>
			<div class="keywords-container" style="display: none;">
				<div class="keywords-notice">
					В данный момент отображаются все группы, относящиеся к вашему поисковому запросу.<br />
					Нажмите на слово ниже для отображения групп только содержащих выбранное ключевое слово.
				</div><!-- .keywords-notice-->
				<div class="keywords-box border-radius-5 white-block"></div>
			</div>
			<?php
		}
	}
