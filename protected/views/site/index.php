<?php
/* @var $this SiteController */
//$this->pageTitle = Yii::app()->name;
?>

<div class="slider-container">
    <div class="center">
        <?/*
        $img = Photo::model()->getRandomPhoto();
        */?><!--
        <div class="slider myslider">
            <img style="width:100%;" src="/images/album/<?/* echo $img['album_id']; */?>/<?/* echo $img['link']; */?>" alt="slider" />
        </div>-->
        <!--<div class="transparent-block input-border">
            <h1 class="transparent-block-info f-right"><a href="<?/* echo Yii::app()->createUrl('group/view',array('id'=>$img['group_id'])) */?>"><?/* echo $img['name'] */?></a></h1>
        </div>-->
        <?
        $img = rand(1,3);
        ?>
        <div class="slider myslider">
            <img class="myimg"  src="/images/slider/<?php echo CHtml::encode($img); ?>.jpg" alt="slider" />
        </div>
        <?
        $i = rand(0,13);
        $list = array(
           // '"В собаке собрано все лучшее, что может быть в человеке"',
          //  '"Собаки —те же люди, только маленькие и покрытые шерстью"',
            '"Идеал – чувствовать себя как дома в любом месте, вообще повсюду."',
            '"Самый верный способ добиться счастья для себя — это искать его для других."',
            '"Жизнь во время путешествия — это мечта в чистом виде."',
            '"Путешествие как самая великая наука и серьезная наука помогает нам вновь обрести себя."',
            '"Нет на свете силы более могущественной, чем любовь"',			'"Лучший способ приободриться - подбодрить кого-нибудь другого."',			'"Моим лучшим другом является тот, кто дал мне книгу, которую я еще не читал."',			'"Дружба не услуга, за нее не благодарят."',			'"В радости друзья узнают нас, в несчастье мы узнаем их."',			'"Мир, счастье, братство людей - вот что нужно нам на этом свете!"',			'"Для преданного друга нельзя никогда сделать слишком много. "',						'"Жизнь — это путь. Выбирай, с кем идти!"',			'"Уметь с умом распорядиться досугом - высшая ступень цивилизованности."',			'"Искренность отношений, правда в общении — вот дружба."');		  $authors = array(			'Джефф Дайер',			'Мартин Лютер',			'Агата Кристи',			'Альбер Камю',			'',			'Марк Твен',			'Авраам Линкольн',			'Гавриил Романович Державин',			'Джон Чэртон Коллинз',			'Марк Твен',			'Генрик Ибсен',						'Петр Солдатенков',			'Бертран Рассел',			'Александр Васильевич Суворов',		  );
        ?>		
        <div class="transparent-block input-border">
            <h1 class="transparent-block-info f-right slider_cite"><? echo CHtml::encode($list[$i]); ?></h1>
            <div class=" slider_author f-right"><? echo CHtml::encode($authors[$i]); ?></div>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="center<? if (isset($_GET['view'])) { if ($_GET['view'] == 'tab') echo ' index-grid';} else echo ' index-grid'; ?>">

<?php if(Yii::app()->user->hasFlash('success_del_group')):?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('success_del_group'); ?>
    </div>
<?php endif; ?>

<?php if(Yii::app()->user->hasFlash('error_del_group')):?>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('error_del_group'); ?>
    </div>
<?php endif; ?>

    <!--<form id="search-form" action="#" method="post" onsubmit="return false;">-->
    <form id="search-form" action="#" method="post">
        <div class="search-field-content">
            <h2 class="search-field-title f-left"><?php echo Yii::t('var', 'Поиск');?></h2>

            <div class="filter f-left" id="myfilter" >
                <h3 class="filter-title f-left"><?php echo Yii::t('var', 'режим просмотра');?></h3>
                <div class="filter-view-list f-left" onclick="window.location.href='<?php echo Yii::app()->createUrl('site/index').'/?view=tab'; ?>'" ></div>
                <div class="filter-view-grid f-left" onclick="window.location.href='<?php echo Yii::app()->createUrl('site/index').'/?view=list'; ?>'" ></div>
            </div>

            <div class="filter f-left">
                <h3 class="filter-title f-left"><?php echo Yii::t('var', 'сортировать по');?></h3>
                <div class="filter-sort f-left"></div>
                <div class="filter-div">
                    <ul class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all sort">
                        <li class="ui-menu-item" sort="date_up"><?php echo Yii::t('var', 'Дата создания (по возврастанию)');?></li>
                        <li class="ui-menu-item" sort="date_down"><?php echo Yii::t('var', 'Дата создания (по убыванию)');?></li>
                        <li class="ui-menu-item" sort="next_meet"><?php echo Yii::t('var', 'Ближайшая встреча');?></li>
                        <li class="ui-menu-item" sort="members"><?php echo Yii::t('var', 'Количество участников в группе');?></li>
                        <li class="ui-menu-item" sort="meets"><?php echo Yii::t('var', 'Общее количество встреч');?></li>
                    </ul>
                </div>

            </div>

            <div class="clear"></div>

            <input name="keyword" type="text" class="select-input f-left" placeholder="<? echo Yii::t('var', 'Впишите слово');?>"
                   value="<? if (isset($_POST['search'])) echo CHtml::encode($_POST['search']); ?>" />
            <div class="or f-left"><? echo Yii::t('var', 'и/или');?></div>

            <?php
            $catId = (int)Yii::app()->request->getParam('cat_id', 0);
            $key = Yii::app()->request->getParam('key', '');
            ?>
            <script>defaultGroupKey = '<?php echo CHtml::encode($key); ?>'</script>

            <select id="categories"  name="category">
                <option value="all" <?php echo $catId ? '' : 'selected'?>>Все категории</option>
                <?php
                $dataReader = Category::model()->getCategories();
                foreach ($dataReader as $val):
                    $catSelected = $catId == $val['id'] ? 'selected' : '';
                ?>
                    <option value="<?php echo CHtml::encode($val['id']); ?>"
                        <?php echo $catSelected; ?>><?php echo CHtml::encode($val['name']); ?></option>
                <?php endforeach; ?>
            </select>
            <select id="countries" name="country" >
                <?php
                $countries = Country::model()->getCountries();
                foreach ($countries as $val) :
                ?>
                    <option value="<?php echo CHtml::encode($val['name']); ?>"><?php echo CHtml::encode($val['name']); ?></option>
                <?php endforeach; ?>
            </select>
            <input id="cities" class="select-input" placeholder="<?php echo Yii::t('var', 'Укажите город'); ?>" name="city"/>
            <script>
                $(function() {
                    var cities = [];
                    var categories = [
                        <?
                        $dataReader = Category::model()->getCategories();
                        foreach ($dataReader as $val) {
                            echo '"'.CHtml::encode($val['name']).'",';
                        }
                        ?>
                    ];
                    var countries = [
                        <?
                        $dataReader = Country::model()->getCountries();
                        foreach ($dataReader as $val) {
                            echo '"'.CHtml::encode($val['name']).'",';
                        }
                        ?>
                    ];

                    chooseCityMessage = '<?php //echo Yii::t('var', 'Укажите город'); ?>';
                    chooseCountryMessage = '<?php echo Yii::t('var', 'Выберите страну'); ?>';

                    $( "#countries" ).autocomplete({
                        source: countries
                    }).on('focusout',function(){
                        $.ajax({
                            type: 'GET',
                            url: '/city/getCityList',
                            data: {country_id: $( this ).val()},
                            success: function(data){
                                var cities = data.split(',');
                                $( "#cities" ).autocomplete({
                                    source: cities
                                });
                            }
                        });
                        $( "#cities" ).autocomplete({
                            source: countries
                        });
                    });
                    $( "select#countries option" ).on('select', function(){
                        alert('asd');
                        $( "#cities" ).val(chooseCityMessage);
                    });

                    $('.filter-sort').on('click',function(){
                        if ($('.filter-div').css('display')=='none') $('.filter-div').show();
                        else $('.filter-div').hide();
                    });

                    $('ul.sort').on('click',function(){
                        $('.filter-div').hide();
                    });

                    $('.ui-menu-item').on('mouseover',function(){
                        $(this).addClass('ui-state-focus');
                    });

                    $('.ui-menu-item').on('mouseout',function(){
                        $(this).removeClass('ui-state-focus');
                    });
                });
            </script>

            <button type="button" name="search-btn-orange" class="search-btn-orange f-right"
                    onclick="getGroupsClick();"><span class="white"><? echo Yii::t('var', 'искать');?></span></button>
        </div>
        <div class="items">
            <div class="keywords-container">
                <div class="keywords-notice">
                    В данный момент отображаются все группы, относящиеся к вашему поисковому запросу.<br />
                    Нажмите на слово ниже для отображения групп только содержащих выбранное ключевое слово.
                </div><!-- .keywords-notice-->
                <div class="keywords-box border-radius-5 white-block">
                    <button class="reset-btn-orange" name="reset-btn-orange">
                        <span>Сбросить</span>
                    </button>
                </div><!-- .keywords-box-->
            </div><!-- .keywords-container-->
        </div><!-- .items-->
    </form>
</div>

<script>
    yii_csrf_token = '<?php echo Yii::app()->request->csrfToken; ?>';
    paginationPageNumber = 1;
    $(document).ready(function(){
        /*$.ajax({
            type: 'GET',
            url: '/ajax/groupListClick',
            data: $('#search-form').serialize()<?
            if (isset($_GET['view'])) {
                if ($_GET['view'] == 'list') echo "+'&view=list'"; else echo "+'&view=tab'";
                } else echo "+'&view=tab'";
            if (isset($_GET['key'])) echo "+'&key=".$_GET['key']."'";
            ?>,
            success: function(data){
                $('.items').html(data);
            }
        });*/

        $(document).on('click','.keywords',function(){
            var keywordId = parseInt( $(this).attr('id').split('-')[2] );
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('#keywords-input-' + keywordId).attr('name','');
            } else {
                $(this).addClass('active');
                $('#keywords-input-' + keywordId).attr('name','keywords[]');
            }
            /*$.ajax({
                type: 'GET',
                url: '/ajax/getFilteredGroups',
                data: $('#search-form').serialize()<?
            if (isset($_GET['view'])) {
                if ($_GET['view'] == 'list') echo "+'&view=list'"; else echo "+'&view=tab'";
                } else echo "+'&view=tab'";
            ?>,
                success: function(data){
                    $('.items .items').html(data);
                }
            });*/
        });

        $('.sort .ui-menu-item').on('click',function(){
            $.ajax({
                type: 'GET',
                url: '/ajax/getFilteredGroups',
                data: $('#search-form').serialize()+'&order='+$(this).attr('sort')<?php
            if (isset($_GET['view'])) {
                if ($_GET['view'] == 'list') {
                    echo "+'&view=list'";
                } else {
                    echo "+'&view=tab'";
                }
            } else {
                echo "+'&view=tab'";
            }
            ?>,
                success: function(data){
                    $('.items .items').html(data);
                }
            });
        });

        $(document).on('click','.reset-btn-orange',function(){
            $('.keywords.active').removeClass('active');
            $('input.keywords-input').attr('name','');
            /*$.ajax({
                type: 'GET',
                url: '/ajax/getFilteredGroups',
                data: $('#search-form').serialize()<?
            if (isset($_GET['view'])) {
                if ($_GET['view'] == 'list') echo "+'&view=list'"; else echo "+'&view=tab'";
                } else echo "+'&view=tab'";
            ?>,
                success: function(data){
                    $('.items .items').html(data);
                }
            });*/
        });

    });

    function getGroupsClick(){
        if ( ! $('.sel-countries').val() ) {
            alert('Необходимо выбрать страну');
            return false;
        }
        var overlay = $('#b-search-loading__overlay').fadeIn();
        var icon = $('#b-search-loading__icon').fadeIn();
        $.ajax({
            type: 'GET',
            url: '/ajax/groupListClick',
            data: $('#search-form').serialize()<?
            if (isset($_GET['view'])) {
                if ($_GET['view'] == 'list') echo "+'&view=list'"; else echo "+'&view=tab'";
                } else echo "+'&view=tab'";
            ?>+'&page_number='+paginationPageNumber,
            success: function(data){
                $('.items').html(data);
                $('.b-search-pagination__item-link').click(function(){
                    paginationPageNumber = $(this).attr('id').split('-')[2];
                    getGroupsClick();
                    return false;
                });
                $('.b-search-pagination__previous').click(function(){
                    paginationPageNumber--;
                    getGroupsClick();
                    return false;
                });
                $('.b-search-pagination__next').click(function(){
                    paginationPageNumber++;
                    getGroupsClick();
                    return false;
                });
                var width = 3;
                $('.b-search-pagination__item').each(function() {
                    width += $(this).outerWidth(true);
                });
                $('.b-search-pagination__list').width(width);
            }
        });
        icon.fadeOut();
        overlay.fadeOut();
    }

    function getFilteredGroups(){
        $.ajax({
            type: 'GET',
            url: '/ajax/getFilteredGroups',
            data: $('#search-form').serialize()<?
            if (isset($_GET['view'])) {
                if ($_GET['view'] == 'list') echo "+'&view=list'"; else echo "+'&view=tab'";
                } else echo "+'&view=tab'";
            ?>,
            success: function(data){
                $('.items').html(data);
            }
        });
    }

    var offset = $(".search-field-content").offset();
    $(document).scroll(function() {
        if ($(window).scrollTop() > offset.top && 0) {
            $(".search-field-content").addClass("fixed").css({'margin':'0'});
            $('#search-form > .items').css({'marginTop':$('.fixed').height()+$('.keywords-container').height()+'px'});
        }
        else {
            $(".search-field-content").removeClass("fixed").css({'margin':'5px 0 15px'});
            $('#search-form > .items').css({'marginTop':'0px'});
        }
    });
    $(document).ready(function(){

        var img = $('.slider img');
        var height = img.height();
        var width = img.width();
        var left,top;
        top = (height - 243)/2;
        img.css({'width':'100%','top':'-'+top+'px'});

    });
</script>
<?php

	$cs = Yii::app()->clientScript;
	$cs->registerScriptFile('https://api-maps.yandex.ru/1.1/index.xml');
	$cs->registerScript('varYMap', "
		var map = new YMaps.Map();
		var userCountry = '';
		var userRegion = '';
		var userCity = '';
		var userProfileCountry = '".Country::model()->getCountryName($user->country_id)."';
		var userProfileRegion = '".Region::model()->getRegionName($user->region_id)."';
		var userProfileCity = '".$user->city."';
		
		if(userProfileCountry){
			userCountry = userProfileCountry;
		}else{
			userCountry = YMaps.location ? YMaps.location.country : '';
		}
		
		if(userProfileRegion){
			userRegion = userProfileRegion;
		}else{
			userRegion = YMaps.location ? YMaps.location.region : '';
		}
		
		if(userProfileCity){
			userCity = userProfileCity;
		}else{
			userCity = YMaps.location ? YMaps.location.city : '';
		}
	", CClientScript::POS_END);
	$cs->registerScriptFile(Yii::app()->request->baseUrl."/js/functions_load_groaps.js");
?>
