<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle='Войти на сайт - ' . Yii::app()->name;
$this->breadcrumbs=array(
	'Вход на сайт',
);
if (User::model()->getUserId()) {
    Yii::app()->request->redirect(Yii::app()->homeUrl);
}
?>



<div class="center">
    <div class="white-block">
        <h1 class="align-center"><? echo Yii::t('var', 'Авторизация');?></h1>
       
        <div class="form">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'login-form',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
            )); ?>
			

            <div class="b-registration">
				<div class="b-registration__enter">
					<p><?php echo Yii::t('var', 'Пожалуйста, заполните следующие поля');?></p>
					<p class="note" style="font-size: 14px;"><?php echo Yii::t('var', 'Поля со');?> <span class="red">*</span> <?php echo Yii::t('var', 'обязательные для заполнения.');?></p>
                    <table class="b-registration__form-table">
                        <tr>
                            <td><?php echo Yii::t('var', 'Имя');?> <span class="red">*</span></td>
                            <td>
                                <?php echo $form->textField($model,'username',array('class'=>'input-border')); ?><br>
                                <?php echo $form->error($model,'username'); ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo Yii::t('var', 'Пароль');?> <span class="red">*</span></td>
                            <td>
                                <?php echo $form->passwordField($model,'password',array('class'=>'input-border')); ?><br>
                                <?php echo $form->error($model,'password'); ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <?php echo $form->checkBox($model,'rememberMe'); ?>
                                <label for="LoginForm_rememberMe"><?php echo Yii::t('var', 'Запомнить меня');?></label>
                                <?php echo $form->error($model,'rememberMe'); ?>
                            </td>
                        </tr>
                    </table>
				</div>
                <div class="b-registration__divider">
					<p>ИЛИ</p>
                </div>
                <div class="b-registration__social-nets">
					<h2 style="margin:10px 0px;"><?php echo Yii::t('var', 'Вы можете авторизироваться через соц.сети:');?></h2>
					<?php Yii::app()->eauth->renderWidget(); ?>
                </div>
                <div class="b-registration__button">
                    <?php echo CHtml::submitButton(Yii::t('var', 'Войти'),array('class'=>'align-center white registration create input-border','style'=>'height:32px')); ?>
                </div>
					<?php $this->endWidget(); ?>
            </div><!-- .b-registration-->
			
            
        </div><!-- form -->
    </div>
</div>
