<?php
/* @var $this GroupController */
/* @var $model Group */

Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerCssFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css') //css
    ->registerScriptFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/jquery-ui.js')
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.ui.datepicker-ru.js')
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/date-inputs-extending.js');

$this->setPageTitle('Стать Модератором сайта - ' . Yii::app()->name);

$this->breadcrumbs=array(
    'Стать модератором сайта',
);

$userId = User::model()->getUserId();

if (!$userId) {
    Yii::app()->request->redirect(Yii::app()->createUrl('login/view'));
}

$requestToken = Yii::app()->request->getParam('rt', 0);
$moderatorRequest = $model->getRequest($userId);
if (empty($moderatorRequest)) {
    Yii::app()->request->redirect('/');
}
$senderId = $moderatorRequest['sender_id'];

?>

<div class="center">
    <aside id="#SideLeft" class="f-left" style="width: 187px; margin: 0 7px 10px 0;">
    </aside>
    <section id="group-content" class="f-left">
        <div class="content-block-wrap">
            <h2 class="sharp align-center">Стать модератором сайта.</h2>
            <div class="content-block">
                <p class="group-description">
                    <?php
                    //$senderLink = Yii::app()->createUrl('user/view') . '/' . $senderId;
                    $message = 'Администратор ' . CHtml::encode(User::model()->getUserName($senderId)) . ' '
                        . CHtml::encode(Yii::app()->name) . ' предлагает вам стать модератором сайта.'
                        . '<br/ >Вам станет доступна часть функций административной панели.'
                        . '<br />(Заявка актуальна в течение 24-х часов)';
                    ?>
                <table>
                    <tr>
                        <td colspan="2">
                            <?php echo $message; ?>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="agree-button"><?php echo Yii::t('var', 'Согласиться');?></div>
                        </td>
                        <td>
                            <div class="disagree-button"><?php echo Yii::t('var', 'Отказаться');?></div>
                        </td>
                    </tr>
                </table>
                </p><!-- .group-description-->
                <script>
                    function answer(agree){
                        var url = '<?php echo Yii::app()->createUrl('site/moderatoragree');?>';
                        var CsrfToken = '<?php echo Yii::app()->request->csrfToken;?>';
                        var requestToken = '<?php echo CHtml::encode($requestToken); ?>';
                        window.location.href = url + '?YII_CSRF_TOKEN=' + CsrfToken
                            + '&rt=' + requestToken + '&agree=' + agree;
                        return false;
                    }
                    $().ready(function(){
                        $('.agree-button').click(function(){
                            answer(1);
                        });
                        $('.disagree-button').click(function(){
                            answer(0);
                        });
                    });
                </script>
            </div>
        </div>
    </section>
    <aside id="#SideRight" class="f-right" style="width: 187px; margin: 0 0 10px 7px;">
    </aside>
</div>





