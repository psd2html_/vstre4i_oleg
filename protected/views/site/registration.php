<?php
/* @var $this SiteController */
/* @var $form CActiveForm  */

$this->pageTitle='Регистрация - ' . Yii::app()->name;
$this->breadcrumbs=array(
    'Регистрация',
);
if (User::model()->getUserId()) {
    Yii::app()->request->redirect(Yii::app()->homeUrl);
}
?>

<div class="center">
    <div class="white-block">
        <h1 class="align-center"><?php echo Yii::t('var', 'Регистрация');?></h1>
       
        <div class="form">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'registration-form',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
            )); ?>
 

            <table class="registration-table">				<tr>					<td colspan="2">						<p><?php echo Yii::t('var', 'Пожалуйста, заполните следующие поля');?></p>						<p class="note" style="font-size: 14px;"><?php echo Yii::t('var', 'Поля со');?> <span class="red">*</span> <?php echo Yii::t('var', 'обязательные для заполнения.');?></p>					</td>					<td rowspan="6" class="ornote_reg" valign="center">						<p>ИЛИ</p>					</td>					<td rowspan="6" class="socailnet">						<h2 style="margin:10px 0px;"><?php echo Yii::t('var', 'Вы можете авторизироваться через соц.сети:');?></h2>						<?php Yii::app()->eauth->renderWidget(); ?>						<?php $this->endWidget(); ?>					</td>			</tr>
                <tr>
                    <td style="width: 180px;">
                        <? echo 'Логин';?> <span class="red">*</span><br>
                    </td>
                    <td>
                        <?php echo $form->textField($model,'name',array('class'=>'input-border')); ?><br>
                        <?php echo $form->error($model,'name'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <? echo Yii::t('var', 'Email');?> <span class="red">*</span><br>
                    </td>
                    <td>
                        <?php echo $form->emailField($model,'email',array('class'=>'input-border')); ?><br>
                        <?php echo $form->error($model,'email'); ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <? echo Yii::t('var', 'Пароль');?> <span class="red">*</span><br>
                    </td>
                    <td>
                        <?php echo $form->passwordField($model,'pass',array('class'=>'input-border')); ?><br>
                        <?php echo $form->error($model,'pass'); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo Yii::t('var', 'Я соглашаюсь с ');?><a href="<?php echo Yii::app()->createUrl('site/index').'/terms/'; ?>"><?php echo Yii::t('var', 'Условиями Пользования');?></a>
                    </td>
                    <td style="vertical-align: middle">
                        <?php echo $form->checkBox($model,'status',array('class'=>'input-border','style'=>'margin-top: 0;float:left;')); ?><?php echo $form->error($model,'terms'); ?>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td style="vertical-align: middle">
                        <?php echo CHtml::submitButton(Yii::t('var', 'Зарегистрироваться'),array('class'=>'align-center white registration create input-border','style'=>'height:32px;')); ?>
                    </td>
                </tr>
            </table>


            
        </div><!-- form -->
    </div>
</div>




