<?php
/* @var $this TranslateController */
/* @var $model Translate */

$this->breadcrumbs=array(
	'Translates'=>array('index'),
	(int)$model->id=>array('view','id'=>(int)$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Translate', 'url'=>array('index')),
	array('label'=>'Create Translate', 'url'=>array('create')),
	array('label'=>'View Translate', 'url'=>array('view', 'id'=>(int)$model->id)),
	array('label'=>'Manage Translate', 'url'=>array('admin')),
);
?>

<h1>Update Translate <?php echo CHtml::encode($model->id); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>