<?php
/* @var $this TranslateController */
/* @var $model Translate */

$this->breadcrumbs=array(
	'Translates'=>array('index'),
	(int)$model->id,
);

$this->menu=array(
	array('label'=>'List Translate', 'url'=>array('index')),
	array('label'=>'Create Translate', 'url'=>array('create')),
	array('label'=>'Update Translate', 'url'=>array('update', 'id'=>(int)$model->id)),
	array('label'=>'Delete Translate', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>(int)$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Translate', 'url'=>array('admin')),
);
?>

<h1>View Translate #<?php echo CHtml::encode($model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'table_name',
		'row_id',
		'language',
		'value',
	),
)); ?>
