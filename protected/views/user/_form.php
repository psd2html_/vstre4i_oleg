<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */ 
//$userId = $model->getUserId();
$isAdmin = Admin::model()->isSiteAdmin(Yii::app()->user->id);
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note"><? echo Yii::t('var', 'Поля со * обязательные для заполнения.');?></p>


    <table class="b-user-update">
        <tr>
            <td>
                <?php echo $form->labelEx($model,'name'); ?>
            </td>
            <td>
                <?php echo $form->textField($model, 'name', array('size'=>60,'maxlength'=>100,
                        'class'=>'input-border search b-user-update__field')); ?>
                <?php echo $form->error($model,'name'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'email'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100,
                    'class'=>'input-border search b-user-update__field')); ?>
                <?php echo $form->error($model,'email'); ?>
            </td>
        </tr>
		<!--<?php //if(!$model->isNewRecord): ?>
		<tr style="height:40px">
			<td valign="middle" style="vertical-align:middle"><a href="" id="clickToChange">Сменить пароль</a></td>
			<td valign="middle" style="vertical-align:middle"></td>
		</tr>
        <?php //endif; ?>-->
		<?php if(Yii::app()->user->id == $model->id && !$model->isNewRecord && !$model->serv_id || $model->isNewRecord && !$model->serv_id): ?>
	   <tr class="toogleOnChange">
            <td>
                <?php 
					if($model->isNewRecord)
						echo $form->labelEx($model,'pass');
					else
						echo '<label  for="User_pass">Введите старый пароль</label>'; 			
				?>
            </td>
            <td>
                <?php echo $form->passwordField($model,'pass',array('size'=>32,'maxlength'=>32,'value'=>'','placeholder'=>"", 'class'=>'input-border search b-user-update__field'));?>
                <?php echo $form->error($model,'pass'); ?>
            </td>
        </tr>
        <?php endif; ?>
		<?php if(!$model->isNewRecord && !$model->serv_id): ?>
        <tr class="toogleOnChange">
            <td>
                <?php echo '<label for="User_newPssword">Введите новый пароль</label>'; ?>
            </td>
            <td>
                <?php echo CHtml::passwordField('User[newPssword]','',array('size'=>32,'maxlength'=>32,'placeholder'=>'', 'class'=>'input-border search b-user-update__field'));?>
            </td>
        </tr>
        <?php endif; ?>
        <?php if (false): ?>
            <tr>
                <td>
                    <? if (!$model->pass) {
                        $model->pass = '1';
                    }
                    ?>
                </td>
                <td>
                    <?php echo $form->passwordField($model,'pass',array('size'=>32,'maxlength'=>32,'value'=>$model->pass,
                        'style'=>'display:none;', 'class'=>'input-border search b-user-update__field')); ?>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'avatar'); ?>
            </td>
            <td>
                <?php echo $form->fileField($model,'avatar'); ?>
				
                <?php echo $form->error($model,'avatar'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'country_id'); ?>
            </td>
            <td>
                <?php echo $form->dropDownList($model,'country_id', Country::countriesList(), array('empty' => 'Выберите страну','class'=>'input-border search b-user-update__field dropDownParam',
                'autocomplete'=>'off','onchange'=>
                    CHtml::ajax(
                        array(
                            'type'=>'GET',
                            'url'=>Yii::app()->createUrl('region/getRegions'),
                            'data' => array('country_id' => 'js:$(this).val()'),
                            'success'=> 'function(html, data){
								$("#regionTr").show();
								$("#User_region_id").html(html).show();
                            }'
                        )
                    )));?>
                <?php echo $form->error($model,'country_id'); ?>
            </td>
        </tr>
        <tr id="regionTr">
            <td>
                <?php echo $form->labelEx($model,'region_id'); ?>
            </td>
            <td>
				<?php echo $form->dropDownList($model,'region_id', Region::regionsList($model->country_id), array('empty' => 'Выберите регион','class'=>'input-border search b-user-update__field dropDownParam', 'autocomplete'=>'off', 'onchange'=>"$('#cityTr').show();"));?>
                <?php echo $form->error($model,'region_id'); ?>
            </td>
        </tr>
        <tr id="cityTr">
            <td>
                <?php echo $form->labelEx($model,'city'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'city',array('size'=>32,'maxlength'=>32,
                    'class'=>'input-border search b-user-update__field','autocomplete'=>'off')); ?>
                <?php echo $form->hiddenField($model,'city_id'); ?>
                <?php echo $form->error($model,'city'); ?>
            </td>
        </tr>
		
       <tr>
            <td>
                <?php echo $form->labelEx($model,'birthday'); ?>
            </td>
            <td>
                <?php echo $form->textField($model,'birthday',array('size'=>32,'maxlength'=>32,
                    'placeholder'=>Yii::t('var', 'Нажмите, чтобы выбрать'),
                    'class'=>'input-border search b-user-update__field b-jui-datepicker',)); ?>
                <?php echo $form->error($model,'birthday'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model,'sex'); ?>
            </td>
            <td>
                <?php echo $form->dropDownList($model,'sex',array(''=>'Выберите пол','1'=>'Муж.','2'=>'Жен.'),
                    array('class'=>'input-border search b-user-update__field dropDownParam','options' => array($model->sex=>array('selected'=>true)))); ?>
                <?php echo $form->error($model,'sex'); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'answer_comment_notice'); ?>
            </td>
            <td>
                <?php echo $form->checkBox($model,'answer_comment_notice'); ?>
                <?php echo $form->error($model,'answer_comment_notice'); ?>
            </td>
        </tr>
        <?php if(false): ?>
            <tr>
                <td>
                    <label for="weekly_group_list_notice">Получать еженедельный <br />список групп в городе <br />согласно указанным интересам</label>
                </td>
                <td>
                    <input name="weekly_group_list_notice" id="weekly_group_list_notice" type="checkbox" />
                </td>
            </tr>
        <?php endif; ?>
    </table>


	<div class="row buttons">
		<?php echo $form->hiddenField($model,'serv_id'); ?>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',array('class'=>'b-user-update__submit')); ?>
	</div>

<?php $this->endWidget(); 
	if(!$model->country_id){
		$cs = Yii::app()->clientScript;
		$cs->registerScript('hideTr', "
			$('#regionTr, #cityTr').hide();
		", CClientScript::POS_END);
	}
	if(!$model->region_id){
		$cs = Yii::app()->clientScript;
		$cs->registerScript('hideTr2', "
			$('#cityTr').hide();
		", CClientScript::POS_END);
	}
	$cs = Yii::app()->clientScript;
	$cs->registerScriptFile(Yii::app()->request->baseUrl."/js/jquery.autocomplete.js");
	$cs->registerScript('getCityAutocomplete', "
		yii_csrf_token = '".Yii::app()->request->csrfToken."';
		
		$('#User_city').devbridgeAutocomplete({
			serviceUrl: '".Yii::app()->createUrl('city/autocomplete')."',
			ajaxSettings:{
				beforeSend: function(jqXHR, settings) {
					settings.url = settings.url + '&region_id=' + $('#User_region_id').val();
					//console.log(settings.url, 'beforeSend_settings');
					//console.log(jqXHR, 'beforeSend_jqXHR');
					//settings.data = $.extend(settings.data, {region_id: //$('#User_region_id').val()});
					return true;
				}
			},
			lookupLimit:5,
			minChars:2,
			onSelect: function (suggestion) {
				$('#User_city_id').val(suggestion.data);
			}
		});
	", CClientScript::POS_END);
	$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.ui.datepicker-ru.js')
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/birthday-date-input.js')
    ->registerCssFile('http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.0/themes/base/jquery-ui.css');
?>


</div><!-- form -->
