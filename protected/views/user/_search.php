<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
    <table class="b-form-table">
        <tr>
            <td><?php echo $form->label($model,'id'); ?></td>
            <td><?php echo $form->textField($model,'id'); ?></td>
        </tr>
        <tr>
            <td><?php echo $form->label($model,'serv_id'); ?></td>
            <td><?php echo $form->textField($model,'serv_id'); ?></td>
        </tr>
        <tr>
            <td><?php echo $form->label($model,'service'); ?></td>
            <td><?php echo $form->textField($model,'service',array('size'=>50,'maxlength'=>50)); ?></td>
        </tr>
        <tr>
            <td><?php echo $form->label($model,'name'); ?></td>
            <td><?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?></td>
        </tr>
        <tr>
            <td><?php echo $form->label($model,'reg_date'); ?></td>
            <td><?php echo $form->textField($model,'reg_date'); ?></td>
        </tr>
        <tr>
            <td><?php echo $form->label($model,'email'); ?></td>
            <td><?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?></td>
        </tr>
        <tr>
            <td><?php echo $form->label($model,'pass'); ?></td>
            <td><?php echo $form->passwordField($model,'pass',array('size'=>32,'maxlength'=>32)); ?></td>
        </tr>
        <div class="row submit">
            <?php echo CHtml::submitButton('Поиск'); ?>
        </div>
    </table>
<?php $this->endWidget(); ?>

</div><!-- search-form -->