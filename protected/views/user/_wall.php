<?php
function showCommentsRecursive($comments)
{	
    static $user_id = -1;
    if ($user_id == -1) {
        $user_id = User::model()->getUserId();
    }

    foreach($comments as $comment):
		//if($comment['depth'] > 1) return;
        if (empty($comment['text'])) {
            continue;
        }
        $date = explode(' ', CHtml::encode($comment['date']));
        $date[0] = '<strong>' . CHtml::encode(date('d.m.Y', strtotime($date[0]))) . '</strong> ';
        $date = implode($date);

        $answerModificator = empty($comment['children'])
            ? ''
            : 'b-comment__answer_border';
        ?>
        <div class="b-comment" id="comment-<?php echo CHtml::encode($comment['id']); ?>">
            <div class="b-comment__content">
                <div class="b-comment__head">
                    <div class="b-comment__author">
                        <a href="<?php echo Yii::app()->createUrl('user/view') . '/' . urlencode($comment['sender_id']); ?>"
                           target="_blank"><?php echo CHtml::encode($comment['sender_name']); ?></a>
                    </div>
                    <div class="b-comment__date">[<?php echo $date; ?>]</div>
                </div><!-- .b-comment__head-->
                <div class="b-comment__body"><?php echo CHtml::encode($comment['text']); ?></div>
                <?php //if ($user_id && $comment['depth'] < 1): ?>
                <?php if ($user_id): ?>
                    <div class="b-comment__buttons" id="answer-buttons-<?php echo (int)$comment['id']; ?>">
                        <a href="#" class="b-comment__answer-close"
                           onclick="closeAnswerForm(<?php echo (int)$comment['id']; ?>);return false;">Закрыть</a>
                        <a href="#" class="b-comment__answer-open"
                           onclick="showAnswerForm(<?php echo (int)$comment['id']; ?>, this);return false;">Ответить</a>
                    </div><!-- .b-comment__buttons-->
                <?php else: ?>
					<div class="b-comment__buttons">
                        <a href="#" class="b-comment__answer-close"></a>
                        <a href="#" class="b-comment__answer-open"></a>
					 </div><!-- .b-comment__buttons-->
                <?php endif; ?>
            </div><!-- .b-comment__content-->
            <div class="b-comment__answer <?php echo $answerModificator; ?>" id="answer-<?php echo (int)$comment['id']; ?>">
                <div class="b-comment__answer-form-block"></div>
                <?php if (!empty($comment['children'])): ?>
                    <div class="b-comment__answer-body">
                        <?php showCommentsRecursive($comment['children']); ?>
                    </div><!-- .b-comment__answer-body-->
                <?php endif; ?>
            </div><!-- .b-comment__answer-->
        </div><!-- .b-comment-->
    <?php
    endforeach;
}
?>
<section id="group-content" class="f-left">

        <?php if ($user_id == (int)$model->id) { ?>
            <div class="welcome">
                Приветствуем Вас, <?php echo CHtml::encode(User::model()->getUserName((int)$user_id));?>
            </div>
            <?php if (!empty($model->serv_id) && !empty($friends)): ?>
            <div class="content-block-wrap">
                <div class="content-block">
                    <div class="slider align-center">
                        <h4 class="align-center"><?php echo Yii::t('var', 'Ваши друзья с ') . ucfirst($model->service);?></h4>
                        <div id="myCarousel" class="carousel slide">
                            <!-- Carousel items -->
                            <div class="carousel-inner social-carousel">
                                <?php for ($i=0;$i<count($friends);$i++): ?>
                                    <?php
                                        if ($friends[$i]->service == 'facebook')
                                            $pic = 'https://graph.facebook.com/'.$friends[$i]->serv_id.'/picture';
                                        else {
                                            $pic = CJSON::decode(file_get_contents('https://api.vk.com/method/users.get?user_id='.$friends[$i]->serv_id.'&fields=photo_medium'));
                                            $pic = $pic['response'][0]['photo_medium'];
                                        }
                                    ?>

                                    <?php if ($i==0): ?>
                                        <div class="item active">
                                    <?php endif; ?>
                                    <a class="group-participant" href="<?php echo Yii::app()->createUrl('site/index').'/user/'.urlencode(User::model()->getId($friends[$i]->serv_id,'facebook'))?>">
                                        <img src="<?php echo $pic?>" alt="participant" /><h5><?php echo $friends[$i]->name?></h5><div class="meetup">Нет встреч<br />Группы</div></a>
                                    <?php if (($i % 3 == 0) && ($i > 0)): ?>
                                        </div><div class="item">
                                    <?php endif; ?>
                                <?php endfor ?>
                                </div>
                            </div>
                            <!-- Carousel nav -->

                            <a class="carousel-control left prev" href="#myCarousel" data-slide="prev"></a>
                            <a class="carousel-control right next" href="#myCarousel" data-slide="next"></a>

                        </div>
                    </div>
                    <div id="photo"></div>
                </div>
            </div>
            <?php endif ?>

            <div class="content-block-wrap">
                <div class="content-block content-block-interest">
                    <h3 class="sharp" style="color:black">Мои интересы:</h3>
                    <?
                    $dataReader = Keyword::model()->getUserInterests($model->id);
                    foreach ($dataReader as $value) {
                        echo '<div class="interest align-center f-left" kid="'.CHtml::encode($value['id']).'">
                            '.CHtml::encode($value['name']).'
                            <input type="image" src="/images/inerest-icon-small.png" class="remove-interest" name="remove-interest" />
                        </div>';
                    }
                    ?>
                    <div class="clear"></div>
                    <button onclick="$('#interest').arcticmodal();" type="button" name="add-interest" class="add-interest"><span><? echo Yii::t('var', 'Добавить интерес');?></span></button>
                </div>
            </div>
        <? } //else { ?>
            <!-- если не личная -->
            <div class="content-block-wrap">
                <div class="content-block">
                    <h3 class="blue sharp align-center"><?php echo CHtml::encode($model->name); ?></h3>
                    <?
                    if ($model->city) {
                        echo '<p class="sharp">Город: '.CHtml::encode($model->city).'</p>';
                    }
                    if ($model->birthday && $model->birthday != '0000-00-00') {
                        echo '<p class="sharp">Дата рождения: '.CHtml::encode(date('d.m.Y',strtotime($model->birthday))).'</p>';
                    }
                    if ($model->about) {
                        echo ' <p class="sharp">О себе: <p style="font-size: 12px;">'.CHtml::encode($model->about).'</p></p>';
                    }
                    ?>
                    <!--<p class="sharp"><?php //echo Yii::t('var', 'Интересы');?>:
                    <p style="font-size: 12px;">
                        <?
                       /*  $i = 0;
                        $dataReader = Keyword::model()->getUserInterests($model->id);
                        foreach ($dataReader as $value) {
                            if ($i == 0) {
                                echo CHtml::encode($value['name']);
                            } else {
                                echo ', '.CHtml::encode($value['name']);
                            }
                            $i++;
                        } */
                        ?>
                    </p>
                    </p>-->
                </div>
            </div>

            <div class="content-block-wrap">
                <h2 class="sharp" style="padding-left: 30px;" id="comments"><?php echo Yii::t('var', 'Стена');?></h2>
                <div class="content-block">
                    <?php

                    $comments = Wall::model()->getFormatedWallRecords($model->id);

					//fb($comments,'comments');
                    showCommentsRecursive($comments);

                    if ($user_id):
                    ?>
                        <form id="wall-form" action="<?php echo Yii::app()->request->baseUrl; ?>/wall/sentRecord"
                              name="sent-record" method="post">
                            <textarea class="b-comment__textarea f-left" name="text"></textarea><br>
                            <input class="f-right" type="image" src="/images/send-comment.png" />
                            <input type="hidden" name="user_id" value="<?php echo CHtml::encode($model->id); ?>" />
                            <input type="hidden" name="sender_id" value="<?php echo CHtml::encode($user_id); ?>" />
                            <input type="hidden" name="YII_CSRF_TOKEN" value="<?php echo Yii::app()->request->csrfToken; ?>" />
                        </form>
                    <?php endif; ?>
                </div>
            </div>
        <? //} ?>
    </section>
