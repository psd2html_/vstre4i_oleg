<?php
/* @var $this GroupController */
/* @var $data Group */

$user_id = User::model()->getUserId();

?>

<h1 class="page-title sharp align-center"><? echo Yii::t('var', 'Участвует в');?></h1>
<div class="center vse-uchastniki-admin vse-uchastniki">
    <?
    $dataReader = Group::model()->getUserGroupList($model->id);
        echo '<div style="width:100%;float: left">';
        foreach($dataReader as $row) {
            $avatar = $row['picture'];
            if ($avatar && $avatar != '') ; else $avatar = 'no-pic.png';
            ?>
            <a class="participants f-left white-block align-center" title="#" href="<? echo Yii::app()->createUrl('site/index').'/group/'.urlencode($row['id']); ?>">
                <img src="/images/group/mini_<?php echo CHtml::encode($avatar); ?>" alt="event">
                <p class="participants-name f-left"><? echo CHtml::encode($row['name']); ?><br /></p>
            </a>
        <?
        }
        echo '</div>';
    ?>
</div>
