<style type="text/css">
	.complaintsBlock{
		margin-bottom:10px;
	}
	.complaintsHeader{
		/* background: #eee; */
		border-bottom:1px solid #388ECD;
	}
	.complaintsBody{
	
	}
	.red{
		color:red;
	}
	.green{
		color:green;
	}
	#complaintsMenu{
		margin-bottom:10px;
		padding:5px 0 0 0;
		background: #fff;
		border-bottom:1px solid #0e8eab;
	}
	#complaintsMenu li{
		display:block;
		float:left;
		margin:0 10px;
		padding:5px 0;
	}
	#complaintsMenu li.activeComplaints{
		background: #0e8eab;
	}
	#complaintsMenu li a{
		padding:0 5px;
	}
	#complaintsMenu li.activeComplaints a{
		background: #0e8eab;
		color:#fff;
	}
</style>
<?php
$this->setPageTitle('Список жалоб');
$listName = $this->route == 'user/complaints' ? 'Поступившие жалобы' : 'Мои жалобы';
$this->breadcrumbs=array(
	$model->name => Yii::app()->createUrl('user/view', array('id'=>$model->id)),
	$listName,
);
?>
<section id="group-content" class="f-left">
	<div class="content-block-wrap">
		<!--<h2 class="sharp" style="padding-left: 30px;" id="comments">Жалобы</h2>-->
		<ul id="complaintsMenu">
			<li class="<?= $this->route == 'user/complaints' ? 'activeComplaints ' : '' ?>complaintsMenuItem"><a href="<?= Yii::app()->createUrl('user/complaints', array('id'=>$user_id));?>">Поступившие жалобы</a></li>
			<li class="<?= $this->route == 'user/mycomplaints' ? 'activeComplaints ' : '' ?>complaintsMenuItem"><a href="<?= Yii::app()->createUrl('user/mycomplaints');?>">Мои жалобы</a></li>
		</ul>
		<div class="content-block">
			<?php 
				//$complaints = Complaint::complaintsList($user_id);
				$count = count($complaints);
				if(!empty($complaints)):
					foreach($complaints as $complaint): 
						if($complaint['text_id'] == 'group' || $complaint['text_id'] == 'meet'){
							$groupModel = Group::model();
							$accusedName = $groupModel->getGroupName($complaint['item_id']);
							if($complaint['text_id'] == 'group'){
								$accusedLink = Yii::app()->createUrl('group/view', array('id'=>$complaint['item_id']));
								$compstr = "группу <a href=\"$accusedLink\">$accusedName</a>";
							}else{
								$accusedLink = Yii::app()->createUrl('group/meet', array('id'=>$complaint['parent_id'], 'mid' => $complaint['item_id']));
								$compstr = "встречу <a href=\"$accusedLink\">$accusedName</a>";
							}
						}else{
							//if($complaint[$compid]['user_id'] == $user_id)
							$compstr = '<a href="'. Yii::app()->createUrl('user/view', array('id'=>$complaint['item_id'])).'">'.User::model()->getName($complaint['item_id']).'</a>';;
							//$userModel = Group::model();
							//$accusedName = $groupModel->getUserName($complaint['item_id']);
						}
					?>
					<div class="complaintsBlock">
						<div class="complaintsHeader">
							<span><i><?= CHtml::encode(date('d.m.Y', strtotime($complaint['date']))).' вы пожаловались на '.$compstr;?></i></span>
							
							<span class="b-comment__date <?=  $complaint['status'] == 0 ? 'red' : 'green'; ?>"><?=  $complaint['status'] == 0 ? '[Не решено]' : '[Решено]'; ?></span>
						</div>
						<div class="complaintsBody">
						<?= $complaint['text'];?>
							<?php
							$count = Complaint::getCountOfAnswersMy($complaint['id'], $user_id);
							$countStr = $count >= 1 ? '<b>есть ответы</b>' : 'нет ответа';
							echo $complaint['status'] == 0 ? '<span><a href="'.Yii::app()->createUrl('user/answer', array('complaint'=>$complaint['id'])).'"> Просмотреть ('.$countStr.')</a></span>' : '<span><a href="'.Yii::app()->createUrl('user/answer', array('complaint'=>$complaint['id'])).'"> Просмотреть</a></span>';
						?>	
						</div>
					</div>
						
						
					
				<?php endforeach;
				else:
					echo 'Нет жалоб';
				endif;
			
			?>
		</div>
	</div>
</section>
