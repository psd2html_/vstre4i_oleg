<ul class="tab-container">
    <li class="active"'><a href="<?php echo CController::createUrl('wall/admin')?>" title="<?php echo Yii::t('var','Стены')?>"><?php echo Yii::t('var','Стены')?></a></li>
    <li ><a href="<?php echo CController::createUrl('comment/admin',array('type'=>'meet'))?>" title="<?php echo Yii::t('var','Встречи')?>"><?php echo Yii::t('var','Встречи')?></a></li>
    <li ><a href="<?php echo CController::createUrl('comment/admin',array('type'=>'group'))?>" title="<?php echo Yii::t('var','Группы')?>"><?php echo Yii::t('var','Группы')?></a></li>
</ul>
<div class="b-admin-content">
    <?php
    /* @var $this WallController */
    /* @var $model Wall */
?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'wall-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
        array(
            'name'=>'user_id',
            'value' => '@$data->user->name."(".$data->user_id.")"',
            'header'=> Yii::t('var','user_id')
        ),
		array(
            'name' =>'text',
            'header' => Yii::t('var','text')
        ),
		array(
            'name' => 'date',
            'header' => Yii::t('var','date')
        ),
        array(
            'name'=>'sender_id',
            'value' => '@$data->sender->name."(".$data->sender_id.")"',
            'header' => Yii::t('var','sender_id')
        ),
		array(
            'class'=>'CButtonColumn',
            'template'=>'{move}{update}{delete}',
            'buttons'=>array
            (
                'move' => array
                (
                    'label'=>'alias',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/ban.png',
                    'visible' => '1',
                    'url'=>'Yii::app()->createUrl("user/view",array("id"=>$data->user_id))',
                ),
            )
		),
	),
)); ?>

</div>
